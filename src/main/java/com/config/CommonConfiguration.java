package com.config;

import com.util.SnowflakeIdWorker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * created in 2021/12/29 10:51
 *
 * @author zhuxuelei
 */
@Configuration
public class CommonConfiguration {
    @Bean
    public SnowflakeIdWorker snowflakeIdWorker() {
        return new SnowflakeIdWorker(0, 0);
    }
}
