package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.TalentShareRule;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-09-17
 */
public interface TalentShareRuleService extends IService<TalentShareRule> {
    TalentShareRule getTalentShareRule();
}
