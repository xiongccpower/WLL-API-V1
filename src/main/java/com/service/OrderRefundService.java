package com.service;

import com.entity.OrderRefund;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface OrderRefundService extends IService<OrderRefund> {

    void checkAndSave(OrderRefund refund);
}
