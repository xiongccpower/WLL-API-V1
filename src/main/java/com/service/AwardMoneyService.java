package com.service;

import com.domain.AwardMoney;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 *
 */
public interface AwardMoneyService extends IService<AwardMoney> {

    String findValueByType(int type);

    AwardMoney getLikeCountAndMoney(String s);

    List<AwardMoney> getList();
}
