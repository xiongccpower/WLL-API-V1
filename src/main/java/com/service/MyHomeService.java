package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.MyHomes;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-07-24
 */
public interface MyHomeService extends IService<MyHomes> {

    public List<MyHomes> findByTelOrMainTel(String tel);

}
