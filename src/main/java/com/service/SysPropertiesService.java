package com.service;

import com.domain.SysProperties;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface SysPropertiesService extends IService<SysProperties> {

    SysProperties findByCode(String code);
}
