package com.service;

import com.domain.BenefitContent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface BenefitContentService extends IService<BenefitContent> {

}
