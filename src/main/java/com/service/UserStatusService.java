package com.service;

import com.domain.UserStatus;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface UserStatusService extends IService<UserStatus> {

    UserStatus findByOpenId(String openId);
}
