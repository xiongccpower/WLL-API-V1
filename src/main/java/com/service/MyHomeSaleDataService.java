package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.MyHomeSaleDatas;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-07-09
 */
public interface MyHomeSaleDataService extends IService<MyHomeSaleDatas> {
    //根据预约优惠id查询有无当前预约商品清单
    MyHomeSaleDatas getSubscribeId(int subscribeId);
}
