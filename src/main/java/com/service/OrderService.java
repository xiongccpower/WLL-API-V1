package com.service;

import com.domain.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface OrderService extends IService<Order> {

    void cancelDeadOrder();

    void updateEmptyOrder(String uid, String tel, String openId);
}
