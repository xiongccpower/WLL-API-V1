package com.service;

import com.domain.ThirdOrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface ThirdOrderDetailService extends IService<ThirdOrderDetail> {

}
