package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.TalentShareRecord;
import com.entity.TalentShareRecordTwo;

import java.util.List;

/**
 * <p>
 * 达人分享记录表 服务类
 * </p>
 *
 * @author czr
 * @since 2021-08-31
 */
public interface TalentShareRecordService extends IService<TalentShareRecord> {
    TalentShareRecord getTime(String talentOpenid,String previewOpenid,String time);
    /**
     * 统计预览数据
     * @param previewMonth
     * @param previewTime
     * @return
     */
    List<TalentShareRecord> statisticsPreview(String previewMonth, String previewTime,String city);

    TalentShareRecord getHelp(String previewTime,String previewMonth,String helpOpenid);

    TalentShareRecord getStatistics(String previewMonth,String previewTime,String talentOpenid);

    List<TalentShareRecord> findPKData(String pkSquareA,String pkSquareB,String month);

    List<TalentShareRecord> findPKDataGroupDay(String openid,String month);
    List<TalentShareRecordTwo> findPKDataGroupDayAll(String pkSquareA, String pkSquareB, String month);

    List<TalentShareRecord> fondHelpData(String helpOpenid,String month);

    List<TalentShareRecordTwo> findUserDataGroupDay(String openid,String month);

}
