package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.BookingGoodsInfos;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-07-08
 */
public interface BookingGoodsInfoService extends IService<BookingGoodsInfos> {

    List<BookingGoodsInfos> findBookingGoods(String time);

    List<BookingGoodsInfos> findBookingGoodsUser(String time,String openid);

}
