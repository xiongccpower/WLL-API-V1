package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.ButlerDataTransfer;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-08-06
 */
public interface ButlerDataTransferService extends IService<ButlerDataTransfer> {

    ButlerDataTransfer getUid(String uid);

}
