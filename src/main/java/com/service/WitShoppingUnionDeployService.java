package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.WitShoppingUnionDeploy;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-06-30
 */

public interface WitShoppingUnionDeployService extends IService<WitShoppingUnionDeploy> {
    /**
     * 查询所有可用联盟信息
     * @return
     */
    public List<WitShoppingUnionDeploy> findUnionDeploy(String sourceChannel);

    public List<WitShoppingUnionDeploy> findUnionDeployAll();

}
