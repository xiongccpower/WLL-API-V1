package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.ShareContent;
import com.service.ShareContentService;
import com.mapper.ShareContentMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ShareContentServiceImpl extends ServiceImpl<ShareContentMapper, ShareContent>
    implements ShareContentService{

}




