package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.ButlerDataTransfer;
import com.mapper.ButlerDataTransferMapper;
import com.service.ButlerDataTransferService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-08-06
 */
@Service
public class ButlerDataTransferServiceImpl extends ServiceImpl<ButlerDataTransferMapper, ButlerDataTransfer> implements ButlerDataTransferService {
    public ButlerDataTransfer getUid(String uid){
        return baseMapper.getUid(uid);
    }
}
