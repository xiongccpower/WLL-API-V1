package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.ThirdOrder;
import com.service.ThirdOrderService;
import com.mapper.ThirdOrderMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ThirdOrderServiceImpl extends ServiceImpl<ThirdOrderMapper, ThirdOrder>
    implements ThirdOrderService{

}




