package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.ProductGuide;
import com.mapper.ProductGuideMapper;
import com.service.ProductGuideService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-08-20
 */
@Service
public class ProductGuideServiceImpl extends ServiceImpl<ProductGuideMapper, ProductGuide> implements ProductGuideService {
    public List<ProductGuide> findProductGuide(String goodsName){
        return baseMapper.findProductGuide(goodsName);
    }
}
