package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.WitShoppingUnionDeploy;
import com.mapper.WitShoppingUnionDeployMapper;
import com.service.WitShoppingUnionDeployService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-06-30
 */
@Service
public class WitShoppingUnionDeployServiceImpl extends ServiceImpl<WitShoppingUnionDeployMapper, WitShoppingUnionDeploy> implements WitShoppingUnionDeployService {
    @Autowired
    private WitShoppingUnionDeployMapper witShoppingUnionDeployMapper;
    /**
     * 查询所有可用联盟信息
     * @return
     */
    public List<WitShoppingUnionDeploy> findUnionDeploy(String sourceChannel){
        return witShoppingUnionDeployMapper.findUnionDeploy(sourceChannel);
    }

    public List<WitShoppingUnionDeploy> findUnionDeployAll(){
        return baseMapper.findUnionDeployAll();
    }

}
