package com.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dao.MyWalletDao;
import com.dao.MyWalletLogDao;
import com.domain.MyWalletSchedule;
import com.entity.MyWallet;
import com.entity.MyWalletLog;
import com.mapper.MyWalletScheduleMapper;
import com.service.MyWalletScheduleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 *
 */
@Service
@Transactional
public class MyWalletScheduleServiceImpl extends ServiceImpl<MyWalletScheduleMapper, MyWalletSchedule>
        implements MyWalletScheduleService {

    @Resource
    private MyWalletLogDao myWalletLogDao;

    @Resource
    private MyWalletDao myWalletDao;

    @Override
    public void syncOrder() {
        QueryWrapper<MyWalletSchedule> query = new QueryWrapper<>();
        LocalDateTime time = LocalDateTime.now().minusDays(10);
        query.lambda().eq(MyWalletSchedule::getStatus, 1).lt(MyWalletSchedule::getCreateTime, time);
        List<MyWalletSchedule> list = baseMapper.selectList(query);
        for (MyWalletSchedule item : list) {
            MyWalletLog myWalletLog = myWalletLogDao.getByOrderNo(item.getOrderCode(), 1);

            float money = myWalletLog.getMoney();
            MyWallet wallet = myWalletDao.getByCode(item.getCode());
            float lastMoney = wallet.getWait_money() - money;
            if (lastMoney < 0) {
                // 待入账不能为空
                // todo 暂时先跳过
                continue;
            }
            wallet.setWait_money(lastMoney);
            wallet.setMoney(wallet.getMoney() + money);

            myWalletLog.setRemark2("");
            myWalletLogDao.saveAndFlush(myWalletLog);
            myWalletDao.saveAndFlush(wallet);
            item.setStatus((byte) 2);
            item.setUpdateTime(LocalDateTime.now());
            baseMapper.updateById(item);
        }
    }

    @Override
    public void cancel(String orderNo) {
        QueryWrapper<MyWalletSchedule> update = new QueryWrapper<>();
        update.lambda().eq(MyWalletSchedule::getOrderCode, orderNo);
        MyWalletSchedule sc = new MyWalletSchedule();
        sc.setStatus((byte) 3);
        baseMapper.update(sc, update);
    }
}




