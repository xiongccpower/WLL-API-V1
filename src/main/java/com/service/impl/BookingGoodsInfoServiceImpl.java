package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.BookingGoodsInfos;
import com.mapper.BookingGoodsInfoMapper;
import com.service.BookingGoodsInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-07-08
 */
@Service
public class BookingGoodsInfoServiceImpl extends ServiceImpl<BookingGoodsInfoMapper, BookingGoodsInfos> implements BookingGoodsInfoService {
    @Autowired
    BookingGoodsInfoMapper bookingGoodsInfoMapper;

    public List<BookingGoodsInfos> findBookingGoods(String time){
        return bookingGoodsInfoMapper.findBookingGoods(time);
    }

    public List<BookingGoodsInfos> findBookingGoodsUser(String time,String openid){
        return bookingGoodsInfoMapper.findBookingGoodsUser(time,openid);
    }
}
