package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.MyServiceInstructRecord;
import com.mapper.MyServiceInstructRecordMapper;
import com.service.MyServiceInstructRecordService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 管家生活服务指令记录表 服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-07-24
 */
@Service
public class MyServiceInstructRecordServiceImpl extends ServiceImpl<MyServiceInstructRecordMapper, MyServiceInstructRecord> implements MyServiceInstructRecordService {


    public List<MyServiceInstructRecord> findServiceInstructRecord(String phone,String time){
        return baseMapper.findServiceInstructRecord(phone,time);
    }

    public MyServiceInstructRecord getMyServiceInstructRecord(String phone,String goodsName){
        return baseMapper.getMyServiceInstructRecord(phone,goodsName);
    }
}
