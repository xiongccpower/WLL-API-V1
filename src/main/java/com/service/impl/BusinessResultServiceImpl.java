package com.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.BusinessResult;
import com.mapper.BusinessResultMapper;
import com.service.BusinessResultService;
import com.wll.wulian.entity.base.R;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class BusinessResultServiceImpl extends ServiceImpl<BusinessResultMapper, BusinessResult>
        implements BusinessResultService {

    @Override
    public R login(BusinessResult businessResult) {
        if (businessResult.getMobile() == null) {
            return R.error(500, "请输入手机号");
        }
        QueryWrapper<BusinessResult> query = new QueryWrapper<>();
        query.lambda().eq(BusinessResult::getMobile, businessResult.getMobile()).eq(BusinessResult::getStatus, 1);
        BusinessResult exist = baseMapper.selectOne(query);
        if (exist == null) {
            return R.error(500, "未查询到录入信息，请联系管理员录入信息");
        }
        if (exist.getDeviceId() == null) {
            // 首次登录，记录设备ID
            exist.setDeviceId(businessResult.getDeviceId());
            baseMapper.updateById(exist);
        } else if (!exist.getDeviceId().equals(businessResult.getDeviceId())) {
            return R.error(500, "设备ID不符，请使用绑定的设备登录");
        }
        return R.ok().put("data", exist);
    }
}