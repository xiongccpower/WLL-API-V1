package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.TalentDearHelp;
import com.mapper.TalentDearHelpMapper;
import com.service.TalentDearHelpService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 达人分享亲友助力表 服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-08-31
 */
@Service
public class TalentDearHelpServiceImpl extends ServiceImpl<TalentDearHelpMapper, TalentDearHelp> implements TalentDearHelpService {
    public TalentDearHelp getHelpOpenid(String helpOpenid,String talentOpenid,String helpTime){
        return baseMapper.getHelpOpenid(helpOpenid,talentOpenid,helpTime);
    }

    public List<TalentDearHelp> findTalentOpenid(String talentOpenid,String helpTime){
        return baseMapper.findTalentOpenid(talentOpenid,helpTime);
    }

    public TalentDearHelp getByHelpOpenid(String helpOpenid,String helpTime){
        return baseMapper.getByHelpOpenid(helpOpenid,helpTime);
    }

    public TalentDearHelp getHelp(String talentOpenid,String helpTime){
        return baseMapper.getHelp(talentOpenid,helpTime);
    }
}
