package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.Family;
import com.service.FamilyService;
import com.mapper.FamilyMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class FamilyServiceImpl extends ServiceImpl<FamilyMapper, Family>
    implements FamilyService{

}




