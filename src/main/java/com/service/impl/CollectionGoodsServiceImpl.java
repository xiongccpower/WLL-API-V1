package com.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.CollectionGoods;
import com.mapper.CollectionGoodsMapper;
import com.service.CollectionGoodsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
*
*/
@Service
public class CollectionGoodsServiceImpl extends ServiceImpl<CollectionGoodsMapper, CollectionGoods> implements CollectionGoodsService {

    @Override
    public void collectionGoodsOut() {
        QueryWrapper<CollectionGoods> qw = new QueryWrapper<>();
        qw.lambda().eq(CollectionGoods::getOut,0);
        List<CollectionGoods> list = baseMapper.selectList(qw);
        LocalDateTime now = LocalDateTime.now();
        long longNow = System.currentTimeMillis()/1000;
        for (CollectionGoods collectionGoods : list) {
            JSONObject jsonObject = JSON.parseObject(collectionGoods.getRests());
            String goodsChannel = jsonObject.getString("goodsChannel");
            String couponEndTime = jsonObject.getString("couponEndTime");
            if (StringUtils.isBlank(couponEndTime)){
                continue;
            }
            if (goodsChannel.equals("jd")){
                if (Long.parseLong(couponEndTime)<=longNow){
                    collectionGoods.setOut(1);
                    baseMapper.updateById(collectionGoods);
                }
            }else {
                if (now.isAfter(LocalDateTime.parse(couponEndTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))||now.equals(LocalDateTime.parse(couponEndTime,DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))){
                    collectionGoods.setOut(1);
                    baseMapper.updateById(collectionGoods);
                }
            }
        }
    }

}
