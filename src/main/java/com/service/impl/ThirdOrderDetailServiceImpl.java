package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.ThirdOrderDetail;
import com.service.ThirdOrderDetailService;
import com.mapper.ThirdOrderDetailMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ThirdOrderDetailServiceImpl extends ServiceImpl<ThirdOrderDetailMapper, ThirdOrderDetail>
    implements ThirdOrderDetailService{

}




