package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.MyHomeSaleDatas;
import com.mapper.MyHomeSaleDataMapper;
import com.service.MyHomeSaleDataService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-07-09
 */
@Service
public class MyHomeSaleDataServiceImpl extends ServiceImpl<MyHomeSaleDataMapper, MyHomeSaleDatas> implements MyHomeSaleDataService {


    public MyHomeSaleDatas getSubscribeId(int subscribeId){
       return baseMapper.getSubscribeId(subscribeId);
    }
}
