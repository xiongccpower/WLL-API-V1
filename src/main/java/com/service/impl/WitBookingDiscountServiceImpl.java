package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.WitBookingDiscount;
import com.mapper.WitBookingDiscountMapper;
import com.service.WitBookingDiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-07-05
 */
@Service
public class WitBookingDiscountServiceImpl extends ServiceImpl<WitBookingDiscountMapper, WitBookingDiscount> implements WitBookingDiscountService {
    @Autowired
    private WitBookingDiscountMapper witBookingDiscountMapper;

    public List<WitBookingDiscount> findBookingDiscount(String time){
        return witBookingDiscountMapper.findBookingDiscount(time);
    }
}
