package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.SuggestionInfo;
import com.mapper.SuggestionInfoMapper;
import com.service.SuggestionInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-08-20
 */
@Service
public class SuggestionInfoServiceImpl extends ServiceImpl<SuggestionInfoMapper, SuggestionInfo> implements SuggestionInfoService {

}
