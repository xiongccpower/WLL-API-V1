package com.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.UserStatus;
import com.mapper.UserStatusMapper;
import com.service.UserStatusService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class UserStatusServiceImpl extends ServiceImpl<UserStatusMapper, UserStatus>
        implements UserStatusService {

    @Override
    public UserStatus findByOpenId(String openId) {
        QueryWrapper<UserStatus> query = new QueryWrapper<>();
        query.lambda().eq(UserStatus::getOpenId, openId);
        return baseMapper.selectOne(query);
    }
}




