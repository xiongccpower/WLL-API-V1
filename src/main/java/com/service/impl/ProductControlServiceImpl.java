package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.ProductControl;
import com.mapper.ProductControlMapper;
import com.service.ProductControlService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品把控配置表 服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-07-05
 */
@Service
public class ProductControlServiceImpl extends ServiceImpl<ProductControlMapper, ProductControl> implements ProductControlService {

}
