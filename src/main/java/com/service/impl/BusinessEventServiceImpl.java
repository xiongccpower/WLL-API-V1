package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.BusinessEvent;
import com.mapper.BusinessEventMapper;
import com.service.BusinessEventService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class BusinessEventServiceImpl extends ServiceImpl<BusinessEventMapper, BusinessEvent>
        implements BusinessEventService {

    @Override
    public String getInvitePwd(Long mbeId) {
        return baseMapper.getInvitePwd(mbeId);
    }
}




