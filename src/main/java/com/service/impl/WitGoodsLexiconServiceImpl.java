package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.WitGoodsLexicon;
import com.mapper.WitGoodsLexiconMapper;
import com.service.WitGoodsLexiconService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-08-17
 */
@Service
public class WitGoodsLexiconServiceImpl extends ServiceImpl<WitGoodsLexiconMapper, WitGoodsLexicon> implements WitGoodsLexiconService {
    public WitGoodsLexicon getWitGoodsLexicon(String goodsName){
        return baseMapper.getWitGoodsLexicon(goodsName);
    }
}
