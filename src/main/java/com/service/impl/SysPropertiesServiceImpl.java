package com.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.SysProperties;
import com.mapper.SysPropertiesMapper;
import com.service.SysPropertiesService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class SysPropertiesServiceImpl extends ServiceImpl<SysPropertiesMapper, SysProperties>
        implements SysPropertiesService {

    @Override
    public SysProperties findByCode(String code) {
        QueryWrapper<SysProperties> query = new QueryWrapper<>();
        query.lambda().eq(SysProperties::getCode, code);
        return baseMapper.selectOne(query);
    }
}




