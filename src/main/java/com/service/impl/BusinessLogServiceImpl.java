package com.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.BusinessLog;
import com.domain.BusinessResult;
import com.domain.UserStatus;
import com.mapper.BusinessLogMapper;
import com.service.BusinessLogService;
import com.service.BusinessResultService;
import com.service.UserStatusService;
import com.wll.wulian.entity.base.R;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 *
 */
@Service
@Transactional
public class BusinessLogServiceImpl extends ServiceImpl<BusinessLogMapper, BusinessLog>
        implements BusinessLogService {

    @Lazy
    @Resource
    private BusinessLogService businessLogService;

    @Resource
    private BusinessResultService businessResultService;

    @Resource
    private UserStatusService userStatusService;

    @Scheduled(cron = "0/20 * * * * ? ")
    public void syncLog() {

        Map<String, List<BusinessLog>> map = new HashMap<>();
        Map<String, BusinessResult> resultMap = new HashMap<>();

        QueryWrapper<BusinessLog> query = new QueryWrapper<>();
        query.lambda().eq(BusinessLog::getStatus, 0);
        List<BusinessLog> logs = businessLogService.list(query);


        for (BusinessLog log : logs) {
            String key = log.getMbeId() + "_" + log.getOrder();
            if (resultMap.get(key) == null) {
                QueryWrapper<BusinessResult> q = new QueryWrapper<>();
                q.lambda().eq(BusinessResult::getMbeId, log.getMbeId())
                        .eq(BusinessResult::getOrder, log.getOrder())
                        .eq(BusinessResult::getStatus, 1);
                BusinessResult res = businessResultService.getOne(q);
                resultMap.put(key, res);
            }

            List<BusinessLog> list = map.computeIfAbsent(key, k -> new ArrayList<>());
            list.add(log);
        }

        List<BusinessResult> readyRes = new ArrayList<>();
        List<BusinessLog> readyLog = new ArrayList<>();

        for (Map.Entry<String, List<BusinessLog>> entry : map.entrySet()) {
            List<BusinessLog> list = entry.getValue();
            int share = 0;
            int register = 0;
            int buy = 0;
            for (BusinessLog l : list) {
                switch (l.getType()) {
                    case 1:
                        share += l.getNum();
                        break;
                    case 2:
                        register += l.getNum();
                        break;
                    case 3:
                        buy += l.getNum();
                        break;
                    default:
                }
                l.setStatus(1);
            }

            readyLog.addAll(list);

            BusinessResult result = resultMap.get(entry.getKey());
            result.setShare(Optional.ofNullable(result.getShare()).orElse(0) + share);
            result.setRegister(Optional.ofNullable(result.getRegister()).orElse(0) + register);
            result.setBuy(Optional.ofNullable(result.getBuy()).orElse(0) + buy);
            readyRes.add(result);
        }

        businessResultService.updateBatchById(readyRes);
        businessLogService.updateBatchById(readyLog);
    }


    @Override
    public R addLog(BusinessLog log) {
        R ok = R.ok();
        if (StringUtils.isBlank(log.getOpenId())) {
            return ok;
        }

        log.setCreateTime(new Date());
        log.setStatus(0);
        if (log.getNum() == null) {
            log.setNum(1);
        }
        UserStatus status = userStatusService.findByOpenId(log.getOpenId());
        if (status == null) {
            // 创建新纪录
            status = new UserStatus();
            status.setOpenId(log.getOpenId());
            status.setCreateTime(LocalDateTime.now());
            status.setShare((byte) 0);
            status.setRegister((byte) 0);
            status.setBuy((byte) 0);
        }
        // todo 后期使用二进制优化
        switch (log.getType()) {
            case 1:
                if (status.getShare() == 1) {
                    log.setStatus(2);
                    log.setRemark("重复分享");
                    ok.put("shared", true);
                } else {
                    status.setShare((byte) 1);
                }
                break;
            case 2:
                if (status.getRegister() == 1) {
                    log.setStatus(2);
                    log.setRemark("已注册");
                } else {
                    status.setRegister((byte) 1);
                }
                break;
            case 3:
                if (status.getBuy() == 1) {
                    log.setStatus(2);
                    log.setRemark("已首购");
                } else {
                    status.setBuy((byte) 1);
                }
                break;
            default:
        }
        userStatusService.saveOrUpdate(status);
        if (log.getMbeId() == null) {
            return ok;
        }
        businessLogService.save(log);
        return ok;
    }

}