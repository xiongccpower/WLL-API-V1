package com.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.Order;
import com.mapper.OrderMapper;
import com.service.OrderService;
import com.wll.wulian.domain.Instorage;
import com.wll.wulian.domain.Repertory;
import com.wll.wulian.intetfaces.InstorageService;
import com.wll.wulian.intetfaces.RepertoryService;
import lombok.extern.log4j.Log4j2;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 *
 */
@Service
@Log4j2
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order>
        implements OrderService {

    @DubboReference
    private InstorageService instorageService;

    @DubboReference
    private RepertoryService repertoryService;

    @Override
    @Transactional
    public void cancelDeadOrder() {
        UpdateWrapper<Order> update = new UpdateWrapper<>();
        LocalDateTime time = LocalDateTime.now().minusMinutes(15);
        update.lambda().eq(Order::getState, 9).le(Order::getCreateTime, time);
        Order order = new Order();
        order.setState(10);
        List<Order> orders = baseMapper.selectList(update);
        log.info("库存方法所执行的数组长度:{}",orders.size());
        for (Order o : orders) {
            Instorage instorage = new Instorage();
            Repertory repertory;
            if ("system".equals(o.getScIds())){
                repertory = repertoryService.getByDeviceId(1);
                instorage.setCommodityId(1);
                instorage.setNumber(1);
                instorage.setComment("超时取消订单");
            }else {
                repertory = repertoryService.getByDeviceId(Integer.valueOf(o.getGoodsIds()));
                instorage.setCommodityId(repertory.getId());
                instorage.setNumber(1);
                instorage.setComment("超时取消订单");
            }
            repertory.setTotalInventory(repertory.getTotalInventory()-1);
            repertory.setTotalShipments(repertory.getTotalShipments()-1);
            repertoryService.updateById(repertory);
            instorageService.addInStorage(instorage);
        }
        baseMapper.update(order, update);
    }

    @Override
    public void updateEmptyOrder(String uid, String tel, String openId) {
        QueryWrapper<Order> query = new QueryWrapper<>();
        query.lambda().and(w -> w.eq(Order::getPhone, tel)).and(w -> w.eq(Order::getUid, "").or().isNull(Order::getUid));
//        query.lambda().eq(Order::getPhone, tel).and(query.lambda().eq(Order::getUid, "").or().isNull(Order::getUid));
        List<Order> exist = baseMapper.selectList(query);
        for (Order order : exist) {
            order.setUid(uid);
        }
        this.saveOrUpdateBatch(exist);
    }
}




