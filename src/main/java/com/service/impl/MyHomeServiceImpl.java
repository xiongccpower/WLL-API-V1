package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.MyHomes;
import com.mapper.MyHomeMapper;
import com.service.MyHomeService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-07-24
 */
@Service
public class MyHomeServiceImpl extends ServiceImpl<MyHomeMapper, MyHomes> implements MyHomeService {

    public List<MyHomes> findByTelOrMainTel(String tel){
        return baseMapper.findByTelOrMainTel(tel);
    }
}
