package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.TalentShareRule;
import com.mapper.TalentShareRuleMapper;
import com.service.TalentShareRuleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-09-17
 */
@Service
public class TalentShareRuleServiceImpl extends ServiceImpl<TalentShareRuleMapper, TalentShareRule> implements TalentShareRuleService {
    public TalentShareRule getTalentShareRule(){
        return baseMapper.getTalentShareRule();
    }
}
