package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.TalentShareRecord;
import com.entity.TalentShareRecordTwo;
import com.mapper.TalentShareRecordMapper;
import com.service.TalentShareRecordService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 达人分享记录表 服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-08-31
 */
@Service
public class TalentShareRecordServiceImpl extends ServiceImpl<TalentShareRecordMapper, TalentShareRecord> implements TalentShareRecordService {
   public TalentShareRecord getTime(String talentOpenid,String previewOpenid,String time){
        return baseMapper.getTime(talentOpenid,previewOpenid,time);
    }
    /**
     * 统计预览数据
     * @param previewMonth
     * @param previewTime
     * @return
     */
    public List<TalentShareRecord> statisticsPreview(String previewMonth, String previewTime,String city){
        return baseMapper.statisticsPreview(previewMonth,previewTime,city);
    }

    public TalentShareRecord getHelp(String previewTime,String previewMonth,String helpOpenid){
        return baseMapper.getHelp(previewTime,previewMonth,helpOpenid);
    }

    public TalentShareRecord getStatistics(String previewMonth,String previewTime,String talentOpenid){
        return baseMapper.getStatistics(previewMonth,previewTime,talentOpenid);
    }

    public List<TalentShareRecord> findPKData(String pkSquareA,String pkSquareB,String month){
        return baseMapper.findPKData(pkSquareA,pkSquareB,month);
    }

    public List<TalentShareRecord> findPKDataGroupDay(String openid,String month){
        return baseMapper.findPKDataGroupDay(openid,month);
    }
    public List<TalentShareRecordTwo> findPKDataGroupDayAll(String pkSquareA, String pkSquareB, String month){
        return baseMapper.findPKDataGroupDayAll(pkSquareA,pkSquareB,month);
    }
    public List<TalentShareRecord> fondHelpData(String helpOpenid,String month){
        return baseMapper.fondHelpData(helpOpenid,month);
    }
    public List<TalentShareRecordTwo> findUserDataGroupDay(String openid,String month){
        return baseMapper.findUserDataGroupDay(openid,month);
    }

}
