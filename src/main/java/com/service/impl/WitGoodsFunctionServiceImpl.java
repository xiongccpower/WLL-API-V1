package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.WitGoodsFunction;
import com.mapper.WitGoodsFunctionMapper;
import com.service.WitGoodsFunctionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-08-17
 */
@Service
public class WitGoodsFunctionServiceImpl extends ServiceImpl<WitGoodsFunctionMapper, WitGoodsFunction> implements WitGoodsFunctionService {
    public  WitGoodsFunction getGoodsFunction(String goodsName){
        return baseMapper.getGoodsFunction(goodsName);
    }
}
