package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.AwardMoney;
import com.mapper.AwardMoneyMapper;
import com.service.AwardMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 *
 */
@Service
public class AwardMoneyServiceImpl extends ServiceImpl<AwardMoneyMapper, AwardMoney>
        implements AwardMoneyService {

    @Autowired
    private AwardMoneyMapper awardMoneyMapper;

    @Override
    public String findValueByType(int type) {
        return null;
    }

    @Override
    public AwardMoney getLikeCountAndMoney(String type) {
        return awardMoneyMapper.getLikeCountAndMoney(type);
    }

    @Override
    public List<AwardMoney> getList() {
        return awardMoneyMapper.selectList(null);
    }

    public AwardMoney queryById(Integer id) {
        return awardMoneyMapper.selectById(id);
    }
}




