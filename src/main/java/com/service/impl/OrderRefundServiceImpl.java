package com.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.OrderRefund;
import com.mapper.OrderRefundMapper;
import com.service.OrderRefundService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class OrderRefundServiceImpl extends ServiceImpl<OrderRefundMapper, OrderRefund>
        implements OrderRefundService {

    @Override
    public void checkAndSave(OrderRefund refund) {
        QueryWrapper<OrderRefund> query = new QueryWrapper<>();
        query.lambda().eq(OrderRefund::getOrderId, refund.getOrderId());
        OrderRefund exist = baseMapper.selectOne(query);
        // 如果已经存在，则更新
        if (exist == null) {
            baseMapper.insert(refund);
        } else {
            refund.setMorId(exist.getMorId());
            baseMapper.updateById(refund);
        }
    }
}




