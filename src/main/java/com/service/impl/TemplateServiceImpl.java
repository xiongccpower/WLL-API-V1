package com.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.Template;
import com.mapper.TemplateMapper;
import com.service.TemplateService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class TemplateServiceImpl extends ServiceImpl<TemplateMapper, Template>
        implements TemplateService {

    @Override
    public List<Template> listByType(Integer type) {
        QueryWrapper<Template> query = new QueryWrapper<>();
        query.lambda().eq(Template::getType, type);
        return baseMapper.selectList(query);
    }
}




