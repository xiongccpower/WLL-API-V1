package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.domain.BenefitContent;
import com.service.BenefitContentService;
import com.mapper.BenefitContentMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class BenefitContentServiceImpl extends ServiceImpl<BenefitContentMapper, BenefitContent>
    implements BenefitContentService{

}




