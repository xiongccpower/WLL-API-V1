package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.MjFamilyRelation;
import com.mapper.MjFamilyRelationMapper;
import com.service.MjFamilyRelationService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-07-28
 */
@Service
public class MjFamilyRelationServiceImpl extends ServiceImpl<MjFamilyRelationMapper, MjFamilyRelation> implements MjFamilyRelationService {

    public List<MjFamilyRelation> findFamilyRelation(){
        return baseMapper.findFamilyRelation();
    }

    public MjFamilyRelation getMjFamilyRelation(String call){
        return baseMapper.getMjFamilyRelation(call);
    }
}
