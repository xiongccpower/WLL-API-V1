package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.MjUser;
import com.mapper.MjUserMapper;
import com.service.MjUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-08-03
 */
@Service
public class MjUserServiceImpl extends ServiceImpl<MjUserMapper, MjUser> implements MjUserService {

    public MjUser getUserByPhone(String phone) {
        return baseMapper.getUserByPhone(phone);
    }

    public MjUser getUser(String uid, String state) {
        return baseMapper.getUser(uid, state);
    }

    @Override
    public Long findMainId(Long aLong) {
        MjUser mjUser = baseMapper.selectById(aLong);
        Integer num = mjUser.getMainNum();
        if (num != null && num != 0) {
            return Long.valueOf(num);
        }
        return aLong;
    }
}
