package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.WitGoodsBrandLexicon;
import com.mapper.WitGoodsBrandLexiconMapper;
import com.service.WitGoodsBrandLexiconService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-08-17
 */
@Service
public class WitGoodsBrandLexiconServiceImpl extends ServiceImpl<WitGoodsBrandLexiconMapper, WitGoodsBrandLexicon> implements WitGoodsBrandLexiconService {
    public WitGoodsBrandLexicon getWitGoodsBrandLexicon(String brandName){
        return baseMapper.getWitGoodsBrandLexicon(brandName);
    }
}
