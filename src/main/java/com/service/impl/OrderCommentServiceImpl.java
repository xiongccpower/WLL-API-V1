package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.OrderComment;
import com.service.OrderCommentService;
import com.mapper.OrderCommentMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class OrderCommentServiceImpl extends ServiceImpl<OrderCommentMapper, OrderComment>
    implements OrderCommentService{

}




