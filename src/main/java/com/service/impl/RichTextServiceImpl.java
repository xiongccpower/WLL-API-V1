package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mapper.RichTextMapper;
import com.entity.RichText;
import com.service.RichTextService;
import org.springframework.stereotype.Service;

@Service
public class RichTextServiceImpl extends ServiceImpl<RichTextMapper, RichText> implements RichTextService {
}
