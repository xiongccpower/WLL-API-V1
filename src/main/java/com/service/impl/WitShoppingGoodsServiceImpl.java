package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.WitShoppingGoods;
import com.mapper.WitShoppingGoodsMapper;
import com.service.WitShoppingGoodsService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 智慧购物辅助商品表 服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-06-30
 */
@Service
@Component
public class WitShoppingGoodsServiceImpl extends ServiceImpl<WitShoppingGoodsMapper, WitShoppingGoods> implements WitShoppingGoodsService {

    public Integer inst(Collection<WitShoppingGoods> list){
        return baseMapper.insertBatchSomeColumn(list);
    }

    /**
     * 搜索商品进行相应排序
     * @param productName
     * @return
     */
    public List<WitShoppingGoods> findSearch(String productName,int pageSize,String openid,String relation,String uid){
        pageSize =(pageSize-1)*8;
        return baseMapper.findSearch(productName,pageSize,openid,relation,uid);
    }

    public Integer timingDeleteGoods(String time){
        return baseMapper.timingDeleteGoods(time);
    }

    public List<WitShoppingGoods> findDirectGoods(String productName , String userId,int pageSize){
        pageSize =(pageSize-1)*8;
        return baseMapper.findDirectGoods(productName,userId,pageSize);
    }

    public List<WitShoppingGoods> findDirectGoodsSy(String productName , String userId,int pageSize){
        pageSize =(pageSize-1)*8;
        return baseMapper.findDirectGoods(productName,userId,pageSize);
    }

    public int getMaxPageIndex(String productName,String openid){

        int pageIndex = baseMapper.getMaxPageIndex(productName,openid);

        return pageIndex;
    }

    public List<WitShoppingGoods> findSearchGoods(String uid, String productName){
        return baseMapper.findSearchGoods(uid,productName);
    }

}
