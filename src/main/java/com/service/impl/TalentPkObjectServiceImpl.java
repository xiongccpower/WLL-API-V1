package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.TalentPkObject;
import com.mapper.TalentPkObjectMapper;
import com.service.TalentPkObjectService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-09-16
 */
@Service
public class TalentPkObjectServiceImpl extends ServiceImpl<TalentPkObjectMapper, TalentPkObject> implements TalentPkObjectService {
    public TalentPkObject getByLaunchOpenid( String launchOpenid,String time){
        return baseMapper.getByLaunchOpenid(launchOpenid,time);
    }

    public TalentPkObject getByReceiveOpenid(String receiveOpenid,String time){
        return baseMapper.getByReceiveOpenid(receiveOpenid,time);
    }

    public TalentPkObject getByPkYq(String receiveOpenid,String time,int state){
        return baseMapper.getByPkYq(receiveOpenid,time,state);
    }

    public TalentPkObject getByPk(String receiveOpenid,String time,int state){
        return baseMapper.getByPk(receiveOpenid,time,state);
    }
}
