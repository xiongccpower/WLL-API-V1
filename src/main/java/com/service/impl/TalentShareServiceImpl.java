package com.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.TalentShare;
import com.mapper.TalentShareMapper;
import com.service.TalentShareService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czr
 * @since 2021-09-09
 */
@Service
public class TalentShareServiceImpl extends ServiceImpl<TalentShareMapper, TalentShare> implements TalentShareService {
    public TalentShare getByOpenId(String openId,String shareMonth){
        return baseMapper.getByOpenId(openId,shareMonth);
    }

    public TalentShare getByHelpOpenid(String openId){
        return baseMapper.getByHelpOpenid(openId);
    }

    public TalentShare getRanking(String time,String talentOpenid){
        return baseMapper.getRanking(time,talentOpenid);
    }

    public List<TalentShare> getPk(String openidA, String openidB, String month){
        return baseMapper.getPk(openidA,openidB,month);
    }
}
