package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.ProductControl;

/**
 * <p>
 * 商品把控配置表 服务类
 * </p>
 *
 * @author czr
 * @since 2021-07-05
 */
public interface ProductControlService extends IService<ProductControl> {

}
