package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.TalentShare;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-09-09
 */
public interface TalentShareService extends IService<TalentShare> {

    TalentShare getByOpenId(String openId,String shareMonth);

    TalentShare getByHelpOpenid(String openId);

    TalentShare getRanking(String time,String talentOpenid);

    List<TalentShare> getPk(String openidA, String openidB, String month);

}
