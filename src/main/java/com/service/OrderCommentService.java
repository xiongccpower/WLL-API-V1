package com.service;

import com.entity.OrderComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface OrderCommentService extends IService<OrderComment> {

}
