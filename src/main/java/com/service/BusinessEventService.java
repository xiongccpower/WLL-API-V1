package com.service;

import com.domain.BusinessEvent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface BusinessEventService extends IService<BusinessEvent> {

    String getInvitePwd(Long mbeId);
}
