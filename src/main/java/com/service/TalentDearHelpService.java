package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.TalentDearHelp;

import java.util.List;

/**
 * <p>
 * 达人分享亲友助力表 服务类
 * </p>
 *
 * @author czr
 * @since 2021-08-31
 */
public interface TalentDearHelpService extends IService<TalentDearHelp> {

    TalentDearHelp getHelpOpenid(String helpOpenid,String talentOpenid,String helpTime);

    List<TalentDearHelp> findTalentOpenid(String talentOpenid,String helpTime);

    TalentDearHelp getByHelpOpenid(String helpOpenid,String helpTime);

    TalentDearHelp getHelp(String talentOpenid,String helpTime);

}
