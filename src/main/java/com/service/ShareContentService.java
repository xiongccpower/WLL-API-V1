package com.service;

import com.domain.ShareContent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface ShareContentService extends IService<ShareContent> {

}
