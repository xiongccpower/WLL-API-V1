package com.service;

import com.domain.MyWalletSchedule;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface MyWalletScheduleService extends IService<MyWalletSchedule> {

    void syncOrder();

    void cancel(String orderNo);
}
