package com.service;

import com.domain.BusinessLog;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wll.wulian.entity.base.R;

/**
 *
 */
public interface BusinessLogService extends IService<BusinessLog> {

    R addLog(BusinessLog log);
}
