package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.domain.CollectionGoods;

/**
*
*/
public interface CollectionGoodsService extends IService<CollectionGoods> {

    void collectionGoodsOut();
}
