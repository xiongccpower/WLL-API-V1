package com.service;

import com.domain.Family;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface FamilyService extends IService<Family> {

}
