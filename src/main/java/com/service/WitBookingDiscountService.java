package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.WitBookingDiscount;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-07-05
 */
public interface WitBookingDiscountService extends IService<WitBookingDiscount> {
    /**
     * 定时查询所有预约优惠商品
     * @param time
     * @return
     */
    List<WitBookingDiscount> findBookingDiscount(String time);
}
