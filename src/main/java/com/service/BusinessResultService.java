package com.service;

import com.domain.BusinessResult;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wll.wulian.entity.base.R;

/**
 *
 */
public interface BusinessResultService extends IService<BusinessResult> {

    R login(BusinessResult businessResult);
}
