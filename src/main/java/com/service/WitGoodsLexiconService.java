package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.WitGoodsLexicon;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-08-17
 */
public interface WitGoodsLexiconService extends IService<WitGoodsLexicon> {
    WitGoodsLexicon getWitGoodsLexicon(String goodsName);
}
