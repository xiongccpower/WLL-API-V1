package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.TalentPkObject;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-09-16
 */
public interface TalentPkObjectService extends IService<TalentPkObject> {
    TalentPkObject getByLaunchOpenid( String launchOpenid,String time);

    TalentPkObject getByReceiveOpenid(String receiveOpenid,String time);

    TalentPkObject getByPkYq(String receiveOpenid,String time,int state);

    TalentPkObject getByPk(String receiveOpenid,String time,int state);
}
