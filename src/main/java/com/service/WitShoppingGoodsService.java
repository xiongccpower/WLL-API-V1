package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.WitShoppingGoods;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 智慧购物辅助商品表 服务类
 * </p>
 *
 * @author czr
 * @since 2021-06-30
 */
//@Component
//@Service
public interface WitShoppingGoodsService extends IService<WitShoppingGoods> {
    /**
     * 插入商品
     * @param list
     * @return
     */
    public Integer inst(Collection<WitShoppingGoods> list);

    /**
     * 定时删除用户搜索的商品
     * @param time
     * @return
     */
    public Integer timingDeleteGoods(String time);

    /**
     * 查询用户搜索的商品
     * @param productName
     * @param pageSize
     * @param openid
     * @return
     */
    List<WitShoppingGoods> findSearch(String productName, int pageSize,String openid,String relation,String uid);

    List<WitShoppingGoods> findDirectGoods(String productName , String userId,int pageSize);

    List<WitShoppingGoods> findDirectGoodsSy(String productName , String userId,int pageSize);

    int getMaxPageIndex(String productName,String openid);

    List<WitShoppingGoods> findSearchGoods(String uid, String productName);
}
