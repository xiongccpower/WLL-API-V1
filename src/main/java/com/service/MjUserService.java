package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.MjUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-08-03
 */
public interface MjUserService extends IService<MjUser> {

    MjUser getUserByPhone(String phone);

    MjUser getUser(String uid,String state);

    Long findMainId(Long aLong);
}
