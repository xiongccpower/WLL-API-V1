package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.SuggestionInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-08-20
 */
public interface SuggestionInfoService extends IService<SuggestionInfo> {

}
