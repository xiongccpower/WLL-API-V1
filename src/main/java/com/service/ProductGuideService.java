package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.ProductGuide;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-08-20
 */
public interface ProductGuideService extends IService<ProductGuide> {
    List<ProductGuide> findProductGuide(String goodsName);
}
