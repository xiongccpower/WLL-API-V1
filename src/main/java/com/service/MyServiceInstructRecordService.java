package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.MyServiceInstructRecord;

import java.util.List;

/**
 * <p>
 * 管家生活服务指令记录表 服务类
 * </p>
 *
 * @author czr
 * @since 2021-07-24
 */
public interface MyServiceInstructRecordService extends IService<MyServiceInstructRecord> {

    public List<MyServiceInstructRecord> findServiceInstructRecord(String phone,String time);

    MyServiceInstructRecord getMyServiceInstructRecord(String phone,String goodsName);

}
