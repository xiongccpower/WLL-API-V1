package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.WitGoodsBrandLexicon;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-08-17
 */
public interface WitGoodsBrandLexiconService extends IService<WitGoodsBrandLexicon> {
    public WitGoodsBrandLexicon getWitGoodsBrandLexicon(String brandName);
}
