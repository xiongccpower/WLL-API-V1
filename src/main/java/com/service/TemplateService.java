package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.domain.Template;

import java.util.List;

/**
 *
 */
public interface TemplateService extends IService<Template> {

    List<Template> listByType(Integer type);
}