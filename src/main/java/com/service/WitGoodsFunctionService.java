package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.WitGoodsFunction;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-08-17
 */
public interface WitGoodsFunctionService extends IService<WitGoodsFunction> {
    WitGoodsFunction getGoodsFunction(String goodsName);
}
