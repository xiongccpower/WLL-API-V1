package com.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.MjFamilyRelation;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czr
 * @since 2021-07-28
 */
public interface MjFamilyRelationService extends IService<MjFamilyRelation> {

    List<MjFamilyRelation> findFamilyRelation();

    MjFamilyRelation getMjFamilyRelation(String call);

}
