package com.service;

import com.domain.ThirdOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface ThirdOrderService extends IService<ThirdOrder> {

}
