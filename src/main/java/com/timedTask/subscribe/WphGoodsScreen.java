package com.timedTask.subscribe;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.entity.WitShoppingGoods;
import com.platformApi.WphGoodsApi;
import com.vip.adp.api.open.service.GoodsCommentsInfo;
import com.vip.adp.api.open.service.GoodsInfo;
import com.vip.adp.api.open.service.GoodsInfoResponse;
import com.vip.adp.api.open.service.StoreServiceCapability;
import com.vip.adp.common.service.PMSCouponInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WphGoodsScreen {
    public static List<WitShoppingGoods> wphGoodsScreen(String productName,String plat) throws Exception{
        List<WitShoppingGoods> list = new ArrayList<>();

        int num = 500;
        int pageIndex = 0;
        for(int j=0;j<10;j++) {
            pageIndex+=1;
            GoodsInfoResponse goods = WphGoodsApi.getPumaProducts(plat,productName,pageIndex+"","50");

            if(goods.getGoodsInfoList()!=null){
                List<GoodsInfo> goodsInfoList = new ArrayList<>();
                goodsInfoList = goods.getGoodsInfoList();
                for (GoodsInfo good: goodsInfoList) {
                    WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
                    witShoppingGoods.setId(IdUtil.objectId());
                    witShoppingGoods.setCommodityName(good.getGoodsName());
                    witShoppingGoods.setCommodityCode(good.getGoodsId());
                    witShoppingGoods.setPictureUrl(good.getGoodsThumbUrl());
                    witShoppingGoods.setCommodityPrice(good.getVipPrice());
                    witShoppingGoods.setRate(good.getCommissionRate());
                    witShoppingGoods.setBrandName(good.getBrandName());
                    witShoppingGoods.setBaoyou("1");
                    PMSCouponInfo coupon = good.getCouponInfo();
                    if(coupon!=null){
                        if(!StrUtil.hasEmpty(coupon.getFav())){
                            witShoppingGoods.setIsCoupon("1");
                            witShoppingGoods.setCouponValue(coupon.getFav());
                            witShoppingGoods.setCouponCount(coupon.getTotalAmount()+"");
                        }
                    }
                    GoodsCommentsInfo commentsInfo = good.getCommentsInfo();
                    witShoppingGoods.setTotalReviewCount(commentsInfo.getComments()+"");
                    if(!StrUtil.hasEmpty(commentsInfo.getGoodCommentsShare())){
                        witShoppingGoods.setGoodRate(commentsInfo.getGoodCommentsShare().substring(0,commentsInfo.getGoodCommentsShare().length()-3)+"");
                    }else{
                        witShoppingGoods.setGoodRate("0");
                    }
                    witShoppingGoods.setProductUrl(good.getDestUrl());
                    StoreServiceCapability storeServiceCapability =good.getStoreServiceCapability();
                    witShoppingGoods.setStoreScore(storeServiceCapability.getStoreScore());
                    Map<Integer,String> map = good.getCpsInfo();
                    witShoppingGoods.setAppletExtendUrl(map.get(2));
                    witShoppingGoods.setSearchTime(DateUtil.today());
                    witShoppingGoods.setKeyword(productName);
                    witShoppingGoods.setImgUrl(good.getGoodsMainPicture());
                    witShoppingGoods.setCreateTime(DateUtil.now());
                    witShoppingGoods.setGoodsChannel("wph");
                    list.add(witShoppingGoods);
                }
            }else{
                break;
            }

        }
        return list;
    }
}
