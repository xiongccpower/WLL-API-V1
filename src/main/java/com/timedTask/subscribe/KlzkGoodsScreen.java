package com.timedTask.subscribe;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.entity.WitShoppingGoods;
import com.haitao.thirdpart.sdk.APIUtil;
import com.timedTask.GoodsDataDelete;
import com.util.ParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class KlzkGoodsScreen {
    protected static Logger log = LoggerFactory.getLogger(KlzkGoodsScreen.class);

    public static List<WitShoppingGoods> klzkGoodsScreen(String productName){
        List<WitShoppingGoods> list = new ArrayList<>();

        int num = 500;
        int pageIndex = 0;
        for(int j=0;j<10;j++) {
            pageIndex+=1;
            Map<String,Object> map = klzkGoddsInfo(productName,pageIndex+"");
            if(map.get("code").equals("0")){
                JSONArray dataList = JSONUtil.parseArray(map.get("dataList"));
                for (int i=0;i<dataList.size();i++){
                    WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
                    witShoppingGoods.setId(IdUtil.objectId());
                    witShoppingGoods.setCommodityCode(dataList.getJSONObject(i).getStr("goodsId"));
                    witShoppingGoods.setCommodityName(dataList.getJSONObject(i).getJSONObject("baseInfo").getStr("goodsTitle"));
                    witShoppingGoods.setCommodityPrice(dataList.getJSONObject(i).getJSONObject("priceInfo").getStr("currentPrice"));
                    witShoppingGoods.setBaoyou(dataList.getJSONObject(i).getJSONObject("activityInfo").getStr("noPostage"));
                    witShoppingGoods.setGoodsDetailUrl(dataList.getJSONObject(i).getJSONObject("linkInfo").getStr("goodsDetailUrl"));
                    witShoppingGoods.setCreateTime(DateUtil.now());
                    witShoppingGoods.setGoodsChannel("klzk");
                    witShoppingGoods.setProductUrl(dataList.getJSONObject(i).getJSONObject("linkInfo").getStr("goodsDetailUrl"));
                    witShoppingGoods.setPictureUrl(dataList.getJSONObject(i).getJSONObject("baseInfo").getJSONArray("imageList").get(0)+"");
                    witShoppingGoods.setImgUrl(dataList.getJSONObject(i).getJSONObject("baseInfo").getJSONArray("imageList").get(0)+"");
                    witShoppingGoods.setBrandName(dataList.getJSONObject(i).getJSONObject("baseInfo").getStr("brandName"));
                    witShoppingGoods.setGoodRate("0");
                    list.add(witShoppingGoods);
                }
            }else{
                break;
            }

        }

        return list;
    }

    /**
     * 调用考拉赚客查询商品接口
     *
     * @return
     */
    public static Map<String,Object> klzkGoddsInfo(String productName,String pageIndex){

        Map<String,Object> result = new HashMap<>();
        try {
            TreeMap<String,String> map = new TreeMap<String,String>();
            String timestamp = DateUtil.now();

            map.put("timestamp", timestamp);
            map.put("v","1.0");
            map.put("signMethod","md5");
            map.put("unionId", ParameterUtil.KLZK_UNION_ID);
            map.put("method", "kaola.zhuanke.api.searchGoods");
            map.put("keyWord", productName);
            map.put("type", "2");
            map.put("pageNo", pageIndex);
            map.put("pageSize", "50");

            String sign = APIUtil.createSign(ParameterUtil.KLZK_APP_SECRET,map);

            Map<String,Object> object = new HashMap<String, Object>();
            object.put("timestamp", timestamp);
            object.put("v","1.0");
            object.put("signMethod","md5");
            object.put("unionId", ParameterUtil.KLZK_UNION_ID);
            object.put("method", "kaola.zhuanke.api.searchGoods");
            object.put("keyWord", productName);
            object.put("type", "2");
            object.put("pageNo", pageIndex);
            object.put("pageSize", "50");
            object.put("sign", sign);

            String result2 = HttpRequest.post(ParameterUtil.KLZK_APP_URL)
                    .header(Header.ACCEPT, "application/json; charset=UTF-8")//头信息，多个头信息多次调用此方法即可
                    .form(object)//表单内容
                    .timeout(20000)//超时，毫秒
                    .execute().body();
            if(result2.equals("sign incorrect")){
                result.put("code","C002");
                result.put("msg","没有数据返回");
            }else{
                JSONObject json = JSONUtil.parseObj(result2);
                log.info("考拉赚客："+json);

                if(json.getInt("code")==200){
                    JSONObject data = JSONUtil.parseObj(json.get("data"));
                    if(data.containsKey("dataList")){
                        JSONArray dataList = JSONUtil.parseArray(data.get("dataList"));
                        if(dataList.size()>0){

                            result.put("code","0");
                            result.put("dataList",dataList);

                        }else{
                            result.put("code","C002");
                            result.put("msg","没有数据返回");
                        }
                    }else{
                        result.put("code","C002");
                        result.put("msg","没有数据返回");
                    }

                }else{
                    result.put("code","C002");
                    result.put("msg","没有数据返回");
                }
            }
        }catch (Exception e){
            log.info("系统异常："+e.getMessage());
            result.put("code","C002");
            result.put("msg","没有数据返回");
        }


        return result;
    }
}
