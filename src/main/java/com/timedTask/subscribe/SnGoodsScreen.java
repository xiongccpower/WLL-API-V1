package com.timedTask.subscribe;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.entity.WitShoppingGoods;
import com.platformApi.SuningGoodsApi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SnGoodsScreen {

    public static List<WitShoppingGoods> snGoodsScreen(String productName){
        List<WitShoppingGoods> list = new ArrayList<>();
        int num = 500;
        int pageIndex = 1;

        for(int j=0;j<11;j++) {
            pageIndex+=0;
            Map<String,Object> map = SuningGoodsApi.suningGoods(productName,pageIndex+"","40");
            if(map.get("code").equals("0")){
                JSONArray lis = JSONUtil.parseArray(map.get("goodsData"));
                if(lis.size()>0){
                    for(int i = 0; i<lis.size(); i++){
                        WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
                        witShoppingGoods.setId(IdUtil.objectId());
                        witShoppingGoods.setKeyword(productName);
                        witShoppingGoods.setGoodsChannel("sn");
                        witShoppingGoods.setCommodityName(lis.getJSONObject(i).getJSONObject("commodityInfo").getStr("commodityName"));
                        witShoppingGoods.setCommodityCode(lis.getJSONObject(i).getJSONObject("commodityInfo").getStr("commodityCode"));
                        witShoppingGoods.setStoreName(lis.getJSONObject(i).getJSONObject("commodityInfo").getStr("storeName"));
                        witShoppingGoods.setSupplierCode(lis.getJSONObject(i).getJSONObject("commodityInfo").getStr("supplierCode"));
                        witShoppingGoods.setMonthSales(lis.getJSONObject(i).getJSONObject("commodityInfo").getStr("monthSales"));
                        witShoppingGoods.setSnPrice(lis.getJSONObject(i).getJSONObject("commodityInfo").getStr("snPrice"));
                        witShoppingGoods.setCommodityPrice(lis.getJSONObject(i).getJSONObject("commodityInfo").getStr("commodityPrice"));
                        witShoppingGoods.setCommodityType(lis.getJSONObject(i).getJSONObject("commodityInfo").getStr("commodityType"));
                        witShoppingGoods.setBaoyou(lis.getJSONObject(i).getJSONObject("commodityInfo").getStr("baoyou"));
                        witShoppingGoods.setSaleStatus(lis.getJSONObject(i).getJSONObject("commodityInfo").getStr("saleStatus"));
                        witShoppingGoods.setPictureUrls(lis.getJSONObject(i).getJSONObject("commodityInfo").getStr("pictureUrl"));
                        JSONArray pictureUrl = lis.getJSONObject(i).getJSONObject("commodityInfo").getJSONArray("pictureUrl");
                        JSONArray picUrl = new JSONArray();

                        if(pictureUrl.size()>0 && pictureUrl!=null){
                            for(int c=0;c<pictureUrl.size();c++){
                                picUrl.add(pictureUrl.getJSONObject(c).getStr("picUrl"));
                            }
                        }
                        witShoppingGoods.setPictureUrls(picUrl.toString());

                        witShoppingGoods.setImgUrl(pictureUrl.getJSONObject(0).getStr("picUrl"));
                        witShoppingGoods.setSearchTime(DateUtil.today());


                        String productUrl = lis.getJSONObject(i).getJSONObject("commodityInfo").getStr("productUrl");
                        JSONObject productUrlJson = new JSONObject();

                        if(!StrUtil.hasEmpty(productUrl)){
                            witShoppingGoods.setProductUrl(productUrl);
                            productUrlJson = SuningGoodsApi.suningWx(productUrl);
                        }else{
                            witShoppingGoods.setProductUrl(lis.getJSONObject(i).getJSONObject("pgInfo").getStr("pgUrl"));
                            productUrlJson = SuningGoodsApi.suningWx(lis.getJSONObject(i).getJSONObject("pgInfo").getStr("pgUrl"));
                        }
                        witShoppingGoods.setAppId(productUrlJson.getStr("appId"));
                        witShoppingGoods.setAppletExtendUrl(productUrlJson.getStr("appletExtendUrl"));
                        if(lis.getJSONObject(i).getJSONObject("couponInfo").getInt("couponCount")>0){
                            witShoppingGoods.setIsCoupon("1");
                            witShoppingGoods.setCouponUrl(lis.getJSONObject(i).getJSONObject("couponInfo").getStr("couponUrl"));
                            witShoppingGoods.setActivityId(lis.getJSONObject(i).getJSONObject("couponInfo").getStr("activityId"));
                            witShoppingGoods.setActivitySecretKey(lis.getJSONObject(i).getJSONObject("couponInfo").getStr("activitySecretKey"));
                            witShoppingGoods.setCouponValue(lis.getJSONObject(i).getJSONObject("couponInfo").getStr("couponValue"));
                            witShoppingGoods.setCouponCount(lis.getJSONObject(i).getJSONObject("couponInfo").getStr("couponCount"));
                            witShoppingGoods.setCouponStartTime(lis.getJSONObject(i).getJSONObject("couponInfo").getStr("couponStartTime"));
                            witShoppingGoods.setCouponEndTime(lis.getJSONObject(i).getJSONObject("couponInfo").getStr("couponEndTime"));
                            witShoppingGoods.setStartTime(lis.getJSONObject(i).getJSONObject("couponInfo").getStr("startTime"));
                            witShoppingGoods.setEndTime(lis.getJSONObject(i).getJSONObject("couponInfo").getStr("endTime"));

                            double couponValue = lis.getJSONObject(i).getJSONObject("couponInfo").getDouble("couponValue");
                            double commodityPrice = lis.getJSONObject(i).getJSONObject("commodityInfo").getDouble("commodityPrice");

                            witShoppingGoods.setAfterCouponPrice(commodityPrice - couponValue + "");

                        }
                        if(lis.getJSONObject(i).containsKey("cmmdtyReviewInfo")){
                            witShoppingGoods.setTotalReviewCount(lis.getJSONObject(i).getJSONObject("cmmdtyReviewInfo").getStr("totalReviewCount"));
                            witShoppingGoods.setGoodReviewCount(lis.getJSONObject(i).getJSONObject("cmmdtyReviewInfo").getStr("goodReviewCount"));
                            String goodRate = lis.getJSONObject(i).getJSONObject("cmmdtyReviewInfo").getStr("goodRate");
                            witShoppingGoods.setGoodRate(goodRate.substring(0,goodRate.length()-1));

                        }else{
                            witShoppingGoods.setGoodRate("0");
                        }
                        list.add(witShoppingGoods);
                    }
                }
            }else{
                break;
            }
        }

        return list;
    }

}
