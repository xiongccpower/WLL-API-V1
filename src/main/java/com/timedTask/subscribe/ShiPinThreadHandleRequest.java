package com.timedTask.subscribe;

import cn.hutool.json.JSONException;
import cn.hutool.json.JSONObject;
import com.entity.WitShoppingGoods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class ShiPinThreadHandleRequest implements Callable<JSONObject> {
    protected static Logger log = LoggerFactory.getLogger(ShiPinThreadHandleRequest.class);

    private  JSONObject parameter;

    public ShiPinThreadHandleRequest(JSONObject parameter) throws JSONException, IOException {
        this.parameter=parameter;

        List<WitShoppingGoods> paramGoods = new ArrayList<>();
        try
        {

            if(this.parameter.getStr("dataType").equals("sn")){
                List<WitShoppingGoods> snList = SnGoodsScreen.snGoodsScreen(this.parameter.getStr("productName"));
                if(snList.size()>0){
                    for (WitShoppingGoods goods : snList) {
                        paramGoods.add(goods);
                    }
                }
            }
            if(this.parameter.getStr("dataType").equals("wph")){
                List<WitShoppingGoods> wphList = WphGoodsScreen.wphGoodsScreen(this.parameter.getStr("productName"),this.parameter.getStr("plat"));

                if(wphList.size()>0){
                    for (WitShoppingGoods goods : wphList) {
                        paramGoods.add(goods);
                    }
                }
            }

            if(this.parameter.getStr("dataType").equals("klzk")){
                List<WitShoppingGoods> klzkList = KlzkGoodsScreen.klzkGoodsScreen(this.parameter.getStr("productName"));

                if(klzkList.size()>0){
                    for (WitShoppingGoods goods : klzkList) {
                        paramGoods.add(goods);
                    }
                }
            }


            if(paramGoods.size()>0){
                this.parameter.put("returnData",paramGoods);
            }else{
                this.parameter.put("returnData","fail");
            }

        }
        catch(Exception e)
        {
            log.info("调用线程异常"+this.parameter.getStr("dataType")+"："+e.getMessage());
            this.parameter.put("returnData","fail");
        }



    }

    //数据回调
    public JSONObject call() throws Exception {


        return this.parameter;
    }
}
