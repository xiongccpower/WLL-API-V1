package com.timedTask;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.entity.WitBookingDiscount;
import com.service.WitBookingDiscountService;
import com.service.WitShoppingGoodsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 预约商品定时筛选
 */
@Component
@Configuration     //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class BookingDiscountTiming {
    protected static Logger log = LoggerFactory.getLogger(BookingDiscountTiming.class);

    @Autowired
    private WitBookingDiscountService witBookingDiscountService;
    @Autowired
    private WitShoppingGoodsService witShoppingGoodsService;

    private BookingDiscountTiming bookingDiscountTiming;

    @PostConstruct
    public void init(){
        bookingDiscountTiming = this;
        bookingDiscountTiming.witBookingDiscountService = this.witBookingDiscountService;
        bookingDiscountTiming.witShoppingGoodsService = this.witShoppingGoodsService;
    }

//    @Scheduled(cron = "00 25 16 * * ?")
    public void bookingDiscountTiming(){
        String time = DateUtil.today();
        List<WitBookingDiscount> list = bookingDiscountTiming.witBookingDiscountService.findBookingDiscount(time);
        if(list.size()>0 && list!=null){

        }
    }

    @Scheduled(cron = "00 0/20 * * * ?")
    public void deleteGoods(){
        Date date = DateUtil.parse(DateUtil.now());
        DateTime newDate3 = DateUtil.offsetHour(date, -2);
       bookingDiscountTiming.witShoppingGoodsService.timingDeleteGoods(newDate3.toString());
    }
}
