package com.timedTask;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.entity.BookingGoodsInfos;
import com.entity.MyHomeSaleDatas;
import com.platformApi.SuningGoodsApi;
import com.service.BookingGoodsInfoService;
import com.service.MyHomeSaleDataService;
import com.timedTask.subscribe.SubscribeGoodsScreen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 定时删除用户商品数据
 *
 */
@Component
@Configuration     //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
@Service
public class GoodsDataDelete {
    protected static Logger log = LoggerFactory.getLogger(GoodsDataDelete.class);

    @Autowired
    BookingGoodsInfoService bookingGoodsInfoService;
    @Autowired
    MyHomeSaleDataService myHomeSaleDataService;

    private static GoodsDataDelete goodsDataDelete;

    @PostConstruct
    public void init(){
        goodsDataDelete = this;
        goodsDataDelete.bookingGoodsInfoService = this.bookingGoodsInfoService;
        goodsDataDelete.myHomeSaleDataService = this.myHomeSaleDataService;
    }

    //3.添加定时任务
    @Scheduled(cron = "00 58 17 * * ?")
    public void delete(){
        log.info("定时任务执行:"+DateUtil.now());
        try {
            String time = DateUtil.now();
            List<BookingGoodsInfos> list = goodsDataDelete.bookingGoodsInfoService.findBookingGoods(time);
            if(list.size()>0){
                for (BookingGoodsInfos bookingGoods : list) {
                    Map<String,String> map = new HashMap<>();
                    map.put("channel",bookingGoods.getChannel());
                    map.put("openid",bookingGoods.getOpenid());
                    if(!StrUtil.hasEmpty(bookingGoods.getGoodsBand())){
                        map.put("goodsName",bookingGoods.getGoodsBand() + bookingGoods.getGoodsName());
                    }else{
                        map.put("goodsName",bookingGoods.getGoodsName());
                    }
                    map.put("goodsBand",bookingGoods.getGoodsBand());

                    Map<String,Object> goodsMap = SubscribeGoodsScreen.goodsSummary(map);

                    if(goodsMap.get("code").equals("0")){
                        JSONArray goodsArray = JSONUtil.parseArray(goodsMap.get("goodsList"));

                        MyHomeSaleDatas myHomeSaleDatas = goodsDataDelete.myHomeSaleDataService.getSubscribeId(bookingGoods.getId());
                        if(myHomeSaleDatas!=null){
                            if(goodsArray.getJSONObject(0).getDouble("commodityPrice") < Double.parseDouble(myHomeSaleDatas.getPrice())){
                                myHomeSaleDatas.setPrice(goodsArray.getJSONObject(0).getStr("commodityPrice"));
                                myHomeSaleDatas.setPicUrl(goodsArray.getJSONObject(0).getStr("imgUrl"));
                                myHomeSaleDatas.setProductName(goodsArray.getJSONObject(0).getStr("commodityName"));
                                myHomeSaleDatas.setWxProductUrl(goodsArray.getJSONObject(0).getStr("appletExtendUrl"));
                                myHomeSaleDatas.setAppId(goodsArray.getJSONObject(0).getStr("appId"));
                                myHomeSaleDatas.setProductCode(goodsArray.getJSONObject(0).getStr("commodityCode"));
                                myHomeSaleDatas.setCreateTime(DateUtil.now());
                                myHomeSaleDatas.setProductChannel(goodsArray.getJSONObject(0).getStr("goodsChannel"));
                                myHomeSaleDatas.setMobileProductUrl(goodsArray.getJSONObject(0).getStr("productUrl"));
                                goodsDataDelete.myHomeSaleDataService.updateById(myHomeSaleDatas);
                            }
                        }else{
                            MyHomeSaleDatas myHomeSale = new MyHomeSaleDatas();
                            myHomeSale.setPrice(goodsArray.getJSONObject(0).getStr("commodityPrice"));
                            myHomeSale.setPicUrl(goodsArray.getJSONObject(0).getStr("imgUrl"));
                            myHomeSale.setProductName(goodsArray.getJSONObject(0).getStr("commodityName"));
                            myHomeSale.setWxProductUrl(goodsArray.getJSONObject(0).getStr("appletExtendUrl"));
                            myHomeSale.setAppId(goodsArray.getJSONObject(0).getStr("appId"));
                            myHomeSale.setProductCode(goodsArray.getJSONObject(0).getStr("commodityCode"));
                            myHomeSale.setCreateTime(DateUtil.now());
                            myHomeSale.setProductChannel(goodsArray.getJSONObject(0).getStr("goodsChannel"));
                            myHomeSale.setMobileProductUrl(goodsArray.getJSONObject(0).getStr("productUrl"));
                            myHomeSale.setSubscribeId(bookingGoods.getId());
                            myHomeSale.setOpenId(bookingGoods.getOpenid());
                            goodsDataDelete.myHomeSaleDataService.save(myHomeSale);
                        }
                    }
                }
            }
        }catch (Exception e){
            log.info("定时获取预约优惠异常："+ e.getMessage());
        }
        log.info("定时任务执行结束:"+DateUtil.now());
    }

}
