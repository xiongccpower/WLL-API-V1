package com.remote;

import com.api.smart.core.GoodsQuery;
import com.api.smart.core.RemoteSource;
import com.weilaili.websocket.domain.SmartQuery;
import com.weilaili.websocket.interfaces.RemoteService;
import lombok.extern.log4j.Log4j2;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

/**
 * created in 2021/12/22 10:39
 *
 * @author zhuxuelei
 */
@Log4j2
@Service
@DubboService
public class RemoteServiceImpl implements RemoteService {

    @Override
    public void searchGoods(String uid, String cmd, String mac, SmartQuery query) {
        GoodsQuery e1 = new GoodsQuery();
        e1.setPage(1);
        e1.setPageSize(10);
        e1.setUid(uid);
        e1.setSocket(true);
        e1.setKeywords(new String[]{query.getKeyword()});
        e1.setSmartQuery(query);
        e1.setCmd(cmd);
        e1.setMac(mac);
        // todo 后续使用消息队列
        RemoteSource.QUEUE.offer(e1);
    }
}
