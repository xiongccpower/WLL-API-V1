package com.remote;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.butlerApi.ButlerApiUtil;
import com.dao.UserDao;
import com.entity.MjUser;
import com.entity.MyHome;
import com.entity.Users;
import com.service.MjUserService;
import com.util.FunctionUtil;
import com.weilaili.websocket.interfaces.UserService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * created in 2021/12/21 9:24
 *
 * @author zhuxuelei
 */
@Service
@DubboService
public class UserServiceImpl implements UserService {
    @Resource
    private MjUserService mjUserService;

    @Resource
    private UserDao userDao;

    @Override
    public Long save(String mainTel, String tel, String nickname, String relation, String sex, String birth, String like) {
        if (!StrUtil.hasEmpty(tel) && !StrUtil.hasEmpty(nickname) && !StrUtil.hasEmpty(sex)) {
            MjUser mjUser = mjUserService.getUserByPhone(mainTel);
            if (mjUser != null) {

                if (mainTel.equals(tel)) {
                    // 主账号添加用户
                    tel = IdWorker.getIdStr();
                }

                MyHome home = new MyHome();

                home.setMainTel(mainTel);
                home.setTel(tel);
                home.setRelation(relation);
                home.setNickname(nickname);
                home.setSex(sex);
                home.setBirth(birth);
                home.setUid(mjUser.getId() + "");
                home.setLike(like);

                Users user = new Users();


                //查询绑定的设备获取设备macId
                Map<String, Object> deviceBinding = ButlerApiUtil.queryDeviceBinding(mjUser.getId() + "");

                if (deviceBinding.get("code").equals("200")) {
                    String macIds = deviceBinding.get("macIds") + "";

                    String checkCode = "";
                    String pwd = tel.substring(tel.length() - 4, tel.length()) + new SimpleDateFormat("sSSS").format(new Date());

                    home.setCheckCode(tel);
                    user.setPhone(tel);
                    user.setIsBuySystem("Y");
                    user.setIsBuyGoods("N");
                    user.setMj_pwd(pwd);
                    user.setSex(sex);
                    userDao.saveAndFlush(user);

                    JSONObject obj = FunctionUtil.userRegister("", "", "", "", "", tel, pwd, "", "中国", "", "", mjUser.getId() + ""); //注册美捷生活小程序
                    if ("true".equals(obj.getString("success"))) {
                        cn.hutool.json.JSONObject data = JSONUtil.parseObj(obj.get("data"));
                        home.setId(data.getInt("id"));
                        JSONObject jo = FunctionUtil.senderUserPwd(tel, pwd, mainTel); //发送密码
                        return data.getLong("id");
                    }
                }
            }
        }
        return null;
    }

    @Override
    public Long findMainId(Long aLong) {
        return mjUserService.findMainId(aLong);
    }

}
