package com.platformApi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.entity.WitShoppingGoods;
import com.entity.union_entity.GoodsDetails;
import com.haitao.thirdpart.sdk.APIUtil;
import com.service.WitShoppingGoodsService;
import com.util.ParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * 调用考拉赚客api接口
 */
@Component
@Service
public class KlzkGoodsApi {

    protected static Logger log = LoggerFactory.getLogger(KlzkGoodsApi.class);

    @Autowired
    private WitShoppingGoodsService witShoppingGoodsService;

    private static KlzkGoodsApi klzkGoodsApi;

    @PostConstruct
    public void init(){
        klzkGoodsApi = this;
        klzkGoodsApi.witShoppingGoodsService = this.witShoppingGoodsService;
    }

    /**
     * 调用考拉赚客查询商品接口
     *
     * @return
     */
    public static Map<String,Object>  KlzkGoddsInfo(Map<String,Object> parameter,String openid){
        //筛选的商品数量
        int judgeNumber = Integer.parseInt(parameter.get("pageSize").toString());
        int pageIndex = Integer.parseInt(parameter.get("pageIndex").toString());
        String productName = parameter.get("productName").toString();
        String pageSize = parameter.get("pageSize").toString();
        String uid = parameter.get("uid").toString();

        String label = null;
        if(!parameter.get("label").equals("1")){
            label = parameter.get("label").toString();
        }

//        pageIndex+=1;

        Map<String,Object> result = new HashMap<>();
        try {
            TreeMap<String,String> map = new TreeMap<String,String>();
            String timestamp = DateUtil.now();
            map.put("timestamp", timestamp);
            map.put("v","1.0");
            map.put("signMethod","md5");
            map.put("unionId", ParameterUtil.KLZK_UNION_ID);
            map.put("method", "kaola.zhuanke.api.searchGoods");
            map.put("keyWord", productName);
            map.put("type", "2");
            map.put("pageNo", pageIndex+"");
            map.put("pageSize", "20");

            String sign = APIUtil.createSign(ParameterUtil.KLZK_APP_SECRET,map);

            Map<String,Object> object = new HashMap<String, Object>();
            object.put("timestamp", timestamp);
            object.put("v","1.0");
            object.put("signMethod","md5");
            object.put("unionId", ParameterUtil.KLZK_UNION_ID);
            object.put("method", "kaola.zhuanke.api.searchGoods");
            object.put("keyWord", productName);
            object.put("type", "2");
            object.put("pageNo", pageIndex+"");
            object.put("pageSize", "50");
            object.put("sign", sign);

            String result2 = HttpRequest.post(ParameterUtil.KLZK_APP_URL)
                    .header(Header.ACCEPT, "application/json; charset=UTF-8")//头信息，多个头信息多次调用此方法即可
                    .form(object)//表单内容
                    .timeout(20000)//超时，毫秒
                    .execute().body();
            if(result2.equals("sign incorrect")){
                result.put("code","C002");
                result.put("msg","没有数据返回");
            }else{
                JSONObject json = JSONUtil.parseObj(result2);
                if(json.getInt("code")==200){
                    JSONObject data = JSONUtil.parseObj(json.get("data"));
                    JSONArray dataList = JSONUtil.parseArray(data.get("dataList"));
                    if(dataList.size()>0){

                        result = preserveGoods(dataList,openid,productName,pageIndex,uid,parameter.get("relation")+"");

//                        result.put("coder","0");
//                        result.put("goodsList",dataList);

                    }else{
                        result.put("code","C002");
                        result.put("msg","没有数据返回");
                        result.put("sqlklzkExecute",0);
                    }
                }else{
                    result.put("code","C002");
                    result.put("msg","没有数据返回");
                    result.put("sqlklzkExecute",0);
                }
            }
        }catch (Exception e){
            log.info("系统异常："+e.getMessage());
            result.put("code","C002");
            result.put("msg","没有数据返回");
            result.put("sqlklzkExecute",0);
        }

        result.put("klzkPageIndex",pageIndex);
        result.put("productName",productName);

        return result;
    }

    public static GoodsDetails goodsDetails(String goodsId,String appid,String appletUrl){
        GoodsDetails goodsDetails = new GoodsDetails();
        TreeMap<String,String> map = new TreeMap<String,String>();
        String timestamp = DateUtil.now();
        map.put("timestamp", timestamp);
        map.put("v","1.0");
        map.put("signMethod","md5");
        map.put("unionId", ParameterUtil.KLZK_UNION_ID);
        map.put("method", "kaola.zhuanke.api.queryGoodsInfo");
        map.put("goodsIds",goodsId);

        String sign = APIUtil.createSign(ParameterUtil.KLZK_APP_SECRET,map);

        Map<String,Object> object = new HashMap<String, Object>();
        object.put("timestamp", timestamp);
        object.put("v","1.0");
        object.put("signMethod","md5");
        object.put("unionId", ParameterUtil.KLZK_UNION_ID);
        object.put("method", "kaola.zhuanke.api.queryGoodsInfo");
        object.put("goodsIds",goodsId);

        object.put("sign", sign);
        String result2 = HttpRequest.post(ParameterUtil.KLZK_APP_URL)
                .header(Header.ACCEPT, "application/json; charset=UTF-8")//头信息，多个头信息多次调用此方法即可
                .form(object)//表单内容
                .timeout(20000)//超时，毫秒
                .execute().body();

        System.out.println("考拉商品数据="+result2);

        JSONObject json = JSONUtil.parseObj(result2);
        if(json.getInt("code")==200){
            JSONArray jsonArr = JSONUtil.parseArray(json.get("data"));
            JSONObject data = JSONUtil.parseObj(jsonArr.get(0));
            goodsDetails.setGoodsId(data.getStr("goodsId"));
            JSONObject baseInfo = JSONUtil.parseObj(data.getStr("baseInfo"));
            JSONArray imageList = JSONUtil.parseArray(baseInfo.get("imageList"));
            JSONArray detailImgList = JSONUtil.parseArray(baseInfo.get("detailImgList"));
            goodsDetails.setGoodsName(baseInfo.getStr("goodsTitle"));
            goodsDetails.setBrandName(baseInfo.getStr("brandName"));
            goodsDetails.setBrandStoreSn(baseInfo.getStr("brandCountryName"));
            goodsDetails.setGoodsCarouselPictures(JSONUtil.toList(imageList,String.class));
            goodsDetails.setGoodsDetailPictures(JSONUtil.toList(detailImgList,String.class));

            JSONObject priceInfo = JSONUtil.parseObj(data.getStr("baseInfo"));
            goodsDetails.setMarketPrice(priceInfo.get("marketPrice")+"");
            goodsDetails.setVipPrice(priceInfo.get("currentPrice")+"");

            JSONObject activityInfo = JSONUtil.parseObj(data.getStr("activityInfo"));
            goodsDetails.setNoPostage(activityInfo.get("noPostage")+"");

            goodsDetails.setAppid(appid);
            goodsDetails.setAppletUrl(appletUrl);

        }

        return goodsDetails;
    }

    /**
     * 保存商品数据
     * @return
     */
    private static Map<String,Object> preserveGoods(JSONArray dataList,String openid, String productName,int pageIndex,String uid,String relation){
        Map<String,Object> result = new HashMap<>();
        Collection<WitShoppingGoods> lists = new ArrayList<>();

        for (int i=0;i<dataList.size();i++){
            WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
            witShoppingGoods.setId(IdUtil.objectId());
            witShoppingGoods.setRelation(relation);
            witShoppingGoods.setUserId(uid);
            witShoppingGoods.setCommodityCode(dataList.getJSONObject(i).getStr("goodsId"));
            witShoppingGoods.setCommodityName(dataList.getJSONObject(i).getJSONObject("baseInfo").getStr("goodsTitle"));
            witShoppingGoods.setCommodityPrice(dataList.getJSONObject(i).getJSONObject("priceInfo").getStr("currentPrice"));
            witShoppingGoods.setBaoyou(dataList.getJSONObject(i).getJSONObject("activityInfo").getStr("noPostage"));
            witShoppingGoods.setGoodsDetailUrl(dataList.getJSONObject(i).getJSONObject("linkInfo").getStr("goodsDetailUrl"));
            witShoppingGoods.setCreateTime(DateUtil.now());
            witShoppingGoods.setGoodsChannel("klzk");
            witShoppingGoods.setKeyword(productName.trim());
            witShoppingGoods.setProductUrl(dataList.getJSONObject(i).getJSONObject("linkInfo").getStr("goodsDetailUrl"));
            witShoppingGoods.setPictureUrl(dataList.getJSONObject(i).getJSONObject("baseInfo").getJSONArray("imageList").get(0)+"");
            witShoppingGoods.setImgUrl(dataList.getJSONObject(i).getJSONObject("baseInfo").getJSONArray("imageList").get(0)+"");
            witShoppingGoods.setAppletExtendUrl("package-product/pages/index?zkTargetUrl=" + dataList.getJSONObject(i).getJSONObject("linkInfo").getStr("goodsDetailUrl"));
            witShoppingGoods.setPictureUrls(dataList.getJSONObject(i).getJSONObject("baseInfo").getStr("imageList"));
            witShoppingGoods.setPageIndex(pageIndex);
            witShoppingGoods.setGoodRate("");
            if(!StrUtil.hasEmpty(openid)){
                witShoppingGoods.setOpenid(openid);
            }
            lists.add(witShoppingGoods);
        }

        if(lists.size()>0){
            int  num =klzkGoodsApi.witShoppingGoodsService.inst(lists);
            result.put("sqlklzkExecute",num);
            result.put("code","0");
        }else{
            result.put("code","C002");
            result.put("msg","没有数据返回");
            result.put("sqlklzkExecute",0);
        }
        return result;
    }


    public static void main(String[] args) {

//        TreeMap<String,String> map = new TreeMap<String,String>();
//        String timestamp = DateUtil.now();
//        map.put("timestamp", timestamp);
//        map.put("v","1.0");
//        map.put("signMethod","md5");
//        map.put("unionId", ParameterUtil.KLZK_UNION_ID);
//        map.put("method", "kaola.zhuanke.api.searchGoods");
//        map.put("keyWord", "手机");
//        map.put("type", "2");
//        map.put("pageNo", "1");
//        map.put("pageSize", "50");
//
//        String sign = APIUtil.createSign(ParameterUtil.KLZK_APP_SECRET,map);
//
//        Map<String,Object> object = new HashMap<String, Object>();
//        object.put("timestamp", timestamp);
//        object.put("v","1.0");
//        object.put("signMethod","md5");
//        object.put("unionId", ParameterUtil.KLZK_UNION_ID);
//        object.put("method", "kaola.zhuanke.api.searchGoods");
//        object.put("keyWord", "手机");
//        object.put("type", "2");
//        object.put("pageNo", "1");
//        object.put("pageSize", "50");
//        object.put("sign", sign);
//
//        String result2 = HttpRequest.post(ParameterUtil.KLZK_APP_URL)
//                .header(Header.ACCEPT, "application/json; charset=UTF-8")//头信息，多个头信息多次调用此方法即可
//                .form(object)//表单内容
//                .timeout(20000)//超时，毫秒
//                .execute().body();
//
//       System.out.println("result2:"+result2);

//        goodsDetails();

    }

}
