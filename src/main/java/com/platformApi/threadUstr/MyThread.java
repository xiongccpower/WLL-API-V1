package com.platformApi.threadUstr;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONException;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.entity.WitShoppingGoods;
import com.platformApi.KlzkGoodsApi;
import com.platformApi.SuningGoodsApi;
import com.platformApi.WphGoodsApi;
import com.platformApi.YiqifaGoodsApi;
import com.timedTask.subscribe.KlzkGoodsScreen;
import com.timedTask.subscribe.SnGoodsScreen;
import com.timedTask.subscribe.WphGoodsScreen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * 执行线程
 */
public class MyThread implements Callable<JSONObject> {
    protected static Logger log = LoggerFactory.getLogger(MyThread.class);


    private  JSONObject parameter;

    public MyThread(JSONObject parameter) throws JSONException, IOException {
        this.parameter=parameter;

        List<WitShoppingGoods> paramGoods = new ArrayList<>();

        try
        {
            String productName = this.parameter.getStr("productName");
            String pageIndex = this.parameter.getStr("pageIndex");
            String label = this.parameter.getStr("label");
            String openId = this.parameter.getStr("openId");

            Map<String,Object> parm = new HashMap<>();
            parm.put("productName",productName);
            parm.put("pageIndex",pageIndex);
            parm.put("label",label);

            if(this.parameter.getStr("dataType").equals("sn")){

                Map<String,Object> snList = SuningGoodsApi.finalData(parm,openId);

                if(snList.get("code").equals("0")){
                    JSONArray goodsSnList = JSONUtil.parseArray(snList.get("goodsList"));

                    if(goodsSnList.size()>0 && goodsSnList!=null){
                        for(int i=0; i<goodsSnList.size(); i++){
                            WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
                            witShoppingGoods.setId(IdUtil.objectId());
                            witShoppingGoods.setKeyword(productName);
                            witShoppingGoods.setGoodsChannel("sn");
                            witShoppingGoods.setCommodityName(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("commodityName"));
                            witShoppingGoods.setCommodityCode(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("commodityCode"));
                            witShoppingGoods.setStoreName(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("storeName"));
                            witShoppingGoods.setSupplierCode(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("supplierCode"));
                            witShoppingGoods.setMonthSales(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("monthSales"));
                            witShoppingGoods.setSnPrice(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("snPrice"));
                            witShoppingGoods.setCommodityPrice(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("commodityPrice"));
                            witShoppingGoods.setCommodityType(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("commodityType"));
                            witShoppingGoods.setBaoyou(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("baoyou"));
                            witShoppingGoods.setSaleStatus(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("saleStatus"));

                            JSONArray pictureUrl = goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getJSONArray("pictureUrl");
                            JSONArray picUrl = new JSONArray();

                            if(pictureUrl.size()>0 && pictureUrl!=null){
                                for(int j=0;j<pictureUrl.size();j++){
                                    picUrl.add(pictureUrl.getJSONObject(j).getStr("picUrl"));
                                }
                            }
                            witShoppingGoods.setPictureUrls(picUrl.toString());

                            witShoppingGoods.setImgUrl(pictureUrl.getJSONObject(0).getStr("picUrl"));
                            witShoppingGoods.setSearchTime(DateUtil.today());

                            String productUrl = goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("productUrl");
                            JSONObject productUrlJson = new JSONObject();

                            if(!StrUtil.hasEmpty(productUrl)){
                                witShoppingGoods.setProductUrl(productUrl);
                                productUrlJson = SuningGoodsApi.suningWx(productUrl);
                            }else{
                                witShoppingGoods.setProductUrl(goodsSnList.getJSONObject(i).getJSONObject("pgInfo").getStr("pgUrl"));
                                productUrlJson = SuningGoodsApi.suningWx(goodsSnList.getJSONObject(i).getJSONObject("pgInfo").getStr("pgUrl"));
                            }

                            witShoppingGoods.setAppId(productUrlJson.getStr("appId"));
                            if(!StrUtil.hasEmpty(openId)){
                                witShoppingGoods.setOpenid(openId);
                            }
                            witShoppingGoods.setAppletExtendUrl(productUrlJson.getStr("appletExtendUrl"));
                            //                witShoppingGoods.setButlerId(butlerId);
//                witShoppingGoods.setPhone(phone);
                            if(!StrUtil.hasEmpty(label)){
                                witShoppingGoods.setHealthyLabel(label);
                            }
                            if(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getInt("couponCount")>0){
                                witShoppingGoods.setIsCoupon("1");
                                witShoppingGoods.setCouponUrl(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("couponUrl"));
                                witShoppingGoods.setActivityId(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("activityId"));
                                witShoppingGoods.setActivitySecretKey(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("activitySecretKey"));
                                witShoppingGoods.setCouponValue(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("couponValue"));
                                witShoppingGoods.setCouponCount(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("couponCount"));
                                witShoppingGoods.setCouponStartTime(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("couponStartTime"));
                                witShoppingGoods.setCouponEndTime(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("couponEndTime"));
                                witShoppingGoods.setStartTime(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("startTime"));
                                witShoppingGoods.setEndTime(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("endTime"));

                                double couponValue = goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getDouble("couponValue");
                                double commodityPrice = goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getDouble("commodityPrice");

                                witShoppingGoods.setAfterCouponPrice(commodityPrice - couponValue + "");

                            }
                            if(goodsSnList.getJSONObject(i).containsKey("cmmdtyReviewInfo")){
                                witShoppingGoods.setTotalReviewCount(goodsSnList.getJSONObject(i).getJSONObject("cmmdtyReviewInfo").getStr("totalReviewCount"));
                                witShoppingGoods.setGoodReviewCount(goodsSnList.getJSONObject(i).getJSONObject("cmmdtyReviewInfo").getStr("goodReviewCount"));
                                witShoppingGoods.setGoodRate(goodsSnList.getJSONObject(i).getJSONObject("cmmdtyReviewInfo").getStr("goodRate"));

                            }else{
                                witShoppingGoods.setGoodRate("");
                            }
                            paramGoods.add(witShoppingGoods);
                        }
                    }
                }
            }

            if(this.parameter.getStr("dataType").equals("wph")){

                Map<String,Object> wphList = WphGoodsApi.finalData(parameter,openId);

                if(wphList.get("code").equals("0")){
                    JSONArray goodsWphList = JSONUtil.parseArray(wphList.get("goodsList"));
                    log.info("唯品会数据："+goodsWphList);
                }

            }
            if(this.parameter.getStr("dataType").equals("yqf")){
                Map<String,Object> yqfList = YiqifaGoodsApi.finalData(parameter,null);

                if(yqfList.get("code").equals("0")){
                    JSONArray goodsWphList = JSONUtil.parseArray(yqfList.get("goodsList"));
                    if(goodsWphList.size()>0){
                        for(int i=0; i<goodsWphList.size(); i++){
//                log.info("商品名称:"+ goodsList.getJSONObject(i).getStr("productName"));
                            WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
                            witShoppingGoods.setId(IdUtil.objectId());
                            witShoppingGoods.setGoodsChannel("yqf");
                            witShoppingGoods.setKeyword(productName);
                            witShoppingGoods.setCommodityName(goodsWphList.getJSONObject(i).getStr("productName"));
                            witShoppingGoods.setStoreName(goodsWphList.getJSONObject(i).getStr("storeName"));
                            witShoppingGoods.setMonthSales(goodsWphList.getJSONObject(i).getStr("salesVolume"));
                            witShoppingGoods.setPcProductUrl(goodsWphList.getJSONObject(i).getStr("pcProductUrl"));
                            witShoppingGoods.setMobileProductUrl(goodsWphList.getJSONObject(i).getStr("mobileProductUrl"));
                            witShoppingGoods.setMobliePrice(goodsWphList.getJSONObject(i).getStr("mobliePrice"));
                            witShoppingGoods.setPcPrice(goodsWphList.getJSONObject(i).getStr("pcPrice"));
                            witShoppingGoods.setPcPriceOri(goodsWphList.getJSONObject(i).getStr("pcPriceOri"));
                            witShoppingGoods.setMobilePriceOri(goodsWphList.getJSONObject(i).getStr("mobilePriceOri"));
                            witShoppingGoods.setDescription(goodsWphList.getJSONObject(i).getStr("description"));
                            witShoppingGoods.setCreateTime(DateUtil.now());
                            witShoppingGoods.setBaoyou(goodsWphList.getJSONObject(i).getStr("ispostFree"));
                            witShoppingGoods.setIsCoupon(goodsWphList.getJSONObject(i).getStr("isCoupon"));
                            witShoppingGoods.setImgUrl(goodsWphList.getJSONObject(i).getStr("imgUrl"));
                            witShoppingGoods.setSearchTime(DateUtil.today());
                            witShoppingGoods.setOpenid(openId);
//                witShoppingGoods.setButlerId(butlerId);
//                witShoppingGoods.setPhone(phone);
                            if(!StrUtil.hasEmpty(label)){
                                witShoppingGoods.setHealthyLabel(label);
                            }
                            if(goodsWphList.getJSONObject(i).getInt("isCoupon")==1){
                                witShoppingGoods.setCouponValue(goodsWphList.getJSONObject(i).getStr("couponAmount"));
                                double mobliePrice = goodsWphList.getJSONObject(i).getDouble("mobliePrice");
                                double pcPrice = goodsWphList.getJSONObject(i).getDouble("pcPrice");
                                double  couponAmount= Double.parseDouble(goodsWphList.getJSONObject(i).getStr("couponAmount"));
                                witShoppingGoods.setMoblieCouponPrice(mobliePrice - couponAmount+"" );
                                witShoppingGoods.setPcCouponPrice(pcPrice - couponAmount+"" );
                            }
                            paramGoods.add(witShoppingGoods);
                        }
                    }
                }
            }

            if(this.parameter.getStr("dataType").equals("klzk")){
                Map<String,Object> klzkList = KlzkGoodsApi.KlzkGoddsInfo(parameter,openId);

                if(klzkList.get("code").equals("0")){
                    JSONArray goodsKlzkList = JSONUtil.parseArray(klzkList.get("goodsList"));
                    if(goodsKlzkList.size()>0 && goodsKlzkList!=null){
                        for (int i=0;i<goodsKlzkList.size();i++){
                            WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
                            witShoppingGoods.setId(IdUtil.objectId());
                            witShoppingGoods.setCommodityCode(goodsKlzkList.getJSONObject(i).getStr("goodsId"));
                            witShoppingGoods.setCommodityName(goodsKlzkList.getJSONObject(i).getJSONObject("baseInfo").getStr("goodsTitle"));
                            witShoppingGoods.setCommodityPrice(goodsKlzkList.getJSONObject(i).getJSONObject("priceInfo").getStr("currentPrice"));
                            witShoppingGoods.setBaoyou(goodsKlzkList.getJSONObject(i).getJSONObject("activityInfo").getStr("noPostage"));
                            witShoppingGoods.setGoodsDetailUrl(goodsKlzkList.getJSONObject(i).getJSONObject("linkInfo").getStr("goodsDetailUrl"));
                            witShoppingGoods.setCreateTime(DateUtil.now());
                            witShoppingGoods.setGoodsChannel("klzk");
                            witShoppingGoods.setKeyword(productName);
                            witShoppingGoods.setProductUrl(goodsKlzkList.getJSONObject(i).getJSONObject("linkInfo").getStr("goodsDetailUrl"));
                            witShoppingGoods.setPictureUrl(goodsKlzkList.getJSONObject(i).getJSONObject("baseInfo").getJSONArray("imageList").get(0)+"");
                            witShoppingGoods.setImgUrl(goodsKlzkList.getJSONObject(i).getJSONObject("baseInfo").getJSONArray("imageList").get(0)+"");
                            witShoppingGoods.setAppletExtendUrl("package-product/pages/index?zkTargetUrl=" + goodsKlzkList.getJSONObject(i).getJSONObject("linkInfo").getStr("goodsDetailUrl"));
                            witShoppingGoods.setPictureUrls(goodsKlzkList.getJSONObject(i).getJSONObject("baseInfo").getStr("imageList"));
                            witShoppingGoods.setGoodRate("");
                            if(!StrUtil.hasEmpty(openId)){
                                witShoppingGoods.setOpenid(openId);
                            }
                            paramGoods.add(witShoppingGoods);
                        }
                    }
                }
            }


            if(paramGoods.size()>0){
                this.parameter.put("returnData",paramGoods);
            }else{
                this.parameter.put("returnData","fail");
            }

        }
        catch(Exception e)
        {
            log.info("调用线程异常"+this.parameter.getStr("dataType")+"："+e.getMessage());
            this.parameter.put("returnData","fail");
        }



    }

    //数据回调
    public JSONObject call() throws Exception {


        return this.parameter;
    }
}
