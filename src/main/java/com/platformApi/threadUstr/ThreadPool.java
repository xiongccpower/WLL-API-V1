package com.platformApi.threadUstr;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONException;
import cn.hutool.json.JSONObject;
import com.entity.WitShoppingGoods;
import com.timedTask.subscribe.ShiPinThreadHandleRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 线程池
 */
public class ThreadPool {
    protected static Logger log = LoggerFactory.getLogger(ThreadPool.class);

    /**
     * 商品汇总
     * @return
     */
    public static Map<String,Object> goodsSummary(JSONArray parameter) throws JSONException, IOException, InterruptedException, ExecutionException {
        Map<String,Object> result = new HashMap<>();



        //申明线程池
        ExecutorService exc = Executors.newFixedThreadPool(parameter.size());
        //申明数据回调处理类List<Future<JSONObject>>
        List<Future<JSONObject>> futures = new ArrayList<Future< JSONObject>>();
        for (int i =0; i < parameter.size(); i++) {

            JSONObject singleobje=parameter.getJSONObject(i);
            //申请单个线程执行类
            MyThread call =new MyThread(singleobje);

            //提交单个线程
            Future< JSONObject> future = exc.submit(call);
            //将每个线程放入线程集合， 这里如果任何一个线程的执行结果没有回调，线程都会自动堵塞
            futures.add(future);

        }
        JSONArray jsonarray = new JSONArray();

        //所有线程执行完毕之后会执行下面的循环，然后通过循环每个个线程后执行线程的get()方法每个线程执行的结果
        for (Future< JSONObject> future : futures) {

            JSONObject json= future.get();
            if(!json.getStr("returnData").equals("fail")){
                JSONArray returnData =json.getJSONArray("returnData");

                log.info("线程执行结果："+json.getJSONArray("returnData"));
            }
        }

        log.info("搜索出的数据："+ jsonarray);

        //关闭线程池
        exc.shutdown();


        if(jsonarray.size()>0){
            result.put("code","0");
            result.put("goodsList",jsonarray);

        }else{
            result.put("code","C001");
            result.put("msg","没有筛选数据");
        }
        return result;
    }

    /**
     * 商品排序计算
     * @return
     */
    private static List<WitShoppingGoods> sortReckon(JSONArray jsonarray){
        List<WitShoppingGoods> list = new ArrayList<>();
        if(jsonarray.size() > 1 ){
            for (int i = 0; i < jsonarray.size()-1; i++) {
                for (int j = 1; j < jsonarray.size()-i; j++) {
                    Double data0 = jsonarray.getJSONObject(j-1).getDouble("commodityPrice");
                    Double data1 = jsonarray.getJSONObject(j).getDouble("commodityPrice");
                    if (data0.compareTo(data1) > 0){
                        JSONObject temp = jsonarray.getJSONObject(j - 1);
                        jsonarray.set((j - 1), jsonarray.get(j));
                        jsonarray.set(j, temp);
                    }
                }
            }
        }
        return list;
    }


    /*
     * 冒泡排序
     * 1,返回值类型,void
     * 2,参数列表,int[] arr
     *
     * 	第一次:arr[0]与arr[1],arr[1]与arr[2],arr[2]与arr[3],arr[3]与arr[4]比较4次
        第二次:arr[0]与arr[1],arr[1]与arr[2],arr[2]与arr[3]比较3次
        第三次:arr[0]与arr[1],arr[1]与arr[2]比较2次
        第四次:arr[0]与arr[1]比较1次
     */
    public static void bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {				//外循环只需要比较arr.length-1次就可以了
            for (int j = 0; j < arr.length - 1 - i; j++) {		//-1为了防止索引越界,-i为了提高效率
                if(arr[j] > arr[j+1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j+1] = temp;
                }
            }
        }
    }



}
