package com.platformApi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import com.entity.union_entity.GoodsDetails;
import com.util.ParameterUtil;
import com.entity.WitShoppingGoods;
import com.service.WitShoppingGoodsService;
import com.vip.adp.api.open.service.*;
import com.vip.adp.common.service.PMSCouponInfo;
import com.vip.osp.sdk.context.InvocationContext;
import com.vip.osp.sdk.exception.OspException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * 唯品会查询商品接口
 */
@Component
@Service
public class WphGoodsApi {
    protected static Logger log = LoggerFactory.getLogger(WphGoodsApi.class);

    @Autowired
    private WitShoppingGoodsService witShoppingGoodsService;

    private static WphGoodsApi wphGoodsApi;

    @PostConstruct
    public void init(){
        wphGoodsApi = this;
        wphGoodsApi.witShoppingGoodsService = this.witShoppingGoodsService;
    }

    /**
     *
     *
     * @return
     */
    public static GoodsInfoResponse getPumaProducts(String plat,String productName, String pageIndex,String pageSize) throws OspException {
        UnionGoodsServiceHelper.UnionGoodsServiceClient client=new UnionGoodsServiceHelper.UnionGoodsServiceClient();
        InvocationContext invocationContext=InvocationContext.Factory.getInstance();

        invocationContext.setAppKey(ParameterUtil.WPH_APP_KEY);
        invocationContext.setAppSecret(ParameterUtil.WPH_APP_SECRET);
        invocationContext.setAppURL(ParameterUtil.WPH_APP_URL);
        invocationContext.setLanguage("zh");
        QueryGoodsRequest request1 = new QueryGoodsRequest();
        //关键词
        request1.setKeyword(productName);
        //排序字段
//            request1.setFieldName("fieldName");
        //排序顺序：0-正序，1-逆序，默认正序
//            request1.setOrder(1);
        //页码
        request1.setPage(Integer.parseInt(pageIndex));
        //页面大小：默认20,最大50
        request1.setPageSize(6);
        //请求id：调用方自行定义，用于追踪请求，单次请求唯一，建议使用UUID
        request1.setRequestId(IdUtil.objectId());
        //价格区间---start
//            request1.setPriceStart("priceStart");
        //价格区间---end
//            request1.setPriceEnd("priceEnd");
        //是否查询商品评价信息:默认不查询，该数据在详情页有返回，没有特殊需求，建议不查询，影响接口性能
        request1.setQueryReputation(true);
        //是否查询店铺服务能力信息：默认不查询，该数据在详情页有返回，没有特殊需求，建议不查询，影响接口性能
        request1.setQueryStoreServiceCapability(true);
        //是否查询库存:默认不查询
        request1.setQueryStock(true);
        //是否查询商品活动信息:默认不查询
        request1.setQueryActivity(true);
        //是否查询商品预付信息:默认不查询
        request1.setQueryPrepay(true);
        //通用参数
        CommonParams commonParams2 = new CommonParams();
        if(plat.equals("applets")){
            //用户平台：1-PC,2-APP,3-小程序,不传默认为APP
            commonParams2.setPlat(3);
            //是否返回cps链接：0-不查询，1-tra_from参数,2-小程序链接，默认为0，查询多个时按照位运算处理，例如：3表示查询tra_from参数+小程序链接
            request1.setQueryCpsInfo(2);
        }else{
            //用户平台：1-PC,2-APP,3-小程序,不传默认为APP
            commonParams2.setPlat(2);
            //是否返回cps链接：0-不查询，1-tra_from参数,2-小程序链接，默认为0，查询多个时按照位运算处理，例如：3表示查询tra_from参数+小程序链接
            request1.setQueryCpsInfo(0);
        }

        //设备号类型：IMEI，IDFA，OAID，有则传入
//            commonParams2.setDeviceType("deviceType");
        //设备号MD5加密后的值，有则传入
//            commonParams2.setDeviceValue("deviceValue");
        //用户ip地址
//            commonParams2.setIp("ip");
        //经度 如:29.590961456298828
//            commonParams2.setLongitude("longitude");
        //纬度 如:106.51573181152344
//            commonParams2.setLatitude("latitude");
        //分仓 VIP_NH：广州仓， VIP_SH：上海仓， VIP_BJ：北京仓， VIP_CD：成都仓， VIP_HZ：华中仓， ALL：全国仓
//            commonParams2.setWarehouse("warehouse");

        request1.setCommonParams(commonParams2);
        //工具商code
//            request1.setVendorCode("vendorCode");
        //用户pid
//            request1.setChanTag("chanTag");
        //是否查询专属红包信息：默认不查询
        request1.setQueryExclusiveCoupon(true);

        //买家手机号
//            request1.setMobile("mobile");
        System.out.println(client.query(request1));
        log.info("唯品会商品数据:"+client.query(request1));
        return client.query(request1);
    }

    /**
     * 查询商品详情
     * @return
     */
    public static GoodsDetails goodsDetails(String goodsId,String appid,String appletUrl){
        GoodsDetails goodsDetails = new GoodsDetails();
        try {
            UnionGoodsServiceHelper.UnionGoodsServiceClient client=new UnionGoodsServiceHelper.UnionGoodsServiceClient();
            InvocationContext invocationContext=InvocationContext.Factory.getInstance();
            invocationContext.setAppKey(ParameterUtil.WPH_APP_KEY);
            invocationContext.setAppSecret(ParameterUtil.WPH_APP_SECRET);
            invocationContext.setAppURL(ParameterUtil.WPH_APP_URL);
            invocationContext.setLanguage("zh");
            ArrayList<String> goodsIdList1 = new ArrayList<String>();
            goodsIdList1.add(goodsId);
            List<GoodsInfo> list = client.getByGoodsIds(goodsIdList1,"requestId","chanTag");
            for (GoodsInfo goods:list) {

                goodsDetails.setGoodsId(goods.getGoodsId());
                goodsDetails.setGoodsName(goods.getGoodsName());
                goodsDetails.setGoodsDesc(goods.getGoodsDesc());
                goodsDetails.setDestUrl(goods.getDestUrl());
                goodsDetails.setGoodsThumbUrl(goods.getGoodsThumbUrl());
                goodsDetails.setGoodsCarouselPictures(goods.getGoodsCarouselPictures());
                goodsDetails.setGoodsDetailPictures(goods.getGoodsDetailPictures());
                goodsDetails.setGoodsMainPicture(goods.getGoodsMainPicture());
                goodsDetails.setMarketPrice(goods.getMarketPrice());
                goodsDetails.setVipPrice(goods.getVipPrice());
                goodsDetails.setCommissionRate(goods.getCommissionRate());
                goodsDetails.setCommission(goods.getCommission());
                goodsDetails.setDiscount(goods.getDiscount());
                goodsDetails.setBrandStoreSn(goods.getBrandStoreSn());
                goodsDetails.setBrandName(goods.getBrandName());
                goodsDetails.setBrandLogoFull(goods.getBrandLogoFull());
                goodsDetails.setStoreId(goods.getStoreInfo().getStoreId());
                goodsDetails.setStoreName(goods.getStoreInfo().getStoreName());
                goodsDetails.setComments(goods.getCommentsInfo().getComments()+"");
                String commentsShare = goods.getCommentsInfo().getGoodCommentsShare();
                if(!StrUtil.hasEmpty(commentsShare)){
                    goodsDetails.setGoodCommentsShare(commentsShare.substring(0,commentsShare.length()-3));
                }

                goodsDetails.setStoreScore(goods.getStoreServiceCapability().getStoreScore()+"");
                goodsDetails.setStoreRankRate(goods.getStoreServiceCapability().getStoreRankRate()+"");

                com.entity.union_entity.PMSCouponInfo pMSCouponInfo = new com.entity.union_entity.PMSCouponInfo();
                if(goods.getCouponInfo()!=null){
                    pMSCouponInfo.setCouponNo(goods.getCouponInfo().getCouponNo());
                    pMSCouponInfo.setCouponName(goods.getCouponInfo().getCouponName());
                    pMSCouponInfo.setBuy(goods.getCouponInfo().getBuy());
                    pMSCouponInfo.setFav(goods.getCouponInfo().getFav());
                    pMSCouponInfo.setActivateBeginTime(goods.getCouponInfo().getActivateBeginTime()+"");
                    pMSCouponInfo.setActivateEndTime(goods.getCouponInfo().getActivateEndTime()+"");
                    pMSCouponInfo.setUseBeginTime(goods.getCouponInfo().getUseBeginTime()+"");
                    pMSCouponInfo.setUseEndTime(goods.getCouponInfo().getUseEndTime()+"");
                    pMSCouponInfo.setTotalAmount(goods.getCouponInfo().getTotalAmount()+"");
                    pMSCouponInfo.setActivedAmount(goods.getCouponInfo().getActivedAmount()+"");
                }


                goodsDetails.setCouponInfo(pMSCouponInfo);
                goodsDetails.setHaiTao(goods.getHaiTao()+"");
                goodsDetails.setAppid(appid);
                goodsDetails.setAppletUrl(appletUrl);

            }
            System.out.println("返回数据="+list);
        } catch(com.vip.osp.sdk.exception.OspException e){
            e.printStackTrace();
        }
        return goodsDetails;
    }

    /**
     * 最终筛选数据
     * @return
     */
    public static Map<String,Object> finalData(Map<String,Object> parameter,String openid) throws OspException{
        Map<String,Object> result = new HashMap<>();
        JSONArray list = new JSONArray();
        JSONArray goodsList = new JSONArray();

        //筛选的商品数量
        int judgeNumber = Integer.parseInt(parameter.get("pageSize").toString());
        int pageIndex = Integer.parseInt(parameter.get("pageIndex").toString());
        String productName = parameter.get("productName").toString();
        String pageSize = parameter.get("pageSize").toString();
        String uid = parameter.get("uid").toString();

        String label = null;
        if(!parameter.get("label").equals("1")){
            label = parameter.get("label").toString();
        }

//        for(;;) {
//            pageIndex+=1;
            GoodsInfoResponse goods = getPumaProducts(parameter.get("plat").toString(),productName,pageIndex+"",pageSize);
            Collection<WitShoppingGoods> lists = new ArrayList<>();

            if(goods!=null){
                List<GoodsInfo> goodsInfoList = goods.getGoodsInfoList();
                if(goodsInfoList!=null){

//                    result.put("coder","0");
//                    result.put("goodsList",goodsInfoList);

                    for (GoodsInfo good: goodsInfoList) {
                        WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
                        witShoppingGoods.setId(IdUtil.objectId());
                        witShoppingGoods.setRelation(parameter.get("relation")+"");
                        witShoppingGoods.setCommodityName(good.getGoodsName());
                        witShoppingGoods.setCommodityCode(good.getGoodsId());
                        witShoppingGoods.setPictureUrl(good.getGoodsThumbUrl());
                        witShoppingGoods.setCommodityPrice(good.getVipPrice());
                        witShoppingGoods.setRate(good.getCommissionRate());
                        witShoppingGoods.setUserId(uid);
                        witShoppingGoods.setBaoyou("1");
                        if(!StrUtil.hasEmpty(openid)){
                            witShoppingGoods.setOpenid(openid);
                        }
                        PMSCouponInfo coupon = good.getCouponInfo();
                        if(coupon!=null){
                            if(!StrUtil.hasEmpty(coupon.getFav())){
                                witShoppingGoods.setIsCoupon("1");
                                witShoppingGoods.setCouponValue(coupon.getFav());
                                witShoppingGoods.setCouponCount(coupon.getTotalAmount()+"");
                            }
                        }
                        GoodsCommentsInfo commentsInfo = good.getCommentsInfo();
                        witShoppingGoods.setTotalReviewCount(commentsInfo.getComments()+"");
                        if(!StrUtil.hasEmpty(commentsInfo.getGoodCommentsShare())){
                            witShoppingGoods.setGoodRate(commentsInfo.getGoodCommentsShare().substring(0,commentsInfo.getGoodCommentsShare().length()-3)+"%");
                        }else{
                            witShoppingGoods.setGoodRate("");
                        }
                        witShoppingGoods.setProductUrl(good.getDestUrl());
                        StoreServiceCapability storeServiceCapability =good.getStoreServiceCapability();
                        witShoppingGoods.setStoreScore(storeServiceCapability.getStoreScore());
                       if(good.getCpsInfo()!=null){
                           Map<Integer,String> map = good.getCpsInfo();
                           witShoppingGoods.setAppletExtendUrl(map.get(2));
                       }
                        JSONArray pictureUrls =new JSONArray();
                        pictureUrls.add(good.getGoodsMainPicture());
                        witShoppingGoods.setPictureUrls(pictureUrls.toString());
                        witShoppingGoods.setSearchTime(DateUtil.today());
                        witShoppingGoods.setKeyword(productName.trim());
                        witShoppingGoods.setImgUrl(good.getGoodsMainPicture());
                        witShoppingGoods.setCreateTime(DateUtil.now());
                        witShoppingGoods.setGoodsChannel("wph");
                        witShoppingGoods.setPageIndex(pageIndex);
                        lists.add(witShoppingGoods);
                    }

                    if(lists.size()>0){

                        int  num =wphGoodsApi.witShoppingGoodsService.inst(lists);
                        result.put("sqlWphExecute",num);

                    }else{
                        result.put("code","C002");
                        result.put("msg","没有数据");
                        result.put("sqlWphExecute",0);

                    }


                }else{
                    result.put("code","C002");
                    result.put("msg","没有数据");
                    result.put("sqlWphExecute",0);
                }


            }else{
                result.put("code","C002");
                result.put("msg","没有数据");
                result.put("sqlWphExecute",0);
            }
//        }
        result.put("wphPageIndex",pageIndex);
        result.put("productName",productName);
        return result;
    }

    public static void main(String[] args) {
        goodsDetails("6919423114266507996","","");
//        try {
//            GoodsInfoResponse goods =  getPumaProducts("applets","苹果","5","50");
//            List<GoodsInfo> goodsInfoList = goods.getGoodsInfoList();
//            System.out.println("商品:"+goodsInfoList);
//        } catch(OspException e){
//            e.printStackTrace();
//        }
    }
}
