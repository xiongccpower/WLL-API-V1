package com.platformApi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.util.ParameterUtil;
import com.entity.WitShoppingGoods;
import com.service.WitShoppingGoodsService;
import com.suning.api.DefaultSuningClient;
import com.suning.api.entity.netalliance.*;
import com.suning.api.exception.SuningApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;


/**
 * 查询苏宁商品api
 */
@Component
@Service
public class SuningGoodsApi {

    protected static Logger log = LoggerFactory.getLogger(SuningGoodsApi.class);

    @Autowired
    private  WitShoppingGoodsService witShoppingGoodsService;

    private static SuningGoodsApi suningGoodsApi;

    @PostConstruct
    public void init(){
        suningGoodsApi = this;
        suningGoodsApi.witShoppingGoodsService = this.witShoppingGoodsService;
    }

    /**
     * 调用苏宁获取商品接口
     * @param productName 商品名称
     * @param pageIndex   页数
     * @return
     */
    public static Map<String,Object>  suningGoods(String productName, String pageIndex,String pageSize){
        Map<String,Object> result = new HashMap<>();

        List<Object> ss=new ArrayList<>();
        SearchcommodityQueryRequest request = new SearchcommodityQueryRequest();
        JSONObject jo = null;

        //1：减枝 2：不减枝 sortType=1（综合） 默认不剪枝 其他排序默认剪枝
//        request.setBranch("1");
        //城市编码 默认025
//        request.setCityCode("025");
        //1:有券；其他:全部
        request.setCoupon("1");
        //1表示拿到券后价，不传按照以前逻辑取不到券后价
//        request.setCouponMark("1");
        //结束价格
//        request.setEndPrice("20.00");
        //关键字
        request.setKeyword(productName);
        //页码 默认为1
        request.setPageIndex(pageIndex);
        //是否拼购 默认为空 1：是
//        request.setPgSearch("1");
        //图片高度 默认200
        request.setPicHeight("1000");
        //图片宽度 默认200
        request.setPicWidth("1000");
        //销售目录ID
//        request.setSaleCategoryCode("50000");
        //每页条数 默认10，最大支持40
        request.setSize("6");
        //是否苏宁服务 1:是
//        request.setSnfwservice("1");
        //是否苏宁国际 1:是
//        request.setSnhwg("1");
        //排序规则 1：综合（默认） 2：销量由高到低 3：价格由高到低 4：价格由低到高 5：佣金比例由高到低 6：佣金金额由高到低 7：两个维度，佣金金额由高到低，销量由高到低8：近30天推广量由高到低9：近30天支出佣金金额由高到低。
        request.setSortType("2");
        //开始价格
//        request.setStartPrice("10.00");
        //是否苏宁自营 默认为空，1：是
//        request.setSuningService("1");
//        request.setCheckParam(true);
        String serverUrl = ParameterUtil.SOP_REQUEST;
        String appKey = ParameterUtil.APP_KEY;
        String appSecret = ParameterUtil.APP_SECRET;
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey,appSecret, "json");
        JSONArray list = new JSONArray();
        try {
            log.info("搜索苏宁商品开始时间：--------"+ DateUtil.now());
            SearchcommodityQueryResponse response = client.excute(request);
//            jo = JSONObject.parseObject(response.getBody());
            log.info("苏宁商品数据："+response.getBody());
            if(!StrUtil.hasEmpty(response.getBody())){
                JSONObject jo1 = JSONUtil.parseObj(response.getBody());
                if(jo1.getJSONObject("sn_responseContent").containsKey("sn_error")){
                    result.put("code","C002");
                    result.put("msg","没有数据返回");
                }else{
                    JSONObject json = new JSONObject();
                    JSONObject object = jo1.getJSONObject("sn_responseContent").getJSONObject("sn_body");
                    list = object.getJSONArray("querySearchcommodity");

//                list =goodsScreen1(JSONUtil.parseObj(response.getBody()));

                    result.put("code","0");
                    result.put("goodsData",list);
                }

            }else{
                result.put("code","C002");
                result.put("msg","没有数据返回");
            }

            log.info("数据："+list);
            log.info("搜索苏宁商品结束时间----------："+ DateUtil.now());

        } catch (SuningApiException e) {
            log.info("查询苏宁联盟商品异常："+e.getMessage());
            result.put("code","C002");
            result.put("msg","没有数据返回");
//            e.printStackTrace();
        }
        return result;
    }

    /**
     * 查询商品详情
     * @return
     */
    public static JSONObject  goodsDetails(String goodsCode){
        JSONObject jo = null;

        UnionInfomationGetRequest request = new UnionInfomationGetRequest();
        request.setGoodsCode(goodsCode);
        //api入参校验逻辑开关，当测试稳定之后建议设置为 false 或者删除该行
        request.setCheckParam(true);
        String serverUrl = ParameterUtil.SOP_REQUEST;
        String appKey = ParameterUtil.APP_KEY;
        String appSecret = ParameterUtil.APP_SECRET;
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey,appSecret, "json");
        try {
            UnionInfomationGetResponse response = client.excute(request);
            jo = JSONUtil.parseObj(response.getBody());
            System.out.println("返回json格式数据 :" + response.getBody());
        } catch (SuningApiException e) {
            log.info("查询苏宁联盟商品详情异常："+e.getMessage());
            e.printStackTrace();
        }

        return jo;
    }


    //店铺信息查询接口
    public static JSONObject shopdataQueryRequest(String supplierCode){
        JSONObject jo = null;

        ShopdataQueryRequest request = new ShopdataQueryRequest();
        request.setSupplierCode(supplierCode);
        //api入参校验逻辑开关，当测试稳定之后建议设置为 false 或者删除该行
        request.setCheckParam(true);
        String serverUrl = ParameterUtil.SOP_REQUEST;
        String appKey = ParameterUtil.APP_KEY;
        String appSecret = ParameterUtil.APP_SECRET;
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey,appSecret, "json");
        try {
            ShopdataQueryResponse response = client.excute(request);
            jo = JSONUtil.parseObj(response.getBody());
            System.out.println("返回json/xml格式数据 :" + response.getBody());
        } catch (SuningApiException e) {
            log.info("查询苏宁联盟店铺信息异常："+e.getMessage());
            e.printStackTrace();
        }
        return jo;
    }

    /**
     * 最终筛选数据
     * @return
     */
    public static Map<String,Object> finalData(Map<String,Object> parameter,String openid){
        Map<String,Object> result = new HashMap<>();

        JSONArray list = new JSONArray();
        JSONArray goodsList = new JSONArray();

        //筛选的商品数量
        int judgeNumber = Integer.parseInt(parameter.get("pageSize").toString());
        int pageIndex = Integer.parseInt(parameter.get("pageIndex").toString());
        String productName = parameter.get("productName").toString();
        String pageSize = parameter.get("pageSize").toString();
        String uid = parameter.get("uid").toString();


        String label = null;
        if(!parameter.get("label").equals("1")){
            label = parameter.get("label").toString();
        }

//        for(;;) {
//            pageIndex+=1;
            Map<String,Object> map = suningGoods(productName,pageIndex+"",pageSize);
            if(map.get("code").equals("0")){
//                JSONArray lis = JSONUtil.parseArray(map.get("goodsData"));
                list = JSONUtil.parseArray(map.get("goodsData"));
//                if(lis.size()>0){
//                    for(int i = 0; i<lis.size(); i++){
//
////                       if(list.size()>=judgeNumber){
////                           break;
////                       }else{
//                          list.add(lis.getJSONObject(i));
////                       }
//
//                    }
//                }
            }
//            else{
//                break;
//            }

//        }
        if(list.size()>0){
            result = preserveGoods(list,productName,label,pageIndex,openid,uid,parameter.get("relation")+"");

//            result.put("coder","0");
//            result.put("goodsList",list);

        }else{
            result.put("code","C002");
            result.put("msg","没有数据");
            result.put("snPageIndex",pageIndex);
            result.put("sqlSnExecute",0);
            result.put("productName",productName);
        }

        return result;
    }
    /**
     * 保存查询出的数据
     * @return
     */
    public static Map<String,Object> preserveGoods(JSONArray goodsSnList, String productName, String label, int pageIndex,String openid,String uid,String relation){
        Map<String,Object> result = new HashMap<>();
        Collection<WitShoppingGoods> list = new ArrayList<>();
        int num= 0;
        if(goodsSnList.size()>0){
            log.info("封装苏宁商品开始时间******："+ DateUtil.now());
            for(int i=0; i<goodsSnList.size(); i++){
                WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
                witShoppingGoods.setId(IdUtil.objectId());
                witShoppingGoods.setKeyword(productName.trim());
                witShoppingGoods.setUserId(uid);
                witShoppingGoods.setCreateTime(DateUtil.now());
                witShoppingGoods.setRelation(relation);
                witShoppingGoods.setGoodsChannel("sn");
                witShoppingGoods.setCommodityName(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("commodityName"));
                witShoppingGoods.setCommodityCode(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("commodityCode"));
                witShoppingGoods.setStoreName(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("storeName"));
                witShoppingGoods.setSupplierCode(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("supplierCode"));
                witShoppingGoods.setMonthSales(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("monthSales"));
                witShoppingGoods.setSnPrice(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("snPrice"));
                witShoppingGoods.setCommodityPrice(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("commodityPrice"));
                witShoppingGoods.setCommodityType(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("commodityType"));
                witShoppingGoods.setBaoyou(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("baoyou"));
                witShoppingGoods.setSaleStatus(goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("saleStatus"));
                witShoppingGoods.setPageIndex(pageIndex);

                JSONArray pictureUrl = goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getJSONArray("pictureUrl");
                JSONArray picUrl = new JSONArray();

                if(pictureUrl.size()>0 && pictureUrl!=null){
                    for(int j=0;j<pictureUrl.size();j++){
                        picUrl.add(pictureUrl.getJSONObject(j).getStr("picUrl"));
                    }
                }
                witShoppingGoods.setPictureUrls(picUrl.toString());

                witShoppingGoods.setImgUrl(pictureUrl.getJSONObject(0).getStr("picUrl"));
                witShoppingGoods.setSearchTime(DateUtil.today());

                String productUrl = goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getStr("productUrl");
                JSONObject productUrlJson = new JSONObject();

                if(!StrUtil.hasEmpty(productUrl)){
                    witShoppingGoods.setProductUrl(productUrl);
                    productUrlJson = SuningGoodsApi.suningWx(productUrl);
                }else{
                    witShoppingGoods.setProductUrl(goodsSnList.getJSONObject(i).getJSONObject("pgInfo").getStr("pgUrl"));
                    productUrlJson = SuningGoodsApi.suningWx(goodsSnList.getJSONObject(i).getJSONObject("pgInfo").getStr("pgUrl"));
                }

                witShoppingGoods.setAppId(productUrlJson.getStr("appId"));
                if(!StrUtil.hasEmpty(openid)){
                    witShoppingGoods.setOpenid(openid);
                }
                witShoppingGoods.setAppletExtendUrl(productUrlJson.getStr("appletExtendUrl"));
                //                witShoppingGoods.setButlerId(butlerId);
//                witShoppingGoods.setPhone(phone);
                if(!StrUtil.hasEmpty(label)){
                    witShoppingGoods.setHealthyLabel(label);
                }
                if(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getInt("couponCount")>0){
                    witShoppingGoods.setIsCoupon("1");
                    witShoppingGoods.setCouponUrl(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("couponUrl"));
                    witShoppingGoods.setActivityId(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("activityId"));
                    witShoppingGoods.setActivitySecretKey(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("activitySecretKey"));
                    witShoppingGoods.setCouponValue(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("couponValue"));
                    witShoppingGoods.setCouponCount(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("couponCount"));
                    witShoppingGoods.setCouponStartTime(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("couponStartTime"));
                    witShoppingGoods.setCouponEndTime(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("couponEndTime"));
                    witShoppingGoods.setStartTime(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("startTime"));
                    witShoppingGoods.setEndTime(goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getStr("endTime"));

                    double couponValue = goodsSnList.getJSONObject(i).getJSONObject("couponInfo").getDouble("couponValue");
                    double commodityPrice = goodsSnList.getJSONObject(i).getJSONObject("commodityInfo").getDouble("commodityPrice");

                    witShoppingGoods.setAfterCouponPrice(commodityPrice - couponValue + "");

                }
                if(goodsSnList.getJSONObject(i).containsKey("cmmdtyReviewInfo")){
                    witShoppingGoods.setTotalReviewCount(goodsSnList.getJSONObject(i).getJSONObject("cmmdtyReviewInfo").getStr("totalReviewCount"));
                    witShoppingGoods.setGoodReviewCount(goodsSnList.getJSONObject(i).getJSONObject("cmmdtyReviewInfo").getStr("goodReviewCount"));
                    witShoppingGoods.setGoodRate(goodsSnList.getJSONObject(i).getJSONObject("cmmdtyReviewInfo").getStr("goodRate"));

                }else{
                    witShoppingGoods.setGoodRate("");
                }
                list.add(witShoppingGoods);
            }
            log.info("封装苏宁商品结束时间******："+ DateUtil.now());
            log.info("插入苏宁商品开始时间******："+ DateUtil.now());
            num = suningGoodsApi.witShoppingGoodsService.inst(list);
            log.info("插入苏宁商品结束时间******："+ DateUtil.now());
        }

        result.put("snPageIndex",pageIndex);
        result.put("sqlSnExecute",num);
        result.put("productName",productName);

        return result;
    }

    /**
     * 将苏宁链接生成小程序链接地址
     * @param htmlUrl
     * @return
     */
    public static JSONObject  suningWx(String htmlUrl){
        AppletextensionlinkGetRequest request = new AppletextensionlinkGetRequest();
        request.setProductUrl(htmlUrl);
        String serverUrl = ParameterUtil.SOP_REQUEST;
        String appKey = ParameterUtil.APP_KEY;
        String appSecret = ParameterUtil.APP_SECRET;
        JSONObject jo =null;
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey,appSecret, "json");
        try {
            AppletextensionlinkGetResponse response = client.excute(request);
//            System.out.println("返回json/xml格式数据 :" + response.getBody());
            jo = JSONUtil.parseObj(response.getBody());
            jo=JSONUtil.parseObj(jo.getStr("sn_responseContent"));
            jo=JSONUtil.parseObj(jo.getStr("sn_body"));
            jo=JSONUtil.parseObj(jo.getStr("getAppletextensionlink"));
//            System.out.println(jo.getStr("sn_responseContent"));
        } catch (SuningApiException e) {
            e.printStackTrace();
        }
        return jo;
    }

    //测试
    public static void main(String[] args) {

        Map<String,Object> map = suningGoods("牛仔裤 男","1","10");
        System.out.println(map);

//        UnionInfomationGetRequest request = new UnionInfomationGetRequest();
//        request.setGoodsCode("11221825040");//api入参校验逻辑开关，当测试稳定之后建议设置为 false 或者删除该行
//        request.setCheckParam(true);
//        String serverUrl = "https://open.suning.com/api/http/sopRequest";
//        String appKey = ParameterUtil.APP_KEY;
//        String appSecret = ParameterUtil.APP_SECRET;
//        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey,appSecret, "json");
//        try {
//            UnionInfomationGetResponse response = client.excute(request);
//            System.out.println("返回json/xml格式数据 :" + response.getBody());
//        } catch (SuningApiException e) {
//            e.printStackTrace();
//        }


    }

}
