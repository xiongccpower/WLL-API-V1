package com.platformApi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.util.HttpClientUtil;
import com.util.ParameterUtil;
import com.entity.WitShoppingGoods;
import com.service.WitShoppingGoodsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * 查询亿起发商品
 *
 */
@Component
@Service
public class YiqifaGoodsApi {
    protected static Logger log = LoggerFactory.getLogger(YiqifaGoodsApi.class);

    @Autowired
    private  WitShoppingGoodsService witShoppingGoodsService;
    private static YiqifaGoodsApi yiqifaGoodsApi;

    @PostConstruct
    public void init(){
        yiqifaGoodsApi = this;
        yiqifaGoodsApi.witShoppingGoodsService = this.witShoppingGoodsService;
    }

    /**
     * 亿起发获取商品接口
     * @param productName 商品名称
     * @param pageIndex   页数
     * @return
     */
    public static Map<String,Object> yiqifaGoddsInfo(String productName, String pageIndex,String pageSize){
        Map<String,Object> result = new HashMap<>();

        String url = ParameterUtil.YQF_URL;
        String key = ParameterUtil.YIQIFA_KEY;
        String secret = ParameterUtil.YIQIFA_SECRET;
        Map<String,String> object = new HashMap<String, String>();

        object.put("method",ParameterUtil.PRODUCT_LIST);
        //应用证书
        object.put("app_key",key);
        //应用密钥
        object.put("app_secret",secret);
        //编码方式，空则表示使用GBK,可选项有：GBK，UTF-8
        object.put("encoding","UTF-8");
        //状态，空则表示获取全部状态的商品；在售:0 售罄：1
        object.put("status","0");
        //是否有券：1（有），0（没有），空则不限
//        object.put("isCoupon","1");
        //是否包邮，空则表示获取不限是否包邮的商品；1=是，0=否
//        object.put("isPostFree","1");
        //页数
        object.put("pageIndex", pageIndex);
        //每页显示条数
//        object.put("pageSize", pageSize);
        object.put("pageSize", "10");
        //商品名称
        object.put("productName", productName);

        String obj  = HttpClientUtil.doGet(url,object);
        JSONObject jsonObject = JSONUtil.parseObj(obj);
        log.info("亿起发商品数据："+jsonObject);
        if(jsonObject.getInt("code")==0){
//            JSONArray results = jsonObject.getJSONObject("result").getJSONArray("data");
//            log.info("请求数量:"+results.size());


//            log.info("筛选数据"+goodsScreen1(jsonObject));

//            JSONArray goodsScreen1 = goodsScreen1(jsonObject);
            JSONArray results = jsonObject.getJSONObject("result").getJSONArray("data");

            if(results.size()>0){
                result.put("code","0");
                result.put("goodsData",results);
            }else{
                result.put("code","C002");
                result.put("msg","没有数据返回");
            }

        }else{
            result.put("code","C002");
            result.put("msg",jsonObject.get("msg"));
        }

        return result;
    }


    /**
     * 最终筛选数据
     * @return
     */
    public static Map<String,Object> finalData(Map<String,Object> parameter,String userId){
        Map<String,Object> result = new HashMap<>();

        YiqifaGoodsApi yiqifaGoodsApi = new YiqifaGoodsApi();
        JSONArray list = new JSONArray();
        //筛选的商品数量
        int judgeNumber = Integer.parseInt(parameter.get("pageSize")+"");
        int pageIndex = Integer.parseInt(parameter.get("pageIndex")+"");
        String productName = parameter.get("productName").toString();
        String pageSize = parameter.get("pageSize").toString();
        String uid = parameter.get("uid").toString();

        String label = null;
        if(!parameter.get("label").equals("1")){
            label = parameter.get("label").toString();
        }

        JSONArray goodsList = new JSONArray();

//        for(;;) {
//            pageIndex+=1;
            Map<String,Object> map = yiqifaGoddsInfo(productName,pageIndex+"",pageSize);
            if(map.get("code").equals("0")){
                JSONArray lis = JSONUtil.parseArray(map.get("goodsData"));
                if(lis.size()>0){
                    for(int i = 0; i<lis.size(); i++){

                       if(!lis.getJSONObject(i).getStr("mallName").equals("拼多多")){
                          if(list.size()>=judgeNumber){
                             break;
                          }else{
                             list .add(lis.get(i));
                          }

                        }

                    }
                }
            }else{
                result.put("code","C002");
                result.put("msg","没有数据");
                result.put("yqfPageIndex",pageIndex);
                result.put("sqlYqfExecute",0);
                result.put("productName",productName);
            }


//        }

//        log.info("返回数据："+list.size());
//        int pdd =0;
//        int jd =0;
//        int mgj =0;
//        int kla =0;
//        int tbtm =0;
//
//        for(int i=0; i<list.size();i++){
//            if(list.getJSONObject(i).getStr("mallName").equals("拼多多")){
//                pdd+=1;
//            }else if(list.getJSONObject(i).getStr("mallName").equals("京东")){
//                jd+=1;
//            }else if(list.getJSONObject(i).getStr("mallName").equals("蘑菇街")){
//                mgj+=1;
//            }else if(list.getJSONObject(i).getStr("mallName").equals("考拉")){
//                kla+=1;
//            }else if(list.getJSONObject(i).getStr("mallName").equals("淘宝天猫")){
//                tbtm+=1;
//            }
//        }
//
//        log.info("拼多多:"+pdd);
//        log.info("京东:"+jd);
//        log.info("蘑菇街:"+mgj);
//        log.info("考拉:"+kla);
//        log.info("淘宝天猫:"+tbtm);

        if(list.size()>0){
            result = yiqifaGoodsApi.preserveGoods(list,productName,label,pageIndex,userId,uid,parameter.get("relation")+"");

//            result.put("coder","0");
//            result.put("goodsList",list);

        }else{
            result.put("code","C002");
            result.put("msg","没有数据");
            result.put("yqfPageIndex",pageIndex);
            result.put("sqlYqfExecute",0);
            result.put("productName",productName);
        }


        return result;
    }

    /**
     * 保存查询出的数据
     * @return
     */
    public  Map<String,Object> preserveGoods(JSONArray goodsList,String productName,String label,int pageIndex,String userId,String uid,String relation){
        Map<String,Object> result = new HashMap<>();
        Collection<WitShoppingGoods> list = new ArrayList<>();
        int num= 0;
        if(goodsList.size()>0){
            for(int i=0; i<goodsList.size(); i++){
//                log.info("商品名称:"+ goodsList.getJSONObject(i).getStr("productName"));
                WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
                witShoppingGoods.setId(IdUtil.objectId());
                witShoppingGoods.setGoodsChannel("yqf");
                witShoppingGoods.setRelation(relation);
                witShoppingGoods.setUserId(uid);
                witShoppingGoods.setGoodsChannel("yqf");
                witShoppingGoods.setKeyword(productName.trim());
                witShoppingGoods.setCommodityName(goodsList.getJSONObject(i).getStr("productName"));
                witShoppingGoods.setStoreName(goodsList.getJSONObject(i).getStr("storeName"));
                witShoppingGoods.setMonthSales(goodsList.getJSONObject(i).getStr("salesVolume"));
                witShoppingGoods.setPcProductUrl(goodsList.getJSONObject(i).getStr("pcProductUrl"));
                witShoppingGoods.setMobileProductUrl(goodsList.getJSONObject(i).getStr("mobileProductUrl"));
                witShoppingGoods.setMobliePrice(goodsList.getJSONObject(i).getStr("mobliePrice"));
                witShoppingGoods.setPcPrice(goodsList.getJSONObject(i).getStr("pcPrice"));
                witShoppingGoods.setPcPriceOri(goodsList.getJSONObject(i).getStr("pcPriceOri"));
                witShoppingGoods.setMobilePriceOri(goodsList.getJSONObject(i).getStr("mobilePriceOri"));
                witShoppingGoods.setDescription(goodsList.getJSONObject(i).getStr("description"));
                witShoppingGoods.setCreateTime(DateUtil.now());
                witShoppingGoods.setBaoyou(goodsList.getJSONObject(i).getStr("ispostFree"));
                witShoppingGoods.setIsCoupon(goodsList.getJSONObject(i).getStr("isCoupon"));
                witShoppingGoods.setImgUrl(goodsList.getJSONObject(i).getStr("imgUrl"));
                witShoppingGoods.setPageIndex(pageIndex);
                witShoppingGoods.setSearchTime(DateUtil.today());
//                witShoppingGoods.setButlerId(butlerId);
//                witShoppingGoods.setPhone(phone);
                if(!StrUtil.hasEmpty(label)){
                    witShoppingGoods.setHealthyLabel(label);
                }
                if(goodsList.getJSONObject(i).getInt("isCoupon")==1){
                    witShoppingGoods.setCouponValue(goodsList.getJSONObject(i).getStr("couponAmount"));
                    double mobliePrice = goodsList.getJSONObject(i).getDouble("mobliePrice");
                    double pcPrice = goodsList.getJSONObject(i).getDouble("pcPrice");
                    double  couponAmount= Double.parseDouble(goodsList.getJSONObject(i).getStr("couponAmount"));
                    witShoppingGoods.setMoblieCouponPrice(mobliePrice - couponAmount+"" );
                    witShoppingGoods.setPcCouponPrice(pcPrice - couponAmount+"" );
                }
                list.add(witShoppingGoods);
            }

            num = yiqifaGoodsApi.witShoppingGoodsService.inst(list);

            result.put("yqfPageIndex",pageIndex);
            result.put("sqlYqfExecute",num);
            result.put("productName",productName);
            result.put("coder","0");

        }else{
            result.put("coder","0");
            result.put("msg","没有数据");
        }
        return result;
    }

    public static com.alibaba.fastjson.JSONObject yiqifaGoddsInfo(String productName, String pageIndex){
        String url = ParameterUtil.YQF_URL;
        String key = ParameterUtil.YIQIFA_KEY;
        String secret = ParameterUtil.YIQIFA_SECRET;
        Map<String,String> object = new HashMap<String, String>();
        object.put("method",ParameterUtil.PRODUCT_LIST);
        object.put("app_key",key);
        object.put("app_secret",secret);
        object.put("encoding","UTF-8");
        object.put("pageIndex", pageIndex);
        object.put("pageSize", "6");
        object.put("productName", productName);
        String obj  = HttpClientUtil.doGet(url,object);
        com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(obj);
        log.info("亿起发数据:"+obj);
        return jsonObject;

    }

    public static void main(String[] args) {
//        JSONArray list = new JSONArray();
//        int judgeNumber =10;
//        int num =0;
        Map<String,Object> parameter = new HashMap<>();
        parameter.put("pageSize","6");
        parameter.put("yqfPageIndex","1");
        parameter.put("productName","裤子女孩");
        parameter.put("label","1");
//
        Map<String,Object> lis = finalData(parameter,null);

//        yiqifaGoddsInfo("裤子女孩","1");

//        for(;;) {
//            num+=1;
//            JSONArray lis = yiqifaGoddsInfo("手机",num+"");
//            if(lis.size()>0){
//                for(int i = 0; i<lis.size(); i++){
//
//                    if(!lis.getJSONObject(i).getStr("mallName").equals("拼多多")){
//                        if(!StrUtil.hasEmpty(lis.getJSONObject(i).getStr("salesVolume"))){
////                            log.info("是否为空："+lis.getJSONObject(i).getStr("salesVolume"));
//                            if(
////                    list.getJSONObject(i).get("ispostFree").equals("1") ||
//                                    Integer.parseInt(lis.getJSONObject(i).getStr("salesVolume"))>10){
//                                list .add(lis.get(i));
//                            }
//                        }
//                    }
//                }
//            }
//
//
//            if(list.size()>=judgeNumber){
//                break;
//            }
//
//        }
//        log.info("数据下标："+list.size());
//        log.info("商铺信息:"+list);

    }
}
