package com.controller;

import cn.hutool.core.util.StrUtil;
import com.dao.*;
import com.entity.*;
import com.service.MjUserService;
import com.service.OrderService;
import com.util.FunctionUtil;
import com.util.MsgUtil;
import com.util.WebUtil;
import com.wll.wulian.domain.Instorage;
import com.wll.wulian.intetfaces.InstorageService;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 操作后台
 *
 * @author Administrator
 */
@RestController
@RequestMapping("admin")
@Validated
@Api(value = "综合类", tags = {"综合类"})
//@CrossOrigin
public class BackstageController {
    protected static Logger log = LoggerFactory.getLogger(BackstageController.class);

    @Resource
    private OrderService orderService;

    @Autowired
    private OrderDao orderD;
    @Autowired
    private ShopCartDao shopCartD;
    @Autowired
    private UserDao UserD;
    @Autowired
    private GoodsDao goodsD;
    @Autowired
    private MyDataDao MyDataD;
    @Autowired
    private MyHomeDao MyHomeD;
    @Autowired
    private DictionariesDao dicDao;
    @Autowired
    private ProblemDao problemDao;
    @Autowired
    private FacilityDao facilityDao;
    @Autowired
    private ReferralDao referralDao;
    @Autowired
    private UseInfoDao useInfoDao;
    @Autowired
    private InvitePwdDao invitePwdDao;
    @Autowired
    private BindInfoDao bindInfoDao;
    @Autowired
    private ShareInfoDao shareInfoDao;
    @Autowired
    private AwardRuleDao awardInfoDao;
    @Autowired
    private CheckInfoDao checkInfoDao;
    @Autowired
    private WayInfoDao wayInfoDao;
    @Autowired
    private SearchDemoDao searchDemoDao;
    @Autowired
    private MjSystemInfoDao systemInfoDao;
    @Autowired
    private MjOpenSoundDao soundDao;

    @Autowired
    private FileUploadDao fileUploadDao;

    @Autowired
    private BookingGoodsDao bookingGoodsDao;
    @Autowired
    private HealthInfoDao healthInfoDao;
    @Autowired
    private MjUserService mjUserService;


    @RequestMapping("/goods")
    public ModelAndView goods() {
        ModelAndView modelAndView = new ModelAndView("goods"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/order")
    public ModelAndView order() {
        ModelAndView modelAndView = new ModelAndView("order"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/systemOrder")
    public ModelAndView systemOrder() {
        ModelAndView modelAndView = new ModelAndView("systemOrder"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/facility")
    public ModelAndView facility() {
        ModelAndView modelAndView = new ModelAndView("facility"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/problem")
    public ModelAndView problem() {
        ModelAndView modelAndView = new ModelAndView("problem"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/user")
    public ModelAndView user() {
        ModelAndView modelAndView = new ModelAndView("user"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @PostMapping("/index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("index"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/referral")
    public ModelAndView referral() {
        ModelAndView modelAndView = new ModelAndView("referral"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/useInfoPage")
    public ModelAndView useInfoPage() {
        ModelAndView modelAndView = new ModelAndView("useInfo"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/bindInfoPage")
    public ModelAndView bindInfoPage() {
        ModelAndView modelAndView = new ModelAndView("bindInfo"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/shareInfoPage")
    public ModelAndView shareInfoPage() {
        ModelAndView modelAndView = new ModelAndView("shareInfo"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/shearchDemo")
    public ModelAndView shearchDemo() {
        ModelAndView modelAndView = new ModelAndView("searchDemo"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/sendMsg")
    public ModelAndView sendMsg() {
        ModelAndView modelAndView = new ModelAndView("sendMsg"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/checkMsg")
    public ModelAndView checkMsg() {
        ModelAndView modelAndView = new ModelAndView("checkMsg"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/awardRule")
    public ModelAndView awardRule() {
        ModelAndView modelAndView = new ModelAndView("awardRule"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @RequestMapping("/wayInfo")
    public ModelAndView wayInfo() {
        ModelAndView modelAndView = new ModelAndView("wayInfo"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @GetMapping("/checkInfo")
    public ModelAndView checkInfo() {
        ModelAndView modelAndView = new ModelAndView("checkInfo"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @GetMapping("/sound")
    public ModelAndView sound() {
        ModelAndView modelAndView = new ModelAndView("soundInfo"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @GetMapping("/homeInfo")
    public ModelAndView homeInfo() {
        ModelAndView modelAndView = new ModelAndView("homeInfo"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @GetMapping("/bookingGoodsPage")
    public ModelAndView bookingGoodsPage() {
        ModelAndView modelAndView = new ModelAndView("bookingGoods"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    @GetMapping("/healthInfoPage")
    public ModelAndView healthInfoPage() {
        ModelAndView modelAndView = new ModelAndView("healthInfo"); // 设置对应JSP的模板文件
        return modelAndView;
    }


    // ==================================================
    @PostMapping("userList")
    public Object userList() {
        List<Users> list = UserD.findAll();
        return WebUtil.ok(list);
    }

    @PostMapping("getUser")
    public Object getUser(Integer id) {
        Users u = UserD.getOne(id);
        return WebUtil.ok(u);
    }

    @PostMapping("userUpdate")
    public Object userUpdate(Users user) {
        Users users = UserD.getOne(user.getId());
        users.setIsBuySystem(user.getIsBuySystem());
        return WebUtil.ok();
    }

    @PostMapping("myHomeList")
    public Object getMyHome(String mainTel) {
        List<MyHome> home = MyHomeD.getByMainTel(mainTel);
        return WebUtil.ok(home);
    }

    @PostMapping("myDataList")
    public Object getMyData(Integer userId) {
        List<MyData> data = MyDataD.getByUserId(userId);
        return WebUtil.ok(data);
    }

    @PostMapping("systemOrderList")
    public Object systemOrderList(String scIds, Integer current, Integer size) {
//        if (current == null || size == null) {
        List<Order> order = orderD.getByScIds(scIds);
        return WebUtil.ok(order);
//        } else {
//            if (current < 1) {
//                return WebUtil.error("页码数从1开始，不能小于1");
//            }
////            int curr = current - 1;
////            int s = (current + 1) * size;
//
//            PageRequest page = PageRequest.of(current, size);
//            Order query = new Order();
//            query.setScIds(scIds);
//
//            ExampleMatcher customExampleMatcher = ExampleMatcher.matchingAny()
//                    .withIgnoreCase("total", "payTotal", "freight");
//            Example<Order> example = Example.of(query, customExampleMatcher);
//            Page<Order> order = orderD.findAll(example, page);
//            List<Order> content = order.getContent();
//            return WebUtil.ok(content);
////            List<Order> order = orderD.getNotSystem(scIds);
////            return WebUtil.ok(order);
//        }
    }

    @PostMapping("orderList")
    public Object orderList(String scIds, Integer current, Integer size) {
        if (current == null || size == null) {
            List<Order> order = orderD.getNotSystem(scIds);
            return WebUtil.ok(order);
        } else {
            if (current < 1) {
                return WebUtil.error("页码数从1开始，不能小于1");
            }
//            int curr = current - 1;
//            int s = (current + 1) * size;

            PageRequest page = PageRequest.of(current, size);
            Order query = new Order();
            query.setScIds(scIds);
            ExampleMatcher customExampleMatcher = ExampleMatcher.matchingAny()
                    .withIgnoreCase("total", "payTotal", "freight");
            Example<Order> example = Example.of(query, customExampleMatcher);
            Page<Order> order = orderD.findAll(example, page);
            List<Order> content = order.getContent();
            Map<String, Object> ok = (Map<String, Object>) WebUtil.ok(content, (int) order.getTotalElements());
            ok.put("pages", order.getTotalPages());
            return ok;
//            List<Order> order = orderD.getNotSystem(scIds);
//            return WebUtil.ok(order);
        }
    }

    @PostMapping("getOrder")
    public Object getOrder(Integer id) {
        Order u = orderD.getOne(id);
        return WebUtil.ok(u);
    }

    @PostMapping("orderUpdate")
    public Object orderUpdate(Order order) {
        Order u = orderD.getOne(order.getId());
        u.setExpressNo(order.getOrderNo());
        u.setState(2);
        orderD.saveAndFlush(u);
        return WebUtil.ok(u);
    }

    @PostMapping("delivery") // 发货
    public Object delivery(Integer id, String expressNo, String expressNum) {
        if (id == null) {
            return MsgUtil.fail("请传入正确的订单ID");
        }
        Order order = orderD.getOne(id);
        if (order == null) {
            return MsgUtil.fail("未查询到该订单");
        }
        order.setState(2);
        // todo 出库
        order.setDeliveryTime(new Date());
        order.setExpressNo(expressNo);
        order.setExpressNum(expressNum);
        orderD.saveAndFlush(order);
        return WebUtil.ok(order);
    }

    // =============商品==================
    @PostMapping("goodsList")
    public Object goodsList() {
        List<Goods> u = goodsD.findAll();
        return WebUtil.ok(u);
    }

    @PostMapping("getGoods")
    public Object getGoods(Integer id) {
        Goods u = goodsD.getOne(id);
        return WebUtil.ok(u);
    }

    @PostMapping("deleteGoods")
    public Object deleteGoods(Integer id) {
        Goods u = goodsD.getOne(id);
        goodsD.delete(u);
        return WebUtil.ok();
    }

    @PostMapping("goodState")
    public Object goodState(Integer id, String state) {
        if ("1".equals(state)) {
            state = "2";
        } else if ("2".equals(state)) {
            state = "1";
        }
        Goods u = goodsD.getOne(id);
        u.setState(state);
        goodsD.saveAndFlush(u);
        return WebUtil.ok();
    }

    @PostMapping("goodsUpdate")
    public Object goodsList(Goods g, String picList) {

        picList = picList.replace("\"", "").replace("[", "").replace("]", "");
        String[] str = picList.split(",");
        List<String> list = Arrays.asList(str);
        g.setPicUrl(list);
        goodsD.saveAndFlush(g);
        return WebUtil.ok();
    }

    //==================login=================
    @GetMapping("login")
    public Object login(String pwd, HttpServletRequest req) {

        HttpSession session = req.getSession();
        Dictionaries dic = dicDao.getByKey("system_pwd");
        session.setAttribute("token", "login");
        session.setMaxInactiveInterval(3600);

        if (!dic.getDicValue().equals(pwd)) {
            return "密码错误";
        }
        ModelAndView modelAndView = new ModelAndView("/index"); // 设置对应JSP的模板文件
        return modelAndView;
    }

    //=============常见问题=====================
    @PostMapping("problemList")
    public Object problemList() {
        List<Problem> list = problemDao.findAllByOrdeByid();
        return WebUtil.ok(list);
    }

    @PostMapping("searchDemoList")
    public Object searchDemoList() {
        List<SearchDemo> list = searchDemoDao.findAll();
        return WebUtil.ok(list);
    }

    @PostMapping("problemSAU")
    public Object problemSAU(Problem p) {
        problemDao.saveAndFlush(p);
        return WebUtil.ok();
    }

    @PostMapping("deleteProblem")
    public Object deleteProblem(Integer id) {
        Problem p = problemDao.getOne(id);
        problemDao.delete(p);
        return WebUtil.ok();
    }

    @PostMapping("searchDemoSAU")
    public Object searchDemoSAU(SearchDemo p) {
        searchDemoDao.saveAndFlush(p);
        return WebUtil.ok();
    }

    @PostMapping("deleteSearchDemo")
    public Object deleteSearchDemo(Integer id) {
        SearchDemo p = searchDemoDao.getOne(id);
        searchDemoDao.delete(p);
        return WebUtil.ok();
    }

    //=============设备管理===================
    @PostMapping("facilityList")
    public Object facilityList() {
        List<Facility> list = facilityDao.findAll();
        for (Facility f : list) {
            Goods goods = goodsD.getOne((f.getGoodsId()));
            Users user = UserD.getOne(f.getUserId());
            f.setGoods(goods);
            f.setUser(user);
        }
        return WebUtil.ok(list);
    }

    @PostMapping("facilitySAU")
    public Object facilitySAU(Facility f) {
        facilityDao.saveAndFlush(f);
        return WebUtil.ok();
    }

    @PostMapping("deleteFacility")
    public Object deleteFacility(Integer id) {
        Facility f = facilityDao.getOne(id);
        if ("Y".equals(f.getIsBind())) {
            f.setIsBind("N");
        } else if ("N".equals(f.getIsBind())) {
            f.setIsBind("Y");
        }
        facilityDao.saveAndFlush(f);
        return WebUtil.ok();
    }
    //=========介绍页面===================

    @PostMapping("referralList")
    public Object referralList() {
        //Referral r= referralDao.getOne(1);
        List<Referral> r = referralDao.findAll();
        //List<Referral> list =new ArrayList<Referral>();
        return WebUtil.ok(r);
    }

    @PostMapping("getReferral")
    public Object getReferral() {
        Referral r = referralDao.getOne(1);
        return WebUtil.ok(r);
    }

    @PostMapping("referralSAU")
    public Object referralSAU(Referral f) {
        referralDao.saveAndFlush(f);
        return WebUtil.ok();
    }

    @PostMapping("deleteReferral")
    public Object deleteReferral(Integer id) {
        Referral r = referralDao.getOne(id);
        referralDao.delete(r);
        return WebUtil.ok();
    }

    @PostMapping("useInfoList")
    public Object useInfolList() {
        //Referral r= referralDao.getOne(1);shearchDemo
        List<UseInfo> r = useInfoDao.findAll();
        //List<Referral> list =new ArrayList<Referral>();
        return WebUtil.ok(r);
    }

    @PostMapping("useInfo")
    public Object useInfo(UseInfo f) {
        useInfoDao.saveAndFlush(f);
        return WebUtil.ok();
    }

    @PostMapping("deleteUseInfo")
    public Object deleteUseInfo(Integer id) {

        UseInfo info = useInfoDao.getOne(id);
        useInfoDao.delete(info);
        return WebUtil.ok();
    }

    @PostMapping("shareInfoList")
    public Object shareInfoList() {
        //Referral r= referralDao.getOne(1);
        List<ShareInfo> r = shareInfoDao.findAll();
        //List<Referral> list =new ArrayList<Referral>();
        return WebUtil.ok(r);
    }

    @PostMapping("shareInfo")
    public Object shareInfo(ShareInfo f) {
        shareInfoDao.saveAndFlush(f);
        return WebUtil.ok();
    }

    @PostMapping("deleteShareInfo")
    public Object deleteShareInfo(Integer id) {
        ShareInfo s = shareInfoDao.getOne(id);
        shareInfoDao.delete(s);
        return WebUtil.ok();
    }

    @PostMapping("bindInfoList")
    public Object bindInfoList() {
        //Referral r= referralDao.getOne(1);
        List<BindInfo> r = bindInfoDao.findAll();
        //List<Referral> list =new ArrayList<Referral>();
        return WebUtil.ok(r);
    }

    @PostMapping("bindInfo")
    public Object useInfo(BindInfo f) {
        bindInfoDao.saveAndFlush(f);
        return WebUtil.ok();
    }

    @PostMapping("deleteBindInfo")
    public Object deleteBindInfo(Integer id) {
        BindInfo f = bindInfoDao.getOne(id);
        bindInfoDao.delete(f);
        return WebUtil.ok();
    }

    @PostMapping("awardRuleList")
    public Object awardRuleList() {
        List<AwardRule> r = awardInfoDao.findAll();
        return WebUtil.ok(r);
    }

    @PostMapping("awardRule")
    public Object awardRule(AwardRule f) {
        awardInfoDao.saveAndFlush(f);
        return WebUtil.ok();
    }

    @PostMapping("deleteAwardRule")
    public Object deleteAwardRule(Integer id) {
        AwardRule f = awardInfoDao.getOne(id);
        awardInfoDao.delete(f);
        return WebUtil.ok();
    }

    @RequestMapping("checkInfoList")
    public Object checkInfoList() {
        List<CheckInfo> r = checkInfoDao.findAll();
        return WebUtil.ok(r);
    }

    @RequestMapping("checkInfo")
    public Object checkInfo(CheckInfo f) {
        checkInfoDao.saveAndFlush(f);
        return WebUtil.ok();
    }

    @PostMapping("deleteCheckInfo")
    public Object deleteCheckInfo(Integer id) {
        CheckInfo f = checkInfoDao.getOne(id);
        checkInfoDao.delete(f);
        return WebUtil.ok();
    }

    @PostMapping("wayInfoList")
    public Object wayInfoList() {
        List<WayInfo> r = wayInfoDao.findAll();
        return WebUtil.ok(r);
    }

    @PostMapping("wayInfo")
    public Object wayInfo(WayInfo f) {
        wayInfoDao.saveAndFlush(f);
        return WebUtil.ok();
    }

    @PostMapping("deleteWayInfo")
    public Object deleteWayInfo(Integer id) {
        WayInfo f = wayInfoDao.getOne(id);
        wayInfoDao.delete(f);
        return WebUtil.ok();
    }


    @PostMapping("systemInfoList")
    public Object systemInfoList() {
        List<MjSystemInfo> r = systemInfoDao.findAll();
        return WebUtil.ok(r);
    }

    @PostMapping("soundInfoList")
    public Object soundInfoList() {
        List<MjOpenSound> r = soundDao.findAll();
        return WebUtil.ok(r);
    }

    @PostMapping("soundInfoListAll")
    public Object soundInfoListAll() {
        List<MjOpenSound> r = soundDao.findAll();
        return MsgUtil.ok(r);
    }

    @PostMapping("soundInfo")
    public Object soundInfo(MjOpenSound f) {
        soundDao.saveAndFlush(f);
        return WebUtil.ok();
    }

    @PostMapping("getSound")
    public Object getSound(Integer id) {
        MjOpenSound f = soundDao.getOne(id);
        return WebUtil.ok(f);
    }


    @RequestMapping("invitePwd")//邀请码生成
    public Object invitePwd(String openId, String uid, String tel) {
//	    System.out.println("openid:"+openId+" tel:"+tel);
        MjUser mjUser;
        if (StringUtils.isNotBlank(uid)) {
            mjUser = mjUserService.getById(uid);
        } else {
            mjUser = mjUserService.getUserByPhone(tel);
        }
        if (mjUser != null) {
            InvitePwd pwd = invitePwdDao.getByOpenId(mjUser.getId() + "");

            if (StringUtils.isNotBlank(openId)) {
                InvitePwd exist = invitePwdDao.getByUid(openId + "");

                if (exist != null && pwd == null && exist.getOpenId() == null) {
                    exist.setOpenId(uid);
                    invitePwdDao.saveAndFlush(exist);
                    return WebUtil.ok(exist.getInvitePwd());
                }
            }

            if (pwd == null) {
                String invitePwd = FunctionUtil.randomPwd();
                InvitePwd exist = invitePwdDao.getByInvitePwd(invitePwd);

                // todo 后期优化
                while (exist != null) {
                    invitePwd = FunctionUtil.randomPwd();
                    exist = invitePwdDao.getByInvitePwd(invitePwd);
                }

                InvitePwd pd = new InvitePwd();
                pd.setPhone(tel);
//                pd.setUid(openId);
                pd.setInvitePwd(invitePwd);
                pd.setOpenId(mjUser.getId() + "");
                invitePwdDao.saveAndFlush(pd);
                System.out.println("生成新的邀请码" + invitePwd);
                return WebUtil.ok(invitePwd);
            } else {
                if (!"".equals(tel)) {
                    pwd.setPhone(tel);
                    invitePwdDao.saveAndFlush(pwd);
                }
                System.out.println("生成原来的邀请码" + pwd.getInvitePwd());
                return WebUtil.ok(pwd.getInvitePwd());
            }
        } else {
            if (openId == null || StringUtils.isBlank(openId)) {
                return WebUtil.error("用户不存在");
            }
            InvitePwd pwd = invitePwdDao.getByUid(openId + "");

            if (pwd == null) {
                String invitePwd = FunctionUtil.randomPwd();
                InvitePwd exist = invitePwdDao.getByInvitePwd(invitePwd);

                // todo 后期优化
                while (exist != null) {
                    invitePwd = FunctionUtil.randomPwd();
                    exist = invitePwdDao.getByInvitePwd(invitePwd);
                }

                InvitePwd pd = new InvitePwd();
                pd.setUid(openId);
                pd.setInvitePwd(invitePwd);
                invitePwdDao.saveAndFlush(pd);
                System.out.println("生成新的邀请码" + invitePwd);
                return WebUtil.ok(invitePwd);
            } else {
                if (!"".equals(tel)) {
                    pwd.setPhone(tel);
                    invitePwdDao.saveAndFlush(pwd);
                }
                System.out.println("生成原来的邀请码" + pwd.getInvitePwd());
                return WebUtil.ok(pwd.getInvitePwd());
            }
        }

    }

    @PostMapping("queryInvitePwd")//查询邀请码
    public Object queryInvitePwd(String uid) {
        System.out.println("查看邀请码：" + uid);
        if (!StrUtil.hasEmpty(uid)) {
            InvitePwd pwd = invitePwdDao.getByOpenId(uid);
            return MsgUtil.ok(pwd);
        } else {
            return MsgUtil.badArgument();
        }

    }

    //联系客服
    @PostMapping("connect")//联系客服
    public Object connect() {
        Dictionaries dic = dicDao.getByKey("system_connect");
        if (dic != null) {
            if ("0".equals(dic.getDicValue())) {
                return MsgUtil.ok("0"); //显示
            } else if ("1".equals(dic.getDicValue())) {
                return MsgUtil.ok("1"); //不显示
            }
        } else {
            return MsgUtil.fail("-1");//未找到
        }
        return MsgUtil.ok("-1");

    }


    @PostMapping("uploadFile")
    public Object uploadFile(FileUpload file) {
        fileUploadDao.saveAndFlush(file);
        return WebUtil.ok(file);
    }

    @PostMapping("fileList")
    public Object fileList() {
        List<FileUpload> list = fileUploadDao.findAll();
        return WebUtil.ok(list);
    }


    @PostMapping("BookingGoodsList")
    public Object BookingGoodsList() {
        List<BookingGoods> r = bookingGoodsDao.findAll();
        return WebUtil.ok(r);
    }

    @PostMapping("deleteBookingGoods")
    public Object deleteBookingGoods(Integer id) {
        BookingGoods bg = bookingGoodsDao.getOne(id);
        bookingGoodsDao.delete(bg);
        return WebUtil.ok();
    }

    @PostMapping("BookingGoods")
    public Object BookingGoods(BookingGoods f) {
        bookingGoodsDao.saveAndFlush(f);
        return WebUtil.ok();
    }


    @PostMapping("HealthInfoList")
    public Object HealthInfoList() {
        List<HealthInfo> healthInfoList = healthInfoDao.findAll();
        return WebUtil.ok(healthInfoList);
    }

    @PostMapping("deleteHealthInfo")
    public Object deleteHealthInfo(Integer id) {
        HealthInfo h = healthInfoDao.getOne(id);
        healthInfoDao.delete(h);
        return WebUtil.ok();
    }

    @PostMapping("HealthInfo")
    public Object HealthInfo(HealthInfo f) {
        healthInfoDao.saveAndFlush(f);
        return WebUtil.ok();
    }

    @PostMapping("getOrderByTime")
    public Object getOrderByTime(String scIds, String startDate, String endDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date start = null;
        Date end = null;
        try {
            start = sdf.parse(startDate);
            end = sdf.parse(endDate);
        } catch (ParseException e) {
            e.getMessage();
        }
        List<Order> list = orderD.getOrderByTime(scIds, start, end);
        return MsgUtil.ok(list);
    }

    @PostMapping("getOrderByCode")
    public Object getOrderByTime(String scIds, String code) {
        List<Order> list = orderD.getOrderByCode(scIds, code);
        return MsgUtil.ok(list);
    }

    @PostMapping("/upload")
    @ResponseBody
    public String upload(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return "上传失败，请选择文件";
        }

        String fileName = file.getOriginalFilename();
        String filePath = "/Users/itinypocket/workspace/temp/";
        File dest = new File(filePath + fileName);
        try {
            file.transferTo(dest);
            return "上传成功";
        } catch (IOException e) {
            log.info("上传失败：" + e.getMessage());
        }
        return "上传失败！";
    }

}
