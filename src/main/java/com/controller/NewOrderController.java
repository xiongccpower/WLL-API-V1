package com.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.domain.Order;
import com.service.OrderService;
import com.wll.wulian.entity.base.R;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/order")
public class NewOrderController {

    @Resource
    private OrderService orderService;

    @PostMapping("orderPage")
    public R getOrderPage(Integer current,Integer pageSize){
        Page<Order> orderPage = new Page<>(current, pageSize);
        QueryWrapper<Order> qw = new QueryWrapper<>();
        qw.lambda().orderByDesc(Order::getCreateTime);
                    // .eq(Order::getScIds,"system");
        return R.ok(orderService.page(orderPage, qw));
    }

}
