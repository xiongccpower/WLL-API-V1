package com.controller;

import com.dao.*;
import com.entity.*;
import com.util.MsgUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("LifeScene")
@Api(value="生活场景",tags={"用户相关的操作"})
@Validated
@CrossOrigin
public class LifeSceneController {

	@Autowired private LifeSceneInfoDao dao;
	@Autowired private UnknwSceneUploadDao adao;
	@Autowired private ConvenientInfoDao cdao;

	private Random random = new Random();

	private AtomicLong sequenceId = new AtomicLong();

	private AtomicLong count = new AtomicLong();

	/** 列表  */ 
	@RequestMapping("getByCode")
    @ApiOperation(value="获取单个用户信息", notes="用openId获取单个用户信息")
    @ApiImplicitParams({
    	@ApiImplicitParam(name = "sceneCode", value = "sceneCode", required = true, dataType = "String")})
	public Object getByOpenId(String sceneCode) {
		List<LifeSceneInfo> list = dao.getBysceneCode(sceneCode);
		return MsgUtil.ok(list);
	}

	@RequestMapping("save")
	@ApiOperation(value="获取单个用户信息", notes="用openId获取单个用户信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "machId", value = "machId", required = true, dataType = "String"),
			@ApiImplicitParam(name = "addressInfo", value = "addressInfo", required = true, dataType = "String"),
			@ApiImplicitParam(name = "sceneInfo", value = "sceneInfo", required = true, dataType = "String"),
		})
	public Object save(String machId,String addressInfo,String sceneInfo) {
		UnknowSceneUpload info = new UnknowSceneUpload();
		info.setAddressInfo(addressInfo);
		info.setCreateTime(new Date());
		info.setMachId(machId);
		info.setSceneInfo(sceneInfo);
		adao.saveAndFlush(info);
		return MsgUtil.ok(info);
	}

	@RequestMapping("getConvenientInfo")
	@ApiOperation(value="获取单个用户信息", notes="用openId获取单个用户信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "area", value = "area", required = true, dataType = "String")})
	public Object getConvenientInfo(String area) {
		List<ConvenientInfo> list =cdao.getByArea(area);
		return MsgUtil.ok(list);
	}


	@RequestMapping("LooPollingServer")
	@ApiOperation(value="获取单个用户信息", notes="用openId获取单个用户信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "area", value = "area", required = true, dataType = "String")})
	public void LooPollingServer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		List<ConvenientInfo> list =cdao.getByArea(area);
//		return MsgUtil.ok(list);
		System.out.println("第" + (count.incrementAndGet()) + "次 longpolling服务端");

		int sleepSecends = 5;
//随机获取等待时间，来通过sleep模拟服务端是否准备好数据

		System.out.println("wait " + sleepSecends + " second,已准备好数据");

		try {
			TimeUnit.SECONDS.sleep(sleepSecends);//sleep
		} catch (InterruptedException e) {

		}

		PrintWriter out = response.getWriter();
		String msg = "返回数据11";
		msg = URLEncoder.encode(msg);
		out.write(msg);
	}

}
