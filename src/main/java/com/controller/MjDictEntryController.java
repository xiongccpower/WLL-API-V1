package com.controller;

import com.dao.MjDictDiretionDao;
import com.dao.MjDictEntryDao;
import com.entity.MjDictDiretion;
import com.entity.MjDictEntry;
import com.util.MsgUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("Dict")
@Validated
@Api(value="喜好选择",tags={"喜好选择"})
@CrossOrigin
public class MjDictEntryController {
    @Autowired private MjDictEntryDao dictEntryDao;
    @Autowired private MjDictDiretionDao dictDiretionDao;

    @RequestMapping("bigListAll")
    @ApiOperation(value="喜好大类列表", notes="喜好大类列表")
    public Object bigListAll() {
        Object result = new Object();

        List<MjDictEntry> list = dictEntryDao.findAll();
        if(list.size()>0 && list!=null){
            result = MsgUtil.ok(list);
        }else{
            result = MsgUtil.fail("暂无喜好数据");
        }
        return result;
    }
    @RequestMapping("smallList")
    @ApiOperation(value="喜好小类列表", notes="喜好小类列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dictEntry", value = "dictEntry", required = true, dataType = "String") })
    public Object smallList(String dictEntry) {
        Object result = new Object();

        List<MjDictDiretion> list = dictDiretionDao.getByDictName(dictEntry);
        if(list.size()>0 && list!=null){
            result = MsgUtil.ok(list);
        }else{
            result = MsgUtil.fail("暂无喜好数据");
        }

        return result;
    }



}
