package com.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.dao.OrderDao;
import com.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dao.GoodsDao;
import com.entity.Goods;
import com.util.MsgUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
@RestController
@RequestMapping("goods")
@Validated
@Api(value="商品类",tags={"商品类"})
@CrossOrigin
public class GoodsController {

	@Autowired private  GoodsDao dao;
	@Autowired private  OrderDao orderDao;

	@RequestMapping("list")
    @ApiOperation(value="获取全部的商品", notes="获取全部的商品")
    @ApiImplicitParams({
    @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer")})
	public Object list(String openId) {
		List<Goods> list = dao.findAll();
		return MsgUtil.ok(list);
	}
	
	@RequestMapping("getById")
    @ApiOperation(value="获取单个商品", notes="获取单个商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "goodsId", value = "商品Id", required = true, dataType = "Integer")
    })
	public Object getById(String openId,Integer goodsId) {
		Optional<Goods> obj = dao.findById(goodsId);

		return MsgUtil.ok(obj);
	}
    
	@RequestMapping("save")
	public Object save() {
		Goods goods = new Goods();
		List<String>list=new ArrayList<String>();
		list.add("/image/test.png");
		list.add("/image/test1.png");
		goods.setPicUrl(list);
		goods.setPrice(100.0);
		goods.setTitle("测试1号商品");
		dao.save(goods);
		return MsgUtil.ok();
	}
	@RequestMapping("getComment")
	@ApiOperation(value="获取单个商品", notes="获取单个商品")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
			@ApiImplicitParam(name = "goodsId", value = "商品Id", required = true, dataType = "String")
	})
	public Object getComment(String openId,String goodsId) {
		//Optional<Goods> obj = dao.findById(goodsId);
		//
		goodsId =goodsId+",";
		List<Order> orderList = orderDao.getByGoodsIds(goodsId);
		List<String> commnetList  = new ArrayList<String>();
		for(Order order:orderList){
			commnetList.add(order.getEvaluate());
		}
		return MsgUtil.ok(commnetList);
	}
}
