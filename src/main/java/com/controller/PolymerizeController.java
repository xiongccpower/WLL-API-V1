package com.controller;

import com.polymerizeAPIUtil.PolymerizeAPI;
import com.util.MsgUtil;
import com.util.ToolUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("polymerize/api")
@Validated
@Api(value="聚合接口调用类",tags={"聚合接口调用类"})
@CrossOrigin
public class PolymerizeController {

    @RequestMapping("get/constellation")
    @ApiOperation(value="查询星座运势", notes="查询星座运势")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "consName", value = "星座名称", required = true, dataType = "String"),
    })
    public Object constellationGetAll(String consName){
        System.out.println("请求参数="+consName);
        Object result = new Object();
        if(ToolUtil.isEmpty(consName)){
            result = MsgUtil.ok("小主，管家未能明白您的意思");
        }else{
           String constellation = PolymerizeAPI.constellationGetAll(consName);
           if(constellation.equals("fail")){
               result = MsgUtil.ok("小主，管家未能明白您的意思");
           }else{
               result = MsgUtil.ok(constellation);
           }
        }
        return result;
    }

}
