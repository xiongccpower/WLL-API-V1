package com.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.domain.SysProperties;
import com.service.SysPropertiesService;
import com.wll.wulian.entity.base.R;
import com.wll.wulian.utils.BasePage;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * created in 2022/3/7 17:34
 *
 * @author zhuxuelei
 */
@RestController
@RequestMapping("properties")
@CrossOrigin
public class PropertiesController {

    @Resource
    private SysPropertiesService sysPropertiesService;

    @GetMapping("get")
    public R getProperty(Long mspId) {
        SysProperties property = sysPropertiesService.getById(mspId);
        return R.ok().put("data", property);
    }

    @GetMapping("list")
    public R listProperty(BasePage page) {
        IPage<SysProperties> iPage = sysPropertiesService.page(page.getPage(SysProperties.class));
        return R.ok(iPage);
    }

    @PostMapping("save")
    public R saveProperty(@RequestBody SysProperties properties) {
        properties.setUpdateTime(LocalDateTime.now());
        sysPropertiesService.saveOrUpdate(properties);
        return R.ok().put("mspId", properties.getMspId());
    }
}
