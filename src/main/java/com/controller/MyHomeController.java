package com.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.butlerApi.ButlerApiUtil;
import com.dao.*;
import com.entity.*;
import com.service.ButlerDataTransferService;
import com.service.MjUserService;
import com.util.FunctionUtil;
import com.util.MsgUtil;
import com.util.ToolUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("home")
@Validated
@Api(value = "家庭数据", tags = {"家庭数据"})
@CrossOrigin
public class MyHomeController {
    protected static Logger log = LoggerFactory.getLogger(MyHomeController.class);

    @Autowired
    private UserDao userDao;
    @Autowired
    private MyHomeDao dao;
    @Autowired
    private MyHobbyDao hobbyDao;
    @Autowired
    private MyDataDao dataDao;
    @Autowired
    private MyBooldSweetDataDao booldSweetDataDao;
    @Autowired
    private MyHeartTimesDataDao heartTimesDataDao;
    @Autowired
    private MyBooldHearDataDao booldHearDataDao;
    @Autowired
    private MyBooldOxDataDao booldOxDataDao;
    @Autowired
    private MjUserService mjUserService;
    @Autowired
    private ButlerDataTransferService butlerDataTransferService;
    @Autowired
    private MjDictDiretionDao mjDictDiretionDao;

    @RequestMapping("list")
    @ApiOperation(value = "家庭成员数据列表", notes = "家庭成员数据列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
            @ApiImplicitParam(name = "memberId", value = "家庭成员id", required = true, dataType = "String"),
    })
    public Object list(String tel, String uid, String memberId) {
        log.info("家庭成员数据列表");
        Object result = new Object();


        if (!StrUtil.hasEmpty(tel) || !StrUtil.hasEmpty(uid)) {
            MjUser mjUser = new MjUser();

            if (!StrUtil.hasEmpty(tel)) {
                mjUser = mjUserService.getUserByPhone(tel);
            }

            if (!StrUtil.hasEmpty(uid)) {
                mjUser = mjUserService.getById(uid);
            }

            if (mjUser != null) {
                Map<String, Object> macId = ButlerApiUtil.queryDeviceBinding(uid);

                if (macId.get("code").equals("200")) {
                    String macIds[] = macId.get("macIds").toString().split(",");

                    Map<String, Object> userFamilyDetail = new HashMap<>();
                    List dataList = new ArrayList();

                    if (!StrUtil.hasEmpty(memberId)) {//子账号进入

                        userFamilyDetail = ButlerApiUtil.queryFamilyMemberInfo(macIds[0], memberId, uid);

                        if (userFamilyDetail.get("code").equals("200")) {
                            cn.hutool.json.JSONObject dataArray = JSONUtil.parseObj(userFamilyDetail.get("data"));

                            Map<String, String> map = new HashMap<>();

                            String booldSweetData = "暂无";
                            String heartTimesData = "暂无";
                            String booldHearData = "暂无";
                            String booldOxData = "暂无";

                            if (dataArray.containsKey("healthData")) {
                                String healthDatas = dataArray.get("healthData").toString();
                                System.out.println("healthData数据：" + StrUtil.hasEmpty(healthDatas));
                                if (!StrUtil.hasEmpty(healthDatas) && !healthDatas.equals("[]") && !healthDatas.equals("null")) {
                                    cn.hutool.json.JSONArray healthData = dataArray.getJSONArray("healthData");
                                    if (healthData.size() > 0 && healthData != null) {
                                        for (int j = 0; j < healthData.size(); j++) {
                                            int type = healthData.getJSONObject(j).getInt("type");
                                            if (type == 0) {
                                                heartTimesData = healthData.getJSONObject(j).get("heartbeat") + "次/分钟";
                                            }
                                            if (type == 6) {
                                                booldHearData = healthData.getJSONObject(j).get("otherValue") + "";
                                                String gy = booldHearData.substring(0, booldHearData.indexOf("~") - 4);
                                                String dy = booldHearData.substring(booldHearData.indexOf("~") + 1, booldHearData.length());
                                                booldHearData = gy + "/" + dy;
                                            }
                                            if (type == 4) {
                                                booldSweetData = healthData.getJSONObject(j).get("otherValue") + "mmol/l";
                                            }
                                            if (type == 5) {
                                                booldOxData = healthData.getJSONObject(j).get("otherValue") + "%";
                                            }
                                        }
                                    }

                                }

                            }
                            map.put("tel", dataArray.get("mobile") + "");
                            map.put("sex", dataArray.get("sexName") + "");
                            map.put("nickName", dataArray.get("nickname") + "");
                            map.put("relation", dataArray.get("familyRoleName") + "");
                            map.put("booldSweetData", booldSweetData);
                            map.put("heartTimesData", heartTimesData);
                            map.put("booldHearData", booldHearData);
                            map.put("booldOxData", booldOxData);
                            map.put("id", dataArray.get("memberId") + "");
                            dataList.add(map);


                            result = MsgUtil.ok(dataList);


                        } else {
                            result = MsgUtil.ok(userFamilyDetail.get("msg") + "");
                        }


                    } else {//主账号进入
                        userFamilyDetail = ButlerApiUtil.queryUserFamilyDetail(uid, macIds[0]);

                        if (userFamilyDetail.get("code").equals("200")) {
                            JSONArray dataArray = JSONUtil.parseArray(userFamilyDetail.get("data"));

                            for (int i = 0; i < dataArray.size(); i++) {
                                Map<String, String> map = new HashMap<>();

                                String booldSweetData = "暂无";
                                String heartTimesData = "暂无";
                                String booldHearData = "暂无";
                                String booldOxData = "暂无";

                                if (dataArray.getJSONObject(i).containsKey("healthData")) {
                                    String healthDatas = dataArray.getJSONObject(i).get("healthData").toString();
                                    System.out.println("healthData数据：" + StrUtil.hasEmpty(healthDatas));
                                    if (!StrUtil.hasEmpty(healthDatas) && !healthDatas.equals("[]") && !healthDatas.equals("null")) {
                                        cn.hutool.json.JSONArray healthData = dataArray.getJSONObject(i).getJSONArray("healthData");
                                        if (healthData.size() > 0 && healthData != null) {
                                            for (int j = 0; j < healthData.size(); j++) {
                                                int type = healthData.getJSONObject(j).getInt("type");
                                                if (type == 0) {
                                                    heartTimesData = healthData.getJSONObject(j).get("heartbeat") + "次/分钟";
                                                }
                                                if (type == 6) {
                                                    booldHearData = healthData.getJSONObject(j).get("otherValue") + "";
                                                    String gy = booldHearData.substring(0, booldHearData.indexOf("~") - 4);
                                                    String dy = booldHearData.substring(booldHearData.indexOf("~") + 1, booldHearData.length());
                                                    booldHearData = gy + "/" + dy;
                                                }
                                                if (type == 4) {
                                                    booldSweetData = healthData.getJSONObject(j).get("otherValue") + "mmol/l";
                                                }
                                                if (type == 5) {
                                                    booldOxData = healthData.getJSONObject(j).get("otherValue") + "%";
                                                }
                                            }
                                        }

                                    }

                                }
                                map.put("tel", dataArray.getJSONObject(i).get("mobile") + "");
                                map.put("sex", dataArray.getJSONObject(i).get("sexName") + "");
                                map.put("nickName", dataArray.getJSONObject(i).get("nickname") + "");
                                map.put("relation", dataArray.getJSONObject(i).get("familyRoleName") + "");
                                map.put("booldSweetData", booldSweetData);
                                map.put("heartTimesData", heartTimesData);
                                map.put("booldHearData", booldHearData);
                                map.put("booldOxData", booldOxData);
                                map.put("id", dataArray.getJSONObject(i).get("memberId") + "");
                                dataList.add(map);

                            }


                            result = MsgUtil.ok(dataList);


                        } else {
                            result = MsgUtil.ok(userFamilyDetail.get("msg") + "");
                        }
                    }


                } else {
                    result = MsgUtil.ok("请绑定未来里管家");
                }

            } else {
                result = MsgUtil.fail("用户不存在");
            }


//            List<MyHome> homeList = dao.getByMainTel(tel);
//            if(homeList!=null && homeList.size()>0){
//                List dataList = new ArrayList();
//
//                String booldSweetData = null;
//                String heartTimesData = null;
//                String booldHearData = null;
//                String booldOxData = null;
//
//                for(int i =0;i< homeList.size();i++){
//                    Map map = new HashMap();
//                    Users user = userDao.getByTel(homeList.get(i).getTel());
//                    if(booldSweetDataDao.getByUserId(user.getId()).size()==0){
//                        booldSweetData = "暂无";
//                    }else{
//                        booldSweetData =booldSweetDataDao.getByUserId(user.getId()).get(0).getContent();
//                    };//血糖
//                    if(heartTimesDataDao.getByUserId(user.getId()).size()==0){
//                        heartTimesData = "暂无";
//                    }else{
//                        heartTimesData = heartTimesDataDao.getByUserId(user.getId()).get(0).getContent();
//                    }; //心率
//                    if(booldHearDataDao.getByUserId(user.getId()).size()==0){
//                        booldHearData = "暂无";
//                    }else{
//                        booldHearData = booldHearDataDao.getByUserId(user.getId()).get(0).getContent();
//                    };//血脂
//                    if(booldOxDataDao.getByUserId(user.getId()).size()==0){
//                        booldOxData = "暂无";
//                    }else{
//                        booldOxData = booldOxDataDao.getByUserId(user.getId()).get(0).getContent();
//                    };//血氧
//                    map.put("tel",homeList.get(i).getTel());
//                    map.put("sex",homeList.get(i).getSex());
//                    map.put("nickName",homeList.get(i).getNickname());
//                    map.put("relation",homeList.get(i).getRelation());
//                    map.put("booldSweetData",booldSweetData);
//                    map.put("heartTimesData",heartTimesData);
//                    map.put("booldHearData", booldHearData);
//                    map.put("booldOxData",booldOxData);
//                    map.put("id",homeList.get(i).getId());
//                    dataList.add(map);
//                }
//            }else{
//                result = MsgUtil.fail("此用户不存在");
//            }


        } else {
            result = MsgUtil.badArgument();
        }

        return result;
    }

    @RequestMapping("relationList")
    @ApiOperation(value = "家庭成员数据列表", notes = "家庭成员数据列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String")})
    public Object relationList(String openId) {
        if (!StrUtil.hasEmpty(openId)) {
            Users user = userDao.getByOpenId(openId);
            if (user != null) {
                List<MyHome> list = dao.getByMainTel(user.getPhone());
                List relationList = new ArrayList();
                for (int i = 0; i < list.size(); i++) {
                    relationList.add(list.get(i).getRelation());
                }
                return MsgUtil.ok(relationList);
            } else {
                return MsgUtil.fail("用户不存在");
            }
        } else {
            return MsgUtil.badArgument();
        }

    }

    @RequestMapping("relationListForSale")
    @ApiOperation(value = "家庭成员数据列表", notes = "家庭成员数据列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
    })
    public Object relationListForSale(String tel, String uid) {

        List relationList = new ArrayList();
        relationList.add("全家通用");
        if (ToolUtil.isEmpty(uid)) {
            MjUser mjUser = mjUserService.getUserByPhone(tel);
            if (mjUser != null) {
                uid = mjUser.getId() + "";
            }
        }


        Map<String, Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
        if (macIdMap.get("code").equals("200")) {
            String macId = macIdMap.get("macIds") + "";
            String macIds[] = macId.split(",");
            Map<String, Object> mapFamily = ButlerApiUtil.queryUserFamilyDetail(uid, macIds[0]);
            if (mapFamily.get("code").equals("200")) {
                JSONArray dataArr = JSONUtil.parseArray(mapFamily.get("data"));
                if (dataArr.size() > 0 && dataArr != null) {
                    for (int i = 0; i < dataArr.size(); i++) {
                        relationList.add(dataArr.getJSONObject(i).get("familyRoleName"));
                    }
                }
            }
        }
//        relationList.add("其他");

        return MsgUtil.ok(relationList);


    }


    @RequestMapping("update")
    @ApiOperation(value = "修改查询家庭成员数据", notes = "修改查询家庭成员数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
    })
    public Object update(String uid, String id) {

        log.info("修改查询家庭成员数据");

        if (!StrUtil.hasEmpty(uid) && !StrUtil.hasEmpty(id)) {
            MjUser user = mjUserService.getById(uid);
            if (user != null) {
                Map<String, Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
                if (macIdMap.get("code").equals("200")) {
                    String macId = macIdMap.get("macIds") + "";
                    String macIds[] = macId.split(",");
                    Map<String, Object> mapFamily = ButlerApiUtil.queryFamilyMemberInfo(macIds[0], id, uid);
                    if (mapFamily.get("code").equals("200")) {
                        if (!StrUtil.hasEmpty(mapFamily.get("data") + "")) {
                            cn.hutool.json.JSONObject json = JSONUtil.parseObj(mapFamily.get("data"));
                            MyHome home = new MyHome();
                            home.setUid(uid);
                            home.setBirth(json.get("birthday") + "");
                            home.setNickname(json.get("nickname") + "");
                            home.setRelation(json.get("familyRoleName") + "");
                            home.setSex(json.get("sexName") + "");
                            home.setTel(json.get("mobile") + "");
                            home.setId(json.getInt("memberId"));
                            home.setLike(json.get("like") + "");
                            return MsgUtil.ok(home);

                        } else {
                            return MsgUtil.fail("暂无家庭成员数据");
                        }
                    } else {
                        return MsgUtil.fail(mapFamily.get("msg") + "");
                    }

                } else {
                    return MsgUtil.fail(macIdMap.get("msg") + "");
                }
            } else {
                return MsgUtil.fail("用户不存在");
            }

        } else {
            return MsgUtil.badArgument();

        }

    }

    @RequestMapping(value = "save")
    @ApiOperation(value = "添加家庭成员数据", notes = "添加家庭成员数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mainTel", value = "mainTel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "nickname", value = "nickname", required = true, dataType = "String"),
            @ApiImplicitParam(name = "relation", value = "relation", required = true, dataType = "String"),
            @ApiImplicitParam(name = "sex", value = "sex", required = true, dataType = "String"),
            @ApiImplicitParam(name = "like", value = "like", required = true, dataType = "String"),
            @ApiImplicitParam(name = "birth", value = "birth", required = true, dataType = "String"),})
    public Object save(String mainTel, String tel, String nickname, String relation, String sex, String birth, String like) {
        log.info("添加家庭成员数据");
        Object result = new Object();

        if (!StrUtil.hasEmpty(tel) && !StrUtil.hasEmpty(nickname) && !StrUtil.hasEmpty(sex)) {
            MjUser mjUser = mjUserService.getUserByPhone(mainTel);
            if (mjUser != null) {
                MyHome home = new MyHome();

                home.setMainTel(mainTel);
                home.setTel(tel);
                home.setRelation(relation);
                home.setNickname(nickname);
                home.setSex(sex);
                home.setBirth(birth);
                home.setUid(mjUser.getId() + "");
                home.setLike(like);

                Users user = new Users();
                //Users mainUser = userDao.getByTel(tel);


                //查询绑定的设备获取设备macId
                Map<String, Object> deviceBinding = ButlerApiUtil.queryDeviceBinding(mjUser.getId() + "");

                if (deviceBinding.get("code").equals("200")) {
                    String macIds = deviceBinding.get("macIds") + "";

                    String checkCode = "";
//                    if(mainUser==null){
                    String pwd = tel.substring(tel.length() - 4, tel.length()) + new SimpleDateFormat("sSSS").format(new Date());

                    home.setCheckCode(tel);
                    user.setPhone(tel);
                    user.setIsBuySystem("Y");
                    user.setIsBuyGoods("N");
                    user.setMj_pwd(pwd);
                    user.setSex(sex);
                    userDao.saveAndFlush(user);

                    JSONObject obj = FunctionUtil.userRegister("", "", "", "", "", tel, pwd, "", "中国", "", "", mjUser.getId() + ""); //注册美捷生活小程序
                    if ("true".equals(obj.getString("success"))) {
                        cn.hutool.json.JSONObject data = JSONUtil.parseObj(obj.get("data"));
                        home.setId(data.getInt("id"));
                        JSONObject jo = FunctionUtil.senderUserPwd(tel, pwd, mainTel); //发送密码
                        if ("false".equals(jo.getString("success"))) {
                            return MsgUtil.fail("发送失败");
                            //短信发送失败
                        } else {
                            //将家庭成员信息保存到管家本地
                            ButlerApiUtil.addFamilyMemberApi(home, macIds);
                        }
                    }
//                    }else {
//
//                        //将家庭成员信息保存到管家本地
//                        ButlerApiUtil.addFamilyMemberApi(home,macIds);
//
//                    }

                    result = MsgUtil.ok(home);

                } else {
                    result = MsgUtil.fail("请先绑定管家");
                }

            } else {
                result = MsgUtil.fail("用户不存在");
            }


        } else {
            result = MsgUtil.badArgument();
        }


        return result;
    }


    @RequestMapping("updateSave")
    @ApiOperation(value = "修改家庭成员数据", notes = "修改家庭成员数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mainTel", value = "mainTel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "nickname", value = "nickname", required = true, dataType = "String"),
            @ApiImplicitParam(name = "relation", value = "relation", required = true, dataType = "String"),
            @ApiImplicitParam(name = "sex", value = "sex", required = true, dataType = "String"),
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
            @ApiImplicitParam(name = "like", value = "like", required = true, dataType = "String"),
            @ApiImplicitParam(name = "birth", value = "birth", required = true, dataType = "String"),})
    public Object updateSave(String mainTel, String tel, String nickname, String relation, String sex, String birth, String id, String like, String uid) {
        log.info("进入修改家庭成员功能");

        //查询绑定的设备获取设备macId
        Map<String, Object> deviceBinding = ButlerApiUtil.queryDeviceBinding(uid);
        if (deviceBinding.get("code").equals("200")) {
            MyHome home = new MyHome();
            home.setTel(tel);
            home.setSex(sex);
            home.setRelation(relation);
            home.setNickname(nickname);
            home.setBirth(birth);
            home.setUid(uid);
            home.setId(Integer.parseInt(id));
            home.setLike(like);

            //修改用户家庭成员信息
            ButlerApiUtil.updateFamilyMember(home, deviceBinding.get("macIds") + "");
            return MsgUtil.ok(home);

        } else {
            return MsgUtil.fail(deviceBinding.get("msg") + "");
        }


//        MyHome home  =dao.getByTel(tel);
//        MyHome home  = new MyHome();
//
//        if(!StrUtil.hasEmpty(id)){
//            home  =dao.getById(Integer.parseInt(id));
//        }else{
//            home  =dao.getByTel(tel);
//        }
//
//        if(home!=null){
//            String phone = home.getTel();
//
//            home.setNickname(nickname);
//            home.setRelation(relation);
//            home.setSex(sex);
//            home.setTel(tel);
//            home.setBirth(birth);
//
//
//
//            dao.saveAndFlush(home);
//            return MsgUtil.ok(home);
//        }else{
//            return MsgUtil.fail("修改用户不存在");
//        }


//		Users user = new Users();
//		Users mainUser = userDao.getByTel(tel);
//		String checkCode ="";
//		if(mainUser==null){
//			String pwd=tel.substring(tel.length()-4,tel.length())+new SimpleDateFormat("sSSS").format(new Date());
//			home.setMainTel(mainTel);
//			home.setTel(tel);
//			home.setRelation(relation);
//			home.setNickname(nickname);
//			home.setSex(sex);
//			home.setBirth(birth);
//			home.setHeight(height);
//			home.setWeight(weight);
//			home.setCheckCode(tel);
//			user.setPhone(tel);
//			user.setIsBuySystem("Y");
//			user.setIsBuyGoods("N");
//			user.setMj_pwd(pwd);
//			user.setSex(sex);
//			userDao.saveAndFlush(user);
//			dao.saveAndFlush(home);
//			JSONObject obj = FunctionUtil.userRegister("","","","","",tel,pwd,"","中国","",""); //注册美捷生活小程序
//			if("true".equals(obj.getString("success"))){
//				JSONObject jo = FunctionUtil.senderUserPwd(tel,pwd); //发送密码
//				if("false".equals(jo.getString("success"))){
//					return MsgUtil.fail("发送失败");
//					//短信发送失败
//				}
//			}
//		}else {
//			MyHome list = dao.getByTel(tel);
//			if(list!=null){
//				checkCode="ran"+new SimpleDateFormat("sSSS").format(new Date());
//				String pwd=new SimpleDateFormat("sSSS").format(new Date());
//				user.setPhone(checkCode);
//				user.setIsBuySystem("Y");
//				user.setIsBuyGoods("N");
//				user.setMj_pwd(pwd);
//				user.setSex(sex);
//				userDao.saveAndFlush(user);
//
//				home.setMainTel(mainTel);
//				home.setTel(checkCode);
//				home.setCheckCode(checkCode);
//				home.setRelation(relation);
//				home.setNickname(nickname);
//				home.setSex(sex);
//				home.setBirth(birth);
//				home.setHeight(height);
//				home.setWeight(weight);
//				//user.setPhone(tel);
//				dao.saveAndFlush(home);
//			}else{
//				home.setMainTel(mainTel);
//				home.setTel(tel);
//				home.setCheckCode(tel);
//				home.setRelation(relation);
//				home.setNickname(nickname);
//				home.setSex(sex);
//				home.setBirth(birth);
//				home.setHeight(height);
//				home.setWeight(weight);
//				//user.setPhone(tel);
//				dao.saveAndFlush(home);
//			}
//
//		}


    }


    @RequestMapping("delete")
    @ApiOperation(value = "修改家庭成员数据", notes = "修改家庭成员数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "MyHome", value = "id sex 性别 ,height 身高,weight 体重,love 喜好,birth 出生年月", required = true, dataType = "Integer")})
    public Object delete(String openId, MyHome home) {

        Users user = userDao.getByOpenId(openId);
        home.setMainTel(user.getPhone());
        dao.delete(home);
        return MsgUtil.ok();
    }


    @RequestMapping("addHobby")
    @ApiOperation(value = "处理喜好接口", notes = "处理喜好接口")
    @ApiImplicitParams({
//			@ApiImplicitParam(name = "tel", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "MyHobby", value = "id sex 性别 ,height 身高,weight 体重,love 喜好,birth 出生年月", required = true, dataType = "Integer")})
    public Object addHobby(String likeArray) {
        Object result = new Object();

        if (!StrUtil.hasEmpty(likeArray)) {
//            likeArray = " ";
            Map mapTypes = JSON.parseObject(likeArray);
            String data = "";

            List<MyHobby> hobbyList = new ArrayList<>();

            for (Object obj : mapTypes.keySet()) {
                System.out.println("key为：" + obj + "值为：" + mapTypes.get(obj));
                String daXh = obj.toString();
                String xXh = "";
                JSONArray jsonArray = JSONUtil.parseArray(mapTypes.get(obj));
                MyHobby myHobby = new MyHobby();
                myHobby.setDictValue(daXh);

                if (jsonArray.size() > 0 && jsonArray != null) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        xXh += jsonArray.get(i).toString() + ",";
                        MjDictDiretion mjDictDiretion = mjDictDiretionDao.getMjDictDiretion(daXh, jsonArray.get(i).toString());
                        if (mjDictDiretion != null) {
                            data += mjDictDiretion.getDictName() + ",";
                        }
                    }
                    myHobby.setEntryValue(xXh.substring(0, xXh.length() - 1));
                }
                hobbyList.add(myHobby);
            }
            if (!StrUtil.hasEmpty(data) && hobbyList.size() > 0) {
                Map<String, Object> map = new HashMap<>();
                map.put("data", data);
                map.put("hobbyList", hobbyList);
                result = MsgUtil.ok(map);
            } else {
                result = MsgUtil.ok("暂无喜好数据");
            }
        } else {
            result = MsgUtil.ok("请选择喜好数据");
        }

        return result;
    }

    @RequestMapping("queryHobby")
    @ApiOperation(value = "获取之前选择的喜好", notes = "获取之前选择的喜好")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String")})
    public Object queryHobby(String id, String uid) {
        Object result = new Object();

        List<MyHobby> hobbyList = new ArrayList<>();
        Map<String, Object> macId = ButlerApiUtil.queryDeviceBinding(uid);
        if (macId.get("code").equals("200")) {
            String macIds[] = macId.get("macIds").toString().split(",");
            Map<String, Object> userFamilyDetail = ButlerApiUtil.queryFamilyMemberInfo(macIds[0], id, uid);
            if (userFamilyDetail.get("code").equals("200")) {
                cn.hutool.json.JSONObject data = JSONUtil.parseObj(userFamilyDetail.get("data"));
                if (!StrUtil.hasEmpty(data.get("like") + "")) {
                    cn.hutool.json.JSONObject xhData = JSONUtil.parseObj(data.get("like") + "");
                    cn.hutool.json.JSONArray hobbyLists = JSONUtil.parseArray(xhData.get("hobbyList").toString());
                    for (int i = 0; i < hobbyLists.size(); i++) {
                        cn.hutool.json.JSONObject hoJson = JSONUtil.parseObj(hobbyLists.get(i));
                        MyHobby myHobby = new MyHobby();
                        myHobby.setDictValue(hoJson.get("dictValue") + "");
                        myHobby.setEntryValue(hoJson.get("entryValue") + "");
                        hobbyList.add(myHobby);
                    }
                }
                result = MsgUtil.ok(hobbyList);

            } else {
                result = MsgUtil.ok(userFamilyDetail.get("msg"));
            }
        } else {
            result = MsgUtil.ok(macId.get("msg"));
        }


        return result;
    }


    @RequestMapping("queryHobbyWithRelation")
    @ApiOperation(value = "修改家庭成员数据", notes = "修改家庭成员数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String")})
    public Object queryHobbyWithRelation(String uid) {
        Object result = new Object();

        Map<String, Object> macId = ButlerApiUtil.queryDeviceBinding(uid);
        if (macId.get("code").equals("200")) {
            String macIds[] = macId.get("macIds").toString().split(",");
            List dataList = new ArrayList();
            Map<String, Object> userFamilyDetail = ButlerApiUtil.queryUserFamilyDetail(uid, macIds[0]);
            if (userFamilyDetail.get("code").equals("200")) {
                JSONArray dataArr = JSONUtil.parseArray(userFamilyDetail.get("data"));
                List<MyHobby> hobbyList = new ArrayList<>();
                if (dataArr.size() > 0 && dataArr != null) {
                    for (int i = 0; i < dataArr.size(); i++) {
                        Map map = new HashMap();
                        map.put("relation", dataArr.getJSONObject(i).get("familyRoleName"));
                        map.put("tel", dataArr.getJSONObject(i).get("mobile"));
                        map.put("id", dataArr.getJSONObject(i).get("memberId"));
                        if (!StrUtil.hasEmpty(dataArr.getJSONObject(i).get("like") + "")) {
                            cn.hutool.json.JSONObject like = JSONUtil.parseObj(dataArr.getJSONObject(i).get("like").toString());
                            JSONArray hobbyArr = JSONUtil.parseArray(like.get("hobbyList"));
                            for (int j = 0; j < hobbyArr.size(); j++) {
                                MyHobby myHobby = new MyHobby();
                                myHobby.setDictValue(hobbyArr.getJSONObject(j).get("dictValue") + "");
                                myHobby.setEntryValue(hobbyArr.getJSONObject(j).get("entryValue") + "");
                                hobbyList.add(myHobby);
                            }
                        }
                        map.put("hobbyList", hobbyList);
                        dataList.add(map);

//                        userFamilyDetail.add(dataArr.getJSONObject(i).get("familyRoleName"));
                    }
                }
                result = MsgUtil.ok(dataList);
            } else {
                result = MsgUtil.ok("请绑录入家庭成员信息");
            }

        } else {
            result = MsgUtil.ok("请绑定未来里管家");
        }


//        List<MyHome> homeList = dao.getByMainTel(tel);
//        List dataList = new ArrayList();
//        for(int i =0;i<homeList.size();i++){
//            Map map  = new HashMap();
//            map.put("relation", homeList.get(i).getRelation());
//            map.put("tel", homeList.get(i).getTel());
//            List<MyHobby> hobbyList = hobbyDao.getByTel(homeList.get(i).getTel());
//            map.put("hobbyList",hobbyList);
//            dataList.add(map);
//
//        }

//        return MsgUtil.ok(dataList);
        return result;
    }


    @RequestMapping("deleteHomeInfo")
    @ApiOperation(value = "删除家庭成员", notes = "删除家庭成员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "openId", required = true, dataType = "String")})
    public Object deleteHomeInfo(String tel) {
        MyHome home = dao.getByTel(tel);
        dao.delete(home);
        return MsgUtil.ok();
    }

    @RequestMapping("getDataTransfer")
    @ApiOperation(value = "查询有无数据备份", notes = "查询有无数据备份")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String")})
    public Object getDataTransfer(String uid) {
        if (!StrUtil.hasEmpty(uid)) {
            ButlerDataTransfer butlerDataTransfer = butlerDataTransferService.getUid(uid);
            if (butlerDataTransfer != null) {
                return MsgUtil.ok(butlerDataTransfer);
            } else {
                return MsgUtil.fail();
            }
        } else {
            return MsgUtil.badArgument();
        }

    }

    @RequestMapping("triggerDataBack")
    @ApiOperation(value = "管家设备备份", notes = "管家设备备份")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String")})
    public Object triggerDataBack(String uid) {
        if (!ToolUtil.isEmpty(uid)) {

            Map<String, Object> macId = ButlerApiUtil.queryDeviceBinding(uid);
            if (macId.get("code").equals("200")) {
                String macIds[] = macId.get("macIds").toString().split(",");
                Map<String, Object> back = ButlerApiUtil.triggerDataBack(macIds[0]);
                if (back.get("code").equals("200")) {
                    ButlerDataTransfer but = butlerDataTransferService.getUid(uid);
                    if (but == null) {
                        ButlerDataTransfer butlerDataTransfer = new ButlerDataTransfer();
                        butlerDataTransfer.setId(IdUtil.objectId());
                        butlerDataTransfer.setUid(uid);
                        butlerDataTransfer.setStates(0);
                        butlerDataTransfer.setBackupsTime(DateUtil.now());
                        butlerDataTransferService.save(butlerDataTransfer);
                    }

                    return MsgUtil.ok();
                } else {
                    return MsgUtil.fail(back.get("msg") + "");
                }

            } else {
                return MsgUtil.fail(macId.get("msg") + "");
            }

        } else {
            return MsgUtil.badArgument();
        }
    }

    @RequestMapping("triggerDataRecover")
    @ApiOperation(value = "管家数据恢复", notes = "管家数据恢复")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
            @ApiImplicitParam(name = "butlerId", value = "butlerId", required = true, dataType = "String"),
    })
    public Object triggerDataRecover(String uid, String butlerId) {
        if (!StrUtil.hasEmpty(uid) && !StrUtil.hasEmpty(butlerId)) {

            Map<String, Object> macId = ButlerApiUtil.queryDeviceBinding(uid);
            if (macId.get("code").equals("200")) {
                String macIds[] = macId.get("macIds").toString().split(",");
                Map<String, Object> back = new HashMap<>();
                for (String maId : macIds) {
                    back = ButlerApiUtil.triggerDataRecover(maId);
                }

                if (back.get("code").equals("200")) {
                    ButlerDataTransfer butlerDataTransfer = butlerDataTransferService.getById(butlerId);
                    if (butlerDataTransfer != null) {
                        if (butlerDataTransfer.getStates() == 0) {
                            butlerDataTransfer.setStates(1);
                            butlerDataTransfer.setRecoveryTime(DateUtil.now());
                            butlerDataTransferService.updateById(butlerDataTransfer);
                        }
                        return MsgUtil.ok();
                    } else {
                        return MsgUtil.fail("迁移的备份数据不存在");
                    }

                } else {
                    return MsgUtil.fail(back.get("msg") + "");
                }

            } else {
                return MsgUtil.fail(macId.get("msg") + "");
            }

        } else {
            return MsgUtil.badArgument();
        }
    }
}
