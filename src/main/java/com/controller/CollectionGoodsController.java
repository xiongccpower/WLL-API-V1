package com.controller;

import cn.hutool.core.date.DateUtil;
import com.api.smart.api.duomai.ApiUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.domain.BenefitContent;
import com.domain.CollectionGoods;
import com.domain.Template;
import com.service.BenefitContentService;
import com.service.CollectionGoodsService;
import com.service.TemplateService;
import com.wll.wulian.entity.base.R;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * created in 2022/2/15 17:54
 *
 * @author zhuxuelei
 */
@RestController
@RequestMapping("benefit")
@EnableScheduling
public class CollectionGoodsController {

    @Resource
    private CollectionGoodsService collectionGoodsService;

    @Resource
    private BenefitContentService benefitContentService;

    @Resource
    private TemplateService templateService;


    /**
     * 优惠券超时
     */
    @Scheduled(cron = "0 0 0/1 * * ?")
    public void collectionGoodsOut() {
        try {
            collectionGoodsService.collectionGoodsOut();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostMapping("batch-delete")
    public R delete(@RequestBody List<Integer> ids) {
        collectionGoodsService.removeByIds(ids);
        return R.ok();
    }

    @PostMapping("list")
    public R list() {
        QueryWrapper<CollectionGoods> qw = new QueryWrapper<>();
        qw.lambda().orderByAsc(CollectionGoods::getOut).orderByDesc(CollectionGoods::getCreateDate);
        List<CollectionGoods> list = collectionGoodsService.list(qw);
        return R.ok(list).put("errno", 0);
    }

    @GetMapping("getCategory")
    public Object getCategory(String shopType) {
        if ("jd".equals(shopType)) {
            return ApiUtil.execute("cps-mesh.cpslink.jd.categorys.get", null);
        }
        return null;
    }

    @PostMapping("delete")
    public R delete(@RequestBody CollectionGoods goods) {
        collectionGoodsService.removeById(goods.getId());
        return R.ok();
    }

    @PostMapping("update")
    public R update(@RequestBody CollectionGoods goods) {
        collectionGoodsService.updateById(goods);
        return R.ok();
    }

    @PostMapping("collection")
    public R collection(@RequestBody CollectionGoods goods) {
        LambdaUpdateWrapper<CollectionGoods> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(CollectionGoods::getIsCollection,goods.getIsCollection());
        updateWrapper.eq(CollectionGoods::getId,goods.getId());
        collectionGoodsService.update(updateWrapper);
        return R.ok();
    }

    @PostMapping("collectionList")
    public R collectionList(@RequestBody CollectionGoods goods) {
        LambdaQueryWrapper<CollectionGoods> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CollectionGoods::getIsCollection,goods.getIsCollection());
        List<CollectionGoods> list = collectionGoodsService.list(wrapper);
        return R.ok(list);
    }

    /**
     * 获取模板
     *
     * @return
     */
    @PostMapping("template")
    public R getTemplate(@RequestBody Template template) {
        if (template.getType() != null) {
            List<Template> list = templateService.listByType(template.getType());
            return R.ok(list);
        }
        return R.ok(templateService.list());
    }

    @PostMapping("save")
    public R save(@RequestBody BenefitContent content) {
        content.setCreateTime(LocalDateTime.now());
        benefitContentService.save(content);
        return R.ok().put("id", content.getMbcId());
    }

    @PostMapping("contentList")
    public R contentList() {
        LambdaQueryWrapper<BenefitContent> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(BenefitContent::getMbcId);
        wrapper.last("limit 1");
        List<BenefitContent> list = benefitContentService.list(wrapper);
        return R.ok().put("data",list.get(0));
    }


    @GetMapping("content/get")
    public R getContent(Integer id) {
        return R.ok().put("data", benefitContentService.getById(id));
    }

    /**
     * 获取文案
     *
     * @return
     */
    @PostMapping("article")
    public R getArticle() {
        return R.ok();
    }


}
