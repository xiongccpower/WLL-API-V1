package com.controller;

import com.dao.MyHomeInfoDao;
import com.entity.MyHomeInfo;
import com.util.MsgUtil;
import com.util.WebUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RestController
@RequestMapping("MyHomeInfo")
@Validated
@Api(value="便民资讯",tags={"便民资讯"})
@CrossOrigin
public class MjHomeInfoController {
    @Autowired private MyHomeInfoDao dao;

    @RequestMapping("list")
    @ApiOperation(value="查询便民咨询列表", notes="查询便民咨询列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
    })
    public Object list(String openId,String tel,String uid) {
        uid ="%"+uid+"%";
        List<MyHomeInfo> list = dao.getByTel(uid);
        if(list.size()>0 && list!=null){
            return MsgUtil.ok(list);
        }else{
            return MsgUtil.fail("暂无数据");
        }


    }

    @RequestMapping("listAll")
    @ApiOperation(value="查询便民咨询列表", notes="查询便民咨询列表")
    public Object listAll() {
        List<MyHomeInfo> list = dao.findAll();
        return WebUtil.ok(list);

    }

    @RequestMapping("updateInfo")
    @ApiOperation(value="查询便民咨询列表", notes="查询便民咨询列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "type", value = "type", required = true, dataType = "String"),
            @ApiImplicitParam(name = "timeLong", value = "timeLong", required = true, dataType = "String"),
            @ApiImplicitParam(name = "content", value = "content", required = true, dataType = "String"),
            @ApiImplicitParam(name = "contentType", value = "contentType", required = true, dataType = "String"),
            @ApiImplicitParam(name = "contentArea", value = "contentArea", required = true, dataType = "String"), })
    public Object updateInfo(Integer id,String type,String timeLong,String content,String contentType,String contentArea) {
        MyHomeInfo info  =dao.getOne(id);
        info.setType(type);
        info.setContent(content);
        info.setContentArea(contentArea);
        info.setContentType(contentType);
        info.setTimeLong(timeLong);
        dao.saveAndFlush(info);
        return MsgUtil.ok();

    }
    @RequestMapping("isNew")
    @ApiOperation(value="是否有新消息", notes="是否有新消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),})
    public Object isNew(String uid) {
        uid = "%"+uid+"%";
        List<MyHomeInfo> list =dao.getisNew(uid, "0");
        if(list!=null){
            if(list.size()>0){
                return MsgUtil.ok(list);
            }else{
                return MsgUtil.ok("-1");
            }
        }else{
            return MsgUtil.fail("-1");
        }

    }

    @RequestMapping("update")
    @ApiOperation(value="修改为不是新消息", notes="修改为不是新消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String")
            })
    public Object update(String tel,String openId,String uid) {
        List<MyHomeInfo> list =dao.getisNew(uid, "0");
        for(int i =0;i<list.size();i++){
            list.get(i).setState("1");
            dao.saveAndFlush(list.get(i));
        }
        return MsgUtil.ok();
    }
    @RequestMapping("delete")
    @ApiOperation(value="修改为不是新消息", notes="修改为不是新消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "Integer")
    })
    public Object delete(Integer id) {
        dao.deleteById(id);
        return MsgUtil.ok();
    }

    @RequestMapping("updateById")
    @ApiOperation(value="修改为不是新消息", notes="修改为不是新消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "contentId", value = "contentId", required = true, dataType = "String"),
    })
    public Object update(String contentId) {
        MyHomeInfo info = dao.getByContentId(contentId);
        if(info!=null){
            info.setState("1");
            dao.saveAndFlush(info);
            return MsgUtil.ok();
        }else{
            return MsgUtil.fail("-1");
        }

    }

}
