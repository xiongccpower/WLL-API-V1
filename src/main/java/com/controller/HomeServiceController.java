package com.controller;


import com.dao.HomeServiceDao;
import com.entity.HomeService;
import com.entity.Users;
import com.util.MsgUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("HomeService")
@Validated
@Api(value = "用户信息", tags = { "用户信息" })
@CrossOrigin
public class HomeServiceController {
    @Autowired
    private HomeServiceDao dao;



    @PostMapping("save")
    @ApiOperation(value="获取单个用户信息", notes="用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "v", required = true, dataType = "String"),
            @ApiImplicitParam(name = "serviceTel", value = "serviceTel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "serviceJzName", value = "serviceName", required = true, dataType = "String"),
            @ApiImplicitParam(name = "serviceFwName", value = "serviceName", required = true, dataType = "String"),
            @ApiImplicitParam(name = "serviceAddress", value = "serviceAddress", required = true, dataType = "String"),
            @ApiImplicitParam(name = "serviceTime", value = "serviceTime", required = true, dataType = "String"),
            @ApiImplicitParam(name = "serviceRemark", value = "serviceRemark", required = true, dataType = "String"),
            @ApiImplicitParam(name = "serviceType", value = "serviceRemark", required = true, dataType = "String"),
            @ApiImplicitParam(name = "serviceFwTel", value = "serviceRemark", required = true, dataType = "String"),})
    public Object save(String tel,String serviceJzName,String serviceFwName,String serviceTel,String serviceAddress,String serviceTime,String serviceRemark,String serviceType,String serviceFwTel) {
        HomeService jzHomeService = new HomeService();
        jzHomeService.setServiceTel(serviceTel);
        jzHomeService.setServiceAddress(serviceAddress);
        jzHomeService.setServiceRemake(serviceRemark);
        jzHomeService.setServiceTime(serviceTime);
        jzHomeService.setServiceState("0");
        jzHomeService.setCreateTime(new Date());
        jzHomeService.setServiceFwName(serviceFwName);
        jzHomeService.setServiceJzName(serviceJzName);
        jzHomeService.setServiceType(serviceType);
        jzHomeService.setTel(tel);
        jzHomeService.setServiceFwTel(serviceFwTel);
        dao.saveAndFlush(jzHomeService);     //创建家政订单

        return  MsgUtil.ok();
    }

    @RequestMapping("update")
    @ApiOperation(value="获取单个用户信息", notes="用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "serviceId", value = "serviceId", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "serviceState", value = "serviceState", required = true, dataType = "String"),})
    public Object update(Integer serviceId,String serviceState) {
        HomeService homeService = dao.getOne(serviceId);
        homeService.setServiceState(serviceState);
        dao.saveAndFlush(homeService);
        return  MsgUtil.ok(homeService);
    }

    @RequestMapping("getListByTel")
    @ApiOperation(value="获取家政端下单信息", notes="获取家政端下单信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String")})
    public Object getListByTel(String tel) {
        List<HomeService> list = dao.getByTel(tel);
        if(list.size()>0 && list!=null){
            return  MsgUtil.ok(list);
        }else{
            return  MsgUtil.fail("暂无下单数据");
        }


    }

    @RequestMapping("getListByServiceFwTel")
    @ApiOperation(value="获取商户端下单信息", notes="获取商户端下单信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String")})
    public Object getListByServiceFwTel(String tel) {
        List<HomeService> list = dao.getListByServiceFwTel(tel);
        if(list.size()>0 && list!=null){
            return  MsgUtil.ok(list);
        }else{
            return  MsgUtil.fail("暂无下单数据");
        }

    }


    @RequestMapping("queryFwPrice")
    @ApiOperation(value="获取单个用户信息", notes="用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String")})
    public Object queryFwPrice(String tel) {

        return  MsgUtil.ok("80");

    }
}
