package com.controller;

import java.io.*;
import java.util.List;
import java.util.UUID;

import com.dao.MjDataBaseDao;
import com.entity.MjDataBase;
import com.util.FunctionUtil;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mock.web.MockMultipartFile;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.dao.DemoDao;
import com.entity.Demo;
import com.util.MsgUtil;
import com.util.ReadUtil;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("demo")
@Validated
@CrossOrigin
public class DemoController {

	@Autowired private  DemoDao dao;
	@Autowired private MjDataBaseDao dataBaseDao;
	
	/** 列表  */ 
	@RequestMapping("get")
	public Object get(Integer id) {
		List<Demo> list = dao.findAll();
		System.out.println(list.get(0).getId());
		Demo demo=dao.getById(id);
		return demo.getTwoA();
	}
	
	@GetMapping("set")
	public Object get() {
		Demo demo=new Demo();
		demo.setId(1);
		demo.setOneA(1);
		demo.setTwoA("two");
		dao.save(demo);
		return "YES";
	}
	
	/** 列表  */ 
	@RequestMapping("post")
	public Object post(Integer id) {
		Demo demo=dao.getById(id);
		return demo.getTwoA();
	}
	
    @RequestMapping(value = "/fileUpload")//文件上传
    public Object fileUpload(@RequestParam(value = "file") MultipartFile file) {
        String fileName = file.getOriginalFilename();  // 文件名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));  // 后缀名
        String filePath =ReadUtil.read("spring.resources.static-locations").split("file:")[1];// String filePath = "D://image//"; // 上传后的路径
        fileName = UUID.randomUUID() + suffixName; // 新文件名
        File dest = new File(filePath + fileName) ;
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return MsgUtil.ok(ReadUtil.read("domain")+"/image/"+fileName); //return WebUtil.ok("https://www.hzchwlkj.cn/image/"+fileName);

    }
	@RequestMapping(value = "/uploadAttachment")//文件上传
	public Object uploadAttachment(@RequestParam(value = "file") MultipartFile file,String tel,String machId) {
		try {
			InputStream str = file.getInputStream();
			byte[] fileByte = FunctionUtil.toByteArray(str);
			String fileName = file.getOriginalFilename();
			MjDataBase database = new MjDataBase();
			database.setTel(tel);
			database.setMachId(machId);
			database.setContent(fileByte);
			database.setFileName(fileName);
			dataBaseDao.saveAndFlush(database);
		}catch (IOException e) {
			e.printStackTrace();
			return MsgUtil.fail("保存失败！");
		}
		return MsgUtil.ok("保存成功！");
	}
	/*@RequestMapping(value = "/DowloadFile")
	public MultipartFile DowloadFile(String  tel) {
		MultipartFile file = null;
		byte[] fileByte = dataBaseDao.getByTel(tel).getContent();
		InputStream inputStream = new ByteArrayInputStream(fileByte);
		try {
			file = new MockMultipartFile(ContentType.APPLICATION_OCTET_STREAM.toString(), inputStream);
		}catch (IOException e){
			e.printStackTrace();
			//return MsgUtil.fail("下载失败！");
		}
		//return MsgUtil.ok(file);
		return file;
	}*/


}
