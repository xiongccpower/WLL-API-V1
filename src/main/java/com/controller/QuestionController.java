package com.controller;

import com.dao.AnswerInfoDao;
import com.dao.MyHomeSaleDataDao;
import com.dao.QuestionInfoDao;
import com.entity.AnswerInfo;
import com.entity.MyHomeSaleData;
import com.entity.QuestionInfo;
import com.util.MsgUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URLDecoder;
import java.util.*;

@RestController
@RequestMapping("question")
@Validated
@Api(value="问题反馈",tags={"问题反馈"})
@CrossOrigin
public class QuestionController {
    @Autowired private QuestionInfoDao dao;
    @Autowired private AnswerInfoDao adao;

    @RequestMapping("list")
    @ApiOperation(value="问题反馈记录", notes="问题反馈记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
             })
    public Object list(String tel) {
        List list =  new ArrayList();
        List<QuestionInfo> questionInfoList = new ArrayList<>();
        if(!"".equals(tel) && tel!=null){
            questionInfoList = dao.getByTel(tel);
        }else{
             questionInfoList = dao.getAll();
        }
        for(int i=0;i<questionInfoList.size();i++){
            Map<String,Object> map = new HashMap<String,Object>();
            List<AnswerInfo> answerInfoList = adao.getByQuestionId(questionInfoList.get(i).getQuestionId());
            map.put("questionInfo",questionInfoList.get(i));
            map.put("AnswerList",answerInfoList);
            list.add(map);
        }
        return MsgUtil.ok(list);



    }


    @RequestMapping("saveQuesion")
    @ApiOperation(value="保存问题", notes="保存问题")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "questionInfo", value = "questionInfo", required = true, dataType = "String"), })
    public Object saveQuesion(String tel,String questionInfo) {
        QuestionInfo info   = new QuestionInfo();
        info.setQuestionId(UUID.randomUUID().toString());
        info.setQuestionInfo(questionInfo);
        info.setCreateTime(new Date());
        info.setTel(tel);
        dao.saveAndFlush(info);
        return MsgUtil.ok(info);

    }


    @RequestMapping(value = "saveAnswer")
    @ApiOperation(value="保存回复", notes="保存回复")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "questionId", value = "questionId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "answerInfo", value = "answerInfo", required = true, dataType = "String"),
            @ApiImplicitParam(name = "answerPerson", value = "answerPerson", required = true, dataType = "String"),})
    public Object saveAnswer(String questionId,String answerInfo,String answerPerson) {
        AnswerInfo info   = new AnswerInfo();
        info.setAnswerPerson(answerPerson);
        info.setQuestionId(questionId);
        info.setAnswerInfo(answerInfo);
        info.setState("0");
        info.setCreateTime(new Date());
        adao.saveAndFlush(info);
        return MsgUtil.ok();

    }

    @RequestMapping(value = "saveAnswerUrl")
    @ApiOperation(value="保存回复", notes="保存回复")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "questionId", value = "questionId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "answerInfo", value = "answerInfo", required = true, dataType = "String"),
            @ApiImplicitParam(name = "answerPerson", value = "answerPerson", required = true, dataType = "String"),})
    public Object saveAnswerUrl(String questionId,String answerInfo,String answerPerson) {
        answerInfo = URLDecoder.decode(answerInfo);
        AnswerInfo info   = new AnswerInfo();
        info.setAnswerPerson(answerPerson);
        info.setQuestionId(questionId);
        info.setAnswerInfo(answerInfo);
        info.setState("0");
        info.setCreateTime(new Date());
        adao.saveAndFlush(info);
        return MsgUtil.ok();

    }


    @RequestMapping("isNew")
    @ApiOperation(value="是否有新消息", notes="是否有新消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            })
    public Object isNew(String tel) {
        List<QuestionInfo> questionInfoList = dao.getByTel(tel);
        for(int i=0;i<questionInfoList.size();i++){
            List<AnswerInfo> answerInfoList = adao.getByQuestionId(questionInfoList.get(i).getQuestionId());
            for(int j=0;j<answerInfoList.size();j++){
                if("1".equals(answerInfoList.get(j).getState())){
                    return MsgUtil.ok("1");
                }
            }
        }
        return MsgUtil.ok("-1");
    }



//    @RequestMapping("updateAnswer")
//    @ApiOperation(value="修改为不是新消息", notes="修改为不是新消息")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
//            @ApiImplicitParam(name = "questionId", value = "questionId", required = true, dataType = "String")})
//    public Object updateAnswer(String tel,String questionId) {
//        List<QuestionInfo> list = dao.getByQuestionId(questionId,tel);
//        for(int i=0;i< list.size();i++){
//            List<AnswerInfo> answerInfoList = adao.getByQuestionId(list.get(i).getQuestionId());
//            for(int j=0;j<answerInfoList.size();j++){
//                answerInfoList.get(j).setState("1");
//                adao.saveAndFlush(answerInfoList.get(j));
//            }
//        }
//        return MsgUtil.ok();
//    }

    @RequestMapping("updateAnswer")
    @ApiOperation(value="修改为不是新消息", notes="修改为不是新消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            })
    public Object updateAnswer(String tel) {
        List<QuestionInfo> list = dao.getByTel(tel);
        for(int i=0;i< list.size();i++){
            List<AnswerInfo> answerInfoList = adao.getByQuestionId(list.get(i).getQuestionId());
            for(int j=0;j<answerInfoList.size();j++){
                answerInfoList.get(j).setState("0");
                adao.saveAndFlush(answerInfoList.get(j));
            }
        }
        return MsgUtil.ok();
    }



    @RequestMapping("answerList")
    @ApiOperation(value="修改为不是新消息", notes="修改为不是新消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "questionId", value = "questionId", required = true, dataType = "String")})
    public Object answerList(String questionId) {
        List<AnswerInfo> answerInfoList = adao.getByQuestionId(questionId);
        return MsgUtil.ok(answerInfoList);
    }

    @RequestMapping("updateAnswerState")
    @ApiOperation(value="修改为不是新消息", notes="修改为不是新消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "String")})
    public Object updateAnswerState(String  id) {
        AnswerInfo info = adao.getOne(Integer.valueOf(id));
        info.setState("1");
        adao.saveAndFlush(info);
        return MsgUtil.ok();
    }


    @RequestMapping("deleteAnswer")
    @ApiOperation(value="删除回答", notes="删除回答")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "questionId", value = "questionId", required = true, dataType = "String")})
    public Object deleteAnswer(String  id,String questionId) {
        AnswerInfo info = adao.getOne(Integer.valueOf(id));
        adao.delete(info);
        List<AnswerInfo> answerInfoList = adao.getByQuestionId(questionId);
        if(answerInfoList.size()!=0){
            AnswerInfo info2 = answerInfoList.get(answerInfoList.size()-1);
            info2.setState("0");
            adao.saveAndFlush(info2);
        }
        return MsgUtil.ok();
    }

}
