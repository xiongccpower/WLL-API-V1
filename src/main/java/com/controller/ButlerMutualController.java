package com.controller;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.entity.MjUser;
import com.entity.MyHomes;
import com.entity.MyServiceInstructRecord;
import com.service.MjUserService;
import com.service.MyHomeService;
import com.service.MyServiceInstructRecordService;
import com.util.MsgUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("butler/api")
@Validated
@Api(value="管家交互类",tags={"管家交互类"})
@CrossOrigin
public class ButlerMutualController {

    @Autowired
    private MyHomeService myHomeService;
    @Autowired
    private MjUserService mjUserService;
    @Autowired
    private MyServiceInstructRecordService myServiceInstructRecordService;

    @RequestMapping("find/service/instruct")
    @ApiOperation(value="查询管家指令记录", notes="查询管家指令记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"), })
    public Object findServiceInstruct(String tel,String openId){
        Object result = new Object();
        if(!StrUtil.hasEmpty(tel)){
            MjUser mjUser = mjUserService.getUserByPhone(tel);
          if(mjUser!=null){
              String time = DateUtil.now();

              Date newDate = DateUtil.offset(DateUtil.parse(time), DateField.MINUTE, -10);

              if(mjUser.getMainNum()!=0){
                  MjUser user = mjUserService.getById(mjUser.getMainNum());
                  if(user!=null){
                      tel = user.getTel();
                  }

              }

              List<MyServiceInstructRecord> list= myServiceInstructRecordService.findServiceInstructRecord(tel,newDate.toString());

              if(list.size()>0 && list!=null){
                  result = MsgUtil.ok(list);
              }else{
                  result = MsgUtil.fail("暂无搜索指令");
              }
          }else{
              result = MsgUtil.fail("用户不存在");
          }
        }else{
            result = MsgUtil.badArgument();
        }
        return result;
    }
    @RequestMapping("read/service/instruct")
    @ApiOperation(value="将指令改为已读", notes="将指令改为已读")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "String"),
    })
    public Object serviceInstructRead(String id){
        if(!StrUtil.hasEmpty(id)){
            MyServiceInstructRecord myServiceInstructRecord = myServiceInstructRecordService.getById(id);
            if(myServiceInstructRecord!=null){
                myServiceInstructRecord.setStarts(1);
                myServiceInstructRecordService.updateById(myServiceInstructRecord);
            }
        }
        return MsgUtil.ok();
    }

}
