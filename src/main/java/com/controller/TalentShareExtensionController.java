package com.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.domain.ShareContent;
import com.entity.*;
import com.service.*;
import com.util.MsgUtil;
import com.util.ToolUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("share/extend")
@Validated
@Api(value = "达人分享推广", tags = {"达人分享推广"})
@CrossOrigin
@Log4j2
public class TalentShareExtensionController {

    @Autowired
    private TalentDearHelpService talentDearHelpService;
    @Autowired
    private TalentShareRecordService talentShareRecordService;
    @Autowired
    private TalentShareService talentShareService;
    @Autowired
    private MjUserService mjUserService;
    @Autowired
    private TalentPkObjectService talentPkObjectService;
    @Autowired
    private TalentShareRuleService talentShareRuleService;

    @Resource
    private ShareContentService shareContentService;

    @GetMapping("share/content")
    public Object getShare(Integer platform, String code) {
        if (platform != null) {
            QueryWrapper<ShareContent> query = new QueryWrapper<>();
            query.lambda().eq(ShareContent::getPlatform, platform);
            List<ShareContent> list = shareContentService.list(query);
            return MsgUtil.ok(list);
        }
        QueryWrapper<ShareContent> query = new QueryWrapper<>();
        query.lambda().eq(ShareContent::getCode, code);
        ShareContent one = shareContentService.getOne(query);
        return MsgUtil.ok(one);
    }


    @RequestMapping("judge/talent")
    @ApiOperation(value = "判断是不是达人", notes = "判断是不是达人")
    @ApiImplicitParams({@ApiImplicitParam(name = "uid", value = "用户id", required = true, dataType = "String"),})
    public Object judgeTalent(String uid) {
        Object result = new Object();

        if (ToolUtil.isEmpty(uid)) {
            result = MsgUtil.ok("参数错误");
        } else {
            MjUser mjUser = mjUserService.getUser(uid, "1");
            if (mjUser != null) {
                result = MsgUtil.ok();
            } else {
                result = MsgUtil.ok("不是达人");
            }
        }
        return result;
    }

    @RequestMapping("record")
    @ApiOperation(value = "达人分享记录", notes = "达人分享记录")
    @ApiImplicitParams({@ApiImplicitParam(name = "openId", value = "分享达人openid", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "用户id", required = true, dataType = "String"),})
    public Object talentShareRecord(String openId, String type, String uid, String nickname, String url) {

        Object result = new Object();

        if (ToolUtil.isEmpty(openId) && ToolUtil.isEmpty(uid)) {
            result = MsgUtil.ok("参数错误");
        } else {

            MjUser mjUser = mjUserService.getUser(uid, "1");
            if (mjUser != null) {
                TalentShare talentShares = new TalentShare();
                if (type.equals("talent")) {

                    String time = DateUtil.today();
                    TalentShare talentShare = talentShareService.getByOpenId(uid,
                            time.substring(0, time.length() - 3));
                    if (talentShare == null) {

                        talentShares.setTalentOpenid(openId);
                        talentShares.setCreateTime(DateUtil.now());
                        talentShares.setTotalNum(0);
                        talentShares.setOperate(mjUser.getOperateName());
                        talentShares.setHead(mjUser.getHead());
                        talentShares.setNickname(nickname);
                        talentShares.setHead(url);
                        talentShares.setShareMonth(time.substring(0, time.length() - 3));
                        talentShares.setUid(mjUser.getId());
                        talentShareService.save(talentShares);
                    }

                } else if (type.equals("friend")) {
                    TalentShare talentShare = talentShareService.getByHelpOpenid(openId);
                    if (talentShares == null) {
                        talentShares.setHelpOpenid(openId);
                        talentShares.setCreateTime(DateUtil.now());
                        talentShares.setTotalNum(0);
                        talentShareService.save(talentShares);
                    }

                }
                result = MsgUtil.ok();
            } else {
                result = MsgUtil.ok("不是达人");
            }
        }
        return result;
    }

    @RequestMapping("browse/record")
    @ApiOperation(value = "达人分享用户浏览记录", notes = "达人分享用户浏览记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "talent", value = "分享达人openid", required = true, dataType = "String"),
            @ApiImplicitParam(name = "helpOpenid", value = "助力亲友openid", required = true, dataType = "String"),
            @ApiImplicitParam(name = "help", value = "浏览者openid", required = true, dataType = "String"),
            @ApiImplicitParam(name = "type", value = "是否是助力 0：达人分享 1：亲友助力", required = true, dataType = "String"),})
    public Object browseRecord(String openId, String talent, String help, String type, String uid, String helpOpenid, String nickname) {
        log.info("记录达人分享记录");
        Object result = new Object();
        boolean isHelp = false;
        if (ToolUtil.isEmpty(talent) || ToolUtil.isEmpty(help) || ToolUtil.isEmpty(type)) {
            return MsgUtil.ok("参数错误");
        }
        String time = DateUtil.today();


        if (StringUtils.isEmpty(uid) || uid.equals("null")) {
            TalentDearHelp helpUser = talentDearHelpService.getByHelpOpenid(talent, time.substring(0, 7));
            if (helpUser == null) {
                return MsgUtil.ok("达人不存在");
            }
            isHelp = true;
            uid = helpUser.getTalentOpenid();
        }

        MjUser mjUser = mjUserService.getUser(uid, "1");
        if (mjUser == null) {
            return MsgUtil.ok("达人不存在");
        }

        if (talent.equals(help)) {
            return MsgUtil.ok("自己不可以分享给自己");
        }

        TalentShare talentShare = talentShareService.getByOpenId(uid, time.substring(0, time.length() - 3));

        if (talentShare == null) {
            return MsgUtil.ok("未查询到分享记录");
        }


        // todo 每个用户每个月，需要浏览3次
        TalentShareRecord talentShareRecord = talentShareRecordService.getTime(uid, help, time.substring(0, time.length() - 3));
        if (talentShareRecord != null) {
            return MsgUtil.ok("当月已经预览过");
        }

        talentShareRecord = new TalentShareRecord();

        talentShareRecord.setId(IdUtil.simpleUUID());
        talentShareRecord.setCreateTime(DateUtil.now());
        talentShareRecord.setPreviewTime(time);
        talentShareRecord.setPreviewMonth(time.substring(0, time.length() - 3));
        talentShareRecord.setPreviewOpenid(help);
        talentShareRecord.setTalentOpenid(uid);
        talentShareRecord.setType(Integer.parseInt(type));
        talentShareRecord.setNickname(nickname);
        talentShareRecord.setHead(mjUser.getHead());
        if (isHelp) {
            talentShareRecord.setHelpOpenid(talent);
        }
        talentShareRecord.setTalentUid(Integer.parseInt(uid));
        talentShareRecordService.save(talentShareRecord);

        int totalNum = talentShare.getTotalNum();
        talentShare.setTotalNum(totalNum + 1);
        talentShareService.updateById(talentShare);

        return MsgUtil.ok();
    }

    @RequestMapping("friend/help")
    @ApiOperation(value = "记录好友助力", notes = "记录好友助力")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "talentOPenid", value = "分享达人openid", required = true, dataType = "String"),
            @ApiImplicitParam(name = "helpOPenid", value = "助力者openid", required = true, dataType = "String"),})
    public Object friendHelp(String uid, String talent, String help) {
        Object result = new Object();
        if (StrUtil.hasEmpty(talent) || StrUtil.hasEmpty(help)) {
            return MsgUtil.fail("参数错误");
        }

        if (talent.equals(help)) {
            return MsgUtil.fail("自己不可以为自己助力");
        }

        List<TalentDearHelp> list = talentDearHelpService.findTalentOpenid(uid, DateUtil.today().substring(0, 7));
        if (list.size() >= 1) {
            return MsgUtil.fail("助力已满");
        }

        TalentDearHelp talentDearHelp = talentDearHelpService.getHelpOpenid(help, uid, DateUtil.today().substring(0, 7));
        if (talentDearHelp != null) {
            return MsgUtil.fail("已是助力亲友");
        }

        String time = DateUtil.today();
        time = time.substring(0, time.length() - 3);

        talentDearHelp = new TalentDearHelp();

        talentDearHelp.setCreateTime(DateUtil.now());
        talentDearHelp.setHelpOpenid(help);
        talentDearHelp.setId(IdUtil.simpleUUID());
        talentDearHelp.setTalentOpenid(uid);
        talentDearHelp.setHelpTime(time);
        talentDearHelp.setState(1);

        talentDearHelpService.save(talentDearHelp);
        result = MsgUtil.ok();

        return result;
    }

    @RequestMapping("get/help")
    @ApiOperation(value = "查询是否助力", notes = "查询是否助力")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "helpOPenid", value = "openid", required = true, dataType = "String"),})
    public Object getHelp(String helpOpenid) {
        Object result = new Object();
        TalentDearHelp talentDearHelp = talentDearHelpService.getByHelpOpenid(helpOpenid,
                DateUtil.today().substring(0, 7));
        if (talentDearHelp != null) {

            result = MsgUtil.ok("已助力", (Object) 1);
        } else {

            result = MsgUtil.ok("未助力", (Object) 0);
        }
        return result;
    }

    @RequestMapping("launch/pk")
    @ApiOperation(value = "发起pk", notes = "发起pk")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "launch", value = "发起者的id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "receive", value = "接收者id", required = true, dataType = "String"),})
    public Object launchPk(String launch, String receive) {
        Object result = new Object();
        if (!ToolUtil.isEmpty(launch) && !ToolUtil.isEmpty(receive)) {
            String item = DateUtil.today().substring(0, 7);
            long time = DateUtil.parse(item + "-10 00:00:00").getTime();
            long now = new Date().getTime();
            if (now >= time) {
                TalentPkObject talentPk = talentPkObjectService.getByLaunchOpenid(launch, item);
                if (talentPk != null) {
                    result = MsgUtil.ok("你有正在进行的pk");
                } else {
                    TalentPkObject talentPkyqz = talentPkObjectService.getByReceiveOpenid(receive, item);
                    if (talentPkyqz != null) {
                        result = MsgUtil.ok("邀请者正在PK中");
                    } else {
                        TalentPkObject talentPkyqzsf = talentPkObjectService.getByLaunchOpenid(receive, item);
                        if (talentPkyqzsf != null) {
                            result = MsgUtil.ok("邀请者正在PK中");
                        } else {
                            TalentPkObject talentPkObject = new TalentPkObject();

                            talentPkObject.setLaunchOpenid(launch);
                            talentPkObject.setReceiveOpenid(receive);
                            talentPkObject.setLaunchTime(DateUtil.now());
                            talentPkObject.setState(1);
                            talentPkObject.setTime(item);

                            talentPkObjectService.save(talentPkObject);
                            result = MsgUtil.ok("success");
                        }
                    }
                }
            } else {
                result = MsgUtil.ok("每月10号零时开始PK");
            }
        } else {
            result = MsgUtil.ok("参数错误");
        }

        return result;
    }

    @RequestMapping("get/launch/pk")
    @ApiOperation(value = "查询有无pk邀请", notes = "查询有无pk邀请")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "接收者openid", required = true, dataType = "String"),})
    public Object getLaunchPk(String uid) {
        Object result = new Object();
        Map<String, Object> map = new HashMap<>();
        if (ToolUtil.isEmpty(uid)) {
            result = MsgUtil.ok("fail");
        } else {
            TalentPkObject talentPkyqz = talentPkObjectService.getByPkYq(uid, DateUtil.today().substring(0, 7), 1);
            if (talentPkyqz != null) {
                String time = DateUtil.today();
                List<TalentShare> pkUser = talentShareService.getPk(talentPkyqz.getLaunchOpenid(),
                        talentPkyqz.getReceiveOpenid(), time.substring(0, 7));
                map.put("pkId", talentPkyqz.getId());
                map.put("pkUser", pkUser);
                // result = MsgUtil.ok("success");
                result = MsgUtil.ok(map);
            } else {
                result = MsgUtil.ok("fail");
            }
        }
        return result;
    }

    @RequestMapping("invitation/operate")
    @ApiOperation(value = "接受pk", notes = "接受pk")
    @ApiImplicitParams({@ApiImplicitParam(name = "type", value = "类型3接受2拒绝", required = true, dataType = "String"),
            @ApiImplicitParam(name = "pkId", value = "pk记录id", required = true, dataType = "String"),})
    public Object pkInvitationOperate(String type, String pkId) {
        Object result = new Object();
        if (ToolUtil.isEmpty(type) && ToolUtil.isEmpty(pkId)) {
            result = MsgUtil.ok("fail");
        } else {
            TalentPkObject talentPk = talentPkObjectService.getById(pkId);
            if (talentPk != null) {
                talentPk.setReceiveTime(DateUtil.now());
                talentPk.setState(Integer.parseInt(type));
                talentPkObjectService.updateById(talentPk);
                result = MsgUtil.ok("success");
            } else {
                result = MsgUtil.ok("fail");
            }
        }
        return result;
    }

    @RequestMapping("share/ranking/list")
    @ApiOperation(value = "查询分享排行榜", notes = "查询分享排行榜 ")
    @ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "达人openid", required = true, dataType = "String"),
            @ApiImplicitParam(name = "city", value = "达人城市", required = true, dataType = "String"),
            @ApiImplicitParam(name = "month", value = "", required = true, dataType = "String"),})
    public Object shareRankingList(String uid, String city, String month) {
        Object result = new Object();
        String time = DateUtil.today();
        String monthtime = "";
        if (month != null) {
            monthtime = month;
        } else {
            monthtime = time.substring(0, time.length() - 3);
        }

        Map<String, Object> map = new HashMap<>();

        if (!ToolUtil.isEmpty(uid)) {
            // 查询当前排名
            TalentShare ranking = talentShareService.getRanking(time.substring(0, 7), uid);
            // 统计个人月日浏览数据
            TalentShareRecord personal = talentShareRecordService.getStatistics(monthtime, time, uid);
            // 查询有无助力亲友
            TalentDearHelp talentDearHelp = talentDearHelpService.getHelp(uid, DateUtil.today().substring(0, 7));
            if (talentDearHelp != null) {
                // 查询统计亲友助力浏览数
                TalentShareRecord help = talentShareRecordService.getHelp(time, monthtime,
                        talentDearHelp.getHelpOpenid());
                if (help != null) {// 助力有数据
                    map.put("previewTodayZL", help.getDatyNum());
                    map.put("monthNumZL", help.getMonthNum());
                } else {// 助力无数据
                    map.put("previewTodayZL", "0");
                    map.put("monthNumZL", "0");
                }

                map.put("isZl", "yes");
            } else {
                map.put("isZl", "not");
            }
            // 统计所有用户的分享浏览数
            List<TalentShareRecord> statisticsPreview = talentShareRecordService.statisticsPreview(monthtime, time,
                    city);
            if (ranking != null && ranking.getRowno() != null) {// 当前排名
                map.put("ranking", Double.valueOf(ranking.getRowno()).intValue());
            } else {
                map.put("ranking", "0");
            }
            if (personal != null) {//
                map.put("head", personal.getHead());
                map.put("nickname", personal.getNickname());
                if (ToolUtil.isEmpty(personal.getDatyNum())) {
                    map.put("previewToday", 0);
                } else {
                    map.put("previewToday", personal.getDatyNum());
                }
                map.put("monthNum", personal.getMonthNum());
            } else {
                map.put("head", "");
                map.put("nickname", "");
                map.put("previewToday", 0);
                map.put("monthNum", 0);
            }

            map.put("previewList", statisticsPreview);

            result = MsgUtil.ok(map);
        } else {
            result = MsgUtil.ok("fail");
        }
        return result;
    }

    @RequestMapping("get/share/rule")
    @ApiOperation(value = "分享规则", notes = "分享规则 ")
    public Object getShareRule() {
        Object result = new Object();
        TalentShareRule shareRule = talentShareRuleService.getTalentShareRule();
        if (shareRule != null) {
            result = MsgUtil.ok(shareRule);
        } else {
            result = MsgUtil.ok("fail");
        }
        return result;
    }

    @RequestMapping("fond/data")
    @ApiOperation(value = "查询当月甲乙pk数据", notes = "查询当月甲乙pk数据 ")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pkOpenid", value = "pk者的openid", required = true, dataType = "String"),})
    public Object fondPKData(String uid) {
        Object result = new Object();
        Map<String, Object> map = new HashMap<>();
        if (ToolUtil.isEmpty(result)) {
            result = MsgUtil.ok("fail");
        } else {
            String time = DateUtil.today();
            TalentPkObject talentPkyqz = talentPkObjectService.getByPk(uid, time.substring(0, 7), 3);

            if (talentPkyqz != null) {
                String other = talentPkyqz.getLaunchOpenid().equals(uid) ? talentPkyqz.getReceiveOpenid() : talentPkyqz.getLaunchOpenid();

                List<TalentShare> pkUser = talentShareService.getPk(uid,
                        other, time.substring(0, 7));

                List<TalentShareRecord> findPKData = talentShareRecordService.findPKData(uid,
                        other, time.substring(0, 7));
                /*
                 * List<TalentShareRecord> findPKDataA =
                 * talentShareRecordService.findPKDataGroupDay(talentPkyqz.getLaunchOpenid(),
                 * time.substring(0,7)); List<TalentShareRecord> findPKDataB =
                 * talentShareRecordService.findPKDataGroupDay(talentPkyqz.getReceiveOpenid(),
                 * time.substring(0,7));
                 */
                List<TalentShareRecordTwo> findPKDataTwo = talentShareRecordService.findPKDataGroupDayAll(
                        uid, other, time.substring(0, 7));

                map.put("pkUser", pkUser);
                map.put("findPKDataTwo", findPKDataTwo);
                map.put("findPKData", findPKData);
                /*
                 * map.put("findPKDataA",findPKDataA); map.put("findPKDataB",findPKDataB);
                 */
                result = MsgUtil.ok(map);

            } else {
                result = MsgUtil.ok("fail");
            }
        }
        return result;
    }

    @RequestMapping("fond/help/data")
    @ApiOperation(value = "查询当月助力数据", notes = "查询当月助力数据 ")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "用户openid", required = true, dataType = "String"),})
    public Object fondHelpData(String openId) {
        Object result = new Object();
        if (ToolUtil.isEmpty(openId)) {
            result = MsgUtil.ok("fail");
        } else {
            TalentDearHelp talentDearHelp = talentDearHelpService.getHelp(openId, DateUtil.today().substring(0, 7));
            if (talentDearHelp != null) {
                String time = DateUtil.today();
                List<TalentShareRecord> talentShareRecord = talentShareRecordService
                        .fondHelpData(talentDearHelp.getHelpOpenid(), time.substring(0, 7));
                if (talentShareRecord != null && talentShareRecord.size() > 0) {
                    result = MsgUtil.ok(talentShareRecord);
                } else {
                    result = MsgUtil.ok("fail");
                }
            } else {
                result = MsgUtil.ok("fail");
            }
        }
        return result;
    }

    @RequestMapping("fond/user/data")
    @ApiOperation(value = "查询个人当月浏览数据", notes = "查询个人当月浏览数据 ")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "用户openid", required = true, dataType = "String"),})
    public Object fondUserData(String uid) {
        Object result = new Object();
        if (ToolUtil.isEmpty(uid)) {
            result = MsgUtil.ok("fail");
        } else {

            String time = DateUtil.today();
            List<TalentShareRecordTwo> talentShareRecord = talentShareRecordService.findUserDataGroupDay(uid,
                    time.substring(0, 7));
            if (talentShareRecord != null && talentShareRecord.size() > 0) {
                result = MsgUtil.ok(talentShareRecord);
            } else {
                result = MsgUtil.ok("fail");
            }

        }
        return result;
    }

}
