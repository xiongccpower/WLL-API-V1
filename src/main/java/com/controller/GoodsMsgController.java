package com.controller;

import com.dao.MjGoodsMsgDao;
import com.dao.MjGoodsStateDao;
import com.entity.MjGoodsMsg;
import com.entity.MjGoodsState;
import com.util.FunctionUtil;
import com.util.MsgUtil;
import com.util.WebUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("GoodsMsg")
@Validated
@Api(value = "商品状态", tags = {"商品状态"})
//@CrossOrigin
public class GoodsMsgController {
    @Autowired
    private MjGoodsMsgDao goodsMsgDao;
    @Autowired
    private MjGoodsStateDao goodsStateDao;


    @RequestMapping("save")
    @ApiOperation(value = "保存缺货提醒", notes = "保存缺货提醒")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String")
    })
    public Object save(String openId, String tel) {
        MjGoodsMsg goodsMsg = new MjGoodsMsg();
        goodsMsg.setOpenId(openId);
        goodsMsg.setTel(tel);
        goodsMsg.setCreateTime(new Date());
        goodsMsgDao.saveAndFlush(goodsMsg);
        return MsgUtil.ok();
    }

    @RequestMapping("querySendMsg")
    @ApiOperation(value = "查询缺货提醒", notes = "查询缺货提醒")
    public Object querySendMsg() {
        List<MjGoodsMsg> list = goodsMsgDao.findAll();
        return WebUtil.ok(list);
    }

    @RequestMapping("queryCheckMsg")
    @ApiOperation(value = "查询通知文案", notes = "查询通知文案")
    public Object queryCheckMsg() {
        List<MjGoodsState> list = goodsStateDao.findAll();
        return WebUtil.ok(list);
    }

    @RequestMapping("saveCheckMsg")
    @ApiOperation(value = "保存通知文案", notes = "保存通知文案")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goodsMsg", value = "goodsMsg", required = true, dataType = "String"),
            @ApiImplicitParam(name = "state", value = "state", required = true, dataType = "String")
    })
    public Object saveCheckMsg(MjGoodsState s) {
        goodsStateDao.saveAndFlush(s);
        return MsgUtil.ok();
    }

    @RequestMapping("sendMsg")
    @ApiOperation(value = "发送补货信息", notes = "发送补货信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
    })
    public Object sendMsg(String tel) {
        String num = FunctionUtil.sendMsg(tel);
        if ("10001".equals(num)) {
            return MsgUtil.fail("版本过低");
        } else if ("1000".equals(num)) {
            return MsgUtil.fail("当日获取验证码短信数量已达上限");
        }
        return MsgUtil.ok(num);
    }
}
