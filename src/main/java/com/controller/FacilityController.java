package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.dao.FacilityDao;
import com.dao.GoodsDao;
import com.dao.UserDao;
import com.entity.Facility;
import com.entity.Goods;
import com.entity.Users;
import com.util.MsgUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
@RestController
@RequestMapping("facility")
@Validated
@Api(value="设备类",tags={"设备类"})
@CrossOrigin
public class FacilityController {

	@Autowired private  FacilityDao dao;
	@Autowired private  UserDao UserDao;
	@Autowired private  GoodsDao goodsDao;


	
	@RequestMapping("list")
    @ApiOperation(value="设备数组", notes="设备数组")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer")})
	public Object get(String  openId) {
		Users  user = UserDao.getByOpenId((String) openId);
		List<Facility>  list = dao.getByUserId(user.getId());
		Goods goods=null;
		for(Facility f:list) {
			goods= goodsDao.getById(f.getGoodsId());
			f.setGoods(goods);
		}

		return MsgUtil.ok(list);
	}
	
	@RequestMapping("save")
    @ApiOperation(value="新增", notes="新增")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer")})
	public Object save(String  openId) {
		Users  user = UserDao.getByOpenId((String) openId);
		Facility f =new Facility();
		Goods g=goodsDao.getOne(1);
		f.setGoodsId(g.getId());
		f.setUserId(user.getId());
		f.setFacilityNo("20201008");
		dao.save(f);
		return MsgUtil.ok();
	}
	
	@RequestMapping("delete")
    @ApiOperation(value="解绑", notes="解绑")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer"),
        @ApiImplicitParam(name = "facilityIds", value = "设备Id数组", required = true, dataType = "Integer")
    })
	public Object save(String  openId,String facilityIds) {
		String[] ids=facilityIds.split(",");
		for(String i :ids ) {
			Facility f = dao.getById(Integer.parseInt(i));
			f.setIsBind("N");
			dao.saveAndFlush(f);
		}
		return MsgUtil.ok();
	}
	
}
