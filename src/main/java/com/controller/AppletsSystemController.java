package com.controller;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.entity.*;
import com.service.MjFamilyRelationService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dao.CouponDao;
import com.dao.DictionariesDao;
import com.dao.ProblemDao;
import com.dao.ReferralDao;
import com.dao.UserDao;
import com.util.HttpClientUtil;
import com.util.JsonUtils;
import com.util.MsgUtil;
import com.util.ReadUtil;
import com.vo.Introduce;
import com.vo.SysExplain;
import com.vo.VXInfoVo;
import com.vo.VXuserInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
/**
 * 小程序系统操作后台
 * @author Administrator
 *
 */
@RestController
@RequestMapping("applets")
@Api(value="小程序后台管理",tags={"小程序后台管理"})
@Validated
@CrossOrigin
public class AppletsSystemController {

	@Autowired private  UserDao userDao;
	@Autowired private  CouponDao couponDao;
	@Autowired private  ProblemDao problemDao;
	@Autowired private  DictionariesDao dicDao;
	@Autowired private  ReferralDao referralDao;
	@Autowired private  UserDao UserDao;

	@Autowired
	private com.service.MjFamilyRelationService MjFamilyRelationService;
	
	/**  1. 新增openid */
	@RequestMapping("setOpenId")
	public Object setOpenId(String code,HttpServletRequest request,String openId) {
		
		Users user = userDao.getByOpenId(openId);
		
		//没有此用户信息 就新增
		if(user==null) {
			String url = "https://api.weixin.qq.com/sns/jscode2session";//vx_intface 获取登陆信息的接口
			Map<String, String> param = new HashMap<String, String>();
			
			param.put("appid", ReadUtil.read("XCX_appid"));
			param.put("secret", ReadUtil.read("XCX_secret"));
			param.put("js_code", code);
			param.put("grant_type", "authorization_code");
			
			String wxResult = HttpClientUtil.doGet(url, param);
			
			VXInfoVo model = JsonUtils.jsonToPojo(wxResult, VXInfoVo.class);
			if(null==model || StringUtils.isBlank(model.getOpenid())){
				MsgUtil.fail();
			}
			//避免重复注册
			user = userDao.getByOpenId(model.getOpenid());
			if(null!=user) {
				MsgUtil.ok(user);
			}
			user=new Users();
			user.setOpenId(model.getOpenid());
			userDao.save(user);
		}
			return MsgUtil.ok(user);
	}
	
	@RequestMapping("setUserInfo")
    @ApiOperation(value="新增用户信息", notes="新增用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userInfo", value = "微信用户信息(avatarUrl;country;province;language;nickName;city;)", required = true, dataType = "object"),
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "pwd", value = "密码", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "parentId", value = "邀请人的OpenId", required = true, dataType = "String"),
    })
	public Object setUserInfo(VXuserInfo userInfo,String openId,String pwd,String userId,String parentId) {
		
		Users user=userDao.getByOpenId(openId);
		Users invite=userDao.getByOpenId(parentId);//邀请的用户
		if(user==null) {
			user=new Users();
			user.setHeadPortrait(userInfo.getAvatarUrl());
			user.setAddress(userInfo.getCountry()+"国"+userInfo.getProvince()+"省"+userInfo.getCity()+"市");
			user.setName(userInfo.getNickName());
			user.setSex("1".equals(userInfo.getGender())?"男":"女");
			user.setOpenId(openId);
			user.setMj_pwd(pwd);
			user.setMj_id(userId);
			user.setPhone(userInfo.getPhone());
			if(invite!=null)user.setInviteId(invite.getId());
			userDao.saveAndFlush(user);
		}
		return MsgUtil.ok();
}

	/** 列表  */ 
	@RequestMapping("getCoupon")
	@ApiOperation(value="获取优惠券", notes="获取优惠券")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer"),
		@ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "Integer"),
	})
	public Object get(String openId,String tel) {
		
		Users  user = userDao.getByOpenId((String) openId);

		List<Coupon> list = new ArrayList<>();

		//判断是否注册
		if(user==null) {
			int id = isRegister(openId,tel);
			list = couponDao.getByUserId(id);
		}else{
			list = couponDao.getByUserId(user.getId());
		}

		return MsgUtil.ok(list);
	}

	public int isRegister(String openId,String phone) {
		String pwd=phone.substring(phone.length()-4,phone.length())+new SimpleDateFormat("sSSS").format(new Date());
		String msg="";
		Users user = new Users();
		user.setOpenId(openId);
		user.setPhone(phone);
		user.setMj_pwd(pwd);
		user.setIsBuyGoods("N");
		user.setIsBuySystem("N");
		user.setWifiPwd(pwd);
		UserDao.saveAndFlush(user);
		return user.getId();
	}
	
	@RequestMapping("relation")
	@ApiOperation(value="获取关系数据", notes="获取关系数据")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer")})
	public Object relation(String openId) {

		List<MjFamilyRelation> list = MjFamilyRelationService.findFamilyRelation();
		if(list.size()>0 && list!=null){
			List<Map<String,String>> relationList = new ArrayList<>();
			for (MjFamilyRelation  mjFamilyRelation : list ) {
				Map<String,String> map = new HashMap<>();
				map.put("relation",mjFamilyRelation.getRelation());
				map.put("sex",mjFamilyRelation.getSex());
				relationList.add(map);
			}
			return MsgUtil.ok(relationList);
		}else{
			return MsgUtil.fail("暂无关系参数");
		}


	}
	
	@RequestMapping("introduce")
	@ApiOperation(value="介绍页面", notes="介绍页面")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer")})
	public Object introduce(String openId) {
		
        List<Referral> list =referralDao.findAll();
		return MsgUtil.ok(list);
	}
	
	@RequestMapping("problem")
	@ApiOperation(value="常见问题", notes="常见问题")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer")})
	public Object problem(String openId) {
		List<Problem> list = problemDao.findAll();
		return MsgUtil.ok(list);
	}
	
	@RequestMapping("customerService")
	@ApiOperation(value="客服", notes="客服")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer")})
	public Object customerService(String openId) {
		return MsgUtil.ok(ReadUtil.read("customerService"));
	}
	
	@RequestMapping("systemPrice")
	@ApiOperation(value="管家售价", notes="管家售价")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer")})
	public Object systemPrice(String openId) {
		Dictionaries d =dicDao.getByKey("system_price");
		return MsgUtil.ok(d.getDicValue());
	}

	@RequestMapping("bindExplain")
	@ApiOperation(value="绑定说明", notes="绑定说明")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer")})
	public Object bindExplain(String openId) {
		Dictionaries d =dicDao.getByKey("bind_explain");
		return MsgUtil.ok(d.getDicValue());
	}
	@RequestMapping("scustomerServiceysExplain")
	@ApiOperation(value="系统说明", notes="系统说明")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer")})
	public Object sysExplain(String openId) {
		SysExplain sys = new SysExplain();
		sys.setPic(dicDao.getByKey("system_explain_pic").getDicValue());
		sys.setContent(dicDao.getByKey("system_explain_content").getDicValue());
		return MsgUtil.ok(sys);
	}
	
}
