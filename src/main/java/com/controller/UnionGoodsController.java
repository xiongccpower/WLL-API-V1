package com.controller;

import cn.hutool.core.util.StrUtil;
import com.util.MsgUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("platform-goods")
@Validated
@Api(value="联盟平台商品类",tags={"联盟平台商品类"})
@CrossOrigin
public class UnionGoodsController {
    protected static Logger log = LoggerFactory.getLogger(UnionGoodsController.class);

    @ApiOperation(value="查询各联盟商品详情", notes="查询各联盟商品详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commodityCode", value = "commodityCode", required = true, dataType = "String"),
            @ApiImplicitParam(name = "platform", value = "platform", required = true, dataType = "String")
    })
    @PostMapping("getUnionGoods")
    public Object getUnionGoods(String commodityCode,String platform){
        Object result = new Object();
        if(!StrUtil.hasEmpty(commodityCode) && !StrUtil.hasEmpty(platform)){

        }else{
            result = MsgUtil.badArgument();
        }
        return result;
    }


}
