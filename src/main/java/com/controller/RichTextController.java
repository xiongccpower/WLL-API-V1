package com.controller;

import com.entity.RichText;
import com.service.RichTextService;
import com.util.MsgUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("richtext")
@Validated
@Api(value="富文本文案",tags={"富文本文案"})
@CrossOrigin
public class RichTextController {

    @Resource
    private RichTextService service;

    @GetMapping("getRich/{id}")
    public Object getRich(@PathVariable("id") Long id){
        RichText richText = service.getById(id);
        if (richText!=null){
            return MsgUtil.ok(richText);
        }else {
            return MsgUtil.fail();
        }
    }
}
