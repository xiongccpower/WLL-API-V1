package com.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.commonly.SemanticsApiUtil;
import com.entity.MjFamilyRelation;
import com.entity.WitGoodsBrandLexicon;
import com.entity.WitGoodsLexicon;
import com.service.MjFamilyRelationService;
import com.service.WitGoodsBrandLexiconService;
import com.service.WitGoodsLexiconService;
import com.util.MsgUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("semantics")
@Validated
@Api(value="语义api接口",tags={"语义api接口"})
@CrossOrigin
public class SemanticsApiController {
    protected static Logger log = LoggerFactory.getLogger(SemanticsApiController.class);
    @Autowired
    private MjFamilyRelationService mjFamilyRelationService;
    @Autowired
    private WitGoodsLexiconService witGoodsLexiconService;
    @Autowired
    private WitGoodsBrandLexiconService witGoodsBrandLexiconService;

    @RequestMapping("participle/api")
    @ApiOperation(value="分词api接口", notes="分词api接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "words", value = "words", required = true, dataType = "String"),
    })
    public Object participleApi(String words){
        Object result = new Object();
        if(!StrUtil.hasEmpty(words)){
            Map<String,String> map = SemanticsApiUtil.participle(words);
            if(map.get("code").equals("0")){
                JSONArray data = JSONUtil.parseArray(map.get("data"));
                JSONObject json = new JSONObject();

                String call = "";
                String sex = "";
                String goodsName = "";
                String brandName = "";

                for(int i=0;i<data.size();i++){
                    JSONObject ke = data.getJSONObject(i);

                    String word = ke.get("word").toString();

                    //查询家庭称呼表
                    MjFamilyRelation mjFamilyRelation = mjFamilyRelationService.getMjFamilyRelation(word);
                    if(mjFamilyRelation!=null){
                        call = mjFamilyRelation.getRelation();
                        sex = mjFamilyRelation.getSex();
                    }
                    if(word.equals("全家")){
                        call = "全家";
                    }
                    if(word.equals("其他")){
                        call = "其他";
                    }
                    //查询商品词库表
                    WitGoodsLexicon witGoodsLexicon = witGoodsLexiconService.getWitGoodsLexicon(word);
                    if(witGoodsLexicon!=null){
                        goodsName = witGoodsLexicon.getGoodsName();
                    }
                    WitGoodsBrandLexicon witGoodsBrandLexicon = witGoodsBrandLexiconService.getWitGoodsBrandLexicon(word);
                    if(witGoodsBrandLexicon!=null){
                        brandName = witGoodsBrandLexicon.getBrandName();
                    }
                }

                if(!StrUtil.hasEmpty(call)){
                    json.put("call",call);
                    json.put("sex",sex);
                }
                if(!StrUtil.hasEmpty(goodsName)){
                    json.put("goodsName",goodsName);
                }
                if(!StrUtil.hasEmpty(brandName)){
                    json.put("brandName",brandName);
                }
                json.put("data",data);

                result = MsgUtil.ok(json);

            }else{
                result = MsgUtil.fail(map.get("msg"));
            }
        }else{
            result = MsgUtil.badArgument();
        }
        return result;
    }

}
