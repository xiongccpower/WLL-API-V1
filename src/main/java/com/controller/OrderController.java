package com.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.api.alipay.AliPayService;
import com.api.smart.constant.Constant;
import com.api.smart.utils.RequestUtil;
import com.api.sms.SmsSendClient;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dao.*;
import com.domain.*;
import com.entity.Order;
import com.entity.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.service.*;
import com.util.*;
import com.vo.Address;
import com.wll.wulian.domain.Device;
import com.wll.wulian.domain.Instorage;
import com.wll.wulian.domain.Repertory;
import com.wll.wulian.intetfaces.DeviceService;
import com.wll.wulian.intetfaces.InstorageService;
import com.wll.wulian.intetfaces.RepertoryService;
import io.netty.util.internal.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Consumer;

@RestController
@RequestMapping("order")
@Api(value = "订单管理", tags = {"订单管理"})
@Validated
@CrossOrigin
@EnableScheduling
public class OrderController {
    protected static Logger log = LoggerFactory.getLogger(OrderController.class);

    @Resource
    private SmsSendClient smsSendClient;

    @DubboReference
    private InstorageService instorageService;

    @DubboReference
    private RepertoryService repertoryService;

    @DubboReference
    private DeviceService deviceService;

    @Resource
    private BusinessLogService businessLogService;

    @Resource
    private SysPropertiesService sysPropertiesService;

    @Autowired
    private OrderDao dao;
    @Autowired
    private ShopCartDao shopCartDao;
    @Autowired
    private UserDao UserDao;
    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private CouponDao couponDao;
    @Autowired
    private DictionariesDao dictionariesDao;
    @Autowired
    private FacilityDao facilityDao;
    @Autowired
    private UserLikeDao userLikeDao;
    @Autowired
    private InvitePwdDao pwdDao;
    @Autowired
    private MyWalletDao walletDao;
    @Autowired
    private MyWalletLogDao logDao;
    @Autowired
    private MjDzOpenIdDao mjDzOpenIdDao;
    @Autowired
    private MjUserService mjUserService;
    @Autowired
    private InvitePwdDao invitePwdDao;
    @Resource
    private MyWalletScheduleService myWalletScheduleService;

    @Resource
    private SnowflakeIdWorker snowflakeIdWorker;

    @Resource
    private UserStatusService userStatusService;

    @Resource
    private OrderService orderService;

    /**
     * 每分钟执行一次,定时取消超时订单，超时时间15分钟
     */
    @Scheduled(cron = "0 0/1 * * * ?")
    public void cancelOrder() {
        try {
            orderService.cancelDeadOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Resource
    private ThirdOrderService thirdOrderService;

    @Resource
    private ThirdOrderDetailService thirdOrderDetailService;

    /**
     * 每十分钟同步一次订单
     */
    @Scheduled(cron = "0 0/10 * * * ? ")
    public void queryOrder() {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("app_key", Constant.DUOMAI_APP_KEY);
        params.put("timestamp", String.valueOf(System.currentTimeMillis() / 1000));
        params.put("service", "cps-mesh.open.orders.query.get");


        LocalDateTime time = LocalDateTime.now();
//        time = time.withMonth(1).withDayOfMonth(21).withHour(11).withMinute(20);
        long start = time.minusMinutes(30).toEpochSecond(ZoneOffset.ofHours(8));
        long end = time.toEpochSecond(ZoneOffset.ofHours(8));
//2022-01-28 11:15:03
        HashMap<String, Object> p1 = new HashMap<>();
//        p1.put("stime", 1644249600);
//        p1.put("etime", 1644332400);
        p1.put("stime", start);
        p1.put("etime", end);
//        p1.put("order_field", "order_datatime");

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            sb.append(entry.getKey()).append(entry.getValue());
        }

        Optional<String> optP = com.api.smart.utils.JSONUtil.toJSONString(p1);
        String json = "";
        if (optP.isPresent()) {
            json = optP.get();
            sb.append(json);
        }

        sb.insert(0, Constant.DUOMAI_APP_SECRET).append(Constant.DUOMAI_APP_SECRET);
        String sign = MD5.create().digestHex(sb.toString()).toUpperCase();
        params.put("sign", sign);

        WebClient.create(RequestUtil.buildRequestUrl(Constant.DUOMAI_BASE_URL, params))
                .post().contentType(MediaType.APPLICATION_JSON)
                .bodyValue(json).retrieve().bodyToMono(String.class).subscribe((Consumer<String>) s -> {
                    com.api.smart.utils.JSONUtil.parse(s).ifPresent(v -> {
                        if (v.get("status").intValue() == 0) {
                            JsonNode data = v.get("data");
                            List<ThirdOrder> orders = JSON.parseArray(data.toString(), ThirdOrder.class);
                            for (ThirdOrder order : orders) {

                                List<ThirdOrderDetail> details = order.getDetails();
                                for (ThirdOrderDetail detail : details) {
                                    detail.setMtoId(order.getMtoId());
                                }
                                QueryWrapper<ThirdOrderDetail> query = new QueryWrapper<>();
                                query.lambda().eq(ThirdOrderDetail::getMtoId, order.getMtoId());


                                thirdOrderDetailService.remove(query);
                                thirdOrderDetailService.saveOrUpdateBatch(details);

                                String euid = order.getEuid();
                                try {
                                    euid = URLDecoder.decode(euid, "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                Optional<JsonNode> opt = com.api.smart.utils.JSONUtil.parse(euid);
                                if (opt.isPresent()) {
                                    JsonNode res = opt.get();
                                    if (res.isInt()) {
                                        order.setUid(res.intValue());
                                    } else {
                                        int uid = res.get("u").asInt();
                                        order.setUid(uid);
                                        JsonNode node = res.get("s");
                                        if (node != null) {
                                            order.setSource(node.asText());
                                        }
                                    }


                                    InvitePwd pwd = invitePwdDao.getByOpenId(String.valueOf(order.getUid()));
                                    if (pwd == null) {
                                        // 暂时跳过，等待登录注册后再匹配入账
                                        continue;
                                    }
                                    MyWalletLog log = logDao.getByOrderNo(String.valueOf(order.getMtoId()));
                                    MyWallet wallet = walletDao.getByCode(pwd.getInvitePwd());

                                    String confirm = order.getConfirmSiterCommission();
                                    if (order.getStatus() == 2) {
                                        if (log == null) {
                                            // 未创建该单据，跳过
                                            continue;
                                        } else {
                                            if (log.getStatus() == 1) {
                                                // 订单已确认，发放金额
                                                BigDecimal fee = new BigDecimal(confirm);
                                                fee = fee.multiply(new BigDecimal("0.3")).setScale(2, RoundingMode.HALF_UP);
                                                BigDecimal wa = BigDecimal.valueOf(wallet.getWait_money()).subtract(fee);
                                                if (wa.compareTo(new BigDecimal(0)) < 0) {
                                                    continue;
                                                }
                                                wallet.setWait_money(wa.floatValue());
                                                wallet.setMoney(BigDecimal.valueOf(wallet.getMoney()).add(fee).floatValue());
                                                log.setStatus(1);
                                                log.setRemark2("已入账");
                                                log.setUpdatetime(new Date());
                                                walletDao.saveAndFlush(wallet);
                                                logDao.saveAndFlush(log);

                                                // 判断是否为首单，如果为首单额外进行奖励
                                            } else {
                                                // 金额已发放，跳过
                                                continue;
                                            }
                                        }
                                    } else if (order.getStatus() == 0 || order.getStatus() == 1) {
                                        // 订单未确认，暂不发放金额
                                        String esti = order.getSiterCommission();
                                        BigDecimal fee = new BigDecimal(esti);
                                        fee = fee.multiply(new BigDecimal("0.3")).setScale(2, RoundingMode.HALF_UP);
                                        if (log == null) {

                                            wallet.setTotal_money(BigDecimal.valueOf(wallet.getTotal_money()).add(fee).floatValue());
                                            wallet.setWait_money(BigDecimal.valueOf(wallet.getWait_money()).add(fee).floatValue());

                                            MyWalletLog mLog = new MyWalletLog();
                                            mLog.setMoney(fee.floatValue());
                                            mLog.setOrder_code(String.valueOf(order.getMtoId()));
                                            mLog.setRemark1("智选购物返现");
                                            mLog.setRemark2("待入账");
                                            mLog.setCode(pwd.getInvitePwd());
                                            mLog.setUid(order.getUid());
                                            mLog.setCreateTime(new Date());
                                            mLog.setUpdatetime(new Date());
                                            mLog.setStatus(1);
                                            mLog.setType(1);
                                            logDao.saveAndFlush(mLog);
                                            walletDao.saveAndFlush(wallet);

                                            // 判断是否为首单，如果为首单额外进行奖励
                                            UserStatus exist = userStatusService.findByOpenId(pwd.getUid());
                                            if (exist != null && exist.getFirst() != null && exist.getFirst() != 0) {
                                                SysProperties properties = sysPropertiesService.getById(1);
                                                try {
                                                    BigDecimal value = new BigDecimal(properties.getValue());
                                                    wallet.setTotal_money(BigDecimal.valueOf(wallet.getTotal_money()).add(value).floatValue());
                                                    wallet.setWait_money(BigDecimal.valueOf(wallet.getWait_money()).add(value).floatValue());

                                                    MyWalletLog a = new MyWalletLog();
                                                    a.setMoney(value.floatValue());
                                                    a.setOrder_code(String.valueOf(order.getMtoId()));
                                                    a.setRemark1("管家首单返现");
                                                    a.setRemark2("待入账");
                                                    a.setCode(pwd.getInvitePwd());
                                                    a.setUid(order.getUid());
                                                    a.setCreateTime(new Date());
                                                    a.setUpdatetime(new Date());
                                                    a.setStatus(1);
                                                    a.setType(1);
                                                    logDao.saveAndFlush(a);
                                                } catch (Exception e) {

                                                }
                                            }
                                        } else {
                                            // 已录入，跳过
                                            continue;
                                        }
                                    } else if (order.getStatus() == -1) {
                                        // 取消
                                        Long mtoId = order.getMtoId();
                                        MyWalletLog exist = logDao.getByOrderNo(mtoId.toString());
                                        if (exist != null && exist.getStatus() == 1) {
                                            exist.setStatus(2);
                                            exist.setRemark1("已取消");
                                            exist.setRemark2("已入账");
                                            wallet.setWait_money(BigDecimal.valueOf(wallet.getWait_money()).add(BigDecimal.valueOf(-exist.getMoney())).floatValue());
                                            wallet.setTotal_money(BigDecimal.valueOf(wallet.getTotal_money()).add(BigDecimal.valueOf(-exist.getMoney())).floatValue());
                                            logDao.saveAndFlush(exist);
                                            walletDao.saveAndFlush(wallet);
                                        }

                                    }
                                }
                            }
                            thirdOrderService.saveOrUpdateBatch(orders);
                        }
                    });
                });
    }


    /**
     * 每20分钟小时同步一次订单
     */
    @Scheduled(cron = "0 0/20 * * * ? ")
    public void syncOrder() {
        myWalletScheduleService.syncOrder();
    }

    /**
     * 列表
     *
     * @throws Exception
     */
    @RequestMapping("save")
    @ApiOperation(value = "下单", notes = "下单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "shopCartIds", value = "(购物车的Ids数组(多个) ','分割)(goods,‘商品id’,‘数量’,‘规格’) ", required = true, dataType = "List<Integer>"),
            @ApiImplicitParam(name = "couponId", value = "优惠券ID", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "address", value = "收货地址", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "addressee", value = "收件人", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "phone", value = "收件人电话", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "payTotal", value = "最终支付价格", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "invoice", value = "是否需要开票(Y/N)", required = true, dataType = "String"),
            @ApiImplicitParam(name = "goodsId", value = "商品主键", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "uid", value = "用户登录id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "tel", value = "用户注册手机号", required = true, dataType = "Integer"),
    })
    public Object save(String openId, String shopCartIds, Integer couponId, Address address, double payTotal, String invoice, Integer goodsId, String uid, String tel) throws Exception {


        Order order = new Order();
        Users user = UserDao.getByOpenId((String) openId);
        //判断是否注册
        if (user == null) {
            int id = isRegister(openId, tel);
            user.setId(id);
        }

        Coupon c = null;

        String orderNo = "ZNSB" + System.currentTimeMillis();//订单号
        String[] str = shopCartIds.split(",");
        List<Integer> ids = new ArrayList<Integer>();
        String scIds = shopCartIds;
        Goods good = goodsDao.getById(goodsId);

        List<ShopCart> sc = new ArrayList<ShopCart>();
        if (shopCartIds.startsWith("goods")) {//直接下单

            ids.add(Integer.parseInt(str[1]));
            ids.add(Integer.parseInt(str[2]));
            String goodsStr[] = shopCartIds.split(",");
            ShopCart obj = new ShopCart();
            Goods goods = goodsDao.getById(Integer.parseInt(goodsStr[1]));
            obj.setGoods(goods);
            obj.setNumber(Integer.parseInt(goodsStr[2]));
            obj.setGoodsId(goods.getId());
            obj.setType(str[3]);
            obj.setUserId(user.getId());
            sc.add(obj);
            ShopCart zjgm = shopCartDao.saveAndFlush(obj);
            scIds = zjgm.getId() + ",";
        } else {
            List<String> list = Arrays.asList(shopCartIds.split(","));
            ids = ListStringToInt(list);
            sc = shopCartDao.findAllById(ids);
        }


        user.setIsBuyGoods("Y");
        order = scToInventory(sc);

        order.setScIds(scIds);
        order.setAddress(address.getAddress());
        order.setAddressee(address.getAddressee());
        order.setPhone(address.getPhone());
        order.setPayTotal(payTotal);
        order.setUserId(user.getId());
        order.setCoupon(couponId);
        order.setCreateTime(new Date());
        order.setState(-1);
        order.setGoodsName(good.getTitle());//陈洋 11.13
        SimpleDateFormat sdf = new SimpleDateFormat("yyyymmddHHmmss");
        order.setOrderNo(sdf.format(new Date()));
        if (StringUtil.isNullOrEmpty(invoice)) {
            invoice = "N";
        }
        order.setInvoice(invoice);
        order.setFreight(0);
        order.setOrderNo(orderNo);
        double paytotal = 0;
        if (couponId != null && couponId != 0) {
            c = couponDao.getOne(couponId);
            if ("*".equals(c.getAlgorithm())) {
                paytotal = order.getTotal() * c.getNumber();
            } else if ("-".equals(c.getAlgorithm())) {
                paytotal = order.getTotal() - c.getNumber();
            }
            couponDao.delete(c);
        }

        order.setScIds(scIds);
        order.setPayTotal(payTotal);
//		order.setUid(uid);
        dao.save(order);

        //录入到订单总表
        JSONObject msg = FunctionUtil.pay(order, openId);
        return MsgUtil.ok(msg);
    }

    @RequestMapping("find/buy/record")
    @ApiOperation(value = "查询有无购买记录", notes = "查询有无购买记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
    })
    public Object findBuyRecord(String uid, String openId) {
        Object result = new Object();

        List<Order> orderList = new ArrayList<>();
        if (!StrUtil.hasEmpty(uid)) {
            orderList = dao.findBuyRecord(uid);
            if (orderList.size() == 0) {
                orderList = dao.findBuyRecordOpenid(openId);
            }
        } else {
            orderList = dao.findBuyRecordOpenid(openId);
        }

        if (orderList.size() > 0 && orderList != null) {
            result = MsgUtil.ok("purchase");
        } else {
            result = MsgUtil.ok("notPurchase");
        }

        return result;
    }


    /**
     * 列表
     *
     * @throws Exception
     */
    @RequestMapping("payTest")
    @ApiOperation(value = "测试购买系统", notes = "测试购买系统")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "address", value = "收货地址", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "addressee", value = "收件人", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "phone", value = "收件人电话", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "number", value = "数量", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "code", value = "邀请码", required = true, dataType = "String"),
            @ApiImplicitParam(name = "value", value = "区", required = true, dataType = "double"),
            @ApiImplicitParam(name = "uid", value = "用户登录id", required = true, dataType = "Integer"),
    })
    public Object payTest(Integer orderId, String openId, String address, String addressee, String phone, Integer number, Double value, String code, String uid, Integer suId, Integer o, Long mbeId) throws Exception {

        String orderNo = "ZHGJ" + snowflakeIdWorker.nextId();//订单号

        Order order = new Order();
        order.setCreateTime(new Date());
        order.setAddress(address);
        order.setAddressee(addressee);
        order.setPhone(phone);
        order.setOrderNo(orderNo);
        order.setPayTotal(value);
        order.setTotal(value);
        order.setScIds("test");
        order.setGoodsIds(0 + "");//2020.11.11
        order.setGoodsName("智能系统");
        List<Inventory> list = new ArrayList<Inventory>();
        Inventory inventory = new Inventory();
        inventory.setNumber(1);
        inventory.setPrice(0.01);
        inventory.setTitle("物联智慧管家");
        inventory.setType("默认");
        inventory.setNumber(number);
        inventory.setGoodsId(0);
        list.add(inventory);
        order.setInventory(list);
        order.setCode(code);
        // 待付款
        order.setState(9);
        order.setUid(uid);
        order.setStoreName("未来里物联");
        order.setOpenId(openId);
        //录入到订单总表
        JSONObject msg = FunctionUtil.pay(order, openId);

        return MsgUtil.ok(msg);
    }

    @Resource
    private AliPayService aliPayService;

    /**
     * 列表
     *
     * @throws Exception
     */
    @RequestMapping("paySystem")
    @ApiOperation(value = "购买系统", notes = "购买系统")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "address", value = "收货地址", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "addressee", value = "收件人", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "phone", value = "收件人电话", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "number", value = "数量", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "code", value = "邀请码", required = true, dataType = "String"),
            @ApiImplicitParam(name = "value", value = "区", required = true, dataType = "double"),
            @ApiImplicitParam(name = "uid", value = "用户登录id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "entrance", value = "购买入口", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "payType", value = "支付方式 0:微信 1:支付宝", required = true, dataType = "Integer"),
    })
    public Object paySystem(Integer orderId, String openId, String address, String addressee, String phone, Integer number, Double value, String code, String uid, Integer suId, Integer o, Long mbeId, Integer entrance, Integer payType, String extra) throws Exception {
        if (payType == null) {
            payType = 0;
        }
        if (orderId != null) {
            // 查找已有的订单信息
            Order exist = dao.getById(orderId);
            if (exist == null) {
                return MsgUtil.fail("未查询到该订单");
            }
            JSONObject msg = FunctionUtil.pay(exist, openId);

            return MsgUtil.ok(msg);
        }

        String name = "";
        if (uid == null) {
            MjUser exist = mjUserService.getUserByPhone(phone);
            if (exist != null) {
                uid = exist.getId().toString();
                name = exist.getNickname();
            }
        }

        Integer userId = null;

        Users user = UserDao.getByOpenId((String) openId);
        //判断是否注册

        if (user == null) {
            if (openId != null) {
                userId = isRegister(openId, phone);
            }
//            user.setId(id);
        } else {
            userId = user.getId();
        }


        System.out.println("value=" + value);
        String picture = dictionariesDao.getByKey("system_picture").getDicValue();
        String orderNo = "ZHGJ" + snowflakeIdWorker.nextId();//订单号
        List<Inventory> list = new ArrayList<Inventory>();

        if (extra != null) {
            try {
                com.alibaba.fastjson.JSONArray array = JSON.parseArray(extra);
                for (Object o1 : array) {
                    JSONObject item = (JSONObject) o1;
                    Inventory in = new Inventory();
                    in.setNumber(item.getInteger("number"));
                    in.setPrice(item.getDouble("price"));
                    in.setTitle(item.getString("name"));
                    in.setPicUrl(item.getString("url"));
                    in.setGoodsId(item.getInteger("goodsId"));
                    in.setType("默认");
                    list.add(in);
                }
            } catch (Exception e) {
            }
        } else {
            Inventory inventory = new Inventory();
            inventory.setNumber(1);
            inventory.setPrice(Double.parseDouble(dictionariesDao.getByKey("system_price").getDicValue()));
            inventory.setTitle("物联智慧管家");
            inventory.setType("默认");
            inventory.setPicUrl(picture);
            inventory.setNumber(number);
            inventory.setGoodsId(0);
            list.add(inventory);
        }

        Order order = new Order();
        order.setCreateTime(new Date());
        order.setAddress(address);
        order.setAddressee(addressee);
        order.setPhone(phone);
        order.setOrderNo(orderNo);
        order.setPayTotal(value);
        order.setTotal(value);
        order.setInventory(list);
        order.setScIds("system");
        order.setUserId(userId);
        order.setGoodsIds(0 + "");//2020.11.11
        order.setGoodsName("智能系统");

        order.setCode(code);
        // 待付款
        order.setState(9);
        order.setUid(uid);
        order.setStoreName("未来里物联");
        order.setOpenId(openId);
        //出库
        Instorage instorage = new Instorage();
        instorage.setCommodityId(1);
        instorage.setNumber(-1);
        instorage.setComment("用户购买");
        boolean b = instorageService.addInStorage(instorage);
        if (!b) {
            return MsgUtil.fail(1, "库存不足");
        }
        //录入到订单总表
        JSONObject msg;
        if (payType == 0) {
            if (openId == null) {
                msg = FunctionUtil.pay(order);
            } else {
                if (entrance != null && entrance == 1) {
                    msg = FunctionUtil.pay1(order, openId);
                } else {
                    msg = FunctionUtil.pay(order, openId);
                }
            }
        } else if (payType == 1) {
            AlipayTradeWapPayModel bizModel = new AlipayTradeWapPayModel();
            bizModel.setOutTradeNo(orderNo);
            bizModel.setTotalAmount(String.valueOf(order.getPayTotal()));
            String body = "";
            if (order.getInventory() == null) {
                body = order.getGoodsName();
            } else {
                body = order.getInventory().get(0).getTitle();
            }
            bizModel.setSubject(body);
            bizModel.setProductCode("QUICK_WAP_WAY");
            return aliPayService.pay(bizModel);
        } else {
            msg = new JSONObject();
        }

        msg.put("uid", uid);
        msg.put("username", name);

        if (suId != null) {
            BusinessLog log = new BusinessLog();
            log.setNum(number);
            log.setOrder(o);
            log.setMbeId(mbeId);
            log.setStatus(1);
            log.setRemark("待付款");
            log.setCreateTime(new Date());
            log.setOpenId(openId);
            log.setType((byte) 3);
            businessLogService.save(log);
            order.setSuId(log.getMblId());
        }
        dao.saveAndFlush(order);
        msg.put("orderId", order.getId());
        return MsgUtil.ok(msg);
    }

    /**
     * 购买第三方设备
     */
    @RequestMapping("payDevice")
    public Object payDevice(Integer orderId, String openId, String phone, Integer deviceId, String address, String addressee, String uid, Double pay) {
        if (orderId != null) {
            // 查找已有的订单信息
            Order exist = dao.getById(orderId);
            if (exist == null) {
                return MsgUtil.fail("未查询到该订单");
            }
            JSONObject msg = FunctionUtil.pay(exist, openId);
            return MsgUtil.ok(msg);
        }
        //判断是否注册
        Users user = UserDao.getByOpenId((String) openId);
        if (user == null) {
            int id = isRegister(openId, phone);
            user.setId(id);
        }
        Device device = deviceService.getById(deviceId);
        Order order = new Order();
        order.setOrderNo("ZNSB" + snowflakeIdWorker.nextId());
        order.setCreateTime(new Date());
        order.setAddress(address);
        order.setAddressee(addressee);
        order.setPhone(phone);
        order.setScIds("device");
        order.setState(9); //待付款
        order.setUid(uid);
        order.setGoodsIds(deviceId + ""); //设备id
        order.setCode(""); //不知作用
        order.setUserId(user.getId());
        order.setOpenId(openId);
        order.setStoreName("未来里物联");
        order.setPayTotal(pay);
        order.setGoodsName(device.getName());
        order.setGoodsImg(device.getImgUrl());
        //出库
        Repertory one = repertoryService.getByDeviceId(deviceId);
        Instorage instorage = new Instorage();
        instorage.setCommodityId(one.getId());
        instorage.setNumber(-1);
        instorage.setComment("用户购买");
        boolean b = instorageService.addInStorage(instorage);
        if (!b) {
            return MsgUtil.fail(1, "库存不足");
        }
        JSONObject msg = FunctionUtil.pay(order, openId);
        dao.saveAndFlush(order);
        return MsgUtil.ok(msg);
    }

    /***
     * 微信支付回调地址
     * @param request
     * @param response
     */
    @RequestMapping("payCallback")
    public void payCallback(HttpServletRequest request, HttpServletResponse response) {
        String inputLine = "";
        String notityXml = "";

        int i = 10000 + (int) (Math.random() * 99999 - 10000);
        String pwd = String.valueOf(i);
        try {
            notityXml = PayUtils.readXmlFromStream(request);
            Map<String, String> map = PayUtils.doXMLParse(notityXml);
            log.info("[payAsyncNotifyVerificationSign] [xml转换为map数据成功] [notifyMapData:{}]", map);
            if ("SUCCESS".equals(map.get("result_code"))) {
                String outTradeNo = map.get("out_trade_no");
                Order order = dao.getByOrderNo(outTradeNo);
                order.setTransactionId(map.get("transaction_id"));
                String tel = order.getPhone();
                if ("system".equals(order.getScIds())) {//如果是购买系统
                    // 没有归属的
                    MjUser exist = mjUserService.getUserByPhone(tel);
                    if (order.getUid() == null || order.getUid().equals("") || order.getUid().equals("null")) {
                        if (exist != null) {
                            order.setUid(String.valueOf(exist.getId()));
                        }
                    }

                    Users user = UserDao.getOne(order.getUserId());
                    user.setIsBuySystem("Y");
                    order.setState(1);
                    UserDao.saveAndFlush(user);
                    log.info(JSON.toJSONString(order) + ":" + tel + ":" + pwd);

                    if (exist == null) {
                        JSONObject obj = FunctionUtil.userRegister(order.getAddress(), "", "", "", "", tel, pwd, "", "中国", "", "", "null".equals(order.getUid()) ? "0" : order.getUid()); //注册美捷生活小程序
                        if ("true".equals(obj.getString("success"))) {
                            smsSendClient.sendSms(tel, "【未来里】您已购买未来里管家，登录未来里管家小程序查看订单，登录账号" + tel + "，默认密码" + pwd);
//                        JSONObject jo = FunctionUtil.senderUserPwd(tel, pwd, order.getUid()); //发送密码
                        }
                        if (order.getUid() == null || order.getUid().equals("") || order.getUid().equals("null")) {
                            JSONObject data = obj.getJSONObject("data");
                            if (data != null) {
                                String id = data.getString("id");
                                order.setUid(id);
                            }
                        }
                    } else {
                        order.setUid(exist.getId().toString());
                    }


                    UserLike like = userLikeDao.getByOpenId(user.getOpenId());
                    if (like != null) {
                        like.setIsBuy(1);
                        like.setLikeCount(0);
                        userLikeDao.saveAndFlush(like); //清空点赞数据
                    }

                    List<MjDzOpenId> list = mjDzOpenIdDao.getByOpenId(user.getOpenId());
                    if (list != null) {
                        for (int a = 0; a < list.size(); a++) {
                            mjDzOpenIdDao.deleteById(list.get(a).getId());
                        }
                    }//删除点赞人
                    if (!"".equals(order.getCode())) {
                        InvitePwd pwds = pwdDao.getByInvitePwd(order.getCode());
                        if (pwds != null) {
                            Map<String, Object> resultMap2 = FunctionUtil.getLikeCountAndMoney("1");
                            Double abc = (Double) resultMap2.get("awardMoney");
                            String getMoney = "" + abc;
                            MyWallet wallet = walletDao.getByCode(pwds.getInvitePwd());
                            MyWalletLog log = new MyWalletLog();
//
                            float money = Float.parseFloat(getMoney);
                            if (wallet != null) {
                                wallet.setWait_money(money + wallet.getWait_money()); //待入账金额
                                wallet.setTotal_money(money + wallet.getTotal_money()); //总金额
                                walletDao.saveAndFlush(wallet);
                                log.setCode(pwds.getInvitePwd());
                                log.setMoney(money);
                                log.setStatus(1);
                                log.setOrder_code(order.getOrderNo());
                                log.setRemark1("推荐购买奖励");
                                log.setRemark2("待入账");
                                log.setType(1);
                                log.setCreateTime(new Date());
                                log.setUpdatetime(new Date());
                                logDao.saveAndFlush(log);
                            } else {
                                MyWallet newWallet = new MyWallet();
                                newWallet.setCode(pwds.getInvitePwd());
                                newWallet.setMoney(0); //可提现金额
                                newWallet.setWait_money(money); //待入账金额
                                newWallet.setTotal_money(money); //总金额
                                newWallet.setStatus(1);
                                newWallet.setType(1);
                                walletDao.saveAndFlush(newWallet);
                                log.setCode(pwds.getInvitePwd());
                                log.setMoney(money);
                                log.setStatus(1);
                                log.setOrder_code(order.getOrderNo());
                                log.setRemark1("推荐购买奖励");
                                log.setRemark2("待入账");
                                log.setType(1);
                                log.setCreateTime(new Date());
                                log.setUpdatetime(new Date());
                                logDao.saveAndFlush(log);
                            }
                        }
                    }

                    if (order.getSuId() != null) {
                        BusinessLog log = businessLogService.getById(order.getSuId());
                        // 开始计入统计
                        log.setStatus(0);
                        log.setRemark("已付款");
                        businessLogService.updateById(log);

                        UserStatus status = userStatusService.findByOpenId(log.getOpenId());
                        if (status != null) {
                            if (status.getBuy() == 0) {
                                status.setFirst((byte) 1);
                                userStatusService.updateById(status);
                            }
                        }
                    }
                    dao.saveAndFlush(order);
                } else if ("device".equals(order.getScIds())) {
                    order.setState(1);
                    dao.saveAndFlush(order);
                } else {
                    order.setState(1);
                    Facility f = null;
                    if (StringUtils.isNotBlank(order.getScIds())) {
                        String scIds[] = order.getScIds().split(",");
                        for (String s : scIds) {
                            ShopCart sc = shopCartDao.getOne(Integer.parseInt(s));
                            f = new Facility();
                            f.setFacilityNo(order.getOrderNo());
                            f.setUserId(order.getUserId());
                            f.setGoodsId(sc.getGoodsId());
                            f.setIsBind("Y");
                            shopCartDao.deleteById(Integer.parseInt(s));
                            facilityDao.saveAndFlush(f);
                        }
                    }
                    if (!"".equals(order.getCode())) {
                        InvitePwd pwds = pwdDao.getByInvitePwd(order.getCode());
                        String getMoney = "" + 0.5;
                        MyWallet wallet = walletDao.getByCode(pwds.getInvitePwd());
                        MyWalletLog log = new MyWalletLog();
//								   int money = Double.valueOf(getMoney).intValue();//转换为Int类型
                        float money = Float.parseFloat(getMoney);
                        if (wallet != null) {
                            wallet.setWait_money(money + wallet.getWait_money()); //待入账金额
                            wallet.setTotal_money(money + wallet.getTotal_money()); //总金额
                            walletDao.saveAndFlush(wallet);
                            log.setCode(pwds.getInvitePwd());
                            log.setMoney(money);
                            log.setStatus(1);
                            log.setOrder_code(order.getOrderNo());
                            log.setRemark1("好友消费奖励");
                            log.setRemark2("待入账");
                            log.setType(1);
                            log.setCreateTime(new Date());
                            log.setUpdatetime(new Date());
                            logDao.saveAndFlush(log);
                        } else {
                            MyWallet newWallet = new MyWallet();
                            newWallet.setCode(pwds.getInvitePwd());
//                            newWallet.setTel(pwds.getInvitePwd());
                            newWallet.setMoney(0); //可提现金额
                            newWallet.setWait_money(money); //待入账金额
                            newWallet.setTotal_money(money); //总金额
                            newWallet.setStatus(1);
                            newWallet.setType(1);
                            walletDao.saveAndFlush(newWallet);
                            log.setCode(pwds.getInvitePwd());
                            log.setMoney(money);
                            log.setStatus(1);
                            log.setOrder_code(order.getOrderNo());
                            log.setRemark1("好友消费奖励");
                            log.setRemark2("待入账");
                            log.setType(1);
                            log.setCreateTime(new Date());
                            log.setUpdatetime(new Date());
                            logDao.saveAndFlush(log);
                        }
                    }
                    dao.saveAndFlush(order);
                }
                StringBuffer buffer = new StringBuffer();
                buffer.append("<xml>");
                buffer.append("<return_code>SUCCESS</return_code>");
                buffer.append("<return_msg>OK</return_msg>");
                buffer.append("</xml>");
                PrintWriter writer = response.getWriter();
                writer.print(buffer.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @RequestMapping("list")
    @ApiOperation(value = "订单列表", notes = "订单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "state", value = "商品状态（0查询全部 1待发货  2待收货  3待评价 4已完成 5待支付 ）", required = true, dataType = "Integer")
    })
    public Object list(String openId, Integer state, String uid) {

        List<Order> list = new ArrayList<Order>();
        // 更新无归属订单
        if (!ToolUtil.isEmpty(uid)) {
            if (state == 0) {
                list = dao.getByUid(uid);
            } else if (state > 0) {
                list = dao.getByStateUid(uid, state);
            }
        } else {
            if (state == 0) {
                list = dao.getByOrder(openId);
            } else if (state > 0) {
                list = dao.getByState(openId, state);
            }
        }


        return MsgUtil.ok(list);
    }

    @RequestMapping("getMessagePwd")
    @ApiOperation(value = "获取验证码", notes = "获取验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "phone", required = true, dataType = "String"),
    })
    public Object getMessagePwd(String phone) {

        String num = FunctionUtil.getMessagePwd(phone);

        if ("10001".equals(num)) {
            return MsgUtil.fail("版本过低");
        } else if ("1000".equals(num)) {
            return MsgUtil.fail("当日获取验证码短信数量已达上限");
        }

        return MsgUtil.ok(num);
    }

    @RequestMapping("getById")
    @ApiOperation(value = "获取单个订单", notes = "获取单个订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单ID", required = true, dataType = "Integer")
    })
    public Object getById(String openId, Integer orderId) {

        Order order = dao.getOne(orderId);

        return MsgUtil.ok(order);
    }

    @RequestMapping("logistics")
    @ApiOperation(value = "查看物流信息", notes = "物流信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderNo", value = "orderNo", required = true, dataType = "String")
            //@ApiImplicitParam(name)
    })
    public Object logistics(String openId, String orderNo) {
        Users user = UserDao.getByOpenId((String) openId);
        Order order = dao.getByOrderNo(orderNo);
        try {
            JSONObject obj = FunctionUtil.expressInfo(order.getExpressNum(), order.getExpressNo(), order.getPhone());

            if (obj.get("status").toString().equals("200")) {
                JSONArray jsonArr = JSONUtil.parseArray(obj.get("data"));
                System.out.println("返回快递数据=" + jsonArr);
                return MsgUtil.ok(jsonArr);
            } else {
                return MsgUtil.fail();
            }
        } catch (Exception e) {
            return MsgUtil.fail();
        }

    }

    @RequestMapping("queryExpress")
    @ApiOperation(value = "查看物流信息", notes = "物流信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderNo", value = "orderNo", required = true, dataType = "String")
    })
    public Object queryExpress(String orderNo) {
        Order order = dao.getByOrderNo(orderNo);
        try {
            JSONObject obj = FunctionUtil.expressInfo(order.getExpressNum(), order.getExpressNo(), order.getPhone());

            if (obj.get("status").toString().equals("200")) {
                JSONArray jsonArr = JSONUtil.parseArray(obj.get("data"));
                System.out.println("返回快递数据=" + jsonArr);
                return MsgUtil.ok(jsonArr);
            } else {
                return MsgUtil.fail();
            }
        } catch (Exception e) {
            return MsgUtil.fail();
        }

    }

    @RequestMapping("returnExchange")
    @ApiOperation(value = "退换货", notes = "退换货")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "conent", value = "退换货内容描述", required = true, dataType = "String"),
    })
    public Object returnExchange(OrderRefund refund) {
        Order order = dao.getOne(refund.getOrderId());
//        order.setState(5);//已完成
        order.setAddState(11);
        dao.saveAndFlush(order);
        refund.setService(1);
        orderRefundService.checkAndSave(refund);
        return MsgUtil.ok("成功");
    }

    private static final String SERVER_PATH = "https://wulianlife.mjshenghuo.com/static/";
    //    private static final String FILE_PATH = "E:\\logs\\static\\";
    private static final String FILE_PATH = "/opt/deploy/static/";

    @RequestMapping("upload")
    @ApiOperation(value = "订单图片上传", notes = "订单图片上传")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "conent", value = "退换货内容描述", required = true, dataType = "String"),
    })
    public Object upload(MultipartFile file) {
        String filename = file.getOriginalFilename();
        if (filename == null) {
            return MsgUtil.fail();
        }
        try {
            String id = UUID.randomUUID().toString().replace("-", "");
            String randomFileName = id + filename.substring(filename.lastIndexOf("."));
            String pathname = FILE_PATH + randomFileName;
            FileUtils.writeByteArrayToFile(new File(pathname), file.getBytes());
            return MsgUtil.ok(SERVER_PATH + randomFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return MsgUtil.fail();
    }

    @RequestMapping("disoffReturnExchange")
    @ApiOperation(value = "取消退换货", notes = "退换货")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "conent", value = "退换货内容描述", required = true, dataType = "String"),
    })
    public Object disoffReturnExchange(String openId, Integer orderId, String conent) {
        Order order = dao.getOne(orderId);
        if (order != null) {
//            if (!StrUtil.hasEmpty(order.getExpressNo())) {
//                order.setState(2);//待收货
//            } else {
//                order.setState(1);//待发货
//            }
            order.setAddState(-1);
            dao.saveAndFlush(order);
            return MsgUtil.ok("成功");
        } else {
            return MsgUtil.ok("失败");
        }

    }

    @Resource
    private OrderRefundService orderRefundService;

    @Resource
    private OrderCommentService orderCommentService;

    @RequestMapping("returnMoney")
    @ApiOperation(value = "申请退款", notes = "申请退款")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer"),
    })
    public Object returnMoney(String openId, OrderRefund refund) {
        Order order = dao.getOne(refund.getOrderId());
//        order.setState(6);//申请退款
        order.setAddState(0);

        dao.saveAndFlush(order);
        refund.setService(0);
        orderRefundService.checkAndSave(refund);
        return MsgUtil.ok();
    }

    @RequestMapping("saveComment")
    @ApiOperation(value = "保存评论", notes = "保存评论")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer"),
    })
    public Object saveComment(OrderComment comment) {
        orderCommentService.save(comment);
        return MsgUtil.ok();
    }


    @RequestMapping("cancel")
    @ApiOperation(value = "取消订单", notes = "取消订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer"),
    })
    public Object cancelOrder(String openId, Integer orderId) {
        Order order = dao.getOne(orderId);
//        order.setState(1);//取消退款
        order.setState(10);
        Instorage instorage = new Instorage();
        Repertory repertory;
        if (order.getScIds().equals("system")) {
            repertory = repertoryService.getByDeviceId(1);
            instorage.setCommodityId(1);
            instorage.setNumber(1);
            instorage.setComment("用户取消订单");
        } else {
            repertory = repertoryService.getByDeviceId(Integer.valueOf(order.getGoodsIds()));
            instorage.setCommodityId(repertory.getId());
            instorage.setNumber(1);
            instorage.setComment("用户取消订单");
        }
        repertory.setTotalInventory(repertory.getTotalInventory() - 1);
        repertory.setTotalShipments(repertory.getTotalShipments() - 1);
        repertoryService.updateById(repertory);
        instorageService.addInStorage(instorage);
        dao.saveAndFlush(order);
        return MsgUtil.ok();
    }

    @RequestMapping("disoffReturnMoney")
    @ApiOperation(value = "取消退款", notes = "取消退款")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer"),
    })
    public Object disoffReturnMoney(String openId, Integer orderId) {
        Order order = dao.getOne(orderId);
//        order.setState(1);//取消退款
        order.setAddState(-1);
        dao.saveAndFlush(order);
        return MsgUtil.ok();
    }

    @RequestMapping("passReturnGoods")
    @ApiOperation(value = "同意退换货", notes = "同意退换货")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer"),
    })
    public Object passReturnGoods(Integer orderId) {
        if (orderId == null) {
            return MsgUtil.fail("请传入正确的订单ID");
        }
        Order order = dao.getOne(orderId);
        if (dao == null) {
            return MsgUtil.fail("未查询到该订单");
        }
        order.setAddState(13);
        dao.saveAndFlush(order);
        return MsgUtil.ok();
    }

    @RequestMapping("rejectReturnGoods")
    @ApiOperation(value = "驳回退换货", notes = "驳回退换货")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer"),
    })
    public Object rejectReturnGoods(Integer orderId, String reason) {
        if (orderId == null) {
            return MsgUtil.fail("请传入正确的订单ID");
        }
        Order order = dao.getOne(orderId);
        if (order == null) {
            return MsgUtil.fail("未查询到该订单");
        }
        order.setAddState(12);
        dao.saveAndFlush(order);
        return MsgUtil.ok();
    }

    @RequestMapping("rejectReturnMoney")
    @ApiOperation(value = "拒绝退款", notes = "拒绝退款")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer"),
    })
    public Object rejectReturnMoney(String openId, Integer orderId) {
        if (orderId == null) {
            return MsgUtil.fail("请传入正确的订单ID");
        }
        Order order = dao.getOne(orderId);
        if (order == null) {
            return MsgUtil.fail("未查询到该订单");
        }
//        order.setState(7);//拒绝退款
        order.setAddState(1);
        dao.saveAndFlush(order);
        return MsgUtil.ok();
    }

    @RequestMapping("changeAddress")
    @ApiOperation(value = "修改地址", notes = "修改地址")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "address", value = "地址", required = true, dataType = "String"),
    })
    public Object changeAddress(Integer orderId, String address) {
        Order order = dao.getById(orderId);
        if (null == order) {
            return MsgUtil.fail("订单不存在");
        }
        order.setAddress(address);
        dao.saveAndFlush(order);
        return MsgUtil.ok();
    }


    @RequestMapping("SignFor")
    @ApiOperation(value = "确认收货", notes = "确认收货")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer")
    })
    public Object SignFor(String openId, Integer orderId) {

        Order order = dao.getById(orderId);
        if (null == order) {
            return MsgUtil.fail("订单不存在");
        }
        order.setState(3);//已完成
        if ("system".equals(order.getScIds()) && !"".equals(order.getCode())) {
            InvitePwd pwd = pwdDao.getByInvitePwd(order.getCode());
//			Map<String,Object> resultMap2 = FunctionUtil.getLikeCountAndMoney("1");
//			Double abc = (Double) resultMap2.get("awardMoney");
//			String getMoney = "" + abc;
////			int money = Double.valueOf(getMoney).intValue();//转换为Int类型
//			float money = Float.parseFloat(getMoney);
            MyWalletLog myWalletLog = logDao.getByOrderNo(order.getOrderNo(), 1);
            if (null != myWalletLog) {
                MyWalletSchedule schedule = new MyWalletSchedule();
                schedule.setOrderCode(order.getOrderNo());
                schedule.setCreateTime(LocalDateTime.now());
                schedule.setStatus((byte) 1);
                schedule.setCode(pwd.getInvitePwd());
                myWalletScheduleService.save(schedule);

//                float money = myWalletLog.getMoney();
//                MyWallet wallet = walletDao.getByCode(pwd.getInvitePwd());
//                wallet.setWait_money(wallet.getWait_money() - money);
//                wallet.setMoney(wallet.getMoney() + money);
//
//                MyWalletLog log = logDao.getByOrderNo(order.getOrderNo());
//                log.setRemark2("");
//                logDao.saveAndFlush(log);
//                walletDao.saveAndFlush(wallet);
            }
        } else if (!"system".equals(order.getScIds()) && !"".equals(order.getCode())) {
            InvitePwd pwd = pwdDao.getByInvitePwd(order.getCode());
            String getMoney = "" + 0.5;
            float money = Float.parseFloat(getMoney);//转换为Int类型
            MyWallet wallet = walletDao.getByCode(pwd.getInvitePwd());
            wallet.setWait_money(wallet.getWait_money() - money);
            wallet.setMoney(wallet.getMoney() + money);

            MyWalletLog log = logDao.getByOrderNo(order.getOrderNo(), 1);
            log.setRemark2("已入账");
            logDao.saveAndFlush(log);
            walletDao.saveAndFlush(wallet);
        }
        dao.saveAndFlush(order);
        return MsgUtil.ok();
    }


    @RequestMapping("evaluate")
    @ApiOperation(value = "写评价", notes = "写评价")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderId", value = "订单号", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "evaluate", value = "评价内容", required = true, dataType = "String")
    })
    public Object evaluate(String openId, Integer orderId, String evaluate) {
        Order order = dao.getOne(orderId);
        order.setEvaluate(evaluate);
        order.setState(4);
        dao.saveAndFlush(order);
        return MsgUtil.ok();
    }


    @RequestMapping("wxRefund")//
    @ApiOperation(value = "退款", notes = "退款")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderNo", value = "订单号", required = true, dataType = "Integer")
    })
    public Object wxRefund(Integer orderNo) {
        String msg = "";
        if (orderNo == null) {
            return MsgUtil.fail("请传入正确的订单号");
        }
        Order order = dao.getOne(orderNo);

        if (order == null) {
            return MsgUtil.fail("未查询到订单");
        }

        order.setAddState(2);

        MyWalletLog log = logDao.getByOrderNo(order.getOrderNo(), 1);
        if (log != null) {
            // 扣除佣金
            MyWallet wallet = walletDao.getByCode(log.getCode());
            if (wallet != null) {
                wallet.setTotal_money(wallet.getTotal_money() - log.getMoney());
                if ("".equals(log.getRemark2())) {
                    // 已入账
                    wallet.setMoney(wallet.getMoney() - log.getMoney());
                } else {
                    wallet.setWait_money(wallet.getWait_money() - log.getMoney());
                }

                myWalletScheduleService.cancel(order.getOrderNo());

                walletDao.saveAndFlush(wallet);
                log.setStatus(2);
                log.setRemark1("好友已退款");
                log.setRemark2("已入账");
                log.setType(1);
                log.setUpdatetime(new Date());
                logDao.saveAndFlush(log);
            }
        }

        dao.saveAndFlush(order);

        String orderCode = order.getOrderNo();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        Map wxMap = new HashMap();
        //String code =
        paramMap.put("order_code", orderCode);
        paramMap.put("y_order_code", "T" + snowflakeIdWorker.nextId());
        paramMap.put("total_price", order.getPayTotal());
        try {

            wxMap = FunctionUtil.newrefund(paramMap);
            if ("000".equals(wxMap.get("code"))) {
                dao.saveAndFlush(order);
            } else {
                return WebUtil.error("退款失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return MsgUtil.ok();
//        return WebUtil.ok(wxMap);
    }

    /**
     * todo check
     *
     * @param uid
     * @param money
     * @return
     */
    @RequestMapping("toUserMoney")//
    @ApiOperation(value = "提现到零钱", notes = "提现到零钱")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "money", value = "money", required = true, dataType = "double")
    })
    public Object toUserMoney(String uid, double money, String password) {
        InvitePwd exist = pwdDao.getByOpenId(uid);
        if (exist == null) {
            return WebUtil.error("未查询到用户");
        }

        Map<String, Object> paramMap = new HashMap<String, Object>();
        Map<String, String> map = new HashMap<String, String>();
        paramMap.put("openId", exist.getUid());
        paramMap.put("money", money);
        try {
            map = FunctionUtil.ToUserMoney(paramMap);
            if ("000".equals(map.get("code"))) {
                float abc = Float.parseFloat(money + "");
                MyWallet wallet = walletDao.getByCode(exist.getInvitePwd());
                wallet.setMoney(wallet.getMoney() - abc);
                wallet.setTotal_money(wallet.getTotal_money() - abc);
                walletDao.saveAndFlush(wallet);
                MyWalletLog log = new MyWalletLog();
                log.setCode(exist.getInvitePwd());
                log.setMoney(abc);
                log.setStatus(1);
                log.setOrder_code("提现");
                log.setRemark1("提现");
                log.setRemark2("已入账");
                log.setType(2);
                log.setCreateTime(new Date());
                log.setUpdatetime(new Date());
                logDao.saveAndFlush(log);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return WebUtil.ok(map);
    }

    @ApiOperation(value = "统计各个订单状态数据", notes = "统计各个订单状态数据")
    @RequestMapping("/statistics")
    public Object statistics(String uid) {
        Object result = new Object();

        if (ToolUtil.isEmpty(uid)) {
            result = MsgUtil.badArgument();
        } else {
            Map<String, Object> map = new HashMap<>();
            int toBeShipped = dao.statistics(uid, 1);
            int receiving = dao.statistics(uid, 2);
            int evaluate = dao.statistics(uid, 3);
            int complete = dao.statistics(uid, 4);

            map.put("toBeShipped", toBeShipped);
            map.put("receiving", receiving);
            map.put("evaluate", evaluate);
            map.put("complete", complete);
            result = MsgUtil.ok(map);
        }
        return result;
    }

    @RequestMapping("/getSign/gzh")   //获得参数(微信统一下单接口生成的prepay_id )
    public Object getSign1(String prepayId, String openId) {
        String appId = ReadUtil.read("GZH_appid");
        String key = ReadUtil.read("GZH_key");
        System.out.println("微信 支付接口生成签名 方法开始");
        //实例化返回对象
        JSONObject resultJson = new JSONObject();

        //创建 时间戳
        String timeStamp = Long.valueOf(System.currentTimeMillis()).toString();

        //创建 随机串
        long date = System.currentTimeMillis();//订单号
        String nonceStr = "ZHGJ" + date;
        //创建 MD5
        String signType = "MD5";

        //创建hashmap(用户获得签名)
        SortedMap<String, String> paraMap = new TreeMap<String, String>();
        //设置
        paraMap.put("appId", appId);
        //设置(时间戳)
        paraMap.put("timeStamp", timeStamp);
        //设置(随机串)
        paraMap.put("nonceStr", nonceStr);
        //设置(数据包)
        paraMap.put("package", "prepay_id=" + prepayId);
        //设置(签名方式)
        paraMap.put("signType", signType);

        //调用逻辑传入参数按照字段名的 ASCII 码从小到大排序（字典序）
        String stringA = PayUtils.formatUrlMap(paraMap, false, false);
        //第二步，在stringA最后拼接上key得到stringSignTemp字符串，并对stringSignTemp进行MD5运算，再将得到的字符串所有字符转换为大写，得到sign值signValue。(签名)
        String sign = PayUtil.sign(stringA, key, "utf-8").toUpperCase();

        if (StringUtils.isNotBlank(sign)) {
            //返回签名信息
            resultJson.put("sign", sign);
            //返回随机串(这个随机串是新创建的)
            resultJson.put("nonceStr", nonceStr);
            //返回时间戳
            resultJson.put("timeStamp", timeStamp);
            //返回数据包
            resultJson.put("package", "prepay_id=" + prepayId);

            System.out.println("微信 支付接口生成签名 设置返回值");
        }
        System.out.println("微信 支付接口生成签名 方法结束");
        return MsgUtil.ok(resultJson);
    }

    @RequestMapping("/getSign")   //获得参数(微信统一下单接口生成的prepay_id )
    public Object getSign(String prepayId, String openId) {
        String appId = ReadUtil.read("XCX_appid");
        String key = ReadUtil.read("XCX_key");
        System.out.println("微信 支付接口生成签名 方法开始");
        //实例化返回对象
        JSONObject resultJson = new JSONObject();

        //创建 时间戳
        String timeStamp = Long.valueOf(System.currentTimeMillis()).toString();

        //创建 随机串
        long date = System.currentTimeMillis();//订单号
        String nonceStr = "ZHGJ" + date;
        //创建 MD5
        String signType = "MD5";

        //创建hashmap(用户获得签名)
        SortedMap<String, String> paraMap = new TreeMap<String, String>();
        //设置
        paraMap.put("appId", appId);
        //设置(时间戳)
        paraMap.put("timeStamp", timeStamp);
        //设置(随机串)
        paraMap.put("nonceStr", nonceStr);
        //设置(数据包)
        paraMap.put("package", "prepay_id=" + prepayId);
        //设置(签名方式)
        paraMap.put("signType", signType);

        //调用逻辑传入参数按照字段名的 ASCII 码从小到大排序（字典序）
        String stringA = PayUtils.formatUrlMap(paraMap, false, false);
        //第二步，在stringA最后拼接上key得到stringSignTemp字符串，并对stringSignTemp进行MD5运算，再将得到的字符串所有字符转换为大写，得到sign值signValue。(签名)
        String sign = PayUtil.sign(stringA, key, "utf-8").toUpperCase();

        if (StringUtils.isNotBlank(sign)) {
            //返回签名信息
            resultJson.put("sign", sign);
            //返回随机串(这个随机串是新创建的)
            resultJson.put("nonceStr", nonceStr);
            //返回时间戳
            resultJson.put("timeStamp", timeStamp);
            //返回数据包
            resultJson.put("package", "prepay_id=" + prepayId);

            System.out.println("微信 支付接口生成签名 设置返回值");
        }
        System.out.println("微信 支付接口生成签名 方法结束");
        return MsgUtil.ok(resultJson);
    }

    public List<Integer> ListStringToInt(List<String> str) {
        List<Integer> intList = new ArrayList<Integer>();
        for (String s : str) {
            intList.add(Integer.parseInt(s));
        }
        return intList;
    }

    public Order scToInventory(List<ShopCart> scList) {
        Order order = new Order();
        List<Inventory> list = new ArrayList<Inventory>();
        Inventory obj = null;
        double total = 0.0;
        String goodsIds = "";//2020.11.11
        for (ShopCart sc : scList) {

            Goods goods = goodsDao.getOne(sc.getGoodsId());

            obj = new Inventory();
            obj.setNumber(sc.getNumber());
            obj.setPicUrl(goods.getPicUrl().get(0));
            obj.setTitle(goods.getTitle());
            obj.setType(sc.getType());
            obj.setPrice(goods.getPrice());
            obj.setGoodsId(goods.getId());
            total += goods.getPrice() * sc.getNumber();
            list.add(obj);
            //goodsIds=goodsIds+",";//2020.11.11
            goodsIds = goodsIds + goods.getId() + ",";
        }
        order.setTotal(total);
        order.setInventory(list);
        order.setGoodsIds(goodsIds);//2020.11.11
        return order;
    }

    public int isRegister(String openId, String phone) {
        String pwd = phone.substring(phone.length() - 4, phone.length()) + new SimpleDateFormat("sSSS").format(new Date());
        String msg = "";
        Users user = new Users();
        user.setOpenId(openId);
        user.setPhone(phone);
        user.setMj_pwd(pwd);
        user.setIsBuyGoods("N");
        user.setIsBuySystem("N");
        user.setWifiPwd(pwd);
        UserDao.saveAndFlush(user);
        return user.getId();
    }


    public static void main(String[] args) {
        double a = 0.3;
        String b = a + "";
        float money = Float.parseFloat(b);
        System.out.println(money);
    }
}
