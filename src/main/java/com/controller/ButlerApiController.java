package com.controller;

import cn.hutool.core.util.StrUtil;
import com.butlerApi.ButlerApiUtil;
import com.entity.ProductGuide;
import com.service.ProductGuideService;
import com.util.MsgUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("butler")
@Validated
@Api(value="管家调用api接口",tags={"管家调用api接口"})
@CrossOrigin
public class ButlerApiController {
    @Autowired
    private ProductGuideService productGuideService;

    @RequestMapping("find/relation/goods/api")
    @ApiOperation(value="查询关联商品api", notes="查询关联商品api")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productName", value = "商品名称", required = true, dataType = "String"),
    })
    public Object findProductGuide(String productName){
        Object result = new Object();
        List<ProductGuide> list = productGuideService.findProductGuide(productName);
        if(list.size()>0 || list!=null){

        }else{

        }
        return result;
    }

    @RequestMapping("get/binding")
    @ApiOperation(value="查询是否绑定", notes="查询是否绑定")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "用户id", required = true, dataType = "String"),
    })
    public Object getBinding(String uid){
        Object result = new Object();
        if(!StrUtil.hasEmpty(uid)){
            Map<String,Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
            if(macIdMap.get("code").equals("200")) {
                String macIds[] = macIdMap.get("macIds").toString().split(",");

                Map<String,Object> familyDetail = ButlerApiUtil.queryUserFamilyDetail(uid,macIds[0]);
                if(familyDetail.get("code").equals("200")){
                    result =  MsgUtil.ok("0");
                }else{
                    result = MsgUtil.ok(familyDetail.get("msg").toString());
                }

            }else{
                result = MsgUtil.ok(macIdMap.get("msg").toString());
            }
        }else{
            result = MsgUtil.ok("参数错误");
        }
        return result;
    }
    @RequestMapping("upWifiPassword")
    @ApiOperation(value="修改管家wifi密码", notes="修改管家wifi密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "用户id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "password", value = "新密码", required = true, dataType = "String"),
    })
    public Object upWifiPassword(String uid,String password){
        Object result = new Object();
        if(!StrUtil.hasEmpty(uid) && !StrUtil.hasEmpty(password)){
            Map<String,Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
            if(macIdMap.get("code").equals("200")) {
                String macIds[] = macIdMap.get("macIds").toString().split(",");
                Map<String,String> map = ButlerApiUtil.upWifiPassword(macIds[0],password);
                if(map.get("code").equals("200")){
                    result = MsgUtil.ok("yes");
                }else{
                    result = MsgUtil.ok(map.get("msg"));
                }
            }else{
                result = MsgUtil.ok(macIdMap.get("msg"));
            }
        }else{
            result = MsgUtil.ok("参数不正确");
        }
        return result;
    }

}
