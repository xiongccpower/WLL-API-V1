package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.GoodsDao;
import com.dao.ShopCartDao;
import com.dao.UserDao;
import com.entity.Goods;
import com.entity.ShopCart;
import com.entity.Users;
import com.util.Json;
import com.util.MsgUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
@RestController
@RequestMapping("shopCart")
@Api(value="购物车",tags={"购物车"})
@Validated
public class ShopCartController {

	@Autowired private  ShopCartDao dao;
	@Autowired private  UserDao UserDao;
	@Autowired private  GoodsDao goodsDao;
	
	@RequestMapping("list")
    @ApiOperation(value="获取用户的购物车商品", notes="获取用户的购物车商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String")})
	public Object list(String openId) {
		
		Users  user = UserDao.getByOpenId((String) openId);
		List<ShopCart> sc = dao.getByUserId(user.getId());
		for( ShopCart obj : sc ) {
			Goods goods = goodsDao.getOne(obj.getGoodsId());
//			obj.setGoods(Json.toJson(goods));
			obj.setGoods(goods);
		}
		return MsgUtil.ok(sc);
	}
	
	@RequestMapping("saveAndFlush")
    @ApiOperation(value="购物车操作", notes="购物车的新增、修改、删除")
    @ApiImplicitParams({
    		@ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "goodsId", value = "商品ID", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "number", value = "添加商品的数量.至少传1，如果为0表示删除购物车的商品", required = true, dataType = "Integer"),
    		@ApiImplicitParam(name = "type", value = "商品类型", required = true, dataType = "Integer")
    		})
	public Object saveAndFlush(String openId,Integer goodsId,Integer number,String type) {
		
		Users  user = UserDao.getByOpenId((String) openId);
		
		ShopCart sc = dao.get(user.getId(), goodsId,type);
		if(sc==null) {
			sc = new ShopCart();
			sc.setNumber(0);
		}
		
		sc.setGoodsId(goodsId);
		sc.setNumber(sc.getNumber()+number);
		sc.setUserId(user.getId());
		sc.setType(type);
		
		if(sc.getNumber()<=0 && sc.getId()!=null) {
			dao.delete(sc);
		}else if(sc.getNumber()>0){
			dao.saveAndFlush(sc);
		}else {
			return MsgUtil.fail("ID为空");
		}
			return MsgUtil.ok();
	}
}
