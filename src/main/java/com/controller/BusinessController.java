package com.controller;

import com.domain.BusinessLog;
import com.domain.BusinessResult;
import com.service.BusinessEventService;
import com.service.BusinessLogService;
import com.service.BusinessResultService;
import com.wll.wulian.entity.base.R;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * created in 2022/3/2 15:29
 *
 * @author zhuxuelei
 */
@RestController
@RequestMapping("business")
public class BusinessController {

    @Resource
    private BusinessLogService businessLogService;

    @Resource
    private BusinessEventService businessEventService;

    @Resource
    private BusinessResultService businessResultService;

    @PostMapping("log/add")
    public R addLog(@RequestBody BusinessLog log) {
        return businessLogService.addLog(log);
    }

    @GetMapping("invite")
    public R getInvitePwd(Long mbeId, String openId) {

        return R.ok().put("invite", businessEventService.getInvitePwd(mbeId));
    }

    @PostMapping("login")
    public R login(@RequestBody BusinessResult businessResult) {
        return businessResultService.login(businessResult);
    }
}
