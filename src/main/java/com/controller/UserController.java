package com.controller;

import com.dao.DictionariesDao;
import com.dao.MyWalletDao;
import com.dao.MyWalletLogDao;
import com.dao.UserDao;
import com.domain.AwardMoney;
import com.domain.SysUser;
import com.domain.dto.SysUserDto;
import com.entity.Dictionaries;
import com.entity.MyWallet;
import com.entity.MyWalletLog;
import com.entity.Users;
import com.mapper.OrderMapper;
import com.mapper.SysUserMapper;
import com.service.impl.AwardMoneyServiceImpl;
import com.util.FunctionUtil;
import com.util.MsgUtil;
import com.wll.wulian.entity.base.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("user")
@Api(value = "用户", tags = {"用户相关的操作"})
@Validated
@CrossOrigin
public class UserController {

    @Autowired
    private UserDao dao;
    @Autowired
    private MyWalletDao walletDao;
    @Autowired
    private MyWalletLogDao logDao;
    @Autowired
    private DictionariesDao dictionariesDao;
    @Autowired
    private AwardMoneyServiceImpl awardMoneyService;
    @Resource
    private SysUserMapper sysUserMapper;

    /**
     * todo 临时使用
     */
    @Resource
    private OrderMapper mapper;

    @RequestMapping("isInner")
    public Object isInner(String tel, String uid) {
        if (tel != null) {
            SysUserDto inner = mapper.isInner(tel);
            return MsgUtil.ok(inner);
        }
        SysUser sysUser = sysUserMapper.selectById(uid);
        if (sysUser == null) {
            return MsgUtil.fail();
        }
        SysUserDto dto = new SysUserDto();
        dto.setWxImgUrl(sysUser.getWxImgUrl());
        return MsgUtil.ok(dto);
//        Map<String, Object> ok = (Map<String, Object>) ;
//        ok.put("wxImgUrl", inner.getWxImgUrl());
    }


    /**
     * 列表
     */
    @RequestMapping("getByOpenId")
    @ApiOperation(value = "获取单个用户信息", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String")})
    public Object getByOpenId(String openId) {
        Users user = dao.getByOpenId(openId);
        return MsgUtil.ok(user);
    }

    @RequestMapping("getByTel")
    @ApiOperation(value = "获取单个用户信息", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String")})
    public Object getByTel(String openId, String tel) {
        Users user = dao.getByTel(tel);
        user.setOpenId(openId);
        dao.saveAndFlush(user);
        return MsgUtil.ok(user);
    }

    /**
     * todo check
     *
     * @param code
     * @return
     */
    @RequestMapping("getWalletInfoByTel")
    @ApiOperation(value = "获取单个用户信息", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String")})
    public Object getWalletInfoByTel(String code) {
        MyWallet wallet = walletDao.getByCode(code);
        if (wallet != null) {
            return MsgUtil.ok(wallet);
        } else {
            MyWallet newWallet = new MyWallet();
//            newWallet.setTel(tel);
            newWallet.setCode(code);
            newWallet.setMoney(0);
            newWallet.setWait_money(0);
            newWallet.setTotal_money(0);
            newWallet.setStatus(1);
            newWallet.setType(1);
            newWallet.setCreateTime(new Date());
            newWallet.setUpdatetime(new Date());
            walletDao.saveAndFlush(newWallet);
            return MsgUtil.ok(newWallet);
        }

    }

    /**
     * todo check
     * 1
     *
     * @param
     * @return
     */
    @RequestMapping("isAwardMoney")
    @ApiOperation(value = "获取单个用户信息", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String")})
    public Object isAwardMoney(String code) {
        MyWallet wallet = walletDao.getByCode(code);
        if (wallet == null) {
            return MsgUtil.fail("-1");
        } else {
            if (wallet.getMoney() != 0) {
                return MsgUtil.ok("1");
            } else {
                return MsgUtil.ok("-1");
            }
        }

    }

    /**
     * todo check
     *
     * @param
     * @param type
     * @return
     */
    @RequestMapping("getWalletLogInfoByTel")
    @ApiOperation(value = "获取单个用户信息", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "type", value = "type", required = true, dataType = "Integer"),})
    public Object getWalletLogInfoByTel(String code, int type) {
        List<MyWalletLog> list = logDao.getByCode(code, type);
        if (list != null) {
            return MsgUtil.ok(list);
        } else {
            return MsgUtil.fail("查询失败！");
        }

    }

    @RequestMapping("selectAwardmoney")
    @ApiOperation(value = "奖励金查询", notes = "奖励金查询")
    public R selectAwardmoney(HttpServletRequest request){
        List<AwardMoney> list = awardMoneyService.getList();
        return R.ok(list);
    }
    @ResponseBody
    @ApiOperation(value = "奖励金详情", notes = "奖励金详情")
    @RequestMapping(value="/awardMoneyNewDetail", method={RequestMethod.GET,RequestMethod.POST})
    public Object awardMoneyNewDetail(HttpServletRequest request,AwardMoney awardMoney,ModelMap map) {
        AwardMoney mjawardMoney= awardMoneyService.queryById(awardMoney.getId());
        if (ObjectUtils.isNotEmpty(mjawardMoney)) {
            return MsgUtil.ok(mjawardMoney);
        } else {
            return MsgUtil.fail("查询失败！");
        }
    }

    /**
     * 奖励金修改
     */
    @ResponseBody
    @ApiOperation(value = "奖励金修改", notes = "奖励金修改")
    @RequestMapping(value="updateAwardMoney", method={RequestMethod.GET,RequestMethod.POST})
    public R updateVillage(HttpServletRequest request,@RequestBody AwardMoney awardMoney, ModelMap map){
        int result=0;
        if("4".equals(awardMoney.getAwardType())) {
            updateSaleMoney(awardMoney.getAwardMoney());//修改管家价格需要修改
        }
        awardMoney.setUpdateUser("运营管理员");
        boolean b = awardMoneyService.updateById(awardMoney);
        if(b){
           return R.ok();
        }else{
            return R.error();
        }
    }


    @RequestMapping("updateSaleMoney")
    @ApiOperation(value = "获取单个用户信息", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "money", value = "tel", required = true, dataType = "String")})
    public Object updateSaleMoney(String money) {
        if ("".equals(money)) {
            return MsgUtil.fail(0, "请输入正确金额！");
        }
        Dictionaries dict = dictionariesDao.getByKey("system_price");
        dict.setDicValue(money);
        dictionariesDao.saveAndFlush(dict);
        return MsgUtil.ok(dict);

    }

    @RequestMapping("updateHead")
    public Object updateHead(String url, String uid) {
        return MsgUtil.ok();
    }

}
