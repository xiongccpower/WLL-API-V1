package com.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.api.smart.api.duomai.*;
import com.api.smart.core.GoodsQuery;
import com.api.smart.core.GoodsResponse;
import com.api.smart.core.RemoteSource;
import com.api.smart.core.SmartShoppingCallback;
import com.api.smart.coupon.dataoke.DaTaoKeCouponService;
import com.api.smart.coupon.zhetaoke.ZheTaoKeCouponService;
import com.api.smart.entity.goods.UnionCoupon;
import com.api.smart.interfaces.ThirdGoodsApi;
import com.api.smart.utils.RequestUtil;
import com.butlerApi.ButlerApiUtil;
import com.dao.*;
import com.domain.CollectionGoods;
import com.domain.GoodsConvert;
import com.entity.*;
import com.entity.union_entity.GoodsDetails;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.platformApi.KlzkGoodsApi;
import com.platformApi.WphGoodsApi;
import com.service.*;
import com.util.FunctionUtil;
import com.util.MsgUtil;
import com.weilaili.websocket.interfaces.WebSocketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.api.smart.api.duomai.TbUnionGoodsRequestHelper.APP_SECRET;

@RestController
@RequestMapping("data")
@Validated
@Api(value = "健康数据", tags = {"健康数据"})
//@CrossOrigin
public class MydataController {
    protected static Logger log = LoggerFactory.getLogger(MydataController.class);


    private static final List<ThirdGoodsApi> apis = new ArrayList<>();

    static {
        apis.add(new TbUnionGoodsRequestHelper());
        apis.add(new JdUnionGoodsRequestHelper());
        apis.add(new KlUnionGoodsRequestHelper());
        apis.add(new VipUnionGoodsRequestHelper());
        apis.add(new SnUnionGoodsRequestHelper());
    }

    @Autowired
    private MyDataDao dao;
    @Autowired
    private UserDao UserDao;
    @Autowired
    private MyHomeDao homeDao;
    @Autowired
    private MyHeightDataDao heightDataDao;
    @Autowired
    private MyWeightDataDao weightDataDao;
    @Autowired
    private MyBooldHearDataDao booldHearDataDao;
    @Autowired
    private MyBooldPerDataDao booldPerDataDao;
    @Autowired
    private MyBooldOxDataDao oxDataDao;
    @Autowired
    private MyBooldSweetDataDao sweetDataDao;
    @Autowired
    private MyHeartTimesDataDao timesDataDao;
    @Autowired
    private HealthDictDao healthDictDao;
    @Autowired
    private HealthContentDao healthContentDao;
    @Autowired
    private WitShoppingGoodsService witShoppingGoodsService;
    @Autowired
    private WitShoppingUnionDeployService witShoppingUnionDeployService;
    @Autowired
    private WitBookingDiscountService witBookingDiscountService;
    @Autowired
    private com.service.MyServiceInstructRecordService myServiceInstructRecordService;
    @Autowired
    private MjUserService mjUserService;
    @Autowired
    private WitGoodsFunctionService witGoodsFunctionService;
    @Autowired
    private MjFamilyRelationService mjFamilyRelationService;


    @RequestMapping("get")
    @ApiOperation(value = "获取个人 健康/设备 数据", notes = "获取个人数据（type JK=‘健康数据’SB='设备数据'）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "userId", value = "userId", required = true, dataType = "Integer"),
    })
    public Object get(String tel, String uid, String userId) {
        log.info("获取个人 健康");
        Object result = new Object();

        Map<String, Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
        MyData data = new MyData();

        data.setHeartTimes("暂无");
        data.setWeight("暂无");
        data.setHeight("暂无");
        data.setBooldHear("暂无");
        data.setBooldSweet("暂无");
        data.setBooldOx("暂无");
        data.setHighPer("暂无");
        data.setLowPer("暂无");

        List<MyData> list = new ArrayList<MyData>();

        if (macIdMap.get("code").equals("200")) {
            String macIds[] = macIdMap.get("macIds").toString().split(",");

            Map<String, Object> familyMember = ButlerApiUtil.queryFamilyMemberInfo(macIds[0], userId, uid);


            if (familyMember.get("code").equals("200")) {
                if (!StrUtil.hasEmpty(familyMember.get("data") + "")) {
                    cn.hutool.json.JSONObject json = JSONUtil.parseObj(familyMember.get("data"));
                    data.setUserId(Integer.parseInt(userId));
                    data.setId(Integer.parseInt(uid));
                    if (!StrUtil.hasEmpty(json.get("healthData") + "") && !json.get("healthData").equals("[]")) {
                        JSONArray array = JSONUtil.parseArray(json.get("healthData"));

                        for (int i = 0; i < array.size(); i++) {
                            int type = array.getJSONObject(i).getInt("type");
                            if (type == 0) {
                                data.setHeartTimes(array.getJSONObject(i).get("heartbeat") + " 次/每分钟");
                            }
                            if (type == 1) {
                                data.setWeight(array.getJSONObject(i).get("weight") + " kg");
                            }
                            if (type == 2) {
                                data.setHeight(array.getJSONObject(i).get("height") + " cm");
                            }
                            if (type == 3) {
                                data.setBooldHear(array.getJSONObject(i).get("otherValue") + " mg/dl");
                            }
                            if (type == 4) {
                                data.setBooldSweet(array.getJSONObject(i).get("otherValue") + " mmol/L");
                            }
                            if (type == 5) {
                                data.setBooldOx(array.getJSONObject(i).get("otherValue") + " %");
                            }
                            if (type == 6) {
                                if (!StrUtil.hasEmpty(array.getJSONObject(i).get("otherValue") + "")) {
                                    String booldHear[] = array.getJSONObject(i).get("otherValue").toString().split("~");

                                    for (int j = 0; j < booldHear.length; j++) {
                                        if (j == 0) {
                                            data.setHighPer(booldHear[j]);
                                        }
                                        if (j == 1) {
                                            data.setLowPer(booldHear[j].substring(0, booldHear[j].length() - 4));
                                        }
                                    }
                                }

                            }
                        }
                        list.add(data);
                        result = MsgUtil.ok(list);
                    } else {

                        list.add(data);
                        result = MsgUtil.ok(list);
                    }

                } else {

                    list.add(data);
                    result = MsgUtil.ok(list);
                }
            } else {

                list.add(data);
                result = MsgUtil.ok(list);
            }

        } else {

            list.add(data);
            result = MsgUtil.ok(list);
        }


//		Users  user = UserDao.getByTel(tel);
//
//		if(user!=null){
//			List<MyWeightData> weightList = weightDataDao.getByUserId(user.getId());
//			List<MyHeightData> heightDataList= heightDataDao.getByUserId(user.getId());
//			List<MyBooldPerData> booldPerDataList = booldPerDataDao.getByUserId(user.getId());
//			List<MyBooldHearData> booldHearDataList = booldHearDataDao.getByUserId(user.getId());
//			List<MyBooldOxData> oxDataList = oxDataDao.getByUserId(user.getId());
//			List<MyBooldSweetData> sweetDataList = sweetDataDao.getByUserId(user.getId());
//			List<MyHeartTimesData> heartTimesDataList = timesDataDao.getByUserId(user.getId());
//			MyData data = new MyData();
//			data.setUserId(user.getId());
//			if(weightList.size()!=0){
//				data.setWeight(weightList.get(0).getContent());
//			}else{
//				data.setWeight("暂无");
//			}
//			if(heightDataList.size()!=0){
//				data.setHeight(heightDataList.get(0).getContent());
//			}else{
//				data.setHeight("暂无");
//			}
//			if(booldPerDataList.size()!=0){
//				data.setHighPer(booldPerDataList.get(0).getHighPer());
//				data.setLowPer(booldPerDataList.get(0).getLowPer());
//			}else{
//				data.setHighPer("暂无");
//				data.setLowPer("暂无");
//			}
//			if(booldHearDataList.size()!=0){
//				data.setBooldHear(booldHearDataList.get(0).getContent());
//			}else {
//				data.setBooldHear("暂无");
//			}
//			if(oxDataList.size()!=0){
//				data.setBooldOx(oxDataList.get(0).getContent());
//			}else {
//				data.setBooldOx("暂无");
//			}
//			if(sweetDataList.size()!=0){
//				data.setBooldSweet(sweetDataList.get(0).getContent());
//			}else {
//				data.setBooldSweet("暂无");
//			}
//			if(heartTimesDataList.size()!=0){
//				data.setHeartTimes(heartTimesDataList.get(0).getContent());
//			}else {
//				data.setHeartTimes("暂无");
//			}
//			List<MyData> list  = new ArrayList<MyData>();
//			list.add(data);
//
//			return MsgUtil.ok(list);
//		}else{
//			return MsgUtil.ok("用户不存在");
//		}

        return result;


    }


    @RequestMapping("getWeightData")
    @ApiOperation(value = "获取个人 体重健康/设备 数据", notes = "获取个人数据（type JK=‘健康数据’SB='设备数据'）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "userId", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "startTime", value = "startTime", required = true, dataType = "String"),
            @ApiImplicitParam(name = "endTime", value = "endTime", required = true, dataType = "String")})
    public Object getWeightData(Integer userId, String startTime, String endTime, String uid) {
        log.info("获取个人 体重健康");
        Object result = new Object();

        Map map = new HashMap();
        List dateList = new ArrayList();
        List dataList = new ArrayList();


        Map<String, Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
        if (macIdMap.get("code").equals("200")) {
            String macIds[] = macIdMap.get("macIds").toString().split(",");

            Map<String, Object> typrMap = ButlerApiUtil.queryMemberDetectionDataList(macIds[0], uid, userId + "", "1", startTime, endTime);
            if (typrMap.get("code").equals("200")) {
                if (!StrUtil.hasEmpty(typrMap.get("data") + "")) {
                    cn.hutool.json.JSONArray json = JSONUtil.parseArray(typrMap.get("data"));
                    List<MyWeightData> heightDataList = new ArrayList<>();
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                    for (int i = 0; i < json.size(); i++) {
                        MyWeightData myWeight = new MyWeightData();
                        myWeight.setUserId(userId);
                        myWeight.setType("体重");
                        myWeight.setDataCome(json.getJSONObject(i).get("dataCome") + "");
                        myWeight.setContent(json.getJSONObject(i).get("weight") + "公斤");
                        myWeight.setCreateTime(DateUtil.parse(json.getJSONObject(i).get("created_time") + ""));

                        String date = json.getJSONObject(i).get("created_time").toString().substring(5, 10);
                        dateList.add(date);
                        dataList.add(json.getJSONObject(i).get("weight"));
                        heightDataList.add(myWeight);
                    }
                    Collections.reverse(dateList);
                    Collections.reverse(dataList);
                    map.put("weightList", heightDataList);
                    map.put("dateList", dateList);
                    map.put("dataList", dataList);

                    result = MsgUtil.ok(map);

                } else {
                    result = MsgUtil.fail("暂无数据");
                }
            } else {
                result = MsgUtil.fail(macIdMap.get("msg") + "");
            }


        } else {
            result = MsgUtil.fail(macIdMap.get("msg") + "");
        }


//		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
//		if(startTime==null || endTime ==null ||startTime=="" || endTime==""){
//			List<MyWeightData> weightList = weightDataDao.getByUserId(userId);
//			for(int i =0;i<weightList.size();i++){
//				dateList.add(sf.format(weightList.get(i).getCreateTime()).substring(5, 10));
//				dataList.add(weightList.get(i).getContent().replace("公斤", ""));
//			}
//			map.put("weightList",weightList);
//			map.put("dateList",dateList);
//			map.put("dataList",dataList);
//
//			return MsgUtil.ok(map);
//		}else{
//			try {
//				String time = DateUtil.now();
//				startTime = startTime+" 00:00:00";
//				endTime = endTime+time.substring(10,time.length());
//				Date startDate = sf.parse(startTime);
//				Date endDate = sf.parse(endTime);
//				List<MyWeightData> weightList = weightDataDao.getByUserIdandAndCreateTime(userId,startDate,endDate);
//				for(int i =0;i<weightList.size();i++){
//					dateList.add(sf.format(weightList.get(i).getCreateTime()).substring(5, 10));
//					dataList.add(weightList.get(i).getContent().replace("公斤", ""));
//				}
//				map.put("weightList",weightList);
//				map.put("dateList",dateList);
//				map.put("dataList",dataList);
//
//			}catch (ParseException e){
//				e.getMessage();
//			}


        return result;
//		}
    }

    @RequestMapping("getHeightData")
    @ApiOperation(value = "获取个人 身高 数据", notes = "获取个人数据（type JK=‘健康数据’SB='设备数据'）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "tel", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "startTime", value = "startTime", required = true, dataType = "String"),
            @ApiImplicitParam(name = "endTime", value = "endTime", required = true, dataType = "String")})
    public Object getHeightData(Integer userId, String startTime, String endTime, String uid) {
        log.info("获取个人 身高健康");
        Object result = new Object();

        Map map = new HashMap();
        List dateList = new ArrayList();
        List dataList = new ArrayList();

        Map<String, Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
        if (macIdMap.get("code").equals("200")) {
            String macIds[] = macIdMap.get("macIds").toString().split(",");

            Map<String, Object> typrMap = ButlerApiUtil.queryMemberDetectionDataList(macIds[0], uid, userId + "", "2", startTime, endTime);
            if (typrMap.get("code").equals("200")) {
                if (!StrUtil.hasEmpty(typrMap.get("data") + "")) {
                    cn.hutool.json.JSONArray json = JSONUtil.parseArray(typrMap.get("data"));
                    List<MyHeightData> heightDataList = new ArrayList<>();
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                    for (int i = 0; i < json.size(); i++) {
                        System.out.println("健康数据：" + json.getJSONObject(i));
                        MyHeightData myHeight = new MyHeightData();
                        myHeight.setUserId(userId);
                        myHeight.setType("身高");
                        myHeight.setDataCome(json.getJSONObject(i).get("dataCome") + "");
                        myHeight.setContent(json.getJSONObject(i).get("height") + "厘米");
                        myHeight.setCreateTime(DateUtil.parse(json.getJSONObject(i).get("created_time") + ""));

                        String date = json.getJSONObject(i).get("created_time").toString().substring(5, 10);
                        dateList.add(date);
                        dataList.add(json.getJSONObject(i).get("height"));
                        heightDataList.add(myHeight);
                    }
                    Collections.reverse(dateList);
                    Collections.reverse(dataList);
                    map.put("heightDataList", heightDataList);
                    map.put("dateList", dateList);
                    map.put("dataList", dataList);

                    result = MsgUtil.ok(map);

                } else {
                    result = MsgUtil.fail("暂无数据");
                }

            } else {
                result = MsgUtil.fail(macIdMap.get("msg") + "");
            }
        } else {
            result = MsgUtil.fail(macIdMap.get("msg") + "");
        }


//
//		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
//		if(startTime==null || endTime ==null ||startTime=="" || endTime=="") {
//			List<MyHeightData> heightDataList = heightDataDao.getByUserId(userId);
//			for (int i = 0; i < heightDataList.size(); i++) {
//				String date = sf.format(heightDataList.get(i).getCreateTime()).substring(5, 10);
//				dateList.add(date);
//
//				dataList.add(heightDataList.get(i).getContent().replace("厘米", ""));
//			}
//			Collections.reverse(dateList);
//			Collections.reverse(dataList);
//
//			map.put("heightDataList", heightDataList);
//			map.put("dateList",dateList );
//			map.put("dataList", dataList);
//
//		}else{
//			try {
//				String time = DateUtil.now();
//				startTime = startTime+" 00:00:00";
//				endTime = endTime+time.substring(10,time.length());
//				Date startDate = sf.parse(startTime);
//				Date endDate = sf.parse(endTime);
//				List<MyHeightData> heightDataList = heightDataDao.getByUserIdandAndCreateTime(userId,startDate,endDate);
//				for(int i =0;i<heightDataList.size();i++){
//					dateList.add(sf.format(heightDataList.get(i).getCreateTime()).substring(5, 10));
//					dataList.add(heightDataList.get(i).getContent().replace("厘米", ""));
//
//				}
//				Collections.reverse(dateList);
//				Collections.reverse(dataList);
//
//				map.put("heightDataList",heightDataList);
//				map.put("dateList",dateList);
//				map.put("dataList",dataList);
//
//			}catch (ParseException e){
//				e.getMessage();
//			}
//		}

        return result;
    }

    @RequestMapping("getPerData")
    @ApiOperation(value = "获取个人 血压健康/设备 数据", notes = "获取个人数据（type JK=‘健康数据’SB='设备数据'）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "tel", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "startTime", value = "startTime", required = true, dataType = "String"),
            @ApiImplicitParam(name = "endTime", value = "endTime", required = true, dataType = "String")})
    public Object getPerData(Integer userId, String startTime, String endTime, String uid) {
        log.info("获取个人 血压健康");
        Object result = new Object();
        Map map = new HashMap();
        List dateList = new ArrayList();
        List highList = new ArrayList();
        List lowList = new ArrayList();


        Map<String, Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
        if (macIdMap.get("code").equals("200")) {
            String macIds[] = macIdMap.get("macIds").toString().split(",");

            Map<String, Object> typrMap = ButlerApiUtil.queryMemberDetectionDataList(macIds[0], uid, userId + "", "6", startTime, endTime);
            if (typrMap.get("code").equals("200")) {

                if (!StrUtil.hasEmpty(typrMap.get("data") + "")) {
                    cn.hutool.json.JSONArray json = JSONUtil.parseArray(typrMap.get("data"));
                    List<MyBooldPerData> heightDataList = new ArrayList<>();
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

                    for (int i = 0; i < json.size(); i++) {
                        MyBooldPerData myHeight = new MyBooldPerData();
                        myHeight.setUserId(userId);
                        myHeight.setType("血压");
                        myHeight.setDataCome(json.getJSONObject(i).get("dataCome") + "");


                        myHeight.setHighPer(json.getJSONObject(i).get("otherValue").toString().split("~")[0]);
                        myHeight.setLowPer(json.getJSONObject(i).get("otherValue").toString().split("~")[1]);

                        myHeight.setCreateTime(DateUtil.parse(json.getJSONObject(i).get("created_time") + ""));

                        String date = json.getJSONObject(i).get("created_time").toString().substring(5, 10);
                        dateList.add(date);

                        String ya[] = json.getJSONObject(i).get("otherValue").toString().split("~");
                        for (int j = 0; j < ya.length; j++) {
                            if (j == 0) {
                                highList.add(ya[j].replace("mmHg", ""));
                            }
                            if (j == 1) {
                                lowList.add(ya[j].replace("mmHg", ""));
                            }
                        }
//					dataList.add(json.getJSONObject(i).get("otherValue"));
                        heightDataList.add(myHeight);
                    }
                    Collections.reverse(dateList);
                    Collections.reverse(highList);
                    Collections.reverse(lowList);
                    map.put("booldPerDataList", heightDataList);
                    map.put("dateList", dateList);
                    map.put("highList", highList);
                    map.put("lowList", lowList);

                    result = MsgUtil.ok(map);
                } else {
                    result = MsgUtil.fail("暂无数据");
                }


            } else {
                result = MsgUtil.fail(macIdMap.get("msg") + "");
            }
        } else {
            result = MsgUtil.fail(macIdMap.get("msg") + "");
        }


//		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
//		if(startTime==null || endTime ==null ||startTime=="" || endTime=="") {
//			List<MyBooldPerData> booldPerDataList = booldPerDataDao.getByUserId(userId);
//			for (int i = 0; i < booldPerDataList.size(); i++) {
//				String date = sf.format(booldPerDataList.get(i).getCreateTime()).substring(5, 10);
//				dateList.add(date);
//				highList.add(booldPerDataList.get(i).getHighPer().replace("mmHg", ""));
//				lowList.add(booldPerDataList.get(i).getLowPer().replace("mmHg", ""));
//			}
//			Collections.reverse(dateList);
//			Collections.reverse(highList);
//			Collections.reverse(lowList);
//			map.put("booldPerDataList", booldPerDataList);
//			map.put("dateList", dateList);
//			map.put("highList", highList);
//			map.put("lowList", lowList);
//		}else{
//			try {
//				String time = DateUtil.now();
//				startTime = startTime+" 00:00:00";
//				endTime = endTime+time.substring(10,time.length());
//				Date startDate = sf.parse(startTime);
//				Date endDate = sf.parse(endTime);
//				List<MyBooldPerData> booldPerDataList = booldPerDataDao.getByUserIdandAndCreateTime(userId,startDate,endDate);
//				for(int i =0;i<booldPerDataList.size();i++){
//					dateList.add(sf.format(booldPerDataList.get(i).getCreateTime()).substring(5, 10));
//					highList.add(booldPerDataList.get(i).getHighPer().replace("mmHg", ""));
//					lowList.add(booldPerDataList.get(i).getLowPer().replace("mmHg", ""));
//				}
//				Collections.reverse(dateList);
//				Collections.reverse(highList);
//				Collections.reverse(lowList);
//				map.put("booldPerDataList",booldPerDataList);
//				map.put("dateList",dateList);
//				map.put("highList", highList);
//				map.put("lowList", lowList);
//
//			}catch (ParseException e){
//				e.getMessage();
//			}
//
//		}


        return MsgUtil.ok(map);
    }

    @RequestMapping("getHearData")
    @ApiOperation(value = "获取个人 血脂健康/设备 数据", notes = "获取个人数据（type JK=‘健康数据’SB='设备数据'）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "tel", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "startTime", value = "startTime", required = true, dataType = "String"),
            @ApiImplicitParam(name = "endTime", value = "endTime", required = true, dataType = "String")})
    public Object getHearData(Integer userId, String startTime, String endTime, String uid) {

        log.info("获取个人 血脂健康");
        Object result = new Object();
        Map map = new HashMap();
        List dateList = new ArrayList();
        List cholesterol = new ArrayList();
        List triglyceride = new ArrayList();
        List lowDensityLipoprotein = new ArrayList();
        List highDensityLipoprotein = new ArrayList();

        Map<String, Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
        if (macIdMap.get("code").equals("200")) {
            String macIds[] = macIdMap.get("macIds").toString().split(",");

            Map<String, Object> typrMap = ButlerApiUtil.queryMemberDetectionDataList(macIds[0], uid, userId + "", "3", startTime, endTime);
            if (typrMap.get("code").equals("200")) {
                if (!StrUtil.hasEmpty(typrMap.get("data") + "")) {
                    cn.hutool.json.JSONArray json = JSONUtil.parseArray(typrMap.get("data"));
                    List<MyBooldHearData> heightDataList = new ArrayList<>();
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

                    for (int i = 0; i < json.size(); i++) {
                        MyBooldHearData myHeight = new MyBooldHearData();
                        myHeight.setUserId(userId);
                        myHeight.setType("血脂");
                        myHeight.setDataCome(json.getJSONObject(i).get("dataCome") + "");
                        if (!json.getJSONObject(i).get("otherValue").equals("55")) {
                            cn.hutool.json.JSONObject jsonXz = JSONUtil.parseObj(json.getJSONObject(i).get("otherValue"));
                            if (jsonXz.containsKey("cholesterol")) {
                                myHeight.setCholesterol(jsonXz.getStr("cholesterol") + "mg/dl");
                                cholesterol.add(jsonXz.getStr("cholesterol"));
                            } else {
                                cholesterol.add("0");
                            }
                            if (jsonXz.containsKey("triglyceride")) {
                                myHeight.setTriglyceride(jsonXz.getStr("triglyceride") + "mg/dl");
                                triglyceride.add(jsonXz.getStr("triglyceride"));
                            } else {
                                triglyceride.add("0");
                            }
                            if (jsonXz.containsKey("lowDensityLipoprotein")) {
                                myHeight.setLowDensityLipoprotein(jsonXz.getStr("lowDensityLipoprotein") + "mg/dl");
                                lowDensityLipoprotein.add(jsonXz.getStr("lowDensityLipoprotein"));
                            } else {
                                lowDensityLipoprotein.add("0");
                            }
                            if (jsonXz.containsKey("highDensityLipoprotein")) {
                                myHeight.setHighDensityLipoprotein(jsonXz.getStr("highDensityLipoprotein") + "mg/dl");
                                highDensityLipoprotein.add(jsonXz.getStr("highDensityLipoprotein"));
                            } else {
                                highDensityLipoprotein.add("0");
                            }
                        }

//						myHeight.setContent(json.getJSONObject(i).get("otherValue")+"mg/dl");

                        myHeight.setCreateTime(DateUtil.parse(json.getJSONObject(i).get("created_time") + ""));

                        String date = json.getJSONObject(i).get("created_time").toString().substring(5, 10);
                        dateList.add(date);
                        heightDataList.add(myHeight);
                    }

                    Collections.reverse(dateList);
                    Collections.reverse(cholesterol);
                    Collections.reverse(triglyceride);
                    Collections.reverse(lowDensityLipoprotein);
                    Collections.reverse(highDensityLipoprotein);
                    map.put("booldHearDataList", heightDataList);
                    map.put("dateList", dateList);
                    map.put("cholesterol", cholesterol);
                    map.put("triglyceride", triglyceride);
                    map.put("lowDensityLipoprotein", lowDensityLipoprotein);
                    map.put("highDensityLipoprotein", highDensityLipoprotein);

                    System.out.println("返回血脂数据=" + JSONUtil.toJsonPrettyStr(map));
                    result = MsgUtil.ok(map);


                } else {
                    result = MsgUtil.fail("暂无数据");
                }

            } else {
                result = MsgUtil.fail(macIdMap.get("msg") + "");
            }
        } else {
            result = MsgUtil.fail(macIdMap.get("msg") + "");
        }

//		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
//		if(startTime==null || endTime ==null || startTime=="" || endTime=="") {
//			List<MyBooldHearData> booldHearDataList = booldHearDataDao.getByUserId(userId);
//			for (int i = 0; i < booldHearDataList.size(); i++) {
//				String date = sf.format(booldHearDataList.get(i).getCreateTime()).substring(5, 10);
//				dateList.add(date);
//
//				dataList.add(booldHearDataList.get(i).getContent().replace("mg/dl", ""));
//			}
//			Collections.reverse(dateList);
//			Collections.reverse(dataList);
//			map.put("booldHearDataList", booldHearDataList);
//			map.put("dateList", dateList);
//			map.put("dataList", dataList);
//		}else{
//			try {
//				String time = DateUtil.now();
//				startTime = startTime+" 00:00:00";
//				endTime = endTime+time.substring(10,time.length());
//				Date startDate = sf.parse(startTime);
//				Date endDate = sf.parse(endTime);
//				List<MyBooldHearData> booldHearDataList = booldHearDataDao.getByUserIdandAndCreateTime(userId,startDate,endDate);
//				for(int i =0;i<booldHearDataList.size();i++){
//					dateList.add(sf.format(booldHearDataList.get(i).getCreateTime()).substring(5, 10));
//					dataList.add(booldHearDataList.get(i).getContent().replace("mg/dl", ""));
//				}
//				Collections.reverse(dateList);
//				Collections.reverse(dataList);
//				map.put("booldHearDataList",booldHearDataList);
//				map.put("dateList",dateList);
//				map.put("dataList",dataList);
//
//			}catch (ParseException e){
//				e.getMessage();
//			}
//
//		}


        return result;
    }

    @RequestMapping("getOxData")
    @ApiOperation(value = "获取个人 血氧健康/设备 数据", notes = "获取个人数据（type JK=‘健康数据’SB='设备数据'）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "tel", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "startTime", value = "startTime", required = true, dataType = "String"),
            @ApiImplicitParam(name = "endTime", value = "endTime", required = true, dataType = "String")})
    public Object getOxData(Integer userId, String startTime, String endTime, String uid) {
        log.info("获取个人 血氧健康");
        Object result = new Object();

        Map map = new HashMap();
        List dateList = new ArrayList();
        List dataList = new ArrayList();

        Map<String, Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
        if (macIdMap.get("code").equals("200")) {
            String macIds[] = macIdMap.get("macIds").toString().split(",");

            Map<String, Object> typrMap = ButlerApiUtil.queryMemberDetectionDataList(macIds[0], uid, userId + "", "5", startTime, endTime);
            if (typrMap.get("code").equals("200")) {
                if (!StrUtil.hasEmpty(typrMap.get("data") + "")) {
                    cn.hutool.json.JSONArray json = JSONUtil.parseArray(typrMap.get("data"));
                    List<MyHeightData> heightDataList = new ArrayList<>();
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                    for (int i = 0; i < json.size(); i++) {
                        MyHeightData myHeight = new MyHeightData();
                        myHeight.setUserId(userId);
                        myHeight.setType("血氧");
                        myHeight.setDataCome(json.getJSONObject(i).get("dataCome") + "");
                        myHeight.setContent(json.getJSONObject(i).get("otherValue") + "%");
                        myHeight.setCreateTime(DateUtil.parse(json.getJSONObject(i).get("created_time") + ""));
                        String date = json.getJSONObject(i).get("created_time").toString().substring(5, 10);
                        dateList.add(date);
                        dataList.add(json.getJSONObject(i).get("otherValue"));
                        heightDataList.add(myHeight);
                    }
                    Collections.reverse(dateList);
                    Collections.reverse(dataList);
                    map.put("oxDataList", heightDataList);
                    map.put("dateList", dateList);
                    map.put("dataList", dataList);

                    result = MsgUtil.ok(map);
                } else {
                    result = MsgUtil.fail("暂无数据");
                }

            } else {
                result = MsgUtil.fail(macIdMap.get("msg") + "");
            }
        } else {
            result = MsgUtil.fail(macIdMap.get("msg") + "");
        }

//
//		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
//		if(startTime==null || endTime ==null || startTime=="" || endTime=="") {
//			List<MyBooldOxData> oxDataList = oxDataDao.getByUserId(userId);
//			for (int i = 0; i < oxDataList.size(); i++) {
//				String date = sf.format(oxDataList.get(i).getCreateTime()).substring(5, 10);
//				dateList.add(date);
//
//				dataList.add(oxDataList.get(i).getContent().replace("%", ""));
//			}
//			Collections.reverse(dateList);
//			Collections.reverse(dataList);
//			map.put("oxDataList", oxDataList);
//			map.put("dateList", dateList);
//			map.put("dataList", dataList);
//		}else{
//			try {
//				String time = DateUtil.now();
//				startTime = startTime+" 00:00:00";
//				endTime = endTime+time.substring(10,time.length());
//				Date startDate = sf.parse(startTime);
//				Date endDate = sf.parse(endTime);
//				List<MyBooldOxData> oxDataList = oxDataDao.getByUserIdandAndCreateTime(userId,startDate,endDate);
//				for(int i =0;i<oxDataList.size();i++){
//					dateList.add(sf.format(oxDataList.get(i).getCreateTime()).substring(5, 10));
//					dataList.add(oxDataList.get(i).getContent().replace("%", ""));
//				}
//				Collections.reverse(dateList);
//				Collections.reverse(dataList);
//				map.put("oxDataList",oxDataList);
//				map.put("dateList",dateList);
//				map.put("dataList",dataList);
//
//			}catch (ParseException e){
//				e.getMessage();
//			}
//
//		}
//


        return result;
    }

    @RequestMapping("getSweetData")
    @ApiOperation(value = "获取个人 血糖健康/设备 数据", notes = "获取个人数据（type JK=‘健康数据’SB='设备数据'）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "tel", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "startTime", value = "startTime", required = true, dataType = "String"),
            @ApiImplicitParam(name = "endTime", value = "endTime", required = true, dataType = "String")})
    public Object getSweetData(Integer userId, String startTime, String endTime, String uid) {
        log.info("获取个人 血糖健康");
        Object result = new Object();

        Map map = new HashMap();
        List dateList = new ArrayList();
        List dataList = new ArrayList();


        Map<String, Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
        if (macIdMap.get("code").equals("200")) {
            String macIds[] = macIdMap.get("macIds").toString().split(",");

            Map<String, Object> typrMap = ButlerApiUtil.queryMemberDetectionDataList(macIds[0], uid, userId + "", "4", startTime, endTime);
            if (typrMap.get("code").equals("200")) {
                if (!StrUtil.hasEmpty(typrMap.get("data") + "")) {
                    cn.hutool.json.JSONArray json = JSONUtil.parseArray(typrMap.get("data"));
                    List<MyHeightData> heightDataList = new ArrayList<>();
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                    for (int i = 0; i < json.size(); i++) {
                        MyHeightData myHeight = new MyHeightData();
                        myHeight.setUserId(userId);
                        myHeight.setType("血糖");
                        myHeight.setDataCome(json.getJSONObject(i).get("dataCome") + "");
                        myHeight.setContent(json.getJSONObject(i).get("otherValue") + "mmol/L");
                        myHeight.setCreateTime(DateUtil.parse(json.getJSONObject(i).get("created_time") + ""));

                        String date = json.getJSONObject(i).get("created_time").toString().substring(5, 10);
                        dateList.add(date);
                        dataList.add(json.getJSONObject(i).get("otherValue"));
                        heightDataList.add(myHeight);
                    }
                    Collections.reverse(dateList);
                    Collections.reverse(dataList);
                    map.put("sweetDataList", heightDataList);
                    map.put("dateList", dateList);
                    map.put("dataList", dataList);

                    result = MsgUtil.ok(map);

                } else {
                    result = MsgUtil.fail("暂无数据");
                }

            } else {
                result = MsgUtil.fail(macIdMap.get("msg") + "");
            }
        } else {
            result = MsgUtil.fail(macIdMap.get("msg") + "");
        }


//		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
//		if(startTime==null || endTime ==null || startTime=="" || endTime=="") {
//			List<MyBooldSweetData> sweetDataList = sweetDataDao.getByUserId(userId);
//			for (int i = 0; i < sweetDataList.size(); i++) {
//				String date = sf.format(sweetDataList.get(i).getCreateTime()).substring(5, 10);
//				dateList.add(date);
//
//				dataList.add(sweetDataList.get(i).getContent().replace("mmol/L", ""));
//			}
//			Collections.reverse(dateList);
//			Collections.reverse(dataList);
//			map.put("sweetDataList", sweetDataList);
//			map.put("dateList", dateList);
//			map.put("dataList", dataList);
//		}else{
//			try {
//				String time = DateUtil.now();
//				startTime = startTime+" 00:00:00";
//				endTime = endTime+time.substring(10,time.length());
//				Date startDate = sf.parse(startTime);
//				Date endDate = sf.parse(endTime);
//				List<MyBooldSweetData> sweetDataList = sweetDataDao.getByUserIdandAndCreateTime(userId,startDate,endDate);
//				for(int i =0;i<sweetDataList.size();i++){
//					dateList.add(sf.format(sweetDataList.get(i).getCreateTime()).substring(5, 10));
//					dataList.add(sweetDataList.get(i).getContent().replace("mmol/L", ""));
//				}
//				Collections.reverse(dateList);
//				Collections.reverse(dataList);
//				map.put("sweetDataList",sweetDataList);
//				map.put("dateList",dateList);
//				map.put("dataList",dataList);
//
//			}catch (ParseException e){
//				e.getMessage();
//			}
//
//		}


        return result;
    }

    @RequestMapping("getTimesData")
    @ApiOperation(value = "获取个人 心率健康/设备 数据", notes = "获取个人数据（type JK=‘健康数据’SB='设备数据'）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "tel", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "startTime", value = "startTime", required = true, dataType = "String"),
            @ApiImplicitParam(name = "endTime", value = "endTime", required = true, dataType = "String")})
    public Object getTimesData(Integer userId, String startTime, String endTime, String uid) {
        log.info("获取个人 心率健康");
        Object result = new Object();
        Map map = new HashMap();
        List dateList = new ArrayList();
        List dataList = new ArrayList();


        Map<String, Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
        if (macIdMap.get("code").equals("200")) {
            String macIds[] = macIdMap.get("macIds").toString().split(",");

            Map<String, Object> typrMap = ButlerApiUtil.queryMemberDetectionDataList(macIds[0], uid, userId + "", "0", startTime, endTime);
            if (typrMap.get("code").equals("200")) {
                if (!StrUtil.hasEmpty(typrMap.get("data") + "")) {
                    cn.hutool.json.JSONArray json = JSONUtil.parseArray(typrMap.get("data"));
                    List<MyHeightData> heightDataList = new ArrayList<>();
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                    for (int i = 0; i < json.size(); i++) {
                        MyHeightData myHeight = new MyHeightData();
                        myHeight.setUserId(userId);
                        myHeight.setType("心率");
                        myHeight.setDataCome(json.getJSONObject(i).get("dataCome") + "");
                        myHeight.setContent(json.getJSONObject(i).get("heartbeat") + "次/分钟");
                        myHeight.setCreateTime(DateUtil.parse(json.getJSONObject(i).get("created_time") + ""));
                        String date = json.getJSONObject(i).get("created_time").toString().substring(5, 10);
                        dateList.add(date);
                        dataList.add(json.getJSONObject(i).get("heartbeat"));
                        heightDataList.add(myHeight);
                    }
                    Collections.reverse(dateList);
                    Collections.reverse(dataList);
                    map.put("heartTimesDataList", heightDataList);
                    map.put("dateList", dateList);
                    map.put("dataList", dataList);

                    result = MsgUtil.ok(map);
                } else {
                    result = MsgUtil.fail("暂无数据");
                }


            } else {
                result = MsgUtil.fail(macIdMap.get("msg") + "");
            }
        } else {
            result = MsgUtil.fail(macIdMap.get("msg") + "");
        }


//		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
//		if(startTime==null || endTime ==null || startTime=="" || endTime=="") {
//			List<MyHeartTimesData> heartTimesDataList = timesDataDao.getByUserId(userId);
//			for (int i = 0; i < heartTimesDataList.size(); i++) {
//				String date = sf.format(heartTimesDataList.get(i).getCreateTime()).substring(5, 10);
//				dateList.add(date);
//
//				dataList.add(heartTimesDataList.get(i).getContent().replace("次/分钟", ""));
//			}
//			Collections.reverse(dateList);
//			Collections.reverse(dataList);
//			map.put("heartTimesDataList", heartTimesDataList);
//			map.put("dateList", dateList);
//			map.put("dataList", dataList);
//		}else{
//			try {
//				String time = DateUtil.now();
//				startTime = startTime+" 00:00:00";
//				endTime = endTime+time.substring(10,time.length());
//				Date startDate = sf.parse(startTime);
//				Date endDate = sf.parse(endTime);
//				List<MyHeartTimesData> heartTimesDataList = timesDataDao.getByUserIdandAndCreateTime(userId,startDate,endDate);
//				for(int i =0;i<heartTimesDataList.size();i++){
//					dateList.add(sf.format(heartTimesDataList.get(i).getCreateTime()).substring(5, 10));
//					dataList.add(heartTimesDataList.get(i).getContent().replace("次/分钟", ""));
//				}
//				Collections.reverse(dateList);
//				Collections.reverse(dataList);
//				map.put("heartTimesDataList",heartTimesDataList);
//				map.put("dateList",dateList);
//				map.put("dataList",dataList);
//
//			}catch (ParseException e){
//				e.getMessage();
//			}
//
//		}


        return result;
    }

    @RequestMapping("saveData")
    @ApiOperation(value = "添加个人 健康/设备 数据", notes = "添加个人数据（type JK=‘健康数据’SB='设备数据'）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "content", value = "content", required = true, dataType = "String"),
            @ApiImplicitParam(name = "relation", value = "relation", required = true, dataType = "String"),
            @ApiImplicitParam(name = "type", value = "type", required = true, dataType = "String"),
            @ApiImplicitParam(name = "dataCome", value = "dataCome", required = true, dataType = "String"),
            @ApiImplicitParam(name = "userId", value = "tel", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "highPer", value = "highPer", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
            @ApiImplicitParam(name = "time", value = "time", required = true, dataType = "String"),
            @ApiImplicitParam(name = "lowPer", value = "lowPer", required = true, dataType = "String"),
            @ApiImplicitParam(name = "cholesterol", value = "总胆固醇", required = true, dataType = "String"),
            @ApiImplicitParam(name = "triglyceride", value = "甘油三酯", required = true, dataType = "String"),
            @ApiImplicitParam(name = "lowDensityLipoprotein", value = "低密度脂蛋白", required = true, dataType = "String"),
            @ApiImplicitParam(name = "highDensityLipoprotein", value = "高密度脂蛋白", required = true, dataType = "String"),
            @ApiImplicitParam(name = "measureTime", value = "测量时间", required = true, dataType = "String"),
    })
    public Object saveData(String content, String relation, String uid, String type, String dataCome, Integer userId, String highPer, String lowPer, String time,
                           String cholesterol, String triglyceride, String lowDensityLipoprotein, String highDensityLipoprotein, String measureTime) {

        log.info("获添加个人 健康1");
        //TaobaoClient
        Date date = new Date();

        Map<String, String> parm = new HashMap<>();

        Map<String, Object> macIdMap = ButlerApiUtil.queryDeviceBinding(uid);
        if (macIdMap.get("code").equals("200")) {
            String macIds[] = macIdMap.get("macIds").toString().split(",");
            parm.put("macId", macIds[0]);
            parm.put("uid", uid);
            parm.put("dataCome", dataCome);
            parm.put("memberId", userId + "");
            if (!StrUtil.hasEmpty(time)) {
                parm.put("created_time", time);
            } else {
                parm.put("created_time", DateUtil.now());
            }

            if ("身高".equals(type)) {

//			MyHeightData heightData = new MyHeightData();
//			heightData.setContent(content);
//			heightData.setCreateTime(date);
//			heightData.setUserId(userId);
//			heightData.setDataCome(dataCome);
//			heightData.setRelation(relation);
//			heightData.setType(type);
//
////			heightDataDao.saveAndFlush(heightData);
//
//			Users user = UserDao.getOne(userId);
//
//			MyHome list  = homeDao.getByTel(user.getPhone());
//			list.setHeight(content);
//			homeDao.saveAndFlush(list);

                parm.put("type", "身高");
                parm.put("height", content);


                Map<String, Object> map = ButlerApiUtil.addDetectionData(parm);
                if (map.get("code").equals("200")) {
                    return MsgUtil.ok("身高添加成功");
                } else {
                    return MsgUtil.fail(map.get("msg") + "");
                }


            } else if ("体重".equals(type)) {
//			MyWeightData weightData = new MyWeightData();
//			weightData.setContent(content);
//			weightData.setCreateTime(date);
//			weightData.setUserId(userId);
//			weightData.setDataCome(dataCome);
//			weightData.setRelation(relation);
//			weightData.setType(type);
//			weightDataDao.saveAndFlush(weightData);
//
//
//			Users user = UserDao.getOne(userId);
//
//			MyHome list  = homeDao.getByTel(user.getPhone());
//			list.setWeight(content);
//			homeDao.saveAndFlush(list);

                parm.put("type", "体重");
                parm.put("weight", content);
                Map<String, Object> map = ButlerApiUtil.addDetectionData(parm);
                if (map.get("code").equals("200")) {
                    return MsgUtil.ok("体重添加成功");
                } else {
                    return MsgUtil.fail(map.get("msg") + "");
                }

            } else if ("血脂".equals(type)) {
//			MyBooldHearData hearData = new MyBooldHearData();
//			hearData.setContent(content);
//			hearData.setCreateTime(date);
//			hearData.setUserId(userId);
//			hearData.setDataCome(dataCome);
//			hearData.setRelation(relation);
//			hearData.setType(type);
//			booldHearDataDao.saveAndFlush(hearData);
//			return MsgUtil.ok("血脂添加成功!");

                parm.put("type", "血脂");
                cn.hutool.json.JSONObject zd = new cn.hutool.json.JSONObject();
                if (!StrUtil.hasEmpty(cholesterol)) {
                    zd.put("cholesterol", cholesterol);
                }
                if (!StrUtil.hasEmpty(triglyceride)) {
                    zd.put("triglyceride", triglyceride);
                }
                if (!StrUtil.hasEmpty(lowDensityLipoprotein)) {
                    zd.put("lowDensityLipoprotein", lowDensityLipoprotein);
                }
                if (!StrUtil.hasEmpty(highDensityLipoprotein)) {
                    zd.put("highDensityLipoprotein", highDensityLipoprotein);
                }
                parm.put("bloodFat", JSONUtil.toJsonPrettyStr(zd));
                Map<String, Object> map = ButlerApiUtil.addDetectionData(parm);
                if (map.get("code").equals("200")) {
                    return MsgUtil.ok("血脂添加成功");
                } else {
                    return MsgUtil.fail(map.get("msg") + "");
                }


            } else if ("血压".equals(type)) {
//			MyBooldPerData perData = new MyBooldPerData();
//			perData.setContent(content);
//			perData.setHighPer(highPer);
//			perData.setLowPer(lowPer);
//			perData.setCreateTime(date);
//			perData.setUserId(userId);
//			perData.setDataCome(dataCome);
//			perData.setRelation(relation);
//			perData.setType(type);
//			booldPerDataDao.saveAndFlush(perData);
//			return MsgUtil.ok("血压添加成功!");
                parm.put("type", "血压");
                parm.put("bloodPressure", highPer + "mmHg~" + lowPer + "mmHg");

                Map<String, Object> map = ButlerApiUtil.addDetectionData(parm);
                if (map.get("code").equals("200")) {
                    return MsgUtil.ok("血压添加成功");
                } else {
                    return MsgUtil.fail(map.get("msg") + "");
                }

            } else if ("血糖".equals(type)) {
//			MyBooldSweetData sweetData = new MyBooldSweetData();
//			sweetData.setContent(content);
//			sweetData.setCreateTime(date);
//			sweetData.setUserId(userId);
//			sweetData.setDataCome(dataCome);
//			sweetData.setRelation(relation);
//			sweetData.setType(type);
//			sweetDataDao.saveAndFlush(sweetData);
//			return MsgUtil.ok("血糖添加成功!");

                parm.put("type", "血糖");
                parm.put("bloodSugar", content);
                parm.put("measureTime", measureTime);
                Map<String, Object> map = ButlerApiUtil.addDetectionData(parm);
                if (map.get("code").equals("200")) {
                    return MsgUtil.ok("血糖添加成功");
                } else {
                    return MsgUtil.fail(map.get("msg") + "");
                }

            } else if ("血氧".equals(type)) {
//			MyBooldPerData perData = new MyBooldPerData();
//			MyBooldOxData oxData = new MyBooldOxData();
//			oxData.setContent(content);
//			oxData.setCreateTime(date);
//			oxData.setUserId(userId);
//			oxData.setDataCome(dataCome);
//			oxData.setRelation(relation);
//			oxData.setType(type);
//			oxDataDao.saveAndFlush(oxData);
//			return MsgUtil.ok("血氧添加成功!");

                parm.put("type", "血氧");
                parm.put("bloodOxygen", content);
                Map<String, Object> map = ButlerApiUtil.addDetectionData(parm);
                if (map.get("code").equals("200")) {
                    return MsgUtil.ok("血氧添加成功");
                } else {
                    return MsgUtil.fail(map.get("msg") + "");
                }

            } else if ("心率".equals(type)) {
//			MyHeartTimesData perData = new MyHeartTimesData();
//			perData.setContent(content);
//			perData.setCreateTime(date);
//			perData.setUserId(userId);
//			perData.setDataCome(dataCome);
//			perData.setRelation(relation);
//			perData.setType(type);
//			timesDataDao.saveAndFlush(perData);
//			return MsgUtil.ok("心率添加成功!");

                parm.put("type", "心率");
                parm.put("heartbeat", content);

                Map<String, Object> map = ButlerApiUtil.addDetectionData(parm);
                if (map.get("code").equals("200")) {
                    return MsgUtil.ok("心率添加成功");
                } else {
                    return MsgUtil.fail(map.get("msg") + "");
                }
            }
        } else {
            return MsgUtil.fail(macIdMap.get("msg") + "");
        }


        return MsgUtil.fail("类型不明确");
    }


    @RequestMapping("saveTkData")
    @ApiOperation(value = "获取个人 健康/设备 数据", notes = "获取个人数据（type JK=‘健康数据’SB='设备数据'）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "content", value = "content", required = true, dataType = "String"),
            @ApiImplicitParam(name = "relation", value = "relation", required = true, dataType = "String"),
            @ApiImplicitParam(name = "type", value = "type", required = true, dataType = "String"),
            @ApiImplicitParam(name = "dataCome", value = "dataCome", required = true, dataType = "String"),
            @ApiImplicitParam(name = "mainTel", value = "mainTel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "highPer", value = "highPer", required = true, dataType = "String"),
            @ApiImplicitParam(name = "lowPer", value = "lowPer", required = true, dataType = "String")})
    public Object saveTkData(String content, String relation, String type, String dataCome, String mainTel, String highPer, String lowPer) {
        log.info("获添加个人 健康2");
        Date date = new Date();
        String tel = homeDao.getByMainTelandAndRelation(mainTel, relation).getTel();
        Integer userId = UserDao.getByTel(tel).getId();
        if ("厘米".equals(type)) {
            MyHeightData heightData = new MyHeightData();
            heightData.setContent(content);
            heightData.setCreateTime(date);
            heightData.setUserId(userId);
            heightData.setDataCome(dataCome);
            heightData.setRelation(relation);
            heightData.setType("身高");
            heightDataDao.saveAndFlush(heightData);

            Users user = UserDao.getOne(userId);

            MyHome list = homeDao.getByTel(user.getPhone());
            list.setHeight(content);
            homeDao.saveAndFlush(list);

            return MsgUtil.ok("身高添加成功!");
        } else if ("公斤".equals(type)) {
            MyWeightData weightData = new MyWeightData();
            weightData.setContent(content);
            weightData.setCreateTime(date);
            weightData.setUserId(userId);
            weightData.setDataCome(dataCome);
            weightData.setRelation(relation);
            weightData.setType("体重");
            weightDataDao.saveAndFlush(weightData);
            Users user = UserDao.getOne(userId);
            MyHome list = homeDao.getByTel(user.getPhone());
            list.setWeight(content);
            homeDao.saveAndFlush(list);
            return MsgUtil.ok("体重添加成功!");
        } else if ("mg/dl".equals(type)) {
            MyBooldHearData hearData = new MyBooldHearData();
            hearData.setContent(content);
            hearData.setCreateTime(date);
            hearData.setUserId(userId);
            hearData.setDataCome(dataCome);
            hearData.setRelation(relation);
            hearData.setType("血脂");
            booldHearDataDao.saveAndFlush(hearData);
            return MsgUtil.ok("血脂添加成功!");
        } else if ("mmHg".equals(type)) {
            MyBooldPerData perData = new MyBooldPerData();
            perData.setHighPer(highPer);
            perData.setLowPer(lowPer);
            perData.setCreateTime(date);
            perData.setUserId(userId);
            perData.setDataCome(dataCome);
            perData.setRelation(relation);
            perData.setType("血压");
            booldPerDataDao.saveAndFlush(perData);
            return MsgUtil.ok("血压添加成功!");
        } else if ("%".equals(type)) {
            MyBooldOxData oxData = new MyBooldOxData();
            oxData.setContent(content);
            oxData.setCreateTime(date);
            oxData.setUserId(userId);
            oxData.setDataCome(dataCome);
            oxData.setRelation(relation);
            oxData.setType(type);
            oxDataDao.saveAndFlush(oxData);
            return MsgUtil.ok("血氧添加成功!");
        } else if ("mmol/L".equals(type)) {
            MyBooldSweetData sweetData = new MyBooldSweetData();
            sweetData.setContent(content);
            sweetData.setCreateTime(date);
            sweetData.setUserId(userId);
            sweetData.setDataCome(dataCome);
            sweetData.setRelation(relation);
            sweetData.setType(type);
            sweetDataDao.saveAndFlush(sweetData);
            return MsgUtil.ok("血糖添加成功!");
        } else if ("次/分钟".equals(type)) {
            MyHeartTimesData perData = new MyHeartTimesData();
            perData.setContent(content);
            perData.setCreateTime(date);
            perData.setUserId(userId);
            perData.setDataCome(dataCome);
            perData.setRelation(relation);
            perData.setType(type);
            timesDataDao.saveAndFlush(perData);
            return MsgUtil.ok("心率添加成功!");
        }
        return MsgUtil.fail("类型不明确");
    }


    @RequestMapping("queryAllHomes")
    @ApiOperation(value = "查询所有家庭成员的基础数据", notes = "查询所有家庭成员的基础数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "Integer")
    })
    public Object queryAllHomes(String openId) {

        Users user = UserDao.getByOpenId(openId);
        List<MyHome> list = homeDao.getByMainTel(user.getPhone());
        return MsgUtil.ok(list);
    }


    @RequestMapping("queryAllHealthDict")
    @ApiOperation(value = "查询所有小病类型", notes = "查询所有小病类型")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String")
    })
    public Object queryAllHealthDict(String tel) {
        List list = new ArrayList();
        List<HealthDict> healthDictList = healthDictDao.getByTel(tel);
        for (int i = 0; i < healthDictList.size(); i++) {
            Map map = new HashMap();
            String healthContent = healthContentDao.getByHealthCode(healthDictList.get(i).getHealthCode()).get(0).getHealthContent();
            String healthDc = healthContentDao.getByHealthCode(healthDictList.get(i).getHealthCode()).get(0).getHealthDc();
            String healthDay = healthContentDao.getByHealthCode(healthDictList.get(i).getHealthCode()).get(0).getHealthDay();
            String healthHos = healthContentDao.getByHealthCode(healthDictList.get(i).getHealthCode()).get(0).getHealthHos();
            int contentCount = healthContentDao.getByHealthCode(healthDictList.get(i).getHealthCode()).size();
            map.put("healthCode", healthDictList.get(i).getHealthCode());
            map.put("healthName", healthDictList.get(i).getHealthName());
            map.put("healthContent", healthContent);
            map.put("healthDc", healthDc);
            map.put("healthDay", healthDay);
            map.put("healthHos", healthHos);
            map.put("contentCount", contentCount);
            list.add(map);
        }
        if (list.size() > 0 && list != null) {
            return MsgUtil.ok(list);
        } else {
            return MsgUtil.fail("暂无小病类型");
        }

    }

    @RequestMapping("getContentByCode")
    @ApiOperation(value = "查询小病病症", notes = "查询小病病症")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "healthCode", value = "healthCode", required = true, dataType = "String")
    })
    public Object getContentByCode(String healthCode) {
        List<HealthContent> list = healthContentDao.getByHealthCode(healthCode);
        if (list.size() > 0 && list != null) {
            return MsgUtil.ok(list);
        } else {
            return MsgUtil.fail("暂无小病病症");
        }

    }

    @RequestMapping("addHealthDict")
    @ApiOperation(value = "添加小病类型", notes = "添加小病类型")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "healthName", value = "healthName", required = true, dataType = "String"),
            @ApiImplicitParam(name = "healthDay", value = "healthDay", required = true, dataType = "String"),
            @ApiImplicitParam(name = "healthDc", value = "healthDc", required = true, dataType = "String"),
            @ApiImplicitParam(name = "healthHos", value = "healthHos", required = true, dataType = "String"),
            @ApiImplicitParam(name = "healthContent", value = "healthContent", required = true, dataType = "String")
    })
    public Object addHealthDict(String tel, String healthName, String healthContent, String healthDay, String healthDc, String healthHos) {
        HealthDict dict = new HealthDict();
        HealthContent content = new HealthContent();
        String healthCode = UUID.randomUUID().toString();
        dict.setTel(tel);
        dict.setHealthName(healthName);
        dict.setHealthCode(healthCode);
        dict.setCreateTime(new Date());
        healthDictDao.saveAndFlush(dict);

        content.setTel(tel);
        content.setHealthCode(healthCode);
        content.setHealthContent(healthContent);
        content.setCreateTime(new Date());
        content.setHealthDay(healthDay);
        content.setHealthDc(healthDc);
        content.setHealthHos(healthHos);
        healthContentDao.saveAndFlush(content);
        return MsgUtil.ok();
    }

    @RequestMapping("addHealthContent")
    @ApiOperation(value = "添加小病类型", notes = "添加小病类型")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "healthCode", value = "healthCode", required = true, dataType = "String"),
            @ApiImplicitParam(name = "healthContent", value = "healthContent", required = true, dataType = "String")
    })
    public Object addHealthContent(String tel, String healthCode, String healthContent, String healthDay, String healthDc, String healthHos) {
        HealthContent content = new HealthContent();
        content.setTel(tel);
        content.setHealthCode(healthCode);
        content.setHealthContent(healthContent);
        content.setCreateTime(new Date());
        content.setHealthDay(healthDay);
        content.setHealthDc(healthDc);
        content.setHealthHos(healthHos);
        healthContentDao.saveAndFlush(content);
        return MsgUtil.ok();
    }

    @RequestMapping("queryYiqifaGoodsInfo")
    @ApiOperation(value = "查亿起发商品接口", notes = "查亿起发商品接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productName", value = "productName", required = true, dataType = "String"),
            @ApiImplicitParam(name = "pageIndex", value = "pageIndex", required = true, dataType = "String")
    })
    public Object queryYiqifaGoodsInfo(String productName, String pageIndex) {

        JSONObject obj = FunctionUtil.yiqifaGoddsInfo(productName, pageIndex);
        return MsgUtil.ok(obj);
    }


//    @RequestMapping("querySuninggoodsInfo")
//    @ApiOperation(value = "查询苏宁商品接口", notes = "查亿起发商品接口")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "productName", value = "productName", required = true, dataType = "String"),
//            @ApiImplicitParam(name = "pageIndex", value = "pageIndex", required = true, dataType = "String"),
//            @ApiImplicitParam(name = "label", value = "自带标签", required = false, dataType = "String"),
//            @ApiImplicitParam(name = "butlerId", value = "智能管家设备号", required = false, dataType = "String"),
//            @ApiImplicitParam(name = "openId", value = "用户微信id", required = false, dataType = "String"),
//            @ApiImplicitParam(name = "uid", value = "用户通用id", required = false, dataType = "String"),
//            @ApiImplicitParam(name = "channel", value = "渠道", required = false, dataType = "String"),
//            @ApiImplicitParam(name = "relation", value = "为谁买", required = false, dataType = "String"),
//    })
//    public Object querySuninggoodsInfo(String productName, String pageIndex, String label, String channel, String butlerId, String openId, String relation, String uid, HttpSession session) {
//        log.info("搜索商品开始时间：" + DateUtil.now());
//
//
//        Object obj = new Object();
////		JSONObject obj = FunctionUtil.suning(productName,pageIndex);
//        MsgUtil msg = new MsgUtil();
//
//        Map<String, Object> goodsMap = new HashMap<>();
//        int num = 0;
//
//        try {
//
//            if (!StrUtil.hasEmpty(productName) && !StrUtil.hasEmpty(channel)) {
//                WitGoodsFunction goodsFunction = witGoodsFunctionService.getGoodsFunction(productName);
//                if (goodsFunction != null) {
//                    productName += goodsFunction.getFunction();
//                }
//
//                //查询有无搜索过
//                List<WitShoppingGoods> findSearch = witShoppingGoodsService.findSearch(productName, Integer.parseInt(pageIndex), openId, relation, uid);
//                Map<String, Object> goodsList = new HashMap<>();
//
//                String pageIndexNums = pageIndex;
//
//                if (findSearch.size() > 0) {
//                    int nums = witShoppingGoodsService.getMaxPageIndex(productName, uid) + 1;
//                    pageIndexNums = nums + "";
//                }
//
//                if (findSearch != null && findSearch.size() == 8) {
//                    goodsList.put("goodsList", findSearch);
//                    obj = MsgUtil.ok(goodsList);
//
//                } else {
//
//                    //查询所有可用联盟平台数据
//                    List<WitShoppingUnionDeploy> unionDeployList = witShoppingUnionDeployService.findUnionDeploy(channel);
//
//                    if (unionDeployList.size() > 0) {
//                        int pageSize = 6;
//
//
//                        Map<String, Object> yqf = new HashMap<>();
//                        Map<String, Object> sn = new HashMap<>();
//                        Map<String, Object> wph = new HashMap<>();
//                        Map<String, Object> klzk = new HashMap<>();
//
//                        Object sessionName = session.getAttribute("goodsSession");
//
//
//                        //接口参数集
//                        Map<String, Object> parameter = new HashMap<>();
//                        if (!StrUtil.hasEmpty(relation)) {
//                            if (relation.equals("全家人使用") || relation.equals("其他")) {
//                                parameter.put("productName", productName + " ");
//                            } else {
//                                MjFamilyRelation mjFamilyRelation = mjFamilyRelationService.getMjFamilyRelation(relation);
//                                if (mjFamilyRelation != null) {
//                                    parameter.put("productName", productName + " " + mjFamilyRelation.getSex());
//                                } else {
//                                    parameter.put("productName", productName + " ");
//                                }
//
//                            }
//                        } else {
//                            parameter.put("productName", productName + " ");
//                        }
//
//
//                        parameter.put("butlerId", butlerId);
//                        parameter.put("relation", relation);
//
//                        if (!StrUtil.hasEmpty(label)) {
//                            parameter.put("label", label);
//
//                        } else {
//                            parameter.put("label", "1");
//                        }
//
//                        parameter.put("yqfPageIndex", "0");
//                        parameter.put("snPageIndex", "0");
//                        parameter.put("wphPageIndex", "0");
//                        parameter.put("klzkPageIndex", "0");
//                        parameter.put("pageIndex", pageIndexNums);
//                        parameter.put("uid", uid);
//
//                        log.info("当前页码：" + pageIndexNums);
//
//                        if (!StrUtil.hasEmpty(sessionName + "")) {
//                            cn.hutool.json.JSONObject json = JSONUtil.parseObj(session.getAttribute("goodsSession"));
//                            String productNames = json.getStr("productName");
//                            if (!StrUtil.hasEmpty(productNames)) {
//                                if (productNames.equals(productName)) {
//                                    String yqfPageIndex = json.getStr("yqfPageIndex");
//                                    String snPageIndex = json.getStr("snPageIndex");
//                                    String wphPageIndex = json.getStr("wphPageIndex");
//
//                                    if (!StrUtil.hasEmpty(yqfPageIndex)) {
//                                        parameter.put("yqfPageIndex", json.getInt("yqfPageIndex"));
//                                    }
//                                    if (!StrUtil.hasEmpty(snPageIndex)) {
//                                        parameter.put("snPageIndex", json.getInt("snPageIndex"));
//                                    }
//                                    if (!StrUtil.hasEmpty(wphPageIndex)) {
//                                        parameter.put("wphPageIndex", json.getInt("wphPageIndex"));
//                                    }
//                                }
//                            }
//
//                        }
//
//
//                        JSONArray parame = new JSONArray();
//                        int b = 0;
//                        //调用联盟平台接口筛选商品数据
//                        for (WitShoppingUnionDeploy unionDeploy : unionDeployList) {
//
////						cn.hutool.json.JSONObject pbj=new cn.hutool.json.JSONObject();
////						pbj.put("dataType", unionDeploy.getChannel());
////						pbj.put("type", unionDeploy.getChannel());
////						pbj.put("productName", productName);
////						pbj.put("plat", channel);
////						pbj.put("openId", openId);
////						pbj.put("pageIndex",pageIndex);
////
////						pbj.put("butlerId",butlerId);
////
////						if(!StrUtil.hasEmpty(label)){
////							pbj.put("label",label);
////
////						}else{
////							pbj.put("label","1");
////						}
////						parame.add(pbj);
//
//
//                            double size = pageSize * unionDeploy.getProportion();
//                            parameter.put("pageSize", Math.round(size));
//
//                            if ("sn".equals(unionDeploy.getChannel())) {
//                                //sn = asyncTask.asyncsn(parameter,openId);
//                                if (num == 0) {
//                                    sn = SuningGoodsApi.finalData(parameter, openId);
//                                    num = Integer.parseInt(sn.get("sqlSnExecute").toString());
//                                    ;
//                                } else {
//                                    asyncTask.asyncsn(parameter, openId);
//                                }
//
//                            } else if ("yqf".equals(unionDeploy.getChannel())) {
//                                if (num == 0) {
//                                    yqf = YiqifaGoodsApi.finalData(parameter, openId);
//                                    num = Integer.parseInt(yqf.get("sqlYqfExecute").toString());
//                                    ;
//                                } else {
//                                    asyncTask.asyncyqf(parameter, openId);
//                                }
//                            } else if ("wph".equals(unionDeploy.getChannel())) {
//                                parameter.put("plat", channel);
//                                //wph =WphGoodsApi.finalData(parameter,openId);
//                                if (num == 0) {
//                                    wph = WphGoodsApi.finalData(parameter, openId);
//                                    num = Integer.parseInt(wph.get("sqlWphExecute").toString());
//                                } else {
//                                    asyncTask.asyncwph(parameter, openId);
//                                }
//                            } else if ("klzk".equals(unionDeploy.getChannel())) {
//                                if (num == 0) {
//                                    klzk = KlzkGoodsApi.KlzkGoddsInfo(parameter, openId);
//                                    num = Integer.parseInt(klzk.get("sqlklzkExecute").toString());
//                                } else {
//                                    asyncTask.asyncklzk(parameter, openId);
//                                }
//
//                                //klzk = asyncTask.asyncklzk(parameter,openId);
//                            }
//
//
//                        }
//
////					Map<String,Object> map = ThreadPool.goodsSummary(parame);
//
//
//						/*goodsMap.put("yqfPageIndex",yqf.get("yqfPageIndex"));
//						goodsMap.put("productName",productName);
//						goodsMap.put("snPageIndex",sn.get("snPageIndex"));
//						goodsMap.put("wphPageIndex",wph.get("wphPageIndex"));
//						goodsMap.put("klzkPageIndex",klzk.get("klzkPageIndex"));
//						//数据保存至session
//						session.setAttribute("goodsSession", goodsMap);
//
//						if(yqf.containsKey("sqlYqfExecute")){
//							num+= Integer.parseInt(yqf.get("sqlYqfExecute").toString());
//						}
//						if(sn.containsKey("sqlSnExecute")){
//							num+= Integer.parseInt(sn.get("sqlSnExecute").toString());
//						}
//						if(wph.containsKey("sqlWphExecute")){
//							num+= Integer.parseInt(wph.get("sqlWphExecute").toString());
//						}
//						if(klzk.containsKey("sqlklzkExecute")){
//							num+= Integer.parseInt(klzk.get("sqlklzkExecute").toString());
//						}
//*/
//
//                        log.info("查看苏宁商品开始时间******：" + DateUtil.now());
//                        List<WitShoppingGoods> findSearchs = witShoppingGoodsService.findSearch(parameter.get("productName") + "", Integer.parseInt(pageIndex), openId, relation, uid);
//                        log.info("查看苏宁商品结束时间******：" + DateUtil.now());
//                        goodsList.put("goodsList", findSearchs);
//
//                        if (findSearchs.size() > 0 || findSearchs != null) {
//                            obj = MsgUtil.ok(goodsList);
//                        } else {
//                            obj = MsgUtil.fail("搜索数据为空");
//                        }
//
//                    } else {
//                        obj = MsgUtil.fail("暂无可用数据");
//                    }
//                }
//            } else {
//                obj = MsgUtil.badArgument();
//            }
//
//        } catch (Exception e) {
//            log.info("智选购物异常：" + e.getMessage());
//            obj = MsgUtil.serious();
//        }
//        log.info("搜索商品参数：uid=" + uid + " openId=" + openId + " relation=" + relation);
//        log.info("搜索商品结束时间：" + DateUtil.now());
//
//        return obj;
//    }


    @RequestMapping("querySuninggoodsUrl")
    @ApiOperation(value = "查询苏宁商品接口", notes = "查亿起发商品接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "htmUrl", value = "htmUrl", required = true, dataType = "String")
    })
    public Object querySuninggoodsUrl(String htmUrl) {

        JSONObject obj = FunctionUtil.suningWx(htmUrl);
        return MsgUtil.ok(obj);
    }

    @GetMapping("getGoods")
    @ApiOperation(value = "商品详情接口", notes = "商品详情接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goodsId", value = "goodsId", required = true, dataType = "String")
    })
    public Object getGoods(String goodsId) {
        Map<String, Object> goodsList = new HashMap<>();

        WitShoppingGoods witShoppingGoods = witShoppingGoodsService.getById(goodsId);

        if (witShoppingGoods != null) {
            goodsList.put("goods", witShoppingGoods);
            return MsgUtil.ok(goodsList);
        } else {
            return MsgUtil.fail("查询数据为空");
        }
    }

    @RequestMapping("union/goods/details")
    @ApiOperation(value = "联盟商品详情接口", notes = "联盟商品详情接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goodsId", value = "goodsId", required = true, dataType = "String")
    })
    public Object getUnionGoodsDetails(String goodsId) {
        Object result = new Object();
        WitShoppingGoods witShoppingGoods = witShoppingGoodsService.getById(goodsId);
        if (witShoppingGoods != null) {
            if (!StrUtil.hasEmpty(witShoppingGoods.getCommodityCode())) {
                String goodsChannel = witShoppingGoods.getGoodsChannel();
                GoodsDetails goodsDetails = new GoodsDetails();
                if (goodsChannel.equals("wph")) {
                    goodsDetails = WphGoodsApi.goodsDetails(witShoppingGoods.getCommodityCode(), witShoppingGoods.getAppId(), witShoppingGoods.getAppletExtendUrl());
                } else if (goodsChannel.equals("klzk")) {
                    goodsDetails = KlzkGoodsApi.goodsDetails(witShoppingGoods.getCommodityCode(), witShoppingGoods.getAppId(), witShoppingGoods.getAppletExtendUrl());
                }
                if (goodsDetails != null) {
                    result = MsgUtil.ok(goodsDetails);
                } else {
                    result = MsgUtil.ok("notResult");
                }
            } else {
                result = MsgUtil.ok("notResult");
            }
        } else {
            result = MsgUtil.ok("notResult");
        }
        return result;
    }

    //    public static final ConcurrentHashMap<String, ResultCallback> callbacks = new ConcurrentHashMap<>();
    public static final ConcurrentHashMap<String, CompletableFuture<GoodsResponse>> MAP = new ConcurrentHashMap<>();

    @DubboReference
    private WebSocketService webSocketService;

    @RequestMapping("querySuninggoodsInfo")
    public Object queryGoods(String productName, String pageIndex, String label, String channel, String butlerId, String openId, String relation, String uid) {
        MjUser mjUser = mjUserService.getById(uid);

        webSocketService.smartQuery(uid, productName, relation);
        return MsgUtil.ok();
//        GoodsQuery e1 = new GoodsQuery();
//        e1.setId(UUID.randomUUID().toString());
//        if (StringUtils.isNotBlank(pageIndex)) {
//            e1.setPage(Integer.valueOf(pageIndex));
//        } else {
//            e1.setPage(1);
//        }
//        e1.setPageSize(10);
//        e1.setUid(mjUser.getId().toString());
//
//        CompletableFuture<GoodsResponse> future = new CompletableFuture<>();
//        e1.setKeywords(new String[]{productName});
//        MAP.put(e1.getId(), future);
//        RemoteSource.QUEUE.offer(e1);
//        Map<String, Object> goodsList = new HashMap<>();
//
//        return Mono.fromFuture(future).map(value -> {
//            goodsList.put("goodsList", value.getList());
//            return MsgUtil.ok(goodsList);
//        });
    }


    @RequestMapping("detail")
    public Mono<Object> detail(String channel, String id) {
        List<ThirdGoodsApi> list = apis.stream().filter(v -> v.support(channel)).collect(Collectors.toList());
        if (list.size() > 0) {
            return list.get(0).detail(id).map(MsgUtil::ok);
        }
        return Mono.just(MsgUtil.fail());
    }

    @RequestMapping("convert")
    public Mono<Object> convertGoods(GoodsConvert convert) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("app_key", TbUnionGoodsRequestHelper.APP_KEY);
        params.put("timestamp", String.valueOf(System.currentTimeMillis() / 1000));
        params.put("service", "cps-mesh.cpslink.links.post");


        HashMap<String, Object> p1 = new HashMap<>();
        p1.put("site_id", TbUnionGoodsRequestHelper.SITE_ID);
        JSONObject obj = new JSONObject();
        ObjectNode jo = com.api.smart.utils.JSONUtil.object().put("u", convert.getSiteId());
        if (StringUtils.isNotBlank(convert.getSource())) {
            jo.put("s", convert.getSource());
        }
        obj.put("euid", jo.toString());
        p1.put("ext", obj);

        p1.put("url", convert.getUrl());
        p1.put("is_coupon", "1");

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            sb.append(entry.getKey()).append(entry.getValue());
        }

        Optional<String> optP = com.api.smart.utils.JSONUtil.toJSONString(p1);
        String json = "";
        if (optP.isPresent()) {
            json = optP.get();
            sb.append(json);
        }

        sb.insert(0, APP_SECRET).append(APP_SECRET);
        String sign = MD5.create().digestHex(sb.toString()).toUpperCase();
        params.put("sign", sign);

        return WebClient.create(RequestUtil.buildRequestUrl("https://open.duomai.com/apis", params))
                .post().contentType(MediaType.APPLICATION_JSON)
                .bodyValue(json).retrieve().bodyToMono(String.class).map(s ->
                        MsgUtil.ok(com.api.smart.utils.JSONUtil.parse(s).get().get("data"))
                );
    }

    @RequestMapping("search/platform")
    public Object searchPlatformGoods(String channel, String productName, String category, String pageIndex, String orderBy, String orderType, String label, String butlerId, String openId, String relation, String uid
            , Double startCommissionRate, Double endCommissionRate, Double startPrice, Double endPrice) {
        List<ThirdGoodsApi> api = apis.stream().filter(v -> v.support(channel)).collect(Collectors.toList());
        if (api.size() == 0) {
            return MsgUtil.fail("暂不支持当前渠道或渠道参数传入有误");
        }

        GoodsQuery e1 = new GoodsQuery();
        e1.setId(UUID.randomUUID().toString());
        if (StringUtils.isNotBlank(pageIndex)) {
            e1.setPage(Integer.valueOf(pageIndex));
        } else {
            e1.setPage(1);
        }
        e1.setStartCommissionRate(startCommissionRate);
        e1.setEndCommissionRate(endCommissionRate);
        e1.setStartPrice(startPrice);
        e1.setEndPrice(endPrice);
        e1.setOrderType(orderType);
        e1.setOrderBy(orderBy);
        e1.setCategory(category);
        e1.setPageSize(28);
        e1.setProductName(productName);
        e1.setKeywords(new String[]{productName});
        CompletableFuture<List<WitShoppingGoods>> future = new CompletableFuture<>();
        api.get(0).findGoods(e1, new SmartShoppingCallback() {
            @Override
            public void onSuccess(List<WitShoppingGoods> witShoppingGoods) {
                future.complete(witShoppingGoods);
            }

            @Override
            public void onFail(Throwable e) {
                future.complete(Collections.emptyList());
            }
        });
        return Mono.fromFuture(future).map(MsgUtil::ok);
    }

    @Resource
    private CollectionGoodsService collectionGoodsService;

    @PostMapping("addCollection")
    public Object addCollectionGoods(@RequestBody CollectionGoods collectionGoods) {
        // collectionGoods.setSysUserId(UserUtil.getUser().getUserId());
        return MsgUtil.ok(collectionGoodsService.save(collectionGoods));
    }

    @RequestMapping("search")
    public Object searchGoods(String productName, String pageIndex, String orderBy, String orderType, String label, String channel, String butlerId, String openId, String relation, String uid) {
        MjUser mjUser = mjUserService.getById(uid);
        if (mjUser == null) {
            return MsgUtil.fail("未查询到该用户");
        }
        GoodsQuery e1 = new GoodsQuery();
        e1.setId(UUID.randomUUID().toString());
        if (StringUtils.isNotBlank(pageIndex)) {
            e1.setPage(Integer.valueOf(pageIndex));
        } else {
            e1.setPage(1);
        }
        e1.setOrderType(orderType);
        e1.setOrderBy(orderBy);
        e1.setPageSize(20);
        e1.setUid(mjUser.getId().toString());

        CompletableFuture<GoodsResponse> future = new CompletableFuture<>();
        e1.setKeywords(new String[]{productName});
        MAP.put(e1.getId(), future);
        RemoteSource.QUEUE.offer(e1);
        Map<String, Object> goodsList = new HashMap<>();

        return Mono.fromFuture(future).map(value -> {
            goodsList.put("goodsList", value.getList());
            return MsgUtil.ok(goodsList);
        });
    }


    @Resource
    private ZheTaoKeCouponService zheTaoKeCouponService;

    @Resource
    private DaTaoKeCouponService daTaoKeCouponService;

    @GetMapping("coupon")
    public Object getCoupon(String content) {
        Mono<List<UnionCoupon>> dtk = daTaoKeCouponService.getCoupon(content);
        Mono<List<UnionCoupon>> ztk = zheTaoKeCouponService.getCoupon(content);
        return Mono.zip(dtk, ztk).flatMap((Function<Tuple2<List<UnionCoupon>, List<UnionCoupon>>, Mono<Object>>) objects -> {
            List<UnionCoupon> list = objects.getT1();
            list.addAll(objects.getT2());
            return Mono.just(MsgUtil.ok(list));
        });
    }

    @RequestMapping("searchGoods")
    public Object cal(@RequestParam("tel") String tel,
                      @RequestParam("productName") String productName,
                      @RequestParam("butlerId") String butlerId, String goodsNames) {
        MjUser mjUser = mjUserService.getUserByPhone(tel);

        GoodsQuery e1 = new GoodsQuery();
        e1.setPage(1);
        e1.setPageSize(10);
        e1.setUid(mjUser.getId().toString());

        e1.setKeywords(new String[]{productName});
        // todo 后续使用消息队列
        RemoteSource.QUEUE.offer(e1);
        return MsgUtil.ok("已为您搜索到合适商品,请前往未来里管家小程序查看下单");
    }

    /*@RequestMapping("searchGoods")
    @ApiOperation(value = "接收管家语音商品搜索", notes = "接收管家语音商品搜索")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "productName", value = "productName", required = true, dataType = "String"),
            @ApiImplicitParam(name = "butlerId", value = "butlerId", required = true, dataType = "String"),
    })
    public Object searchGoods(@RequestParam("tel") String tel,
                              @RequestParam("productName") String productName,
                              @RequestParam("butlerId") String butlerId, String goodsNames) {

        log.info("接收管家参数值:" + "tel=" + tel + " productName=" + productName + " butlerId=" + butlerId + " goodsNames=" + goodsNames);

        Object result = new Object();

        productName = productName.replace(" ", "");


        if (!StrUtil.hasEmpty(goodsNames)) {
            //查询产品新功能
            WitGoodsFunction goodsFunction = witGoodsFunctionService.getGoodsFunction(goodsNames);
            if (goodsFunction != null) {
                productName += goodsFunction.getFunction();
            }
        }

        System.out.println("最终查询商品名称：" + productName);

        //查询所有可用联盟平台数据
        List<WitShoppingUnionDeploy> unionDeployList = witShoppingUnionDeployService.findUnionDeployAll();
        if (unionDeployList.size() > 0 && unionDeployList != null) {

            MjUser mjUser = mjUserService.getUserByPhone(tel);
            if (mjUser != null) {
                //接口参数集
                Map<String, Object> parameter = new HashMap<>();
                parameter.put("productName", productName);
                parameter.put("butlerId", butlerId);
                parameter.put("label", "1");
                parameter.put("pageIndex", "1");
                parameter.put("pageSize", "20");
                parameter.put("uid", mjUser.getId());

                try {
                    List<WitShoppingGoods> findSearch = witShoppingGoodsService.findSearchGoods(mjUser.getId() + "", productName);
                    if (findSearch != null && findSearch.size() > 0) {
                        result = MsgUtil.ok("以为您搜索到合适商品,请前往未来里小程序查看");
                    } else {
                        for (WitShoppingUnionDeploy shoppingUnion : unionDeployList) {
                            if ("sn".equals(shoppingUnion.getChannel())) {
                                asyncTask.asyncsn(parameter, mjUser.getId() + "");
                            } else if ("yqf".equals(shoppingUnion.getChannel())) {
                                asyncTask.asyncyqf(parameter, mjUser.getId() + "");
                            } else if ("klzk".equals(shoppingUnion.getChannel())) {
                                asyncTask.asyncklzk(parameter, mjUser.getId() + "");
                            } else if ("wph".equals(shoppingUnion.getChannel())) {
                                parameter.put("plat", "applets");
                                asyncTask.asyncwph(parameter, mjUser.getId() + "");
                            }
                        }

                        result = MsgUtil.ok("以为您搜索到合适商品,请前往未来里管家小程序查看");
                    }
                    MyServiceInstructRecord myServiceInstructRecord = myServiceInstructRecordService.getMyServiceInstructRecord(tel, productName);
                    if (myServiceInstructRecord == null) {
                        MyServiceInstructRecord myServiceInstruct = new MyServiceInstructRecord();
                        myServiceInstruct.setId(IdUtil.objectId());
                        myServiceInstruct.setServiceType(8);
                        myServiceInstruct.setMjPhone(tel);
                        myServiceInstruct.setDeviceCode(butlerId);
                        myServiceInstruct.setCreateTime(DateUtil.now());
                        myServiceInstruct.setGoodsName(productName);
                        myServiceInstructRecordService.save(myServiceInstruct);
                    } else {
                        myServiceInstructRecord.setCreateTime(DateUtil.now());
                        myServiceInstructRecordService.saveOrUpdate(myServiceInstructRecord);
                    }

                } catch (Exception e) {
                    log.info("管家语音搜索商品异常：" + e.getMessage());
                    result = MsgUtil.ok("未能搜索到合适产品");
                }
            } else {
                result = MsgUtil.ok("未能搜索到合适产品");
            }

        } else {
            result = MsgUtil.ok("未能搜索到合适产品");
        }


        return result;
    }*/

    @GetMapping("findDirectGoods")
    @ApiOperation(value = "查询指令商品", notes = "查询指令商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "userId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "productName", value = "productName", required = true, dataType = "String"),
            @ApiImplicitParam(name = "pageIndex", value = "pageIndex", required = true, dataType = "String"),
            @ApiImplicitParam(name = "channel", value = "channel", required = true, dataType = "String"),
    })
    public Object findDirectGoods(String userId, String productName, String pageIndex, String channel) {
        Object result = new Object();
        if (!StrUtil.hasEmpty(userId) && !StrUtil.hasEmpty(productName) && !StrUtil.hasEmpty(channel) && !StrUtil.hasEmpty(pageIndex)) {
            List<WitShoppingGoods> list = new ArrayList<>();

            if (channel.equals("applets")) {
                list = witShoppingGoodsService.findDirectGoods(productName, userId, Integer.parseInt(pageIndex));
            } else {
                list = witShoppingGoodsService.findDirectGoodsSy(productName, userId, Integer.parseInt(pageIndex));
            }

            if (list.size() > 0 && list != null) {
                result = MsgUtil.ok(list);
            } else {
                result = MsgUtil.fail("暂无搜索数据");
            }
        } else {
            result = MsgUtil.badArgument();
        }
        return result;
    }


}
