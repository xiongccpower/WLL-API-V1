package com.controller;

import cn.hutool.core.util.StrUtil;
import com.dao.DictionariesDao;
import com.dao.MjDzOpenIdDao;
import com.dao.UserLikeDao;
import com.domain.AwardMoney;
import com.entity.MjDzOpenId;
import com.entity.UserLike;
import com.service.AwardMoneyService;
import com.util.FunctionUtil;
import com.util.MsgUtil;
import com.util.ToolUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("userLike")
@Api(value = "点赞", tags = {"点赞"})
@Validated
@CrossOrigin
public class UserLikeController {
    @Autowired
    private UserLikeDao dao;
    @Autowired
    private DictionariesDao dictionariesDao;
    @Autowired
    private MjDzOpenIdDao mjDzOpenIdDao;

    @Resource
    private AwardMoneyService awardMoneyService;

    /**
     * 点赞数
     */
    @RequestMapping("getByOpenId")
    @ApiOperation(value = "获取用户当前点赞数", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "openId", required = true, dataType = "String"),
    })
    public Object getByOpenId(String openId, String uid) {
        System.out.println("获取点赞openid=" + openId + " 获取点赞uid=" + uid);
        Object result = new Object();
        //Map<String, Object> resultMap = FunctionUtil.getLikeCountAndMoney("3");
        AwardMoney resultMap = awardMoneyService.getLikeCountAndMoney("3");
        System.out.println("AwardMoney===================:"+resultMap.getAwardRule());
        UserLike like = new UserLike();
        if (!StrUtil.hasEmpty(uid) && !uid.equals("null") && !uid.equals("undefined")) {
            like = dao.getByUid(uid);
        } else {
            like = dao.getByOpenId(openId);
        }

        if (like == null) {
            result = MsgUtil.ok("该账户未分享过！！", "-1");
        } else {

            if ((like.getLikeCount() - resultMap.getAwardRule()) >= 0) {
                like.setIsFull(1);
                result = MsgUtil.ok(like);//点赞已满
            } else {
                result = MsgUtil.ok(like);
            }
        }

        return result;
    }

    @RequestMapping("getByOpenIdIsShow")
    @ApiOperation(value = "获取用户当前点赞数", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
    })
    public Object getByOpenIdIsShow(String openId, String uid) {
        UserLike like = new UserLike();
        if (!StrUtil.hasEmpty(uid) && !uid.equals("null")) {
            like = dao.getByUid(uid);
        } else {
            like = dao.getByOpenId(openId);
        }
        //Map<String, Object> resultMap = FunctionUtil.getLikeCountAndMoney("3");
        AwardMoney resultMap = awardMoneyService.getLikeCountAndMoney("3");
        if (like == null) {
            return MsgUtil.ok("该账户未分享过！！", true);
        } else {
            if ((like.getLikeCount() - resultMap.getAwardRule()) < 0) {
                return MsgUtil.ok(true);
            }
        }

        return MsgUtil.ok(false);
    }

    /**
     * 是否分享
     */
    @RequestMapping("isShare")
    @ApiOperation(value = "是否分享", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
    })
    public Object isShare(String openId, String uid) {
        UserLike like = new UserLike();
        if (!StrUtil.hasEmpty(uid) && !uid.equals("null")) {
            like = dao.getByUid(uid);
        } else {
            like = dao.getByOpenId(openId);
        }
        if (like == null) {
            return MsgUtil.ok("1");  //首次分享
        } else {
            if (like.getIsBuy() == 1) {
                return MsgUtil.ok("1"); //购买后在此分享
            }
            return MsgUtil.ok("2"); //已分享但是未购买
        }

    }

    /**
     * 是否点赞
     */
    @RequestMapping("isLike")
    @ApiOperation(value = "是否点赞", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "dzOpenId", value = "dzOpenId", required = true, dataType = "dzOpenId")
    })
    public Object isLike(String openId, String dzOpenId) {
        MjDzOpenId dz = mjDzOpenIdDao.getByDzOpenIdAndOpenId(dzOpenId, openId);
        if (dz == null) {
            return MsgUtil.ok("1");  //未点赞
        } else {
            return MsgUtil.ok("2"); //已点赞
        }
    }


    /**
     * 点赞
     */
    @RequestMapping("like")
    @ApiOperation(value = "点赞", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "dzOpenId", value = "dzOpenId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String")
    })
    public Object like(String openId, String dzOpenId, String uid) {
        System.out.println("点赞接口;=" + openId + ":uid=" + uid + "dzOpenId=" + dzOpenId);
        UserLike like = new UserLike();
//        if (!StrUtil.hasEmpty(uid)) {
//            like = dao.getByUid(uid);
//        } else {
        like = dao.getByOpenId(openId);
//        }
        if (like == null) {
            return MsgUtil.ok("该账户未分享过！！", "-1");
        }
        //Map<String, Object> resultMap = FunctionUtil.getLikeCountAndMoney("3");
        AwardMoney resultMap = awardMoneyService.getLikeCountAndMoney("3");
        if ((like.getLikeCount() -  resultMap.getAwardRule()) >= 0) {
            like.setIsFull(1);
            return MsgUtil.ok(like);//点赞已满
        } else {
            MjDzOpenId dz = new MjDzOpenId();
            dz.setOpenId(openId);
            if (!StrUtil.hasEmpty(dzOpenId)) {
                dz.setDzOpenId(dzOpenId);
            }
            dz.setUid(uid);
            mjDzOpenIdDao.saveAndFlush(dz);

            Integer likeCount = like.getLikeCount() + 1;
            like.setLikeCount(likeCount);
            dao.saveAndFlush(like);
            return MsgUtil.ok("点赞成功！");
        }

    }

    /**
     * 取消赞
     */
    @RequestMapping("dislike")
    @ApiOperation(value = "取消赞", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "dzOpenId", value = "dzOpenId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
    })
    public Object dislike(String openId, String dzOpenId, String uid) {
        System.out.println("取消点赞接口数据:openId=" + " dzOpenId=" + dzOpenId + " uid=" + uid);
        //mjDzOpenIdDao.de
        UserLike like = new UserLike();
        if (!StrUtil.hasEmpty(uid) && !uid.equals("null")) {
            like = dao.getByUid(uid);
        } else {
            like = dao.getByOpenId(openId);
        }
        if (like == null) {
            return MsgUtil.ok("该账户未分享过！！", "-1");
        } else if (like.getLikeCount() == 0) {
            return MsgUtil.fail("还没有人点赞！");
        }

        MjDzOpenId dz = mjDzOpenIdDao.getByDzOpenId(dzOpenId, uid);
        mjDzOpenIdDao.delete(dz);//删除点赞人id

        Integer likeCount = like.getLikeCount() - 1;
        like.setLikeCount(likeCount);
        dao.saveAndFlush(like);
        return MsgUtil.ok();
    }

    @RequestMapping("shareOff")
    @ApiOperation(value = "分享", notes = "分享")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
    })
    public Object shareOff(String openId, String uid) {
        System.out.println("分享接口数据：openid=" + openId + " uid=" + uid);
        UserLike like = new UserLike();
        if (!ToolUtil.isEmpty(uid) && !ToolUtil.isEmpty(openId)) {
            like = dao.getByUid(uid);
        }
        if (ToolUtil.isEmpty(uid) && !ToolUtil.isEmpty(openId)) {
            like = dao.getByOpenId(openId);
        }
        if (!ToolUtil.isEmpty(uid) && ToolUtil.isEmpty(openId)) {
            like = dao.getByUid(uid);
        }

        if (like == null) {
            UserLike userLike = new UserLike();
            userLike.setLikeCount(0);
            if (!ToolUtil.isEmpty(openId)) {
                userLike.setOpenId(openId);
            }
            userLike.setIsBuy(0);
            userLike.setIsFull(0);
            userLike.setUid(uid);
            dao.saveAndFlush(userLike);
        } else {
            if (!ToolUtil.isEmpty(uid)) {
                if (ToolUtil.isEmpty(like.getUid())) {
                    like.setUid(uid);
                    dao.saveAndFlush(like);
                }

            }
        }

        return MsgUtil.ok();
    }

    @RequestMapping("likeOff")
    @ApiOperation(value = "查询当前价格", notes = "用openId获取单个用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "uid", value = "uid", required = true, dataType = "String"),
    })
    public Object likeOff(String openId, String uid) {
        UserLike like = new UserLike();
        if (!StrUtil.hasEmpty(uid) && !uid.equals("null")) {
            like = dao.getByUid(uid);
        } else {
            like = dao.getByOpenId(openId);
        }

        if (like == null) {
            return MsgUtil.ok("该账户未分享过！！", "-1");
        }

        awardMoneyService.findValueByType(4);
        //Map<String, Object> resultMap = FunctionUtil.getLikeCountAndMoney("3");
        AwardMoney resultMap = awardMoneyService.getLikeCountAndMoney("3");
        Double orgPrice = Double.parseDouble(dictionariesDao.getByKey("system_price").getDicValue()); //原价
        Double awardMoney = Double.parseDouble(resultMap.getAwardMoney());
        Double count = Double.valueOf(like.getLikeCount());
        Double yhPrice = awardMoney * count; //优惠价格
        // Double prePrice  = orgPrice-yhPrice; //现价
        BigDecimal aa = new BigDecimal(String.valueOf(orgPrice));
        BigDecimal bb = new BigDecimal(String.valueOf(yhPrice));
        BigDecimal prePrice = aa.subtract(bb);
        if (prePrice.compareTo(new BigDecimal(0.00)) == -1) {
            prePrice = new BigDecimal(0.01);
        }
        Map<String, Object> priceMap = new HashMap<String, Object>();
        priceMap.put("orgPrice", aa);
        priceMap.put("yhPrice", bb);
        priceMap.put("prePrice", prePrice);
        return MsgUtil.ok(priceMap);
    }


    @RequestMapping("getOff")
    @ApiOperation(value = "查询当前点赞减价数", notes = "查询当前点赞减价数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String")})
    public Object getOff(String openId) {

        Map<String, Object> resultMap = new HashMap<String, Object>();
        //Map<String, Object> resultMap1 = FunctionUtil.getLikeCountAndMoney("3");
        AwardMoney resultMap1 = awardMoneyService.getLikeCountAndMoney("3");
        Double likeMoney = Double.parseDouble(resultMap1.getAwardMoney());
        //Map<String, Object> resultMap2 = FunctionUtil.getLikeCountAndMoney("1");
        AwardMoney resultMap2 = awardMoneyService.getLikeCountAndMoney("1");
        Double getMoney = Double.parseDouble(resultMap2.getAwardMoney());
        resultMap.put("likeMoney", likeMoney);
        resultMap.put("getMoney", getMoney);
        return MsgUtil.ok(resultMap);
    }

//    public static void main(String[] args) {
//
//        String orgPrice = "0.01";
//        String yhPrice = "6.00";
//        BigDecimal aa = new BigDecimal(String.valueOf(orgPrice));
//        BigDecimal bb = new BigDecimal(String.valueOf(yhPrice));
//        BigDecimal prePrice =aa.subtract(bb);
//
//
//        if(prePrice.compareTo(new BigDecimal(0.00))==-1){
//            prePrice = new BigDecimal(0.01);
//        }
//
//        System.out.println(prePrice);
//    }
}
