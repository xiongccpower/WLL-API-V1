package com.controller;

import com.dao.MyHomeZjDao;
import com.entity.MyHomeZj;
import com.util.MsgUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("MyHomeZj")
@Validated
@Api(value="足迹",tags={"足迹"})
@CrossOrigin
public class MjHomeZjController {
    @Autowired private MyHomeZjDao dao;

    @RequestMapping("list")
    @ApiOperation(value="查询足迹列表", notes="查询足迹列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "productDate", value = "openId", required = true, dataType = "String"), })
    public Object list(String openId,String tel,String productDate) throws ParseException{
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
        List<MyHomeZj> list = null;
        if("".equals(productDate) || productDate==null){
            list = dao.getByTel(tel, openId);
        }else{
            list = dao.getByOpenId(tel, openId, simpleDateFormat.parse(productDate));
        }
        return MsgUtil.ok(list);
    }


    @RequestMapping("save")
    @ApiOperation(value="添加足迹", notes="添加足迹")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "picUrl", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "wxProductUrl", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "appId", value = "appId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "price", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "goodsChannel", value = "goodsChannel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "appletExtendUrl", value = "appletExtendUrl", required = true, dataType = "String"),
            @ApiImplicitParam(name = "productDate", value = "openId", required = true, dataType = "String")})
    public Object save(String openId,String tel,String picUrl,String wxProductUrl,String appId,String price,String productDate,String goodsChannel,String appletExtendUrl) throws ParseException{

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
        MyHomeZj zj = new MyHomeZj();
        zj.setTel(tel);
        zj.setOpenId(openId);
        zj.setPicUrl(picUrl);
        zj.setWxProductUrl(wxProductUrl);
        zj.setProductDate(simpleDateFormat.parse(productDate));
        zj.setPrice(price);
        zj.setCreateTime(new Date());
        zj.setAppId(appId);
        zj.setGoodsChannel(goodsChannel);
        zj.setAppletExtendUrl(appletExtendUrl);
        dao.saveAndFlush(zj);

        return MsgUtil.ok();
    }

    @RequestMapping("deleteOne")
    @ApiOperation(value="单个删除足迹", notes="单个删除足迹")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "Integer")})
    public Object deleteOne(Integer id ) {
        dao.deleteById(id);
        return MsgUtil.ok();
    }


    @RequestMapping("deleteBatch")
    @ApiOperation(value="批量删除添加足迹", notes="批量删除添加足迹")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "Integer")})
    public Object deleteBatch(@RequestParam(value = "ids",required = true ) List<Integer> ids)  {
        for (int i=0;i<ids.size();i++){
            dao.deleteById(ids.get(i));
        }
        return MsgUtil.ok();
    }

    @RequestMapping("deleteAll")
    @ApiOperation(value="批量删除添加足迹", notes="批量删除添加足迹")
    @ApiImplicitParams({
           })
    public Object deleteAll()  {
        dao.deleteAll();
        return MsgUtil.ok();
    }

}
