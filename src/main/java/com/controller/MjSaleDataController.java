package com.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.dao.BookingGoodsInfoDao;
import com.dao.MyHomeSaleDataDao;
import com.dao.SaleWordsInfoDao;
import com.entity.BookingGoodsInfo;
import com.entity.BookingGoodsInfos;
import com.entity.MyHomeSaleData;
import com.entity.SaleWordsInfo;
import com.service.BookingGoodsInfoService;
import com.util.MsgUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("MyHomeSaleData")
@Validated
@Api(value="家庭清单",tags={"家庭清单"})
@CrossOrigin
public class MjSaleDataController {
    @Autowired private MyHomeSaleDataDao dao;
    @Autowired private BookingGoodsInfoDao goodDao;
    @Autowired private SaleWordsInfoDao saleWordsInfoDao;
    @Autowired
    private BookingGoodsInfoService bookingGoodsInfoService;

    @RequestMapping("list")
    @ApiOperation(value="家庭清单", notes="家庭清单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"), })
    public Object list(String openId,String tel) {
        List<MyHomeSaleData> list = dao.getByTel(tel, openId);
        return MsgUtil.ok(list);

    }

    @RequestMapping("BookingGoodsAdd")
    @ApiOperation(value="添加预约特惠商品", notes="家庭清单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goodsInfo", value = "tel", required = true, dataType = "String")})
    public Object BookingGoodsAdd(BookingGoodsInfo goodsInfo) {
        goodDao.saveAndFlush(goodsInfo);
        return MsgUtil.ok(goodsInfo);

    }
    @RequestMapping("delete")
    @ApiOperation(value="删除家庭清单某个商品", notes="删除家庭清单某个商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "orderId", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String")})
    public Object smallList(Integer orderId,String tel,String openId) {
        MyHomeSaleData data = dao.getOne(orderId);
        dao.delete(data);
        return MsgUtil.ok();
    }

    @RequestMapping("deleteList")
    @ApiOperation(value="批量删除家庭清单", notes="批量删除家庭清单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "ids", required = true)})
    public Object deleteList(@RequestParam (value = "ids",required = true )List<Integer> ids) {

        for(int i=0;i<ids.size();i++){
            dao.deleteById(ids.get(i));
        }
        return MsgUtil.ok();
    }

    @RequestMapping("save")
    @ApiOperation(value="添加家庭清单", notes="添加家庭清单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tel", value = "tel", required = true, dataType = "String"),
            @ApiImplicitParam(name = "openId", value = "openId", required = true, dataType = "String"),
            @ApiImplicitParam(name = "productName  ", value = "productName ", required = true, dataType = "String"),
            @ApiImplicitParam(name = "picUrl  ", value = "picUrl ", required = true, dataType = "String"),
            @ApiImplicitParam(name = "wxProductUrl", value = "wxProductUrl ", required = true, dataType = "String"),
            @ApiImplicitParam(name = "appId", value = "appId ", required = true, dataType = "String"),
            @ApiImplicitParam(name = "price", value = "price ", required = true, dataType = "String"),
            @ApiImplicitParam(name = "goodsChannel", value = "goodsChannel ", required = true, dataType = "String"),
            @ApiImplicitParam(name = "productCode", value = "productCode ", required = true, dataType = "String"),})
    public Object save(String tel,String openId,String  productName,String  picUrl,String  wxProductUrl,String appId,String  price,String productCode,String goodsChannel) {
        MyHomeSaleData data = new MyHomeSaleData();
        data.setOpenId(openId);
        data.setTel(tel);
        data.setProductName(productName);
        data.setPicUrl(picUrl);
        data.setWxProductUrl(wxProductUrl);
        data.setAppId(appId);
        data.setProductCode(productCode);
        data.setPrice(price);
        data.setProductChannel(goodsChannel);
        data.setProductDate(new Date());
        data.setAppId(appId);
        dao.saveAndFlush(data);
        return MsgUtil.ok();
    }

    @RequestMapping("isAdd")
    @ApiOperation(value="添加家庭清单", notes="添加家庭清单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productCode", value = "productCode ", required = true, dataType = "String"),})
    public Object isAdd(String productCode) {
        MyHomeSaleData data  = dao.getByProductCode(productCode);
        if(data!=null){
           return MsgUtil.ok("1");
        }else{
            return MsgUtil.ok("-1");
        }
    }

    @RequestMapping("saleWordsList")
    @ApiOperation(value="添加家庭清单", notes="添加家庭清单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goodsName", value = "goodsName ", required = true, dataType = "String"),})
    public Object saleWordsList(String goodsName) {
        List<SaleWordsInfo> list= saleWordsInfoDao.getByKeyWords(goodsName);
        if(list.size()>0){
            return MsgUtil.ok(list);
        }else{
            return MsgUtil.ok("-1");
        }
    }

//    @RequestMapping("saleWordsList")
//    @ApiOperation(value="添加家庭清单", notes="添加家庭清单")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "goodsName", value = "goodsName ", required = true, dataType = "String"),})
//    public Object saleWordsList(String goodsName) {
//        List<SaleWordsInfo> list= saleWordsInfoDao.getByKeyWords(goodsName);
//        if(list.size()>0){
//            return MsgUtil.ok(list);
//        }else{
//            return MsgUtil.ok("-1");
//        }
//    }

    @RequestMapping("querySubscribe")
    @ApiOperation(value="查询预约优惠", notes="查询预约优惠")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "openId", value = "openId ", required = true, dataType = "String"),
    })
    public Object querySubscribe(String openId){
        if(!StrUtil.hasEmpty(openId)){
            String time = DateUtil.now();
            List<BookingGoodsInfos> list = bookingGoodsInfoService.findBookingGoodsUser(time,openId);
            
            if(list.size()>0 && list!=null){
                return MsgUtil.ok(list);
            }else{
                return MsgUtil.fail("暂无预约数据");
            }

        }else{
            return MsgUtil.badArgument();
        }

    }

    @RequestMapping("getSubscribe")
    @ApiOperation(value="查询商品是否在家庭清单中", notes="查询商品是否在家庭清单中")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productCode", value = "productCode ", required = true, dataType = "String"),
            @ApiImplicitParam(name = "openId", value = "openId ", required = true, dataType = "String"),
    })
    public Object getSubscribe(String productCode,String openId){
        if(!StrUtil.hasEmpty(productCode) && !StrUtil.hasEmpty(openId)){
            MyHomeSaleData myHomeSaleData = dao.getByProductCodeOPenid(productCode,openId);
            if(myHomeSaleData!=null){
                return MsgUtil.ok("家庭清单商品");
            }else{
                return MsgUtil.fail("不在家庭清单中");
            }

        }else{
            return MsgUtil.badArgument();
        }
    }

}
