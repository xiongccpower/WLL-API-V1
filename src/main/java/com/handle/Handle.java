//package com.handle;
//
//import java.io.PrintWriter;
//import java.lang.reflect.Method;
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.method.HandlerMethod;
//import org.springframework.web.servlet.HandlerInterceptor;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.dao.UserDao;
//import com.entity.Users;
//
//
//public class Handle implements HandlerInterceptor{
//	@Autowired private UserDao userDao;
//
//	@Override
//	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
//			throws Exception {
//        // 判断接口 有没有加OpenIdHandle
//        HandlerMethod handlerMethod = (HandlerMethod) handler;
//        Method method = handlerMethod.getMethod();
//		OpenIdHandle methodHandle = method.getAnnotation(OpenIdHandle.class);
//		if(methodHandle==null) {//有加其中一个
//			return HandlerInterceptor.super.preHandle(request, response, handler);
//		}else {
//			String openId=request.getParameter("openId");
//			if(StringUtils.isNotBlank(openId)) {
//				Users user = userDao.getByOpenId(openId);
//				if(user!=null) {
//					return HandlerInterceptor.super.preHandle(request, response, handler);
//				}
//			}
//			PrintWriter writer = null;
//			response.setCharacterEncoding("UTF-8");
//			response.setContentType("text/html; charset=utf-8");
//	        writer = response.getWriter();
//	       Map<String, Object> map = new HashMap();
//	       map.put("errno",600);
//	       String json=JSON.toJSONString(map);
//	       writer.print(json);
//		}
//			return false;
//
//		
//	}
//
//}
