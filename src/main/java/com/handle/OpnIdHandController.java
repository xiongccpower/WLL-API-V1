//package com.handle;
//
//
//import java.lang.annotation.Documented;
//import java.lang.annotation.ElementType;
//import java.lang.annotation.Retention;
//import java.lang.annotation.RetentionPolicy;
//import java.lang.annotation.Target;
//
//import org.springframework.stereotype.Component;
///**
// * 在需要登录验证的Controller的方法上使用此注解
// */
//@Target({ElementType.TYPE})// 可用在方法名上
//@Retention(RetentionPolicy.RUNTIME)// 运行时有效
////@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
////@Retention(RetentionPolicy.RUNTIME)
//@Documented
//@Component
//public @interface OpnIdHandController {
//}
