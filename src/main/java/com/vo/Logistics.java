package com.vo;

import java.util.Date;

/**
 * 物流实体
 * @author Administrator
 *
 */
public class Logistics {
	
	private String content;//内容
	private Date time;//时间
	private String courierNo;//快递单号
	private String userId;//快递单号
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCourierNo() {
		return courierNo;
	}
	public void setCourierNo(String courierNo) {
		this.courierNo = courierNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
}
