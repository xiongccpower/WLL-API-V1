package com.vo;
/**
 * 收货地址
 * @author Administrator
 *
 */
public class Address {
	
	private String address;
	private String phone;
	private String addressee;
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddressee() {
		return addressee;
	}
	public void setAddressee(String addressee) {
		this.addressee = addressee;
	}
}
