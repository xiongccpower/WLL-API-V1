package com.mapper;

import com.domain.AwardMoney;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * @Entity com.domain.AwardMoney
 */
@Repository
public interface AwardMoneyMapper extends BaseMapper<AwardMoney> {

    AwardMoney getLikeCountAndMoney(String type);
}




