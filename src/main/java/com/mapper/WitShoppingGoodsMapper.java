package com.mapper;

import com.commonly.EasyBaseMapper;
import com.entity.WitShoppingGoods;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 智慧购物辅助商品表 Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-06-30
 */
@Mapper
public interface WitShoppingGoodsMapper extends EasyBaseMapper<WitShoppingGoods> {


    @Select(" select * from wit_shopping_goods where  user_id=#{uid} and keyword=#{productName} and relation=#{relation} ORDER BY commodity_price asc ,month_sales desc,baoyou desc,good_rate desc  limit #{pageSize},8 ")
    List<WitShoppingGoods> findSearch(@Param("productName") String productName, @Param("pageSize") int pageSize ,@Param("openid") String openid,@Param("relation") String relation,@Param("uid") String uid);

    @Delete(" delete from wit_shopping_goods where create_time<=#{time} ")
    public Integer timingDeleteGoods(@Param("time")  String time);


    @Select(" select * from wit_shopping_goods where user_id=#{userId} and keyword=#{productName} and goods_channel!='yqf'  ORDER BY commodity_price asc ,month_sales desc,baoyou desc,good_rate desc  limit #{pageSize},8 ")
    List<WitShoppingGoods> findDirectGoods(@Param("productName") String productName ,@Param("userId") String userId,@Param("pageSize") int pageSize);

    @Select(" select * from wit_shopping_goods where user_id=#{userId} and keyword=#{productName}   ORDER BY commodity_price asc ,month_sales desc,baoyou desc,good_rate desc  limit #{pageSize},8 ")
    List<WitShoppingGoods> findDirectGoodsSy(@Param("productName") String productName ,@Param("userId") String userId,@Param("pageSize") int pageSize);

    @Select(" select max(page_index) maxPageIndex from wit_shopping_goods where user_id=#{uid} and keyword=#{productName} ")
    int getMaxPageIndex(@Param("productName") String productName,@Param("uid") String uid);

    @Select(" select * from  wit_shopping_goods where user_id=#{uid} and keyword=#{productName} ")
    List<WitShoppingGoods> findSearchGoods(@Param("uid") String uid,@Param("productName") String productName);

}
