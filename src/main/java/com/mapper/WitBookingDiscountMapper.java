package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.WitBookingDiscount;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-07-05
 */
public interface WitBookingDiscountMapper extends BaseMapper<WitBookingDiscount> {

    @Select(" select * from wit_booking_discount where start_time<=#{time} and end_time>=#{time} ")
    List<WitBookingDiscount> findBookingDiscount(@Param("time") String time);

}
