package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.WitGoodsFunction;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-08-17
 */
public interface WitGoodsFunctionMapper extends BaseMapper<WitGoodsFunction> {
    @Select(" select * from wit_goods_function where goods_name=#{goodsName} and states=1 ")
    WitGoodsFunction getGoodsFunction(@Param("goodsName") String goodsName);
}
