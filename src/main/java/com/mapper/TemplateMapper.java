package com.mapper;

import com.domain.Template;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.domain.Template
 */
public interface TemplateMapper extends BaseMapper<Template> {

}




