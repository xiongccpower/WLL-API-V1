package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.ProductGuide;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-08-20
 */
public interface ProductGuideMapper extends BaseMapper<ProductGuide> {
    @Select(" select * from product_guide where product_name=#{goodsName} ")
    List<ProductGuide> findProductGuide(@Param("goodsName") String goodsName);
}
