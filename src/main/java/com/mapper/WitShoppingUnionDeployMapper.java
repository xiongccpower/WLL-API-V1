package com.mapper;

import com.commonly.EasyBaseMapper;
import com.entity.WitShoppingUnionDeploy;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-06-30
 */
@Mapper
public interface WitShoppingUnionDeployMapper extends EasyBaseMapper<WitShoppingUnionDeploy> {

    @Select(" select * from wit_shopping_union_deploy where states=1 and source_channel like  CONCAT('%',#{sourceChannel},'%')   ")
    public List<WitShoppingUnionDeploy> findUnionDeploy(@Param("sourceChannel") String sourceChannel);

    @Select(" select * from wit_shopping_union_deploy where states=1 ")
    public List<WitShoppingUnionDeploy> findUnionDeployAll();

}
