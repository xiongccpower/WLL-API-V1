package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.MjFamilyRelation;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-07-28
 */
public interface MjFamilyRelationMapper extends BaseMapper<MjFamilyRelation> {

    @Select(" select * from mj_family_relation where states=1")
    List<MjFamilyRelation> findFamilyRelation();

    @Select(" select * from mj_family_relation where relation=#{call} ")
    MjFamilyRelation getMjFamilyRelation(@Param("call") String call);
}
