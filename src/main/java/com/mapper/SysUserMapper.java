package com.mapper;

import com.domain.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.domain.SysUser
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}




