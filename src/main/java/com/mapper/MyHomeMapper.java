package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.MyHomes;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-07-24
 */
public interface MyHomeMapper extends BaseMapper<MyHomes> {

    @Select(" select a.* from my_home a LEFT JOIN users b on a.main_tel=b.phone where a.main_tel=#{tel} or a.tel=#{tel} ")
    public List<MyHomes> findByTelOrMainTel(@Param("tel") String tel);

}
