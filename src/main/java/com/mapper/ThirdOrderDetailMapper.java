package com.mapper;

import com.domain.ThirdOrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.domain.ThirdOrderDetail
 */
public interface ThirdOrderDetailMapper extends BaseMapper<ThirdOrderDetail> {

}




