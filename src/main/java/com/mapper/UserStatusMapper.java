package com.mapper;

import com.domain.UserStatus;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.domain.UserStatus
 */
public interface UserStatusMapper extends BaseMapper<UserStatus> {

}




