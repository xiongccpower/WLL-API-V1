package com.mapper;

import com.domain.BusinessEvent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.domain.BusinessEvent
 */
public interface BusinessEventMapper extends BaseMapper<BusinessEvent> {

    String getInvitePwd(Long mbeId);
}




