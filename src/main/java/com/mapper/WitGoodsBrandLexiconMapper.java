package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.WitGoodsBrandLexicon;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-08-17
 */
public interface WitGoodsBrandLexiconMapper extends BaseMapper<WitGoodsBrandLexicon> {
    @Select(" select * from wit_goods_brand_lexicon where brand_name=#{brandName} ")
    WitGoodsBrandLexicon getWitGoodsBrandLexicon(@Param("brandName") String brandName);
}
