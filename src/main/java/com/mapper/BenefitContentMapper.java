package com.mapper;

import com.domain.BenefitContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.domain.BenefitContent
 */
public interface BenefitContentMapper extends BaseMapper<BenefitContent> {

}




