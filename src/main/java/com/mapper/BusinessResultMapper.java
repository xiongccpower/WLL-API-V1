package com.mapper;

import com.domain.BusinessResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.domain.BusinessResult
 */
public interface BusinessResultMapper extends BaseMapper<BusinessResult> {

}




