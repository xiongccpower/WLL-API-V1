package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.TalentDearHelp;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 达人分享亲友助力表 Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-08-31
 */
public interface TalentDearHelpMapper extends BaseMapper<TalentDearHelp> {

    @Select(" select * from talent_dear_help where talent_openid=#{talentOpenid} and help_openid=#{helpOpenid}  and help_time = #{helpTime}")
    TalentDearHelp getHelpOpenid(@Param("helpOpenid") String helpOpenid, @Param("talentOpenid") String talentOpenid, String helpTime);

    @Select("select * from talent_dear_help where talent_openid=#{talentOpenid} and help_time = #{helpTime}")
    List<TalentDearHelp> findTalentOpenid(@Param("talentOpenid") String talentOpenid, String helpTime);

    @Select(" select * from talent_dear_help where  help_openid=#{helpOpenid} and help_time = #{helpTime}")
    TalentDearHelp getByHelpOpenid(@Param("helpOpenid") String helpOpenid, String helpTime);

    /**
     * 查询助力亲友
     *
     * @param talentOpenid
     * @return
     */
    @Select("select * from talent_dear_help where talent_openid =#{talentOpenid} and state=1 and help_time = #{helpTime} limit 1")
    TalentDearHelp getHelp(@Param("talentOpenid") String talentOpenid, String helpTime);


}
