package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.SuggestionInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-08-20
 */
public interface SuggestionInfoMapper extends BaseMapper<SuggestionInfo> {

}
