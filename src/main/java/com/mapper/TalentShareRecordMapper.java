package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.TalentShareRecord;
import com.entity.TalentShareRecordTwo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 达人分享记录表 Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-08-31
 */
public interface TalentShareRecordMapper extends BaseMapper<TalentShareRecord> {

    @Select("select * from talent_share_record where talent_openid=#{talentOpenid} and preview_openid=#{previewOpenid} and preview_month=#{time}")
    TalentShareRecord getTime(@Param("talentOpenid") String talentOpenid, @Param("previewOpenid") String previewOpenid, @Param("time") String time);

    /**
     * 统计所有预览数据
     *
     * @param previewMonth
     * @param previewTime
     * @return
     */
    @Select("select DISTINCT a.talent_openid,a.monthNum,b.datyNum,f.nickname,f.head,c.state as 'pkState',c.launch_openid as 'launch',c.receive_openid as 'receive',a.operate,d.state as 'zlState' from  " +
            "(select count(talent_openid) as 'monthNum',talent_openid,talent_uid,nickname,head,operate from talent_share_record  where  preview_month=#{previewMonth} GROUP BY talent_openid) a LEFT JOIN " +
            "(select count(talent_openid) as 'datyNum',talent_openid from talent_share_record  where  preview_time=#{previewTime} GROUP BY talent_openid) b on a.talent_openid=b.talent_openid " +
            "LEFT JOIN talent_pk_object c on (c.launch_openid = a.talent_openid OR c.receive_openid = a.talent_openid )AND c.state != 2 " +
            "LEFT JOIN talent_dear_help d on d.talent_openid=a.talent_openid " +
            "LEFT JOIN mj_user_address e ON a.talent_uid = e.uid AND e.defaultadd = 1 " +
            "LEFT JOIN talent_share f ON a.talent_uid = f.uid AND f.share_month = #{previewMonth}" +
            "WHERE e.city = #{city}" +
            "ORDER BY a.monthNum desc LIMIT 20")
    List<TalentShareRecord> statisticsPreview(@Param("previewMonth") String previewMonth, @Param("previewTime") String previewTime, @Param("city") String city);

    /**
     * 统计个人数据
     *
     * @param previewMonth
     * @param previewTime
     * @param talentOpenid
     * @return
     */
    @Select("select a.monthNum,b.datyNum,b.preview_time,a.nickname,a.head,a.operate,a.talent_openid from  " +
            "(select count(talent_openid) as 'monthNum',talent_openid,nickname,head,operate " +
            "from talent_share_record  where  preview_month=#{previewMonth} and talent_openid=#{talentOpenid} GROUP BY talent_openid) a LEFT JOIN " +
            "(select count(talent_openid) as 'datyNum',preview_time,talent_openid " +
            "from talent_share_record  where  preview_time=#{previewTime} and talent_openid=#{talentOpenid} GROUP BY talent_openid) b on a.talent_openid=b.talent_openid")
    TalentShareRecord getStatistics(@Param("previewMonth") String previewMonth, @Param("previewTime") String previewTime, @Param("talentOpenid") String talentOpenid);

    /**
     * 查询统计亲友助力数
     *
     * @param previewTime
     * @param previewMonth
     * @param helpOpenid
     * @return
     */
    @Select("select a.monthNum,b.datyNum,a.nickname,a.head,a.operate,a.help_openid from " +
            "(select count(help_openid) as 'monthNum',help_openid,nickname,head,operate from talent_share_record  where  preview_month=#{previewMonth} and help_openid=#{helpOpenid} and type=1 GROUP BY help_openid) a LEFT JOIN " +
            "(select count(help_openid) as 'datyNum',help_openid from talent_share_record  where  preview_time=#{previewTime} and help_openid=#{helpOpenid} and type=1 GROUP BY help_openid) b on a.help_openid=b.help_openid ")
    TalentShareRecord getHelp(@Param("previewTime") String previewTime, @Param("previewMonth") String previewMonth, @Param("helpOpenid") String helpOpenid);

    /**
     * 查询当月pk票数
     *
     * @param
     * @param month
     * @return
     */
    @Select("select count(talent_openid) as 'pkSquareA',0 as 'pkSquareB',talent_openid,preview_month " +
            "from talent_share_record  where  talent_openid=#{pkSquareA} and preview_month=#{month} " +
            "UNION ALL " +
            "select 0 as 'pkSquareA', count(talent_openid) as 'pkSquareB',talent_openid,preview_month " +
            "from talent_share_record  where  talent_openid=#{pkSquareB} and preview_month=#{month} ")
    List<TalentShareRecord> findPKData(@Param("pkSquareA") String pkSquareA, @Param("pkSquareB") String pkSquareB, @Param("month") String month);

    /**
     * 查询当月pk票数(按天统计)
     *
     * @param
     * @param month
     * @return
     */
    @Select("SELECT talent_openid, DATE_FORMAT(preview_time, '%Y-%m-%d' ) AS preview_time,count( talent_openid ) AS 'datyNum'" +
            "FROM talent_share_record WHERE talent_openid = #{openid}  AND preview_month = #{month} GROUP BY preview_time")
    List<TalentShareRecord> findPKDataGroupDay(@Param("openid") String openid, @Param("month") String month);


    /**
     * 查询当月pk票数All(按天统计)
     *
     * @param
     * @param month
     * @return
     */
    @Select("SELECT\n" +
            " a.talent_openid launchOpenid,\n" +
            " b.talent_openid receiveOpenid,\n" +
            " a.datyNum datyNumA,\n" +
            " b.datyNum datyNumB,\n" +
            " a.preview_time \n" +
            "FROM\n" +
            " (\n" +
            " SELECT\n" +
            "  talent_openid,\n" +
            "  DATE_FORMAT( preview_time, '%Y-%m-%d' ) AS preview_time,\n" +
            "  count( talent_openid ) AS datyNum \n" +
            " FROM\n" +
            "  talent_share_record \n" +
            " WHERE\n" +
            "  talent_openid = #{pkSquareA} \n" +
            "  AND preview_month = #{month} \n" +
            " GROUP BY\n" +
            "  preview_time \n" +
            " ) a\n" +
            " LEFT JOIN (\n" +
            " SELECT\n" +
            "  talent_openid,\n" +
            "  DATE_FORMAT( preview_time, '%Y-%m-%d' ) AS preview_time,\n" +
            "  count( talent_openid ) AS datyNum \n" +
            " FROM\n" +
            "  talent_share_record \n" +
            " WHERE\n" +
            "  talent_openid = #{pkSquareB} \n" +
            "  AND preview_month = #{month} \n" +
            " GROUP BY\n" +
            "  preview_time \n" +
            " ) b ON a.preview_time = b.preview_time\n" +
            "UNION\n" +
            "SELECT\n" +
            " a.talent_openid launchOpenid,\n" +
            " b.talent_openid receiveOpenid,\n" +
            " a.datyNum datyNumA,\n" +
            " b.datyNum datyNumB,\n" +
            " b.preview_time \n" +
            "FROM\n" +
            " (\n" +
            " SELECT\n" +
            "  talent_openid,\n" +
            "  DATE_FORMAT( preview_time, '%Y-%m-%d' ) AS preview_time,\n" +
            "  count( talent_openid ) AS datyNum \n" +
            " FROM\n" +
            "  talent_share_record \n" +
            " WHERE\n" +
            "  talent_openid = #{pkSquareA} \n" +
            "  AND preview_month = #{month} \n" +
            " GROUP BY\n" +
            "  preview_time \n" +
            " ) a\n" +
            " RIGHT JOIN (\n" +
            " SELECT\n" +
            "  talent_openid,\n" +
            "  DATE_FORMAT( preview_time, '%Y-%m-%d' ) AS preview_time,\n" +
            "  count( talent_openid ) AS datyNum \n" +
            " FROM\n" +
            "  talent_share_record \n" +
            " WHERE\n" +
            "  talent_openid = #{pkSquareB} \n" +
            "  AND preview_month = #{month} \n" +
            " GROUP BY\n" +
            " preview_time \n" +
            " ) b ON a.preview_time = b.preview_time")
    List<TalentShareRecordTwo> findPKDataGroupDayAll(@Param("pkSquareA") String pkSquareA, @Param("pkSquareB") String pkSquareB, @Param("month") String month);

    /**
     * 查询亲友助力数据
     *
     * @return
     */
    @Select("select * from talent_share_record where help_openid=#{helpOpenid} and preview_month=#{month} ")
    List<TalentShareRecord> fondHelpData(@Param("helpOpenid") String helpOpenid, @Param("month") String month);


    /**
     * 查询当月pk票数(按天统计)
     *
     * @param
     * @param month
     * @return
     */
    @Select("SELECT\n" +
            "\ta.talent_openid launchOpenid,\n" +
            "\ta.dayNum datyNumA,\n" +
            "\tb.dayNum datyNumB,\n" +
            "\ta.preview_time \n" +
            "FROM\n" +
            "\t(\n" +
            "\tSELECT\n" +
            "\t\ttalent_openid,\n" +
            "\t\tDATE_FORMAT( preview_time, '%Y-%m-%d' ) AS preview_time,\n" +
            "\t\tcount(talent_openid) AS 'dayNum' \n" +
            "\tFROM\n" +
            "\t\ttalent_share_record \n" +
            "\tWHERE\n" +
            "\t\ttalent_openid = #{openid} \n" +
            "\t\tAND preview_month = #{month} \n" +
            "\t\tAND type = 0 \n" +
            "GROUP BY\n" +
            "\tpreview_time\n" +
            "\t)a LEFT JOIN (\n" +
            "\tSELECT\n" +
            "\t\ttalent_openid,\n" +
            "\t\tDATE_FORMAT( preview_time, '%Y-%m-%d' ) AS preview_time,\n" +
            "\t\tcount( talent_openid ) AS 'dayNum' \n" +
            "\tFROM\n" +
            "\t\ttalent_share_record \n" +
            "\tWHERE\n" +
            "\t\ttalent_openid = #{openid} \n" +
            "\t\tAND preview_month = #{month} \n" +
            "\t\tAND type = 1 \n" +
            "GROUP BY\n" +
            "\tpreview_time\n" +
            "\t)b ON a.preview_time = b.preview_time\n" +
            "\t\n" +
            "\tUNION\n" +
            "\t\n" +
            "\tSELECT\n" +
            "\ta.talent_openid launchOpenid,\n" +
            "\ta.dayNum datyNumA,\n" +
            "\tb.dayNum datyNumB,\n" +
            "\ta.preview_time \n" +
            "FROM\n" +
            "\t(\n" +
            "\tSELECT\n" +
            "\t\ttalent_openid,\n" +
            "\t\tDATE_FORMAT( preview_time, '%Y-%m-%d' ) AS preview_time,\n" +
            "\t\tcount( talent_openid ) AS 'dayNum' \n" +
            "\tFROM\n" +
            "\t\ttalent_share_record \n" +
            "\tWHERE\n" +
            "\t\ttalent_openid = #{openid} \n" +
            "\t\tAND preview_month = #{month} \n" +
            "\t\tAND type = 0 \n" +
            "GROUP BY\n" +
            "\tpreview_time\n" +
            "\t)a RIGHT JOIN (\n" +
            "\tSELECT\n" +
            "\t\ttalent_openid,\n" +
            "\t\tDATE_FORMAT( preview_time, '%Y-%m-%d' ) AS preview_time,\n" +
            "\t\tcount( talent_openid ) AS 'dayNum' \n" +
            "\tFROM\n" +
            "\t\ttalent_share_record \n" +
            "\tWHERE\n" +
            "\t\ttalent_openid = #{openid} \n" +
            "\t\tAND preview_month = #{month} \n" +
            "\t\tAND type = 1 \n" +
            "GROUP BY\n" +
            "\tpreview_time\n" +
            "\t)b ON a.preview_time = b.preview_time\n" +
            "\t ORDER BY preview_time DESC")
    List<TalentShareRecordTwo> findUserDataGroupDay(@Param("openid") String openid, @Param("month") String month);


}
