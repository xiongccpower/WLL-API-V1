package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.RichText;

public interface RichTextMapper extends BaseMapper<RichText> {

}
