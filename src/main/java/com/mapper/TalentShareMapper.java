package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.TalentShare;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-09-09
 */
public interface TalentShareMapper extends BaseMapper<TalentShare> {

    @Select(" select * from talent_share where uid=#{openId} and share_month=#{shareMonth} ")
    TalentShare getByOpenId(@Param("openId") String openId, @Param("shareMonth") String shareMonth);

    @Select(" select * from talent_share where help_openid=#{openId} ")
    TalentShare getByHelpOpenid(@Param("openId") String openId);

    /**
     * 查询当前排名
     *
     * @param time
     * @param talentOpenid
     * @return
     */
    @Select("SELECT total_num,uid,share_month,rowno FROM ( " +
            "select * from " +
            "( " +
            " select * from( " +
            "SELECT  total_num,uid,share_month,(@rowno:=@rowno+1) as rowno " +
            " FROM talent_share csss where share_month = #{time}  ORDER BY total_num desc " +
            ") bcc,(select (@rowno:=0)) b ORDER BY total_num DESC " +
            ") tempst where tempst.share_month =#{time} ORDER BY tempst.total_num desc " +
            ") c " +
            "WHERE   c.uid=#{talentOpenid} ")
    TalentShare getRanking(@Param("time") String time, @Param("talentOpenid") String talentOpenid);

    /**
     * 查询pk榜双方信息
     *
     * @param openidA
     * @param openidB
     * @param month
     * @return
     */
    @Select("select uid,talent_openid,nickname,head,operate,total_num from  talent_share where uid =#{openidA} and share_month=#{month} " +
            "UNION ALL " +
            "select uid,talent_openid,nickname,head,operate,total_num from  talent_share where uid =#{openidB} and share_month=#{month} ")
    List<TalentShare> getPk(@Param("openidA") String openidA, @Param("openidB") String openidB, @Param("month") String month);
}
