package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.MyServiceInstructRecord;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 管家生活服务指令记录表 Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-07-24
 */
public interface MyServiceInstructRecordMapper extends BaseMapper<MyServiceInstructRecord> {

        @Select(" select * from my_service_instruct_record where mj_phone=#{phone} and create_time>=#{time} and starts=0 ")
        public List<MyServiceInstructRecord> findServiceInstructRecord(@Param("phone") String phone,@Param("time") String time);

        @Select(" select * from  my_service_instruct_record where mj_phone=#{phone} and  goods_name=#{goodsName} ")
        MyServiceInstructRecord getMyServiceInstructRecord(@Param("phone") String phone,@Param("goodsName") String goodsName);
}
