package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.ButlerDataTransfer;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-08-06
 */
public interface ButlerDataTransferMapper extends BaseMapper<ButlerDataTransfer> {

    @Select(" select * from butler_data_transfer where uid=#{uid} and states=0 ")
    ButlerDataTransfer getUid(String uid);

}
