package com.mapper;

import com.entity.OrderComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.entity.OrderComment
 */
public interface OrderCommentMapper extends BaseMapper<OrderComment> {

}




