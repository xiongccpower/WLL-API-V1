package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.MyHomeSaleDatas;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-07-09
 */
public interface MyHomeSaleDataMapper extends BaseMapper<MyHomeSaleDatas> {
    @Select(" select * from my_home_sale_data where subscribe_id=#{subscribeId} ")
    MyHomeSaleDatas getSubscribeId(@Param("subscribeId") int subscribeId);
}
