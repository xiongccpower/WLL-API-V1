package com.mapper;

import com.domain.ShareContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.domain.ShareContent
 */
public interface ShareContentMapper extends BaseMapper<ShareContent> {

}




