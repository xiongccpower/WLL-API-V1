package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.TalentShareRule;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-09-17
 */
public interface TalentShareRuleMapper extends BaseMapper<TalentShareRule> {
    @Select("select * from talent_share_rule where state =1 ")
    TalentShareRule getTalentShareRule();
}
