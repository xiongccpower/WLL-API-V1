package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.BookingGoodsInfos;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-07-08
 */
public interface BookingGoodsInfoMapper extends BaseMapper<BookingGoodsInfos> {

    @Select(" select * from booking_goods_info where end_time>=#{time} ")
    List<BookingGoodsInfos> findBookingGoods( @Param("time") String time);

    @Select(" select * from booking_goods_info where end_time>=#{time} and openid=#{openid} ")
    List<BookingGoodsInfos> findBookingGoodsUser(@Param("time") String time,@Param("openid")  String openid);
}
