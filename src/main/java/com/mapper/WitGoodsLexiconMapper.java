package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.WitGoodsLexicon;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-08-17
 */
public interface WitGoodsLexiconMapper extends BaseMapper<WitGoodsLexicon> {
    @Select(" select * from wit_goods_lexicon where goods_name=#{goodsName} ")
    WitGoodsLexicon getWitGoodsLexicon(@Param("goodsName") String goodsName);
}
