package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.MjUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-08-03
 */
public interface MjUserMapper extends BaseMapper<MjUser> {

    @Select(" select * from mj_user where tel=#{phone} ")
    MjUser getUserByPhone(@Param("phone") String phone);

    @Select("select a.*,b.`name` as 'operateName' from mj_user a LEFT JOIN sys_user b on a.sys_user_id=b.user_id where a.id=#{uid} and a.state=#{state}")
    MjUser getUser(@Param("uid") String uid,@Param("state") String state);

}
