package com.mapper;

import com.domain.BusinessLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.domain.BusinessLog
 */
public interface BusinessLogMapper extends BaseMapper<BusinessLog> {

}




