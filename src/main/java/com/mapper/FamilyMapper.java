package com.mapper;

import com.domain.Family;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.domain.Family
 */
public interface FamilyMapper extends BaseMapper<Family> {

}




