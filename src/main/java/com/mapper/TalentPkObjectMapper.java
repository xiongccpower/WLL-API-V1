package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.TalentPkObject;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-09-16
 */
public interface TalentPkObjectMapper extends BaseMapper<TalentPkObject> {

    @Select("select * from talent_pk_object where launch_openid=#{launchOpenid} and time=#{time} and state in (1,3) ")
    TalentPkObject getByLaunchOpenid(@Param("launchOpenid") String launchOpenid,@Param("time") String time);

    @Select("select * from talent_pk_object where receive_openid=#{receiveOpenid} and time=#{time} and state in (1,3) ")
    TalentPkObject getByReceiveOpenid(@Param("receiveOpenid") String launchOpenid,@Param("time") String time);

    @Select("select * from talent_pk_object where receive_openid=#{receiveOpenid} and time=#{time} and state=#{state} ")
    TalentPkObject getByPkYq(@Param("receiveOpenid") String receiveOpenid,@Param("time") String time,@Param("state") int state);

    @Select("select * from talent_pk_object where receive_openid=#{receiveOpenid} or launch_openid=#{receiveOpenid} and time=#{time} and state=#{state} ")
    TalentPkObject getByPk(@Param("receiveOpenid") String receiveOpenid,@Param("time") String time,@Param("state") int state);

}
