package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.ProductControl;

/**
 * <p>
 * 商品把控配置表 Mapper 接口
 * </p>
 *
 * @author czr
 * @since 2021-07-05
 */
public interface ProductControlMapper extends BaseMapper<ProductControl> {

}
