package com.mapper;

import com.domain.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.domain.dto.SysUserDto;

/**
 * @Entity com.domain.Order
 */
public interface OrderMapper extends BaseMapper<Order> {

    SysUserDto isInner(String tel);
}




