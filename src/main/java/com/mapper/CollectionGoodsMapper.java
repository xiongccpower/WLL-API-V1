package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.domain.CollectionGoods;

/**
* @Entity com.domain.CollectionGoods
*/
public interface CollectionGoodsMapper extends BaseMapper<CollectionGoods> {


}
