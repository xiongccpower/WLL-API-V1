package com.commonly;
import lombok.Getter;
import lombok.Setter;

/*
 * @description: 全局统一返回实体类
 * */
@Getter
@Setter
public class Result {
    private int code;
    private String msg;
    private Object data;
}
