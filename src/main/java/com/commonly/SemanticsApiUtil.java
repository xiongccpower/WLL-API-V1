package com.commonly;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * 语义api接口
 */
public class SemanticsApiUtil {
    protected static Logger log = LoggerFactory.getLogger(SemanticsApiUtil.class);

    // webapi接口地址
    private static final String WEBTTS_URL = "https://ltpapi.xfyun.cn/v1/ke";
    // 应用ID
    private static final String APPID = "5fbdd0e2";
    // 接口密钥
    private static final String API_KEY = "e5978d9cdd5d2e6d8d257509c54bbeb6";
    // 文本
    private static final String TEXT = "苹果";


    private static final String TYPE = "dependent";

    public static void main(String[] args) throws IOException {
        System.out.println(TEXT.length());
        Map<String, String> header = buildHttpHeader();
        String result = HttpUtil.doPost1(WEBTTS_URL, header, "text=" + URLEncoder.encode(TEXT, "utf-8"));
        JSONObject json = JSONUtil.parseObj(result);
        if(json.get("code").equals("0")){
            JSONObject data = JSONUtil.parseObj(json.get("data"));
            JSONArray ke = JSONUtil.parseArray(data.get("ke"));
        }
        System.out.println("itp 接口调用结果：" + result);
    }

    /**
     * 调用分词接口
     * @param participle
     * @return
     */
    public static Map<String,String> participle(String participle){
        Map<String,String> result = new HashMap<>();
        try {
            Map<String, String> header = buildHttpHeader();
            String results = HttpUtil.doPost1(WEBTTS_URL, header, "text=" + URLEncoder.encode(participle, "utf-8"));
            JSONObject json = JSONUtil.parseObj(results);
            if(json.get("code").equals("0")){
                JSONObject data = JSONUtil.parseObj(json.get("data"));
                JSONArray ke = JSONUtil.parseArray(data.get("ke"));
                result.put("code","0");
                result.put("data",ke.toString());

            }else{
                result.put("code","500");
                result.put("msg","分词识别失败");
            }
        }catch (IOException e){
            log.info("查询分词接口异常："+e.getMessage());
            result.put("code","500");
            result.put("msg","分词识别失败");
        }

        return result;
    }



    /**
     * 组装http请求头
     */
    private static Map<String, String> buildHttpHeader() throws UnsupportedEncodingException {
        String curTime = System.currentTimeMillis() / 1000L + "";
        String param = "{\"type\":\"" + TYPE +"\"}";
        String paramBase64 = new String(Base64.encodeBase64(param.getBytes("UTF-8")));
        String checkSum = DigestUtils.md5Hex(API_KEY + curTime + paramBase64);
        Map<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        header.put("X-Param", paramBase64);
        header.put("X-CurTime", curTime);
        header.put("X-CheckSum", checkSum);
        header.put("X-Appid", APPID);
        return header;
    }
}
