package com.polymerizeAPIUtil;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.util.HttpClientUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 聚合接口工具类
 *
 */
public class PolymerizeAPI {
    //星座key
    private static String CONSTELLATION_KEY="2063a126848d2dad8ae1d809e33b23e6";
    //星座接口
    private static String CONSTELLATION_URl="http://web.juhe.cn/constellation/getAll";
    //查询全国今日油价key
    private static String OIL_PRICE_KEY="30389321e70189f94b542eb6c5c6b68d";
    //查询全国今日油价接口
    private static String OIL_PRICE_URl="http://apis.juhe.cn/gnyj/query";

    /**
     * 星座运势查询
     * @param consName
     * @return
     */
    public static String constellationGetAll(String consName){
        String result ="fail";
        Map<String, String> param = new HashMap<>();
        param.put("key", CONSTELLATION_KEY);
        param.put("consName", consName);
        param.put("type", "today");

       String get = HttpClientUtil.doGet(CONSTELLATION_URl,param);
       JSONObject json = JSONUtil.parseObj(get);
       if(json.getInt("error_code")==0){
           result = json.getStr("summary");
       }

        return result;
    }

    /**
     * 今日国内油价查询
     * @return
     */
    public static JSONObject oilPriceQG(String city){
        JSONObject result = new JSONObject();
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("key", OIL_PRICE_KEY);

        String oilPrice= HttpUtil.get(OIL_PRICE_URl, paramMap);
        JSONObject json = JSONUtil.parseObj(oilPrice);
        if(json.getInt("error_code")==0){
            JSONArray data = JSONUtil.parseArray(json.get("result"));
            for(int i =0;i<data.size();i++){
                JSONObject yj = JSONUtil.parseObj(data.get(i));
                if(yj.getStr("city").equals(city)){
                    result = JSONUtil.parseObj(data.get(i));

                    break;
                }
            }
        }else{
            result.put("error_code","fail");
        }

        return result;
    }

    public static void main(String[] args) {
       System.out.println(oilPriceQG("江苏"));
    }

}
