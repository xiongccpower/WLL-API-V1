package com.api.alipay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeWapPayResponse;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * created in 2022/8/11 17:30
 *
 * @author zhuxuelei
 */
@Service
public class AliPayService {

    @Resource
    private AliPayProperties aliPayProperties;

    private AlipayClient alipayClient;

    @PostConstruct
    public void init() {
        this.alipayClient = new DefaultAlipayClient(aliPayProperties.getServerUrl(),
                aliPayProperties.getAppId(),
                aliPayProperties.getAppPrivateKey(),
                aliPayProperties.getFormat(),
                aliPayProperties.getCharset(),
                aliPayProperties.getAliPublicKey(),
                aliPayProperties.getSignType());
    }

    public AlipayTradeWapPayResponse pay(AlipayTradeWapPayModel bizModel) {
        try {
            AlipayTradeWapPayRequest payRequest = new AlipayTradeWapPayRequest();

            payRequest.setBizModel(bizModel);
            AlipayTradeWapPayResponse response = this.alipayClient.execute(payRequest);
            System.out.println(response);
            return response;
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }
}
