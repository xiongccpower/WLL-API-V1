package com.api.alipay;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * created in 2022/8/11 16:41
 *
 * @author zhuxuelei
 */
@Data
@ConfigurationProperties(prefix = "pay.ali")
public class AliPayProperties {
    /**
     * 应用id
     */
    private String appId;

    /**
     * 应用私钥
     */
    private String appPrivateKey;
    /**
     * 支付宝公钥
     */
    public String aliPublicKey;
    /**
     * 字符编码
     */
    public String charset;
    /**
     * 签名方式
     */
    public String signType;
    /**
     * 数据格式
     */
    public String format;
    /**
     * 商家id
     */
    private String mchId;
    /**
     * 调用接口的url
     */
    private String serverUrl;
    /**
     * 支付回调url
     */
    private String notifyUrl;

    /**
     * pc支付前台通知
     */
    private String returnUrl;

    /**
     * 退款url
     */
    private String refundUrl;

}
