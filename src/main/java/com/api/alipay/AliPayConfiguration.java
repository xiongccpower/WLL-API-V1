package com.api.alipay;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * created in 2022/8/11 16:41
 *
 * @author zhuxuelei
 */
@Configuration
@EnableConfigurationProperties(AliPayProperties.class)
public class AliPayConfiguration {
}
