package com.api.smart.entity.common;

public interface Response {

    /**
     * 是否成功
     *
     * @return
     */
    boolean isSuccess();


    /**
     * 错误码
     *
     * @return
     */
    String errorCode();

    /**
     * 错误信息
     *
     * @return
     */
    String errorMsg();
}
