package com.api.smart.entity.goods;

import lombok.Data;

/**
 * created in 2021/12/13 17:39
 *
 * @author zhuxuelei
 */
@Data
public class UnionCoupon {

    private String couponId; //优惠券ID: "271dbf9d1f7c498eb8f19ae3cc38f5de",
    private String couponAmount; //优惠券金额: 40,
    private String couponTotalNum; //优惠券总数: 100000,
    private String couponReceiveNum; //领券量: 7000,
    private String couponRemainNum; //优惠券剩余数: 93000,
    private String couponConditions; //优惠券使用条件: "59",
    private String couponStartTime; //优惠券开始时间: "2021-01-21",
    private String couponEndTime; //优惠券使用条件: "2021-01-23",
    private String couponLink; //优惠券链接: "https://uland.taobao.com/quan/detail?sellerId=752847099&activityId=271dbf9d1f7c498eb8f19ae3cc38f5de"
}
