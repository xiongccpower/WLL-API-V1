//package com.api.smart.entity.goods;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.extension.activerecord.Model;
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//
//import java.io.Serializable;
//
///**
// * <p>
// * 智慧购物辅助商品表
// * </p>
// *
// * @author czr
// * @since 2021-06-30
// */
//@Data
//@EqualsAndHashCode(callSuper = false)
//public class WitShoppingGoods extends Model<WitShoppingGoods> {
//
//    private static final long serialVersionUID = 1L;
//
//    @TableId(value = "id", type = IdType.INPUT)
//    private String id;
//
//    /**
//     * 商品名称
//     */
//    private String commodityName;
//
//    /**
//     * 商品编码
//     */
//    private String commodityCode;
//    /**
//     * 店铺名称
//     */
//    private String storeName;
//
//    /**
//     * 供应商编码
//     */
//    private String supplierCode;
//
//    /**
//     * 商品图片地址
//     */
//    private String pictureUrl;
//
//    /**
//     * 月销量
//     */
//    private String monthSales;
//
//    /**
//     * 易购价
//     */
//    private String snPrice;
//
//    /**
//     * 商品价格
//     */
//    private String commodityPrice;
//
//    /**
//     * 商品类型
//     */
//    private String commodityType;
//
//    /**
//     * 价格类型
//     */
//    private String priceType;
//
//    /**
//     * 价格类型编码
//     */
//    private String priceTypeCode;
//
//    /**
//     * 是否包邮 0：不包邮 1：包邮
//     */
//    private String baoyou;
//
//    /**
//     * 佣金比例
//     */
//    private String rate;
//
//    /**
//     * 是否可售 0:可售，1:无货，2:本地暂不销售
//     */
//    private String saleStatus;
//
//    /**
//     * 是否有券1：是 0：否
//     */
//    private String isCoupon;
//
//    /**
//     * 券链接URL
//     */
//    private String couponUrl;
//
//    /**
//     * 券活动编码
//     */
//    private String activityId;
//
//    /**
//     * 券秘钥
//     */
//    private String activitySecretKey;
//
//    /**
//     * 券面额
//     */
//    private String couponValue;
//
//    /**
//     * 券总数
//     */
//    private String couponCount;
//
//    /**
//     * 券领取开始时间
//     */
//    private String couponStartTime;
//
//    /**
//     * 券领取结束时间
//     */
//    private String couponEndTime;
//
//    /**
//     * 券使用开始时间
//     */
//    private String startTime;
//
//    /**
//     * 券使用结束时间
//     */
//    private String endTime;
//
//    /**
//     * 券后价
//     */
//    private String afterCouponPrice;
//
//    /**
//     * 评价总数
//     */
//    private String totalReviewCount;
//
//    /**
//     * 好评数量
//     */
//    private String goodReviewCount;
//
//    /**
//     * 好评率
//     */
//    private String goodRate;
//
//    /**
//     * 商品链接
//     */
//    private String productUrl;
//    /**
//     * 店铺评分：保留两位小数
//     */
//    private String storeScore;
//
//    /**
//     * PC端商品链接
//     */
//    private String pcProductUrl;
//
//    /**
//     * 移动端商品链接
//     */
//    private String mobileProductUrl;
//
//    /**
//     * 移动商品现价
//     */
//    private String mobliePrice;
//    /**
//     * 移动券后价
//     *
//     */
//    private String moblieCouponPrice;
//
//    /**
//     * PC商品现价
//     */
//    private String pcPrice;
//    /**
//     * pc券后价
//     */
//    private String pcCouponPrice;
//
//    /**
//     * PC商品原价
//     */
//    private String pcPriceOri;
//
//    /**
//     * 移动商品原价
//     */
//    private String mobilePriceOri;
//
//    /**
//     * 商品描述
//     */
//    private String description;
//
//    /**
//     * 商品来源渠道
//     */
//    private String goodsChannel;
//
//    /**
//     * 健康标签
//     */
//    private String healthyLabel;
//
//    /**
//     * 搜索创建时间
//     */
//    private String createTime;
//    /**
//     * 智能管家id
//     */
//    private String butlerId;
//    /**
//     *商品图片url
//     */
//    private String imgUrl;
//    /**
//     * 搜索的关键字
//     */
//    private String keyword;
//    /**
//     * 搜索时间
//     */
//    private String searchTime;
//    /**
//     * 用户手机号
//     */
//    private String phone;
//    /**
//     * 小程序页面地址
//     */
//    private String appletExtendUrl;
//    /**
//     * 小程序appid
//     */
//    private String appId;
//    /**
//     * 用户小程序id
//     */
//    private String openid;
//    //商品详情页
//    private String goodsDetailUrl;
//    /**
//     * 商品品牌名称
//     */
//    private String brandName;
//    /**
//     * 图片列表
//     */
//    private String pictureUrls;
//    /**
//     * 用户id
//     */
//    private String userId;
//    /**
//     * 为谁买
//     */
//    private String relation;
//    /**
//     * 第几页时添加
//     */
//    private Integer pageIndex;
//
//
//
//    @Override
//    protected Serializable pkVal() {
//        return this.id;
//    }
//
//}
