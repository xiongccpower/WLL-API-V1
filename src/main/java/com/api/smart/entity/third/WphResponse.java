package com.api.smart.entity.third;

import com.api.smart.entity.common.Response;
import com.vip.adp.api.open.service.GoodsInfoResponse;
import lombok.Data;

@Data
public class WphResponse implements Response {
    private GoodsInfoResponse result;

    private String returnCode;

    @Override
    public boolean isSuccess() {
        return "0".equals(returnCode);
    }

    @Override
    public String errorCode() {
        return returnCode;
    }

    @Override
    public String errorMsg() {
        return "";
    }
}
