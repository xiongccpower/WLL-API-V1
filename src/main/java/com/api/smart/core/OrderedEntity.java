package com.api.smart.core;


import com.alibaba.fastjson.JSONObject;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public interface OrderedEntity {

    default JSONObject getOrderedJson() {
        // todo 忽略JSONField配置的忽略字段, 父类字段获取
        Field[] fields = this.getClass().getSuperclass().getDeclaredFields();
        Map<String, Field> map = Arrays.stream(fields).collect(Collectors.toMap(Field::getName, v -> v));
        String[] array = Arrays.stream(fields).map(Field::getName).toArray(String[]::new);
        Arrays.sort(array);
        JSONObject obj = new JSONObject(true);
        for (String name : array) {
            Field field = map.get(name);
            try {
                field.setAccessible(true);
                Object o = field.get(this);
                obj.put(name, o);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return obj;
    }

    default String getOrderedJsonString() {
        return getOrderedJson().toJSONString();
    }
}
