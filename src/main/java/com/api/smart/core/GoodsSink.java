package com.api.smart.core;

import com.alibaba.fastjson.JSON;
import com.controller.MydataController;
import com.weilaili.websocket.interfaces.WebSocketService;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.util.concurrent.CompletableFuture;

public class GoodsSink extends RichSinkFunction<GoodsResponse> {

    public static WebSocketService webSocketService;

    @Override
    public void invoke(GoodsResponse value, Context context) throws Exception {
        System.out.println(value.getList().size());
        // todo 入库或者直接返回

        // 对value进行排序，商品排序，1.淘宝,2.京东,3.唯品会,4.考拉,5.苏宁
        // 对list中的goodsChanea进行筛选，已实现
        System.out.println("准备排序");
        value.getList().sort((o1, o2) -> {
            int or1 = getOrder(o1.getGoodsChannel());
            int or2 = getOrder(o2.getGoodsChannel());
            if (or1 == or2) {
                return 0;
            }
            return or1 - or2 > 0 ? 1 : -1;
        });


        // 对价格进行基本筛选
        // 对list中的goodsSnPrice进行筛选，已实现
        System.out.println(2);
        value.getList().sort((o1, o2) -> {
            int or1 = getOrder(o1.getSnPrice());
            int or2 = getOrder(o2.getSnPrice());
            if (or1 == or2) {
                return 0;
            }
            return or1 < or2 ? 1 : -1;
        });
//
//        WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
//        if (witShoppingGoods.getPcPrice() != null){
//
//
//        }

        // 优惠券排序
        value.getList().sort((o1, o2) -> {
            boolean b1 = StringUtils.isNotBlank(o1.getCouponValue());
            boolean b2 = StringUtils.isNotBlank(o2.getCouponValue());
            if (b1 && b2) {
                return 0;
            }
            return b1 ? 1 : -1;
        });

        GoodsQuery query = value.getQuery();
        if (query.getId() != null) {
            CompletableFuture<GoodsResponse> future = MydataController.MAP.get(query.getId());
            if (future != null) {
                future.complete(value);
            }
        }
        if (query.isSocket()) {
            // 发送websocket消息
            query.getSmartQuery().setList(JSON.toJSONString(value.getList()));
            webSocketService.send(query.getUid(), query.getCmd(), query.getMac(), query.getSmartQuery());
        }
        super.invoke(value, context);
    }

    private int getOrder(String channel) {
        switch (channel) {
            case "tb":
                return 0;
            case "jd":
                return 1;
            case "wph":
                return 2;
            case "klzk":
                return 3;
            case "sn":
                return 4;
            default:
                return 5;
        }
    }
}
