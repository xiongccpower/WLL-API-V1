package com.api.smart.core;

import com.weilaili.websocket.interfaces.WebSocketService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class UnionGoodsService implements ApplicationRunner {
    @DubboReference
    private WebSocketService webSocketService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        GoodsSink.webSocketService = webSocketService;

        // 初始化job
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<GoodsResponse> stream = env.addSource(new RemoteSource());

        SingleOutputStreamOperator<GoodsResponse> result = stream.keyBy(GoodsResponse::getSessionId).process(new ResponseAggregator()).name("aggregator");

        result.addSink(new GoodsSink());
        try {
            env.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
