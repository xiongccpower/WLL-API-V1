package com.api.smart.core;

import com.weilaili.websocket.domain.SmartQuery;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class GoodsQuery implements Cloneable {

    private String id;
     /**
     * 每页数量
     */
    private Integer pageSize = 10;

    /**
     * 当前页
     */
    private Integer page = 1;

    /**
     * 品类
     */
    private String category;

    /**
     * 排序字段
     */
    private String orderBy;

    /**
     * 排序类型
     */
    private String orderType;

    /**
     * 起始价格
     */
    private Double startPrice;

    /**
     * 结束价格
     */
    private Double endPrice;

    /**
     * 起始佣金比例
     */
    private Double startCommissionRate;

    /**
     * 结束佣金比例
     */
    private Double endCommissionRate;

    /**
     * 用户ID
     */
    private String uid;

    /**
     * 关系
     */
    private String relation = "全家通用";

    /**
     * 产品名
     */
    private String productName;

    /**
     * 多关键字
     */
    private String[] keywords;

    /**
     * 平台
     */
    private Integer platform;

    /**
     *
     */
    private String sessionId;

    // 自带参数

    /**
     * 开始时间
     */
    private long startTime;

    /**
     * 超时时间
     */
    private long timeout;

    /**
     * 是否发送消息
     */
    private boolean socket;

    private SmartQuery smartQuery;

    private String cmd;

    private String mac;

    @Override
    public GoodsQuery clone() {
        try {
            GoodsQuery clone = (GoodsQuery) super.clone();
            // TODO: copy mutable state here, so the clone can't change the internals of the original
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
