package com.api.smart.core;

import lombok.extern.log4j.Log4j2;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
public class ResponseAggregator extends KeyedProcessFunction<String, GoodsResponse, GoodsResponse> {


    @Override
    public void processElement(GoodsResponse value, KeyedProcessFunction<String, GoodsResponse, GoodsResponse>.Context ctx, Collector<GoodsResponse> out) throws Exception {
        GoodsQuery query = value.getQuery();
        if (!query.isSocket()) {
            long now = System.currentTimeMillis();
            if (now - query.getStartTime() > query.getTimeout()) {
                // 请求已超时，丢弃数据
                log.info("数据已超时，丢弃数据，开始时间：{}，当前时间：{}，耗时：{}毫秒", query.getStartTime(), now, now - query.getStartTime());
                return;
            }
        }

        String key = ctx.getCurrentKey();
        GoodsResponse exist = DataManager.get(key);
        log.info("接收到请求结果数据");
        if (exist == null) {
            DataManager.put(key, value);
            value.setCount(1);
            value.setCollector(out);
        } else {
            if (exist.getList() == null) {
                System.out.println("11111111111111111111");
                exist.setList(new ArrayList<>());
            }

            if (value.getList() != null) {
                exist.getList().addAll(value.getList().stream().filter(Objects::nonNull).collect(Collectors.toList()));
            }
            exist.setCount(exist.getCount() + 1);
            if (exist.getCount().equals(exist.getTotal())) {
                log.info("数据准备完毕，提交数据，耗时:{}毫秒", System.currentTimeMillis() - exist.getQuery().getStartTime());
                out.collect(exist);
                DataManager.remove(key);
            }
        }
    }
}
