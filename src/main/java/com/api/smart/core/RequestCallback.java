package com.api.smart.core;

public interface RequestCallback<T> {
    void onSuccess(T t);

    void onFail(Throwable e);
}
