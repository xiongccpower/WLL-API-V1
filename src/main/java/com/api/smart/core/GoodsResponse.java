package com.api.smart.core;

import com.entity.WitShoppingGoods;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.flink.util.Collector;

import java.util.List;

@Data
public class GoodsResponse {

    private GoodsQuery query;

    private String sessionId;

    private List<WitShoppingGoods> list;

    /**
     * 总数
     */
    private Integer total;

    /**
     * 实际完成数量
     */
    private Integer count;

    @JsonIgnore
    private Collector<GoodsResponse> collector;
}
