package com.api.smart.core;


import lombok.extern.log4j.Log4j2;
import org.apache.flink.util.Collector;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Log4j2
public class DataManager {

    private static final ConcurrentHashMap<String, GoodsResponse> MAP = new ConcurrentHashMap<>();

    private static final ScheduledExecutorService EXECUTOR_SERVICE = new ScheduledThreadPoolExecutor(10);

    public static GoodsResponse get(String key) {
        return MAP.get(key);
    }

    public static void put(String key, GoodsResponse value) {
        MAP.put(key, value);
    }

    public static long getTimeout() {
        return 2000;
    }

    public static void remove(String key) {
        MAP.remove(key);
    }

    public static void start(String key) {
        EXECUTOR_SERVICE.schedule(() -> {
            GoodsResponse response = MAP.get(key);
            if (response != null) {
                Collector<GoodsResponse> collector = response.getCollector();
                response.setCollector(null);
                collector.collect(response);
                log.info("已超时，提交数据");
            }
            MAP.remove(key);
        }, getTimeout(), TimeUnit.MILLISECONDS);
    }
}
