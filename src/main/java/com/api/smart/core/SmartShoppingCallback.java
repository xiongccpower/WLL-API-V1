package com.api.smart.core;

import com.entity.WitShoppingGoods;

import java.util.List;

public interface SmartShoppingCallback extends RequestCallback<List<WitShoppingGoods>> {
}
