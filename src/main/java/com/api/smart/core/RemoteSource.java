package com.api.smart.core;

import com.api.smart.api.duomai.*;
import com.api.smart.interfaces.ThirdGoodsApi;
import com.entity.WitShoppingGoods;
import lombok.extern.log4j.Log4j2;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingDeque;

@Log4j2
public class RemoteSource implements SourceFunction<GoodsResponse> {


    private static final List<ThirdGoodsApi> apis = new ArrayList<>();

    static {
        apis.add(new TbUnionGoodsRequestHelper());
        apis.add(new JdUnionGoodsRequestHelper());
        apis.add(new KlUnionGoodsRequestHelper());
        apis.add(new VipUnionGoodsRequestHelper());
        apis.add(new SnUnionGoodsRequestHelper());
//        apis.add(new KlzkUnionGoodsRequestHelper());
//        apis.add(new WphUnionGoodsRequestHelper());
//        apis.add(new SuNingUnionGoodsRequestHelper());
    }


    // todo  暂时用本地内存队列，后续接入消息队列
    public static final LinkedBlockingDeque<GoodsQuery> QUEUE = new LinkedBlockingDeque<>();

    @Override
    public void run(SourceContext<GoodsResponse> sourceContext) throws Exception {
        int times = 3; // 查询3倍的商品

        while (true) {
            GoodsQuery exist = QUEUE.poll();
            if (exist == null) {
                continue;
            }
            exist.setStartTime(System.currentTimeMillis());
            exist.setTimeout(DataManager.getTimeout());

            String uuid = UUID.randomUUID().toString();
            if (!exist.isSocket()) {
                DataManager.start(uuid);
            }

            for (ThirdGoodsApi api : apis) {
                for (int i = 0; i < times; i++) {
                    for (String name : exist.getKeywords()) {
                        GoodsQuery n = exist.clone();
                        n.setProductName(name);
                        n.setPage(n.getPage() * times + i);
                        api.findGoods(n, new SmartShoppingCallback() {
                            @Override
                            public void onSuccess(List<WitShoppingGoods> witShoppingGoods) {
//                        Optional<String> optStr = JSONUtil.toJSONString(witShoppingGoods);
//                        optStr.ifPresent(System.out::println);
                                log.info("{}:查询到商品：{}条", api.getName(), witShoppingGoods.size());

                                GoodsResponse response = new GoodsResponse();
                                response.setQuery(n);
                                response.setTotal(apis.size() * exist.getKeywords().length * times);
                                response.setSessionId(uuid);
                                response.setList(witShoppingGoods);
                                sourceContext.collect(response);
                            }

                            @Override
                            public void onFail(Throwable e) {

                            }
                        });
                    }
                }
            }
        }
    }

    @Override
    public void cancel() {

    }
}
