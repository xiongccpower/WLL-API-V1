package com.api.smart.utils;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class SignUtil {
    private SignUtil() {

    }


    public static Map<String, String> sortMap(Map<String, String> map) {
        Map<String, String> sortedMap = new LinkedHashMap<>();
        String[] keys = map.keySet().toArray(new String[0]);
        Arrays.sort(keys);
        for (String key : keys) {
            sortedMap.put(key, map.get(key));
        }
        return sortedMap;
    }

    public static String getSortedParams(Map<String, String> queryParams) {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, String> entry : queryParams.entrySet()) {
            builder.append(entry.getKey()).append(entry.getValue());
        }
        return builder.toString();
    }
}
