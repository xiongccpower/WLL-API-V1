package com.api.smart.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Map;

public class RequestUtil {
    private RequestUtil() {
    }

    public static String buildRequestUrl(String url, Map<String, String> queryParams) {
        return url + (url.endsWith("/") ? "?" : "/?") + getQueryString(queryParams);
    }

    public static String getQueryString(Map<String, String> params) {
        try {
            StringBuilder query = new StringBuilder();
            String[] keys = params.keySet().toArray(new String[0]);
            Arrays.sort(keys);

            for (String key : keys) {
                String value = params.get(key);
                query.append(key).append("=").append(URLEncoder.encode(value, "UTF-8")).append("&");
            }
            query.delete(query.length() - 1, query.length());
            return query.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }
}
