package com.api.smart.api.klzk;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.api.smart.core.GoodsQuery;
import com.api.smart.core.SmartShoppingCallback;
import com.api.smart.interfaces.ThirdGoodsApi;
import com.entity.WitShoppingGoods;
import com.entity.union_entity.GoodsDetails;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.haitao.thirdpart.sdk.APIUtil;
import com.util.ParameterUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;

/**
 * 调用考拉赚客api接口
 */
@Log4j2
public class KlzkUnionGoodsRequestHelper implements ThirdGoodsApi, Serializable {

    @Override
    public void findGoods(GoodsQuery query, SmartShoppingCallback callback) {
            query(query).onErrorMap(throwable -> {
            throwable.printStackTrace();
            return throwable;
        }).subscribe((Consumer<String>) s -> {
            com.api.smart.utils.JSONUtil.parse(s).ifPresent(v -> {
                if (v.get("code").intValue() == 200) {
                    ArrayNode array = (ArrayNode) v.get("data").get("dataList");
                    callback.onSuccess(convert(array));
                }
            });
        });
    }

    @Override
    public String getName() {
        return "考拉海淘";
    }

    @Override
    public boolean support(String channel) {
        return false;
    }

    @Override
    public Mono<WitShoppingGoods> detail(String id) {
        return null;
    }

    /**
     * 调用考拉赚客查询商品接口
     *
     * @return
     */
    public static Mono<String> query(GoodsQuery query) {
        TreeMap<String, String> map = new TreeMap<>();
        String timestamp = DateUtil.now();
        map.put("timestamp", timestamp);
        map.put("v", "1.0");
        map.put("signMethod", "md5");
        map.put("unionId", ParameterUtil.KLZK_UNION_ID);
        map.put("method", "kaola.zhuanke.api.searchGoods");
        map.put("keyWord", query.getProductName());
        map.put("type", "2");
        map.put("pageNo", query.getPage().toString());
        map.put("pageSize", query.getPageSize().toString());


        String sign = APIUtil.createSign(ParameterUtil.KLZK_APP_SECRET, map);

        MultiValueMap<Object, Object> params = new LinkedMultiValueMap<>();
        params.add("timestamp", timestamp);
        params.add("v", "1.0");
        params.add("signMethod", "md5");
        params.add("unionId", ParameterUtil.KLZK_UNION_ID);
        params.add("method", "kaola.zhuanke.api.searchGoods");
        params.add("keyWord", query.getProductName());
        params.add("type", "2");
        params.add("pageNo", query.getPage().toString());
        params.add("pageSize", query.getPageSize().toString());
        params.add("sign", sign);

        return WebClient.create(ParameterUtil.KLZK_APP_URL).post().bodyValue(params).header(
                "Accept", "application/json; charset=UTF-8"
        ).retrieve().bodyToMono(String.class);
    }

    public static GoodsDetails goodsDetails(String goodsId, String appid, String appletUrl) {
        GoodsDetails goodsDetails = new GoodsDetails();
        TreeMap<String, String> map = new TreeMap<>();
        String timestamp = DateUtil.now();
        map.put("timestamp", timestamp);
        map.put("v", "1.0");
        map.put("signMethod", "md5");
        map.put("unionId", ParameterUtil.KLZK_UNION_ID);
        map.put("method", "kaola.zhuanke.api.queryGoodsInfo");
        map.put("goodsIds", goodsId);

        String sign = APIUtil.createSign(ParameterUtil.KLZK_APP_SECRET, map);

        Map<String, Object> object = new HashMap<String, Object>();
        object.put("timestamp", timestamp);
        object.put("v", "1.0");
        object.put("signMethod", "md5");
        object.put("unionId", ParameterUtil.KLZK_UNION_ID);
        object.put("method", "kaola.zhuanke.api.queryGoodsInfo");
        object.put("goodsIds", goodsId);

        object.put("sign", sign);
        String result2 = HttpRequest.post(ParameterUtil.KLZK_APP_URL)
                .header(Header.ACCEPT, "application/json; charset=UTF-8")//头信息，多个头信息多次调用此方法即可
                .form(object)//表单内容
                .timeout(20000)//超时，毫秒
                .execute().body();

        System.out.println("考拉商品数据=" + result2);

        JSONObject json = JSONUtil.parseObj(result2);
        if (json.getInt("code") == 200) {
            JSONArray jsonArr = JSONUtil.parseArray(json.get("data"));
            JSONObject data = JSONUtil.parseObj(jsonArr.get(0));
            goodsDetails.setGoodsId(data.getStr("goodsId"));
            JSONObject baseInfo = JSONUtil.parseObj(data.getStr("baseInfo"));
            JSONArray imageList = JSONUtil.parseArray(baseInfo.get("imageList"));
            JSONArray detailImgList = JSONUtil.parseArray(baseInfo.get("detailImgList"));
            goodsDetails.setGoodsName(baseInfo.getStr("goodsTitle"));
            goodsDetails.setBrandName(baseInfo.getStr("brandName"));
            goodsDetails.setBrandStoreSn(baseInfo.getStr("brandCountryName"));
            goodsDetails.setGoodsCarouselPictures(JSONUtil.toList(imageList, String.class));
            goodsDetails.setGoodsDetailPictures(JSONUtil.toList(detailImgList, String.class));

            JSONObject priceInfo = JSONUtil.parseObj(data.getStr("baseInfo"));
            goodsDetails.setMarketPrice(priceInfo.get("marketPrice") + "");
            goodsDetails.setVipPrice(priceInfo.get("currentPrice") + "");

            JSONObject activityInfo = JSONUtil.parseObj(data.getStr("activityInfo"));
            goodsDetails.setNoPostage(activityInfo.get("noPostage") + "");

            goodsDetails.setAppid(appid);
            goodsDetails.setAppletUrl(appletUrl);

        }

        return goodsDetails;
    }

    /**
     * 保存商品数据
     *
     * @param dataList
     * @return
     */
    private static List<WitShoppingGoods> convert(ArrayNode dataList) {
        Map<String, Object> result = new HashMap<>();
        List<WitShoppingGoods> lists = new ArrayList<>();

        if (dataList == null || dataList.size() == 0) {
            return new ArrayList<>();
        }

        for (JsonNode item : dataList) {
            WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
            witShoppingGoods.setId(IdUtil.objectId());
//            witShoppingGoods.setRelation(relation);
//            witShoppingGoods.setUserId(uid);
            witShoppingGoods.setStoreName("");
            witShoppingGoods.setCommodityCode(item.get("goodsId").asText(""));
            witShoppingGoods.setCommodityName(item.get("baseInfo").get("goodsTitle").asText(""));
            witShoppingGoods.setCommodityPrice(item.get("priceInfo").get("currentPrice").asText(""));
            witShoppingGoods.setBaoyou(item.get("activityInfo").get("noPostage").asText(""));
            witShoppingGoods.setGoodsDetailUrl(item.get("linkInfo").get("goodsDetailUrl").asText(""));
            witShoppingGoods.setCreateTime(DateUtil.now());
            witShoppingGoods.setGoodsChannel("klzk");
//            witShoppingGoods.setKeyword(productName.trim());
            witShoppingGoods.setProductUrl(item.get("linkInfo").get("goodsDetailUrl").asText(""));
            witShoppingGoods.setPictureUrl(item.get("baseInfo").get("imageList").get(0) + "");
            witShoppingGoods.setImgUrl(item.get("baseInfo").get("imageList").get(0) + "");
            witShoppingGoods.setPictureUrls(item.get("baseInfo").get("imageList").asText(""));
            witShoppingGoods.setAppletExtendUrl("package-product/pages/index?zkTargetUrl=" + item.get("linkInfo").get("goodsDetailUrl").asText(""));
//            witShoppingGoods.setPageIndex(pageIndex);
            witShoppingGoods.setGoodRate("");
//            if (!StrUtil.hasEmpty(openid)) {
//                witShoppingGoods.setOpenid(openid);
//            }
            lists.add(witShoppingGoods);
        }
        return lists;
    }


    public static void main(String[] args) {

//        TreeMap<String,String> map = new TreeMap<String,String>();
//        String timestamp = DateUtil.now();
//        map.put("timestamp", timestamp);
//        map.put("v","1.0");
//        map.put("signMethod","md5");
//        map.put("unionId", ParameterUtil.KLZK_UNION_ID);
//        map.put("method", "kaola.zhuanke.api.searchGoods");
//        map.put("keyWord", "手机");
//        map.put("type", "2");
//        map.put("pageNo", "1");
//        map.put("pageSize", "50");
//
//        String sign = APIUtil.createSign(ParameterUtil.KLZK_APP_SECRET,map);
//
//        Map<String,Object> object = new HashMap<String, Object>();
//        object.put("timestamp", timestamp);
//        object.put("v","1.0");
//        object.put("signMethod","md5");
//        object.put("unionId", ParameterUtil.KLZK_UNION_ID);
//        object.put("method", "kaola.zhuanke.api.searchGoods");
//        object.put("keyWord", "手机");
//        object.put("type", "2");
//        object.put("pageNo", "1");
//        object.put("pageSize", "50");
//        object.put("sign", sign);
//
//        String result2 = HttpRequest.post(ParameterUtil.KLZK_APP_URL)
//                .header(Header.ACCEPT, "application/json; charset=UTF-8")//头信息，多个头信息多次调用此方法即可
//                .form(object)//表单内容
//                .timeout(20000)//超时，毫秒
//                .execute().body();
//
//       System.out.println("result2:"+result2);

//        goodsDetails();

    }


}
