package com.api.smart.api.suning;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.api.smart.core.GoodsQuery;
import com.api.smart.core.SmartShoppingCallback;
import com.api.smart.interfaces.ThirdGoodsApi;
import com.api.smart.utils.JSONUtil;
import com.entity.WitShoppingGoods;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.suning.api.entity.netalliance.SearchcommodityQueryRequest;
import com.suning.api.exception.SuningApiException;
import com.suning.api.util.SecurityUtil;
import com.util.ParameterUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

@Log4j2
public class SuNingUnionGoodsRequestHelper implements ThirdGoodsApi, Serializable {
    @Override
    public void findGoods(GoodsQuery query, SmartShoppingCallback callback) {
        log.info("SuNingUnionGoodsRequestHelper：{}", System.currentTimeMillis());
        query(query).onErrorMap(throwable -> {
            throwable.printStackTrace();
            return throwable;
        }).subscribe((Consumer<String>) s -> {
            Optional<JsonNode> parse = JSONUtil.parse(s);
            if (parse.isPresent()) {
                JsonNode node = parse.get();
                JsonNode content = node.get("sn_responseContent");
                if (content != null) {
                    JsonNode body = content.get("sn_body");
                    if (body != null) {
                        JsonNode list = body.get("querySearchcommodity");
                        if (list != null) {
                            callback.onSuccess(convert((ArrayNode) list));
                        }
                    }
                }
//                Optional.ofNullable(node.get("sn_responseContent"))
//                        .map(a -> a.get("sn_body"))
//                        .map(b -> b.get("querySearchcommodity"))
//                        .ifPresent(list -> {
//                            callback.onSuccess(convert((ArrayNode) list));
//                        });
            }
        });
    }

    @Override
    public String getName() {
        return "苏宁";
    }

    @Override
    public boolean support(String channel) {
        return false;
    }

    @Override
    public Mono<WitShoppingGoods> detail(String id) {
        return null;
    }

    public Mono<String> query(GoodsQuery goodsQuery) {
        SearchcommodityQueryRequest request = buildRequest(goodsQuery);
        Map<String, String> sysParams = request.getSysParams();
        sysParams.put("appKey", ParameterUtil.APP_KEY);
        sysParams.put("versionNo", "v1.2");
        sysParams.put("appRequestTime", request.getAppRequestTime());
        sysParams.put("format", "json");
        try {
            String sign = SecurityUtil.dataSign(request, request.getResParams(), ParameterUtil.APP_KEY, ParameterUtil.APP_SECRET);
            sysParams.put("signInfo", sign);
        } catch (SuningApiException e) {
            e.printStackTrace();
        }

        // todo 暂时
        ObjectNode params = JSONUtil.object();
        params.put("coupon", "0") // 是否有优惠券
                .put("keyword", goodsQuery.getProductName())
                .put("pageIndex", goodsQuery.getPage().toString())
                .put("size", goodsQuery.getPageSize().toString())
                .put("sortType", "2");
        JsonNode res = JSONUtil.object().set("sn_request", JSONUtil.object().set("sn_body", JSONUtil.object().set("querySearchcommodity", params)));

        return WebClient.create(ParameterUtil.SOP_REQUEST + "/" + request.getApiMethdeName()).post()
                .contentType(MediaType.APPLICATION_JSON)
                .headers(httpHeaders -> {
                    for (Map.Entry<String, String> entry : sysParams.entrySet()) {
                        httpHeaders.add(entry.getKey(), entry.getValue());
                    }
                })
                .bodyValue(res.toString()).retrieve().bodyToMono(String.class);
//        JSONObject req = new JSONObject();
//        JSONObject obj = request.getOrderedJson();
//        req.put("request", obj);
//        Map<String, String> params = buildSystemParams();
//        String sign = createSign(params, req.toJSONString(), ParameterUtil.WPH_APP_SECRET);
//        params.put("sign", sign);
//        String url = RequestUtil.buildRequestUrl(ParameterUtil.WPH_APP_URL, params);
//        return WebClient.create(url).post().contentType(MediaType.APPLICATION_JSON).bodyValue(req.toJSONString()).retrieve().bodyToMono(String.class);
    }

    /**
     * 保存查询出的数据
     *
     * @return
     */
    public static List<WitShoppingGoods> convert(ArrayNode goodsList) {
        if (goodsList == null || goodsList.size() == 0) {
            return new ArrayList<>();
        }

        List<WitShoppingGoods> list = new ArrayList<>();

        for (JsonNode item : goodsList) {
            WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
            witShoppingGoods.setId(IdUtil.objectId());
//                witShoppingGoods.setKeyword(productName.trim());
//                witShoppingGoods.setUserId(uid);
            witShoppingGoods.setCreateTime(DateUtil.now());
//                witShoppingGoods.setRelation(relation);
//            witShoppingGoods.setPageIndex(pageIndex);
            witShoppingGoods.setGoodsChannel("sn");
            witShoppingGoods.setCommodityName(item.get("commodityInfo").get("commodityName").asText());
            witShoppingGoods.setCommodityCode(item.get("commodityInfo").get("commodityCode").asText());
            witShoppingGoods.setStoreName(Optional.ofNullable(item.get("commodityInfo").get("supplierName")).orElse(new TextNode("")).asText());
            witShoppingGoods.setSupplierCode(item.get("commodityInfo").get("supplierCode").asText());
            witShoppingGoods.setMonthSales(item.get("commodityInfo").get("monthSales").asText());
            witShoppingGoods.setSnPrice(item.get("commodityInfo").get("snPrice").asText());
            witShoppingGoods.setCommodityPrice(item.get("commodityInfo").get("commodityPrice").asText());
            witShoppingGoods.setCommodityType(item.get("commodityInfo").get("commodityType").asText());
            witShoppingGoods.setBaoyou(item.get("commodityInfo").get("baoyou").asText());
            witShoppingGoods.setSaleStatus(item.get("commodityInfo").get("saleStatus").asText());

            ArrayNode pictureUrl = (ArrayNode) item.get("commodityInfo").get("pictureUrl");
            ArrayNode array = JSONUtil.array();
            if (pictureUrl != null && pictureUrl.size() > 0) {
                for (JsonNode url : pictureUrl) {
                    array.add(url.get("picUrl").asText());
                }
            }

            witShoppingGoods.setPictureUrls(array.toString());

            witShoppingGoods.setImgUrl(pictureUrl.get(0).get("picUrl").asText());
            witShoppingGoods.setSearchTime(DateUtil.today());

            String productUrl = Optional.ofNullable(item.get("commodityInfo").get("productUrl")).orElse(new TextNode("")).asText();

//            JSONObject productUrlJson = new JSONObject();
            if (!StringUtils.isEmpty(productUrl)) {
                witShoppingGoods.setProductUrl(productUrl);
//                productUrlJson = SuningGoodsApi.suningWx(productUrl);
            } else {
                witShoppingGoods.setProductUrl(item.get("pgInfo").get("pgUrl").asText());
//                productUrlJson = SuningGoodsApi.suningWx(goodsList.getJSONObject(i).getJSONObject("pgInfo").getStr("pgUrl"));
            }

//            witShoppingGoods.setAppId(productUrlJson.getStr("appId"));
//            if (!StrUtil.hasEmpty(openid)) {
//                witShoppingGoods.setOpenid(openid);
//            }
//            witShoppingGoods.setAppletExtendUrl(productUrlJson.getStr("appletExtendUrl"));
            //                witShoppingGoods.setButlerId(butlerId);
//                witShoppingGoods.setPhone(phone);
//            if (!StrUtil.hasEmpty(label)) {
//                witShoppingGoods.setHealthyLabel(label);
//            }
            if (item.get("couponInfo").get("couponCount").asInt() > 0) {
                witShoppingGoods.setIsCoupon("1");
                witShoppingGoods.setCouponUrl(item.get("couponInfo").get("couponUrl").asText());
                witShoppingGoods.setActivityId(item.get("couponInfo").get("activityId").asText());
                witShoppingGoods.setActivitySecretKey(item.get("couponInfo").get("activitySecretKey").asText());
                witShoppingGoods.setCouponValue(item.get("couponInfo").get("couponValue").asText());
                witShoppingGoods.setCouponCount(item.get("couponInfo").get("couponCount").asText());
                witShoppingGoods.setCouponStartTime(item.get("couponInfo").get("couponStartTime").asText());
                witShoppingGoods.setCouponEndTime(item.get("couponInfo").get("couponEndTime").asText());
                witShoppingGoods.setStartTime(item.get("couponInfo").get("startTime").asText());
                witShoppingGoods.setEndTime(item.get("couponInfo").get("endTime").asText());

                double couponValue = item.get("couponInfo").get("couponValue").asDouble();
                double commodityPrice = item.get("commodityInfo").get("commodityPrice").asDouble();

                witShoppingGoods.setAfterCouponPrice(commodityPrice - couponValue + "");

            }
            if (item.has("cmmdtyReviewInfo")) {
                witShoppingGoods.setTotalReviewCount(item.get("cmmdtyReviewInfo").get("totalReviewCount").asText());
                witShoppingGoods.setGoodReviewCount(item.get("cmmdtyReviewInfo").get("goodReviewCount").asText());
                witShoppingGoods.setGoodRate(item.get("cmmdtyReviewInfo").get("goodRate").asText());

            } else {
                witShoppingGoods.setGoodRate("");
            }
            list.add(witShoppingGoods);
        }

        return list;
    }

    private SearchcommodityQueryRequest buildRequest(GoodsQuery query) {
        SearchcommodityQueryRequest request = new SearchcommodityQueryRequest();
        //1：减枝 2：不减枝 sortType=1（综合） 默认不剪枝 其他排序默认剪枝
//        request.setBranch("1");
        //城市编码 默认025
//        request.setCityCode("025");
        //1:有券；其他:全部
        request.setCoupon("0");
        //1表示拿到券后价，不传按照以前逻辑取不到券后价
//        request.setCouponMark("1");
        //结束价格
//        request.setEndPrice("20.00");
        //关键字
        request.setKeyword(query.getProductName());
        //页码 默认为1
        request.setPageIndex(query.getPage().toString());
        //是否拼购 默认为空 1：是
//        request.setPgSearch("1");
        //图片高度 默认200
//        request.setPicHeight("1000");
        //图片宽度 默认200
//        request.setPicWidth("1000");
        //销售目录ID
//        request.setSaleCategoryCode("50000");
        //每页条数 默认10，最大支持40
        request.setSize(query.getPageSize().toString());
        //是否苏宁服务 1:是
//        request.setSnfwservice("1");
        //是否苏宁国际 1:是
//        request.setSnhwg("1");
        //排序规则 1：综合（默认） 2：销量由高到低 3：价格由高到低 4：价格由低到高 5：佣金比例由高到低 6：佣金金额由高到低 7：两个维度，佣金金额由高到低，销量由高到低8：近30天推广量由高到低9：近30天支出佣金金额由高到低。
        request.setSortType("2");
        return request;
    }
}
