package com.api.smart.api;

import com.api.smart.api.duomai.JdUnionGoodsRequestHelper;
import com.api.smart.core.GoodsQuery;
import com.api.smart.core.RemoteSource;
import com.api.smart.core.SmartShoppingCallback;
import com.api.smart.coupon.dataoke.*;
import com.entity.WitShoppingGoods;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("test")
public class TestRunner {

    @RequestMapping("query")
    public String query(String keyword) {
        GoodsQuery goodsQuery = new GoodsQuery();
        goodsQuery.setProductName(keyword);
        goodsQuery.setPage(1);
        goodsQuery.setPageSize(50);
        new JdUnionGoodsRequestHelper().findGoods(goodsQuery, new SmartShoppingCallback() {
            @Override
            public void onSuccess(List<WitShoppingGoods> witShoppingGoods) {

            }

            @Override
            public void onFail(Throwable e) {

            }
        });
        return "";
    }


    @RequestMapping("cal")
    public String cal(@RequestBody String[] keywords) {
        GoodsQuery e1 = new GoodsQuery();
        e1.setPage(1);
        e1.setPageSize(10);
        e1.setKeywords(keywords);
        // todo 后续使用消息队列
        RemoteSource.QUEUE.offer(e1);
        return "";
    }

    @RequestMapping("search")
    public Object queryGoods(String productName, String pageIndex) {

        String pageId = "1";


        DtkApiClient client = DtkApiClient.getInstance(AppKeyConstant.appKey, AppKeyConstant.appSecret);
        DtkItemsQueryRequest request = new DtkItemsQueryRequest();
        request.setVersion("v1.2.4");
        request.setSort("1");
        request.setPageId("1");
        request.setPageSize(200);
        DtkApiResponse<DtkItemsQueryResponse> execute = client.execute(request);
        DtkItemsQueryResponse data = execute.getData();
        System.out.println(data);
        return "";
//        GoodsQuery e1 = new GoodsQuery();
//        e1.setId(UUID.randomUUID().toString());
//        if (StringUtils.isNotBlank(pageIndex)) {
//            e1.setPage(Integer.valueOf(pageIndex));
//        } else {
//            e1.setPage(1);
//        }
//        e1.setPageSize(10);
//        e1.setUid("1");
//
//        CompletableFuture<GoodsResponse> future = new CompletableFuture<>();
//        e1.setKeywords(new String[]{productName});
//        MydataController.MAP.put(e1.getId(), future);
//        RemoteSource.QUEUE.offer(e1);
//        Map<String, Object> goodsList = new HashMap<>();
//
//        return Mono.fromFuture(future).map(value -> {
//            goodsList.put("goodsList", value.getList());
//            return MsgUtil.ok(goodsList);
//        });
    }

}
