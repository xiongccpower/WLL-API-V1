package com.api.smart.api.duomai;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.digest.MD5;
import com.api.smart.constant.Constant;
import com.api.smart.core.GoodsQuery;
import com.api.smart.core.SmartShoppingCallback;
import com.api.smart.interfaces.ThirdGoodsApi;
import com.api.smart.utils.JSONUtil;
import com.api.smart.utils.RequestUtil;
import com.entity.WitShoppingGoods;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.TextNode;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;

@Log4j2
public class TbUnionGoodsRequestHelper implements ThirdGoodsApi, Serializable {

    public static final String APP_KEY = "437294";
    public static final String SITE_ID = "437297";

    public static final String APP_SECRET = "34035554086ee943430d3c5f12c1ba93";

    private static final String API = "cps-mesh.cpslink.alimama.products.get";

    private static final String BASE_URL = "https://open.duomai.com/apis";

    private static final String DETAIL_API = "cps-mesh.cpslink.alimama.desc.get";

    @Override
    public void findGoods(GoodsQuery query, SmartShoppingCallback callback) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("app_key", APP_KEY);
        params.put("timestamp", String.valueOf(System.currentTimeMillis() / 1000));
        params.put("service", API);


        HashMap<String, Object> p1 = new HashMap<>();
        Optional.ofNullable(query.getProductName()).ifPresent(v->p1.put("query", v));
        // p1.put("query", query.getProductName());
        p1.put("is_hot", "1");
//        p1.put("is_self", "1");
        p1.put("max_coupon", "1");
        p1.put("page", query.getPage());
        p1.put("page_size", query.getPageSize());
        p1.put("is_coupon", "1");

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            sb.append(entry.getKey()).append(entry.getValue());
        }

        Optional<String> optP = JSONUtil.toJSONString(p1);
        String json = "";
        if (optP.isPresent()) {
            json = optP.get();
            sb.append(json);
        }

        sb.insert(0, APP_SECRET).append(APP_SECRET);
        String sign = MD5.create().digestHex(sb.toString()).toUpperCase();
        params.put("sign", sign);

        WebClient.create(RequestUtil.buildRequestUrl(BASE_URL, params))
                .post().contentType(MediaType.APPLICATION_JSON)
                .bodyValue(json).retrieve().bodyToMono(String.class).doOnError(v -> {
            System.out.println(v);
        }).subscribe((Consumer<String>) s -> {
            try {
                JSONUtil.parse(s).ifPresent(v -> {
                    if (v.get("status").intValue() == 0) {
                        JsonNode data = v.get("data");
                        if (data.isArray()) {
                            ArrayNode array = (ArrayNode) data;
                            callback.onSuccess(convert(array));
                        } else {
                            callback.onSuccess(new ArrayList<>());
                        }
                    }
                });
            } catch (Exception e) {
                callback.onSuccess(new ArrayList<>());
            }
        });
    }

    private List<WitShoppingGoods> convert(ArrayNode goodsList) {
        if (goodsList == null || goodsList.size() == 0) {
            return new ArrayList<>();
        }

        List<WitShoppingGoods> list = new ArrayList<>();

        for (JsonNode item : goodsList) {
            WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
            witShoppingGoods.setId(IdUtil.objectId());
//                witShoppingGoods.setKeyword(productName.trim());
//                witShoppingGoods.setUserId(uid);
            witShoppingGoods.setCreateTime(DateUtil.now());
//                witShoppingGoods.setRelation(relation);
//            witShoppingGoods.setPageIndex(pageIndex);
            witShoppingGoods.setGoodsChannel("tb");
            witShoppingGoods.setCommodityName(item.get("item_title").asText());
            witShoppingGoods.setCommodityCode(item.get("item_id").asText());
            witShoppingGoods.setStoreName(Optional.ofNullable(item.get("seller_name")).orElse(new TextNode("")).asText());
            witShoppingGoods.setSupplierCode(item.get("seller_id").asText());
            witShoppingGoods.setMonthSales(item.get("item_volume").asText());
            witShoppingGoods.setSnPrice(item.get("item_final_price").asText());
            witShoppingGoods.setCommodityPrice(item.get("item_price").asText());
            witShoppingGoods.setCommodityType(item.get("category").asText());
            witShoppingGoods.setBaoyou("");
            witShoppingGoods.setSaleStatus("0");

            ArrayNode pictureUrl = (ArrayNode) item.get("item_small_pictures");

            witShoppingGoods.setPictureUrls(pictureUrl.toString());

            witShoppingGoods.setImgUrl(pictureUrl.get(0).asText());
            witShoppingGoods.setSearchTime(DateUtil.today());

            witShoppingGoods.setProductUrl(item.get("item_url").asText());

            witShoppingGoods.setIsCoupon("1");
            witShoppingGoods.setCouponValue(item.get("coupon_price").asText());
            witShoppingGoods.setCouponUrl(item.get("coupon").asText());
            witShoppingGoods.setCouponQuota(item.get("coupon_quota").asText());
            // 佣金比例
            witShoppingGoods.setFee(item.get("commission_amount").asText());
            witShoppingGoods.setFeePercent(item.get("commission_rate").asText());

            witShoppingGoods.setCouponStartTime(item.get("coupon_start_time").asText());
            witShoppingGoods.setCouponEndTime(item.get("coupon_end_time").asText());

            witShoppingGoods.setAfterCouponPrice(item.get("item_final_price").asText());
            witShoppingGoods.setInStock(item.get("in_stock").asBoolean(true));
            witShoppingGoods.setGoodRate("");

            list.add(witShoppingGoods);
        }

        return list;
    }

    @Override
    public String getName() {
        return "多麦联盟-淘宝";
    }

    @Override
    public boolean support(String channel) {
        return "tb".equals(channel);
    }

    private WitShoppingGoods convert(JsonNode item) {

        WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
        witShoppingGoods.setId(IdUtil.objectId());
//                witShoppingGoods.setKeyword(productName.trim());
//                witShoppingGoods.setUserId(uid);
        witShoppingGoods.setCreateTime(DateUtil.now());
//                witShoppingGoods.setRelation(relation);
//            witShoppingGoods.setPageIndex(pageIndex);
        witShoppingGoods.setGoodsChannel("jd");
        witShoppingGoods.setCommodityName(item.get("item_title").asText());
        witShoppingGoods.setCommodityCode(item.get("item_id").asText());
        witShoppingGoods.setStoreName(Optional.ofNullable(item.get("seller_name")).orElse(new TextNode("")).asText());
        witShoppingGoods.setSupplierCode(item.get("seller_id").asText());
        witShoppingGoods.setMonthSales(item.get("item_volume").asText());
        witShoppingGoods.setSnPrice(item.get("item_final_price").asText());
        witShoppingGoods.setCommodityPrice(item.get("item_price").asText());
        witShoppingGoods.setCommodityType(item.get("category").asText());
        witShoppingGoods.setBaoyou("");
        witShoppingGoods.setSaleStatus("0");

        ArrayNode pictureUrl = (ArrayNode) item.get("item_small_pictures");

        witShoppingGoods.setPictureUrls(pictureUrl.toString());

        witShoppingGoods.setImgUrl(pictureUrl.get(0).asText());
        witShoppingGoods.setSearchTime(DateUtil.today());

        witShoppingGoods.setProductUrl(item.get("item_url").asText());

        witShoppingGoods.setIsCoupon("1");
        witShoppingGoods.setCouponValue(item.get("coupon_price").asText());
        witShoppingGoods.setCouponUrl(item.get("coupon").asText());
        witShoppingGoods.setCouponQuota(item.get("coupon_quota").asText());

        // 佣金比例
        witShoppingGoods.setFee(item.get("commission_amount").asText());
        witShoppingGoods.setFeePercent(item.get("commission_rate").asText());


        witShoppingGoods.setCouponStartTime(item.get("coupon_start_time").asText());
        witShoppingGoods.setCouponEndTime(item.get("coupon_end_time").asText());

        witShoppingGoods.setAfterCouponPrice(item.get("item_final_price").asText());

        witShoppingGoods.setInStock(item.get("in_stock").asBoolean(true));

        witShoppingGoods.setGoodRate("");
        return witShoppingGoods;
    }

    @Override
    public Mono<WitShoppingGoods> detail(String id) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("app_key", Constant.DUOMAI_APP_KEY);
        params.put("timestamp", String.valueOf(System.currentTimeMillis() / 1000));
        params.put("service", DETAIL_API);


        HashMap<String, Object> p1 = new HashMap<>();
        p1.put("id", id);

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            sb.append(entry.getKey()).append(entry.getValue());
        }

        Optional<String> optP = JSONUtil.toJSONString(p1);
        String json = "";
        if (optP.isPresent()) {
            json = optP.get();
            sb.append(json);
        }

        sb.insert(0, Constant.DUOMAI_APP_SECRET).append(Constant.DUOMAI_APP_SECRET);
        String sign = MD5.create().digestHex(sb.toString()).toUpperCase();
        params.put("sign", sign);

        return WebClient.create(RequestUtil.buildRequestUrl(Constant.DUOMAI_BASE_URL, params))
                .post().contentType(MediaType.APPLICATION_JSON)
                .bodyValue(json).retrieve().bodyToMono(String.class).map(new Function<String, WitShoppingGoods>() {
                    @Override
                    public WitShoppingGoods apply(String s) {
                        try {
                            Optional<JsonNode> optJson = JSONUtil.parse(s);
                            if (optJson.isPresent()) {
                                JsonNode node = optJson.get();
                                if (node.get("status").intValue() == 0) {
                                    JsonNode data = node.get("data");
                                    return convert(data);
                                }
                            }
                            return new WitShoppingGoods();
                        } catch (Exception e) {
                            return new WitShoppingGoods();
                        }
                    }
                });
    }
}
