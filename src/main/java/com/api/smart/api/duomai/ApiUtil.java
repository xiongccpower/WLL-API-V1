package com.api.smart.api.duomai;

import cn.hutool.crypto.digest.MD5;
import com.api.smart.utils.JSONUtil;
import com.api.smart.utils.RequestUtil;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

/**
 * created in 2022/2/21 16:37
 *
 * @author zhuxuelei
 */
public  class ApiUtil {
    private ApiUtil() {
    }

    private static final String APP_KEY = "437294";

    private static final String APP_SECRET = "34035554086ee943430d3c5f12c1ba93";

    private static final String BASE_URL = "https://open.duomai.com/apis";

    public static Mono<String> execute(String api, Map<String, Object> params) {
        TreeMap<String, String> map = new TreeMap<>();
        map.put("app_key", APP_KEY);
        map.put("timestamp", String.valueOf(System.currentTimeMillis() / 1000));
        map.put("service", api);

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            sb.append(entry.getKey()).append(entry.getValue());
        }

        Optional<String> optP = JSONUtil.toJSONString(params);
        String json = "";
        if (optP.isPresent()) {
            json = optP.get();
            sb.append(json);
        }

        sb.insert(0, APP_SECRET).append(APP_SECRET);
        String sign = MD5.create().digestHex(sb.toString()).toUpperCase();
        map.put("sign", sign);

        return WebClient.create(RequestUtil.buildRequestUrl(BASE_URL, map))
                .post().contentType(MediaType.APPLICATION_JSON)
                .bodyValue(json).retrieve().bodyToMono(String.class);
    }
}
