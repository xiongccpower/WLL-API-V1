package com.api.smart.api.wph;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.api.smart.core.GoodsQuery;
import com.api.smart.core.SmartShoppingCallback;
import com.api.smart.entity.query.GoodsRequest;
import com.api.smart.entity.third.WphResponse;
import com.api.smart.interfaces.ThirdGoodsApi;
import com.api.smart.utils.JSONUtil;
import com.api.smart.utils.RequestUtil;
import com.entity.WitShoppingGoods;
import com.util.ParameterUtil;
import com.vip.adp.api.open.service.GoodsCommentsInfo;
import com.vip.adp.api.open.service.GoodsInfo;
import com.vip.adp.api.open.service.GoodsInfoResponse;
import com.vip.adp.api.open.service.StoreServiceCapability;
import com.vip.adp.common.service.PMSCouponInfo;
import com.vip.osp.sdk.util.HmacUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;

@Log4j2
public class WphUnionGoodsRequestHelper implements ThirdGoodsApi, Serializable {

    @Override
    public void findGoods(GoodsQuery query, SmartShoppingCallback callback) {
        this.query(query).onErrorMap(throwable -> {
            throwable.printStackTrace();
            callback.onFail(throwable);
            return throwable;
        }).subscribe((Consumer<String>) s -> {
            if (!StringUtils.isBlank(s)) {
                Optional<WphResponse> optRes = JSONUtil.parse(s, WphResponse.class);
                if (optRes.isPresent()) {
                    WphResponse response = optRes.get();
                    long end = System.currentTimeMillis();
                    callback.onSuccess(convert(response.getResult()));
                }
            }
            callback.onFail(null);
        });
    }

    @Override
    public String getName() {
        return "唯品会";
    }

    @Override
    public boolean support(String channel) {
        return false;
    }

    @Override
    public Mono<WitShoppingGoods> detail(String id) {
        return null;
    }

    public List<WitShoppingGoods> convert(GoodsInfoResponse result) {
        if (result == null) {
            return new ArrayList<>();
        }
        List<GoodsInfo> goodsInfoList = result.getGoodsInfoList();
        if (goodsInfoList == null) {
            return new ArrayList<>();
        }
        List<WitShoppingGoods> list = new ArrayList<>();

        for (GoodsInfo good : goodsInfoList) {
            WitShoppingGoods witShoppingGoods = new WitShoppingGoods();
            witShoppingGoods.setId(IdUtil.objectId());
//            witShoppingGoods.setRelation(parameter.get("relation") + "");
//            witShoppingGoods.setUserId(uid);
//            witShoppingGoods.setBaoyou("1");
//            witShoppingGoods.setKeyword(productName.trim());
//            witShoppingGoods.setPageIndex(pageIndex);
            witShoppingGoods.setStoreName(good.getStoreInfo().getStoreName());
            witShoppingGoods.setCommodityName(good.getGoodsName());
            witShoppingGoods.setCommodityCode(good.getGoodsId());
            witShoppingGoods.setPictureUrl(good.getGoodsThumbUrl());
            witShoppingGoods.setCommodityPrice(good.getVipPrice());
            witShoppingGoods.setRate(good.getCommissionRate());

            PMSCouponInfo coupon = good.getCouponInfo();
            if (coupon != null) {
                if (!StrUtil.hasEmpty(coupon.getFav())) {
                    witShoppingGoods.setIsCoupon("1");
                    witShoppingGoods.setCouponValue(coupon.getFav());
                    witShoppingGoods.setCouponCount(coupon.getTotalAmount() + "");
                }
            }
            GoodsCommentsInfo commentsInfo = good.getCommentsInfo();
            witShoppingGoods.setTotalReviewCount(commentsInfo.getComments() + "");
            if (!StrUtil.hasEmpty(commentsInfo.getGoodCommentsShare())) {
                witShoppingGoods.setGoodRate(commentsInfo.getGoodCommentsShare().substring(0, commentsInfo.getGoodCommentsShare().length() - 3) + "%");
            } else {
                witShoppingGoods.setGoodRate("");
            }
            witShoppingGoods.setProductUrl(good.getDestUrl());
            StoreServiceCapability storeServiceCapability = good.getStoreServiceCapability();
            witShoppingGoods.setStoreScore(storeServiceCapability.getStoreScore());
            if (good.getCpsInfo() != null) {
                Map<Integer, String> map = good.getCpsInfo();
                witShoppingGoods.setAppletExtendUrl(map.get(2));
            }
            JSONArray pictureUrls = new JSONArray();
            pictureUrls.add(good.getGoodsMainPicture());
            witShoppingGoods.setPictureUrls(pictureUrls.toString());
            witShoppingGoods.setSearchTime(DateUtil.today());

            witShoppingGoods.setImgUrl(good.getGoodsMainPicture());
            witShoppingGoods.setCreateTime(DateUtil.now());
            witShoppingGoods.setGoodsChannel("wph");

            list.add(witShoppingGoods);
        }
        return list;
    }

    public Mono<String> query(GoodsQuery goodsQuery) {
        GoodsRequest request = buildRequest(goodsQuery);
        JSONObject req = new JSONObject();
        JSONObject obj = request.getOrderedJson();
        req.put("request", obj);
        Map<String, String> params = buildSystemParams();
        String sign = createSign(params, req.toJSONString(), ParameterUtil.WPH_APP_SECRET);
        params.put("sign", sign);
        String url = RequestUtil.buildRequestUrl(ParameterUtil.WPH_APP_URL, params);
        return WebClient.create(url).post().contentType(MediaType.APPLICATION_JSON).bodyValue(req.toJSONString()).retrieve().bodyToMono(String.class);
    }

    private GoodsRequest buildRequest(GoodsQuery query) {
        GoodsRequest request = new GoodsRequest();
        //关键词
        request.setKeyword(query.getProductName());
        //排序字段
//            request1.setFieldName("fieldName");
        //排序顺序：0-正序，1-逆序，默认正序
//            request1.setOrder(1);
        //页码
        request.setPage(query.getPage());
        //页面大小：默认20,最大50
        request.setPageSize(query.getPageSize());
        //请求id：调用方自行定义，用于追踪请求，单次请求唯一，建议使用UUID
        request.setRequestId(IdUtil.objectId());
        //价格区间---start
//            request1.setPriceStart("priceStart");
        //价格区间---end
//            request1.setPriceEnd("priceEnd");
        //是否查询商品评价信息:默认不查询，该数据在详情页有返回，没有特殊需求，建议不查询，影响接口性能
        request.setQueryReputation(true);
        //是否查询店铺服务能力信息：默认不查询，该数据在详情页有返回，没有特殊需求，建议不查询，影响接口性能
        request.setQueryStoreServiceCapability(true);
        //是否查询库存:默认不查询
        request.setQueryStock(true);
        //是否查询商品活动信息:默认不查询
        request.setQueryActivity(true);
        //是否查询商品预付信息:默认不查询
        request.setQueryPrepay(true);
        //通用参数
//        CommonParams commonParams2 = new CommonParams();
//        if (plat.equals("applets")) {
//            //用户平台：1-PC,2-APP,3-小程序,不传默认为APP
//            commonParams2.setPlat(3);
//            //是否返回cps链接：0-不查询，1-tra_from参数,2-小程序链接，默认为0，查询多个时按照位运算处理，例如：3表示查询tra_from参数+小程序链接
//            request.setQueryCpsInfo(2);
//        } else {
        //用户平台：1-PC,2-APP,3-小程序,不传默认为APP
//        commonParams2.setPlat(2);
        //是否返回cps链接：0-不查询，1-tra_from参数,2-小程序链接，默认为0，查询多个时按照位运算处理，例如：3表示查询tra_from参数+小程序链接
        request.setQueryCpsInfo(0);
//        }

        //设备号类型：IMEI，IDFA，OAID，有则传入
//            commonParams2.setDeviceType("deviceType");
        //设备号MD5加密后的值，有则传入
//            commonParams2.setDeviceValue("deviceValue");
        //用户ip地址
//            commonParams2.setIp("ip");
        //经度 如:29.590961456298828
//            commonParams2.setLongitude("longitude");
        //纬度 如:106.51573181152344
//            commonParams2.setLatitude("latitude");
        //分仓 VIP_NH：广州仓， VIP_SH：上海仓， VIP_BJ：北京仓， VIP_CD：成都仓， VIP_HZ：华中仓， ALL：全国仓
//            commonParams2.setWarehouse("warehouse");

//        request.setCommonParams(commonParams2);
        //工具商code
//            request1.setVendorCode("vendorCode");
        //用户pid
//            request1.setChanTag("chanTag");
        //是否查询专属红包信息：默认不查询
        request.setQueryExclusiveCoupon(true);
        return request;
    }

    private Map<String, String> buildSystemParams() {
        Map<String, String> params = new TreeMap<>();
//        if (lastInvocation.getAccessToken() != null && !lastInvocation.getAccessToken().trim().isEmpty()) {
//            params.put("accessToken", lastInvocation.getAccessToken());
//        }
//
//        if (lastInvocation.getLanguage() != null && !lastInvocation.getLanguage().trim().isEmpty()) {
//            params.put("language", lastInvocation.getLanguage());
//        }

        params.put("service", "com.vip.adp.api.open.service.UnionGoodsService");
        params.put("method", "query");
        params.put("version", "1.0.0");
        params.put("timestamp", String.valueOf(System.currentTimeMillis() / 1000L));
        params.put("format", "JSON");
        params.put("appKey", ParameterUtil.WPH_APP_KEY);
        return params;
    }

    private String createSign(Map<String, String> queryParams, String requestBody, String appSecret) {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, String> entry : queryParams.entrySet()) {
            builder.append(entry.getKey()).append(entry.getValue());
        }
        builder.append(requestBody);
        try {
            return HmacUtils.byte2hex(HmacUtils.encryptHMAC(builder.toString(), appSecret));
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

}
