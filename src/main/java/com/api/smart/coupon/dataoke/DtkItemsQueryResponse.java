package com.api.smart.coupon.dataoke;

import lombok.Data;

import java.util.List;

/**
 * created in 2022/3/8 15:29
 *
 * @author zhuxuelei
 */
@Data
public class DtkItemsQueryResponse {
    private List<DtkItem> list;

    private String pageId;

    private Integer totalNum;
}
