package com.api.smart.coupon.dataoke;

import com.fasterxml.jackson.core.type.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * created in 2022/3/8 15:27
 *
 * @author zhuxuelei
 */
@Getter
@Setter
public class DtkItemsQueryRequest implements DtkApiRequest<DtkApiResponse<DtkItemsQueryResponse>> {
    @ApiModelProperty(value = "版本号", example = "v1.0.0")
    private String version = "1.0.0";
    private String pageId;

    private Integer pageSize;

    private String sort;

    @ApiModelProperty("优惠券查询请求path")
    private final String requestPath = "/goods/get-goods-list";


    @Override
    public Map<String, String> getTextParams() throws IllegalAccessException {
        return ObjectUtil.objToMap(this);
    }

    @Override
    public String apiVersion() {
        return this.version;
    }

    @Override
    public TypeReference<DtkApiResponse<DtkItemsQueryResponse>> responseType() {
        return new TypeReference<DtkApiResponse<DtkItemsQueryResponse>>() {
        };
    }

    @Override
    public String requestUrl() {
        return this.requestPath;
    }
}
