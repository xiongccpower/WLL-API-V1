package com.api.smart.coupon.dataoke;

import lombok.Data;

import java.util.List;

/**
 * created in 2022/3/8 16:01
 *
 * @author zhuxuelei
 */
@Data
public class DtkItem {

    private String id;
    private String goodsId;
    private String itemLink;
    private String title;
    private String dtitle;
    private String desc;
    private String cid;
    private Integer[] subcid;
    private String tbcid;
    private String mainPic;
    private String marketingMainPic;
    private String video;
    private String originalPrice;
    private String actualPrice;
    private String discounts;
    private String commissionType;
    private String commissionRate;
    private String couponLink;
    private String couponTotalNum;
    private String couponReceiveNum;
    private String couponEndTime;
    private String couponStartTime;
    private String couponPrice;
    private String couponConditions;
    private String monthSales;
    private String twoHoursSales;
    private String dailySales;
    private String brand;
    private String brandId;
    private String brandName;
    private String createTime;
    private String activityType;
    private String activityStartTime;
    private String activityEndTime;
    private String shopType;
    private String haitao;
    private String sellerId;
    private String shopName;
    private String shopLevel;
    private String descScore;
    private String dsrScore;
    private String dsrPercent;
    private String shipScore;
    private String shipPercent;
    private String serviceScore;
    private String servicePercent;
    private String hotPush;
    private String teamName;
    private String quanMLink;
    private String hzQuanOver;
    private String yunfeixian;
    private String estimateAmount;
    private String shopLogo;
    private List<String> specialText;
    private String freeshipRemoteDistrict;
    private String goldSellers;
    private String directCommissionType;
    private String directCommission;
    private String directCommissionLink;
    private String discountType;
    private String discountFull;
    private String discountCut;
    private String inspectedGoods;
}
