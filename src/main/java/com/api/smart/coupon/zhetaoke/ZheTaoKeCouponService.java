package com.api.smart.coupon.zhetaoke;

import com.api.smart.coupon.CouponService;
import com.api.smart.entity.goods.UnionCoupon;
import com.api.smart.utils.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ZheTaoKeCouponService implements CouponService {
    //    private static final String API = "https://api.zhetaoke.com:10002/api/api_detail.ashx";
    private static final String API = "https://api.zhetaoke.com:10001/api/open_activity_id.ashx?appkey={0}&sid={1}&content={2}&type=1";

    private static final String APP_KEY = "e91e8a53fb894840bf0c8f1ac9e6a867";

    private static final String SID = "68169";

    public Mono<List<UnionCoupon>> getCoupon(String content) {
        try {
            return WebClient.create(MessageFormat.format(API, APP_KEY, SID, URLEncoder.encode(content, "utf-8"))).get().retrieve().bodyToMono(String.class)
                    .onErrorMap(throwable -> {
                        throwable.printStackTrace();
                        return throwable;
                    }).map(v -> {
                        ArrayList<UnionCoupon> list = new ArrayList<>();
                        Optional<JsonNode> optJson = JSONUtil.parse(v);
                        if (optJson.isPresent()) {
                            if (StringUtils.isNotBlank(optJson.get().get("activity_id").asText())) {
                                // 添加到列表中
                                return list;
                            }
                        }
                        return list;
                    });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        // 获取优惠券ID和商品ID拼接
        // https://uland.taobao.com/coupon/edetail?activityId=134ef80c0faa45a59a9e3beb1a52a657&itemId=573789453821&pid=mm_000_000_000
        // https://uland.taobao.com/coupon/edetail?activityId=881756b0c1a84bf691207995450f4f01&itemId=613203023742
        return Mono.just(new ArrayList<>());
    }

//    public static void main(String[] args) throws UnsupportedEncodingException {
//        getCoupon("https://detail.tmall.com/item.htm?id=613203023742");
//    }
}
