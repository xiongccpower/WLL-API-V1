package com.api.smart.coupon;

import com.api.smart.entity.goods.UnionCoupon;
import reactor.core.publisher.Mono;

import java.util.List;

public interface CouponService {
    Mono<List<UnionCoupon>> getCoupon(String content);
}
