package com.api.smart.interfaces;

import com.api.smart.core.GoodsQuery;
import com.api.smart.core.SmartShoppingCallback;
import com.entity.WitShoppingGoods;
import reactor.core.publisher.Mono;

public interface ThirdGoodsApi {
    void findGoods(GoodsQuery query, SmartShoppingCallback callback);

    String getName();

    boolean support(String channel);

    /**
     * todo 后续进行接口合并，目前有代码冗余
     *
     * @param id
     * @return
     */
    Mono<WitShoppingGoods> detail(String id);
}
