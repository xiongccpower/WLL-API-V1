package com.api.sms;

import com.alibaba.fastjson.JSON;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * String smsmsg = "【未来里】您已购买未来里管家，登录未来里管家小程序查看订单，登录账号"
 * + ordernew.getTel() + "，默认密码" + ordernew.getPassward();
 *
 * @author tianyh
 * @Description:��ͨ���ŷ���
 */
@Log4j2
@Service
public class SmsSendClient {

    public static final String charset = "utf-8";
    // �û�ƽ̨API�˺�(�ǵ�¼�˺�,ʾ��:N1234567)
    public static String account = "N1763545";
    // �û�ƽ̨API����(�ǵ�¼����)
    public static String pswd = "2HDXyd9TOf1a18";

    //Ӫ���˺�
    public static String yx_account = "M6613135";
    // Ӫ���û�ƽ̨API����(�ǵ�¼����)
    public static String yx_pswd = "rD2QcpNz4hac20";

    public static String smsSingleRequestServerUrl = "http://smssh1.253.com/msg/send/json";

    public static String smsVariableRequestUrl = "http://smssh1.253.com/msg/variable/json";

    public static String report = "true";


    public static void main(String[] args) throws UnsupportedEncodingException {

        //�����ַ���¼253��ͨѶ����ͨƽ̨�鿴����ѯ�������������˻�ȡ
        String smsSingleRequestServerUrl = "http://smssh1.253.com/msg/send/json";
        // ��������
        String msg = "【未来里】您已购买未来里管家，登录未来里管家小程序查看订单，登录账号18120148212，默认密码82124523";
        //�ֻ�����
        String phone = "18120148212";
        //״̬����
        String report = "true";

        SmsSendRequest smsSingleRequest = new SmsSendRequest(account, pswd, msg, phone, report);

        String requestJson = JSON.toJSONString(smsSingleRequest);

        System.out.println("before request string is: " + requestJson);

        String response = ChuangLanSmsUtil.sendSmsByPost(smsSingleRequestServerUrl, requestJson);

        System.out.println("response after request result is :" + response);

        SmsSendResponse smsSingleResponse = JSON.parseObject(response, SmsSendResponse.class);

        System.out.println("response  toString is :" + smsSingleResponse);

    }

    public void sendSms(String phone, String msg) throws IOException {
        SmsSendRequest smsSingleRequest = new SmsSendRequest(account, pswd, msg, phone, report);

        String requestJson = JSON.toJSONString(smsSingleRequest);

        log.error("before request string is: " + requestJson);

        String response = ChuangLanSmsUtil.sendSmsByPost(smsSingleRequestServerUrl, requestJson);

        log.error("response after request result is :" + response);

        SmsSendResponse smsSingleResponse = JSON.parseObject(response, SmsSendResponse.class);

        log.error("response  toString is :" + smsSingleResponse);
    }
//
//    public void sendSmsYX(String phone, String msg) throws IOException {
//        SmsSendRequest smsSingleRequest = new SmsSendRequest(yx_account, yx_pswd, msg, phone, report);
//
//        String requestJson = JSON.toJSONString(smsSingleRequest);
//
//        _LOGGER.error("before request string is: " + requestJson);
//
//        String response = ChuangLanSmsUtil.sendSmsByPost(smsSingleRequestServerUrl, requestJson);
//
//        _LOGGER.error("response after request result is :" + response);
//
//        SmsSendResponse smsSingleResponse = JSON.parseObject(response, SmsSendResponse.class);
//
//        _LOGGER.error("response  toString is :" + smsSingleResponse);
//    }
//
//    public void sendVariable(String tel, String name, String service, String tel2) throws IOException {
//
//        //�����ַ���¼253��ͨѶ����ͨƽ̨�鿴����ѯ�������������˻�ȡ
////	        String smsVariableRequestUrl = "http://xxx/msg/variable/json";
//        //��������****ҵ���µ�ԤԼ����*****���񣬾�����ϵҵ������ϵ��ʽ��13888888888
////	        String msg = "����������𾴵�{$var},����,������֤����{$var},{$var}��������Ч";
//        String msg = "���������{$var}ҵ���µ�ԤԼ����{$var}���񣬾�����ϵҵ������ϵ��ʽ��{$var}";
//        //������
////	        String params = "159*******,����,123456,3;130********,����,123456,3;";
//
//        String params = tel + "," + name + "," + service + "," + tel2;
//        //״̬����
//        String report = "true";
//
//        SmsVariableRequest smsVariableRequest = new SmsVariableRequest(account, pswd, msg, params, report);
//
//        String requestJson = JSON.toJSONString(smsVariableRequest);
//
//        System.out.println("before request string is: " + requestJson);
//
//        String response = ChuangLanSmsUtil.sendSmsByPost(smsVariableRequestUrl, requestJson);
//
//        System.out.println("response after request result is : " + response);
//
//        SmsVariableResponse smsVariableResponse = JSON.parseObject(response, SmsVariableResponse.class);
//
//        System.out.println("response  toString is : " + smsVariableResponse);
//
//    }
//
//    public void sendVariable2(String tel, String name, String mname, String service, String tel1, String tel2) throws IOException {
//
//        //�����ַ���¼253��ͨѶ����ͨƽ̨�鿴����ѯ�������������˻�ȡ
////	        String smsVariableRequestUrl = "http://xxx/msg/variable/json";
//        //��������****ҵ���µ�ԤԼ����*****���񣬾�����ϵҵ������ϵ��ʽ��13888888888
////	        String msg = "����������𾴵�{$var},����,������֤����{$var},{$var}��������Ч";
//        String msg = "";
//        if (BaseUtils.DEBUG) {
//            msg = "�������������-{$var}ҵ��ԤԼ{$var}ʦ��{$var}����ҵ����{$var}��ʦ����{$var}";
//        } else {
//            msg = "���������{$var}ҵ��ԤԼ{$var}ʦ��{$var}����ҵ����{$var}��ʦ����{$var}";
//        }
//        //������
////	        String params = "159*******,����,123456,3;130********,����,123456,3;";
//
//        String params = tel + "," + name + "," + mname + "," + service + "," + tel1 + "," + tel2;
//        //״̬����
//        String report = "true";
//
//        SmsVariableRequest smsVariableRequest = new SmsVariableRequest(account, pswd, msg, params, report);
//
//        String requestJson = JSON.toJSONString(smsVariableRequest);
//
//        System.out.println("before request string is: " + requestJson);
//
//        String response = ChuangLanSmsUtil.sendSmsByPost(smsVariableRequestUrl, requestJson);
//
//        System.out.println("response after request result is : " + response);
//
//        SmsVariableResponse smsVariableResponse = JSON.parseObject(response, SmsVariableResponse.class);
//
//        System.out.println("response  toString is : " + smsVariableResponse);
//
//    }

}




