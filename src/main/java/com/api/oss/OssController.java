package com.api.oss;

import com.aliyun.oss.OSSClient;
import com.wll.wulian.entity.base.R;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * created in 2022/1/18 9:53
 *
 * @author zhuxuelei
 */
@RequestMapping("oss")
@RestController
public class OssController {

    @PostMapping("upload")
    public R upload(String uid, MultipartFile file) {
        try {
            OSSClient client = AliyunOSSClientUtil.getOSSClient();
            String path = AliyunOSSClientUtil.uploadByteOSS(client, file.getOriginalFilename(), file.getBytes(), OSSClientConstants.BACKET_NAME, uid);
            String url = AliyunOSSClientUtil.getUrl(client, OSSClientConstants.BACKET_NAME, path);
            return R.ok().put("path", path).put("url", url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.error();
    }

//    @PostMapping("upload/m3u8")
//    public R upload(List<MultipartFile> files) {
//        try {
//            OSSClient client = AliyunOSSClientUtil.getOSSClient();
//            String path = AliyunOSSClientUtil.uploadByteOSS(client, file.getOriginalFilename(), file.getBytes(), OSSClientConstants.BACKET_NAME, uid);
//            String url = AliyunOSSClientUtil.getUrl(client, OSSClientConstants.BACKET_NAME, path);
//            return R.ok().put("path", path).put("url", url);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return R.error();
//    }

    @PostMapping("rich/image")
    public R uploadImage(MultipartFile file) {
        try {
            OSSClient client = AliyunOSSClientUtil.getOSSClient();
            String path = AliyunOSSClientUtil.uploadByteOSS(client, file.getOriginalFilename(), file.getBytes(), OSSClientConstants.BACKET_NAME, "0");
            String url = AliyunOSSClientUtil.getUrl(client, OSSClientConstants.BACKET_NAME, path);
            List<OssUrl> list = new ArrayList<>();
            list.add(new OssUrl(url));
            return R.ok().put("errno", 0).put("data", list);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.error().put("errno", -1);
    }

    @PostMapping("rich/video")
    public R uploadVideo(MultipartFile file) {
        try {
            OSSClient client = AliyunOSSClientUtil.getOSSClient();
            String path = AliyunOSSClientUtil.uploadByteOSS(client, file.getOriginalFilename(), file.getBytes(), OSSClientConstants.BACKET_NAME, "0");
            String url = AliyunOSSClientUtil.getUrl(client, OSSClientConstants.BACKET_NAME, path);
            return R.ok().put("errno", 0).put("data", new OssUrl(url));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.error().put("errno", -1);
    }
}
