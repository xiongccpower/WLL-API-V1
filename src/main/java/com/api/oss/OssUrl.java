package com.api.oss;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * created in 2022/1/18 13:50
 *
 * @author zhuxuelei
 */
@Data
@NoArgsConstructor
public class OssUrl {

    private String url;

    private String alt;

    private String href;

    public OssUrl(String url) {
        this.url = url;
    }
}
