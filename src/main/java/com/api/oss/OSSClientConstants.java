package com.api.oss;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
* @ClassName: OSSClientConstants 
* @Description: OSS阿里云常用变量
* @author xuchunlei
* @date 2018年7月4日 上午14:24:27
*
 */
public class OSSClientConstants {
    //阿里云API的外网域名  
    public static final String ENDPOINT = "http://oss-cn-hangzhou.aliyuncs.com";  
    //阿里云API的外网访问域名  
    public static final String VISIT_ENDPOINT = "https://mjsh.oss-cn-hangzhou.aliyuncs.com";  
    //阿里云API的密钥Access Key ID  
    public static final String ACCESS_KEY_ID = "LTAIHGDSrd4bhaBq";  
    //阿里云API的密钥Access Key Secret  
    public static final String ACCESS_KEY_SECRET = "1016NsyO5b7LNUkkczxnNxuosCGIiz";  
    //阿里云API的bucket名称  
    public static final String BACKET_NAME = "mjsh";  
    //阿里云API的文件夹名称  
    public static final String FOLDER="pic/"; 
    public static final String FOLDER_VIDEO="video/";
    public static final String FORMAT = new SimpleDateFormat("yyyyMMdd").format(new Date());
    public static final String FORMATS = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    
    //视频截图
    public static final String VIDEO_CUT="x-oss-process=video/snapshot,t_1000,f_jpg,w_800,h_600,m_fast";
}