package com.api.map;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * created in 2022/1/18 16:06
 *
 * @author zhuxuelei
 */
public class MapUtil {
    public static GpsModel getGpsInfo(String longitude, String latitude) throws MalformedURLException {
        GpsModel gpsModel = new GpsModel();
        gpsModel.setLatitude(latitude);
        gpsModel.setLongitude(longitude);
        BufferedReader in = null;
        URL tirc = new URL(
                "http://api.map.baidu.com/geocoder/v2/?ak=hsAYKUd4KRB5MGzXL8nhPkGqtFYQOlyd&callback=renderReverse&location="
                        + latitude + "," + longitude + "&output=json&pois=1");
        try {
            in = new BufferedReader(new InputStreamReader(tirc.openStream(), "UTF-8"));
            String res;
            StringBuilder sb = new StringBuilder("");
            while ((res = in.readLine()) != null) {
                sb.append(res.trim());
            }
            String str = sb.toString();
            str = str.substring(str.indexOf("{"), str.length() - 1);
            System.out.println(str);
            ObjectMapper mapper = new ObjectMapper();
            // 允许出现特殊字符和转义符
            mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
            JsonNode root = mapper.readTree(str);

            String province = root.path("result").path("addressComponent").path("province").asText();
            String city = root.path("result").path("addressComponent").path("city").asText();
            String district = root.path("result").path("addressComponent").path("district").asText();
            gpsModel.setProvince(province);
            gpsModel.setCity(city);
            gpsModel.setDistrict(district);
            return gpsModel;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return gpsModel;

    }
}
