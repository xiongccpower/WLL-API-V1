package com.api.map;

import lombok.Data;

@Data
public class GpsModel {
    private String longitude;

    private String latitude;

    private String province;//省

    private String city;//市

    private String district;//区
}