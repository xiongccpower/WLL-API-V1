package com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @TableName mj_order_refund
 */
@TableName(value = "mj_order_refund")
@Data
public class OrderRefund implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long morId;

    /**
     * 订单id,关联mj_order表 的ID
     */
    private Integer orderId;

    /**
     * 说明
     */
    private String remark;

    /**
     * 0，退款，1，退换货
     */
    private Integer service;

    /**
     * 原因
     */
    private String reason;

    /**
     * 照片
     */
    private String images;

    /**
     * 联系电话
     */
    private String mobile;

    /**
     * 申请类型（1：仅退款，2：退款退货）
     */
    private Byte type;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}