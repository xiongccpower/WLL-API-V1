package com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 商品把控配置表
 * </p>
 *
 * @author czr
 * @since 2021-07-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ProductControl extends Model<ProductControl> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 品牌名称
     */
    private String brandName;

    /**
     * 状态  0：关闭 1：打开 
     */
    private Integer states;

    /**
     * 出现的具体问题
     */
    private String problem;

    /**
     * 型号
     */
    private String model;

    /**
     * 商品名称
     */
    private String productName;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
