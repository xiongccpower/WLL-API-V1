package com.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity  
@Table(name="MyBooldOxData")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class MyBooldOxData {

    @Id  
    @GeneratedValue(strategy=GenerationType.AUTO)  
    @Column(name="id",unique=true)    
	private Integer id;
    
	@Column(name="userId")  private Integer userId;
	@Column(name="content")  private String content;//血氧
	@Column(name="type",columnDefinition="varchar(50) default '血氧'")  private String type;//血氧
    @Column(name="relation")  private String relation;//家庭关系
    @Column(name="dataCome")  private String dataCome;//家庭关系
	@Column(name="createTime")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date createTime;//下单时间

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getDataCome() {
        return dataCome;
    }

    public void setDataCome(String dataCome) {
        this.dataCome = dataCome;
    }

    public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
