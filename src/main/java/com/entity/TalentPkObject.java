package com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *  用户pk对象表
 * </p>
 *
 * @author czr
 * @since 2021-09-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TalentPkObject extends Model<TalentPkObject> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 发起者的openid
     */
    private String launchOpenid;

    /**
     * 接收者openid
     */
    private String receiveOpenid;

    /**
     * 发起pk时间
     */
    private String launchTime;

    /**
     * 接收pk时间
     */
    private String receiveTime;

    /**
     * 状态 1：发起pk 2：拒绝pk 3：接受pk
     */
    private Integer state;
    /**
     * 时间
     */
    private String time;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
