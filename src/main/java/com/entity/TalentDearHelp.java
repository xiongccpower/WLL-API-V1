package com.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 达人分享亲友助力表
 * </p>
 *
 * @author czr
 * @since 2021-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TalentDearHelp extends Model<TalentDearHelp> {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 达人openid
     */
    private String talentOpenid;

    /**
     * 助力亲友openid
     */
    private String helpOpenid;

    /**
     * 创建时间
     */
    private String createTime;
    /**
     *助力时间
     *
     */
    private String helpTime;
    /**
     * 状态1：助力中 2：取消助力
     */
    private Integer state;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
