package com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author czr
 * @since 2021-06-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WitShoppingUnionDeploy extends Model<WitShoppingUnionDeploy> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * key
     */
    private String key;

    /**
     * 秘钥
     */
    private String secret;

    /**
     * 调用接口url
     */
    private String interfaceUrl;

    /**
     * 调用接口名称
     */
    private String interfaceName;

    /**
     * 渠道
     */
    private String channel;

    /**
     * 渠道名称
     */
    private String channelName;
    /**
     * 占比
     */
    private Double proportion;
    /**
     * 状态 1：打开  2：关闭
     */
    private Integer states;
    /**
     * 来源渠道：sapplets:小程序 APP：app web:网页
     */
    private String sourceChannel;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
