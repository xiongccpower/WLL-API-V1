package com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author czr
 * @since 2021-08-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WitGoodsFunction extends Model<WitGoodsFunction> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品类型
     */
    private String goodsType;

    /**
     * 商品新功能
     */
    private String function;

    /**
     * 状态 0禁用 1启用
     */
    private Integer states;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
