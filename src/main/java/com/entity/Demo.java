package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity  
@Table(name="demo") 
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class Demo {

    @Id  
    @GeneratedValue(strategy=GenerationType.AUTO)  
    @Column(name="id",unique=true)    
	private Integer id;
    
//	@Column(name="one",columnDefinition="INT default 1")  private Integer income=1;
	@Column(name="oneA")  private Integer oneA;
//	@Column(name="two",columnDefinition="varchar(2) default 'two'")  private String isPicture="two";
	@Column(name="twoA")  private String twoA;
	@Transient private Integer teamTotal;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getOneA() {
		return oneA;
	}
	public void setOneA(Integer oneA) {
		this.oneA = oneA;
	}
	public String getTwoA() {
		return twoA;
	}
	public void setTwoA(String twoA) {
		this.twoA = twoA;
	}
	public Integer getTeamTotal() {
		return teamTotal;
	}
	public void setTeamTotal(Integer teamTotal) {
		this.teamTotal = teamTotal;
	}
	
	
}
