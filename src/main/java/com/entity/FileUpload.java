package com.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="FileUpload")
public class FileUpload {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id",unique=true)
    private Integer id;

    @Column(name="newVision")  private String newVision; //.最新版本号
    @Column(name="minVersion")  private String minVersion; //最小支持版本号
    @Column(name="apkUrl")  private String apkUrl;//apk下载url
    @Column(name="isUpdate")  private String isUpdate; //是否有更新
    @Column(name="updateDescription")  private String updateDescription; //更新文案
    @Column(name="forceUpdate")  private String forceUpdate; //是否强制更新
    @Column(name="md5")  private String md5; //apk的文件MD5值
    @Column(name="apkSize")  private String apkSize; //apk文件大小

    @Column(name="createTime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;//创建时间

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNewVision() {
        return newVision;
    }

    public void setNewVision(String newVision) {
        this.newVision = newVision;
    }

    public String getMinVersion() {
        return minVersion;
    }

    public void setMinVersion(String minVersion) {
        this.minVersion = minVersion;
    }

    public String getApkUrl() {
        return apkUrl;
    }

    public void setApkUrl(String apkUrl) {
        this.apkUrl = apkUrl;
    }

    public String getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(String isUpdate) {
        this.isUpdate = isUpdate;
    }

    public String getForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(String forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getApkSize() {
        return apkSize;
    }

    public void setApkSize(String apkSize) {
        this.apkSize = apkSize;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateDescription() {
        return updateDescription;
    }

    public void setUpdateDescription(String updateDescription) {
        this.updateDescription = updateDescription;
    }
}
