package com.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Transient;

@Entity  
@Table(name="MyHome") 
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class MyHome {

    @Id  
    @GeneratedValue(strategy=GenerationType.AUTO)  
    @Column(name="id",unique=true)    
	private Integer id;
    
	@Column(name="relation")  private String relation;//关系
	@Column(name="sex")  private String sex;
	@Column(name="height")  private String height;
	@Column(name="weight")  private String weight;
	@Column(name="nickname") private String nickname;//昵称
	@Column(name="tel") private String tel;//账号电话
	@Column(name="mainTel") private String mainTel;//主账号
	@Column(name="birth") private String birth;//出生年月
	@Column(name="checkCode") private String checkCode;//判断是否同一手机号录入

	@Transient
	private String uid;//用户统一id
	@TableField(exist = false)
	private String like;//喜好


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getMainTel() {
		return mainTel;
	}

	public void setMainTel(String mainTel) {
		this.mainTel = mainTel;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getLike() {
		return like;
	}

	public void setLike(String like) {
		this.like = like;
	}
}
