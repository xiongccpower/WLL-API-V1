package com.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 达人分享用户预览记录表
 * </p>
 *
 * @author czr
 * @since 2021-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TalentShareRecordTwo extends Model<TalentShareRecordTwo> {

    private static final long serialVersionUID = 1L;


    /**
     * 发起者的openid
     */
    private String launchOpenid;

    /**
     * 接收者openid
     */
    private String receiveOpenid;

    /**
     * 是否是助力 0：达人分享 1：亲友助力
     */
    private Integer datyNumA;
    private Integer datyNumB;

    /**
     * 预览时间
     */
    private String previewTime;






}
