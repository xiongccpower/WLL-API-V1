package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@Entity  
@Table(name="MyDate") 
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class MyData {

    @Id  
    @GeneratedValue(strategy=GenerationType.AUTO)  
    @Column(name="id",unique=true)    
	private Integer id;
    
	@Column(name="userId")  private Integer userId;
	@Column(name="height")  private String height;//身高
	@Column(name="weight")  private String weight;//体重
	@Column(name="highPer")  private String highPer;//高压
	@Column(name="lowPer")  private String lowPer;//低压
	@Column(name="booldHear")  private String booldHear;//血脂
	@Column(name="booldOx")  private String booldOx;//血氧
	@Column(name="booldSweet")  private String booldSweet;//血压
	@Column(name="heartTimes")  private String heartTimes;//心率
	@Column(name="createTime")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date createTime;//下单时间

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getHighPer() {
		return highPer;
	}

	public void setHighPer(String highPer) {
		this.highPer = highPer;
	}



	public String getLowPer() {
		return lowPer;
	}

	public void setLowPer(String lowPer) {
		this.lowPer = lowPer;
	}

	public String getBooldHear() {
		return booldHear;
	}

	public void setBooldHear(String booldHear) {
		this.booldHear = booldHear;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getBooldOx() {
		return booldOx;
	}

	public void setBooldOx(String booldOx) {
		this.booldOx = booldOx;
	}

	public String getBooldSweet() {
		return booldSweet;
	}

	public void setBooldSweet(String booldSweet) {
		this.booldSweet = booldSweet;
	}

	public String getHeartTimes() {
		return heartTimes;
	}

	public void setHeartTimes(String heartTimes) {
		this.heartTimes = heartTimes;
	}
}
