package com.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import net.bytebuddy.implementation.bind.annotation.Default;

import javax.persistence.*;
import java.util.Date;

@Entity  
@Table(name="HomeService")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class HomeService {

    @Id  
    @GeneratedValue(strategy=GenerationType.AUTO)  
    @Column(name="id",unique=true)    
	private Integer id;
    
	@Column(name="serviceTel")  private String serviceTel; //用户手机号
	@Column(name="tel")  private String tel; //家政手机号
	@Column(name="serviceFwTel")  private String serviceFwTel; //师傅手机号
	@Column(name="serviceJzName") private String serviceJzName; //家政名称
	@Column(name="serviceFwName") private String serviceFwName; //商户名称
	@Column(name="serviceAddress") private String serviceAddress; //上门地址
	@Column(name="serviceTime") private String serviceTime; //预约上门时间 yyyy-MM-dd ss ：mm
	@Column(name="serviceState") private String serviceState; //订单状态
	@Column(name="serviceRemake") private String serviceRemake; //订单备注
	@Column(name="serviceType") private String serviceType; //订单类型
	@Column(name="servicePrice")
	private String servicePrice = "80"; //订单低价


	@JsonFormat(pattern="yyyy-MM-dd HH:mm",timezone = "GMT+8")
	private Date createTime;//下单时间



	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getServiceTel() {
		return serviceTel;
	}

	public void setServiceTel(String serviceTel) {
		this.serviceTel = serviceTel;
	}

	public String getServiceAddress() {
		return serviceAddress;
	}

	public void setServiceAddress(String serviceAddress) {
		this.serviceAddress = serviceAddress;
	}

	public String getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(String serviceTime) {
		this.serviceTime = serviceTime;
	}


	public String getServiceState() {
		return serviceState;
	}

	public void setServiceState(String serviceState) {
		this.serviceState = serviceState;
	}

	public String getServiceRemake() {
		return serviceRemake;
	}

	public void setServiceRemake(String serviceRemake) {
		this.serviceRemake = serviceRemake;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getServiceJzName() {
		return serviceJzName;
	}

	public void setServiceJzName(String serviceJzName) {
		this.serviceJzName = serviceJzName;
	}

	public String getServiceFwName() {
		return serviceFwName;
	}

	public void setServiceFwName(String serviceFwName) {
		this.serviceFwName = serviceFwName;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getServiceFwTel() {
		return serviceFwTel;
	}

	public void setServiceFwTel(String serviceFwTel) {
		this.serviceFwTel = serviceFwTel;
	}

	public String getServicePrice() {
		return servicePrice;
	}

	public void setServicePrice(String servicePrice) {
		this.servicePrice = servicePrice;
	}
}

