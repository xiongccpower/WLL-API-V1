package com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author czr
 * @since 2021-08-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ProductGuide extends Model<ProductGuide> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品类型
     */
    private String productType;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 关联数据集合
     */
    private String tableMap;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
