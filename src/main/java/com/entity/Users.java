package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;

@Entity  
@Table(name="users") 
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class Users {

    @Id  
    @GeneratedValue(strategy=GenerationType.AUTO)  
    @Column(name="id",unique=true)    
	private Integer id;
    
    @Column(name="inviteId")  private Integer inviteId;//邀请人Id
    
	@Column(name="name")  private String name;
	@Column(name="phone")  private String phone;
	@Column(name="openId")  private String openId;
	
	@Column(name="address")  private String address;
	@Column(name="sex")  private String sex;
	@Column(name="headPortrait")  private String headPortrait;
	
	@Column(name="mj_id")  private String mj_id;
	@Column(name="mj_pwd")  private String mj_pwd;
	
	@ApiModelProperty(value = "是否购买系统")
	@Column(name="isBuySystem",columnDefinition="varchar(2) default 'N'")  
	private String isBuySystem="N";
	
	@ApiModelProperty(value = "是否购买商品")
	@Column(name="isBuyGoods",columnDefinition="varchar(2) default 'N'")  
	private String isBuyGoods="N";


	@Column(name="wifi_pwd")  private String wifiPwd;



	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getIsBuySystem() {
		return isBuySystem;
	}
	public void setIsBuySystem(String isBuySystem) {
		this.isBuySystem = isBuySystem;
	}
	public String getIsBuyGoods() {
		return isBuyGoods;
	}
	public void setIsBuyGoods(String isBuyGoods) {
		this.isBuyGoods = isBuyGoods;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getHeadPortrait() {
		return headPortrait;
	}
	public void setHeadPortrait(String headPortrait) {
		this.headPortrait = headPortrait;
	}
	public String getMj_id() {
		return mj_id;
	}
	public void setMj_id(String mj_id) {
		this.mj_id = mj_id;
	}
	public String getMj_pwd() {
		return mj_pwd;
	}
	public void setMj_pwd(String mj_pwd) {
		this.mj_pwd = mj_pwd;
	}
	public Integer getInviteId() {
		return inviteId;
	}
	public void setInviteId(Integer inviteId) {
		this.inviteId = inviteId;
	}
	public String getWifiPwd() {
		return wifiPwd;
	}
	public void setWifiPwd(String wifiPwd) {
		this.wifiPwd = wifiPwd;
	}
}
