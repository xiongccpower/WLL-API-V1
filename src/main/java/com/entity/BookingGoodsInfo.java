package com.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity  
@Table(name="BookingGoodsInfo")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class BookingGoodsInfo {

    @Id  
    @GeneratedValue(strategy=GenerationType.AUTO)  
    @Column(name="id",unique=true)    
	private Integer id;

	@Column(name="goodsName")  private String goodsName; //商品名称
	@Column(name="goodsBand") private String goodsBand; //意向品牌

	@Column(name="endTime")
	private String endTime;//截止时间

	@Column(name="goodsRealtion") private String goodsRealtion; //为那个家庭成员购买

	@Column(name="isCheck") private String isCheck; //是否需要管家通知
	@Column(name="channel") private String channel; //来源渠道：applets:小程序 APP：app web:网页
	@Column(name="openid") private String openId; //用户id

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsBand() {
		return goodsBand;
	}

	public void setGoodsBand(String goodsBand) {
		this.goodsBand = goodsBand;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getGoodsRealtion() {
		return goodsRealtion;
	}

	public void setGoodsRealtion(String goodsRealtion) {
		this.goodsRealtion = goodsRealtion;
	}

	public String getIsCheck() {
		return isCheck;
	}

	public void setIsCheck(String isCheck) {
		this.isCheck = isCheck;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}
}
