package com.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="LifeSceneInfo")
public class LifeSceneInfo {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id",unique=true)
    private Integer id;

    @Column(name="sceneCode")  private String sceneCode;//场景编号
    @Column(name="sceneMsg")  private String sceneMsg; //场景概括
    @Column(name="sceneInfo")  private String sceneInfo; //场景文案
    @Column(name="state")  private String state; //状态 0已查看 1.未查看
    @Column(name="createTime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;//创建时间

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getSceneCode() {
        return sceneCode;
    }

    public void setSceneCode(String sceneCode) {
        this.sceneCode = sceneCode;
    }

    public String getSceneMsg() {
        return sceneMsg;
    }

    public void setSceneMsg(String sceneMsg) {
        this.sceneMsg = sceneMsg;
    }

    public String getSceneInfo() {
        return sceneInfo;
    }

    public void setSceneInfo(String sceneInfo) {
        this.sceneInfo = sceneInfo;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
