package com.entity;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 商品表
 * @author Administrator
 *
 */
@Entity  
@Table(name="goods") 
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class Goods{


	@Id  
    @GeneratedValue(strategy=GenerationType.AUTO)  
    @Column(name="id",unique=true)    
	private Integer id;
    
    @ElementCollection
    @CollectionTable(name="pic_url", joinColumns=@JoinColumn(name="id"))
    @Column(name="pic_url")
    private List<String> picUrl;//①产品展示图片
    
	@Column(name="price")  private double price;//①价格：售价+零售价+购买人数 //原价
	@Column(name="yhPrice") private double yhPrice;//优惠价格 现价
	@Column(name="title")  private String title;//②产品主标题：产品名称
	@Column(name="subtitle")  private String subtitle;// ③产品副标题：产品卖点关键词
	@Column(name="activity")  private String activity;//	④促销活动   ---
	@Column(name="service")  private String service;//⑤服务：是否包邮/7天无理由等特色服务   
	@Column(name="number")  private String number;//⑥已经购买的数量
	@Column(name="freight")   private double freight=0;//运费
	
	@Column(name="summary")  private String summary;//③产品概述：商品介绍
	@Column(name="parameter")  private String parameter;//④产品参数：商品规格与包装
	@Column(name="comment")  private String comment;//⑤评论：商品售后评论
	@Column(name="operation")  private String operation;//操作的内容
	@Column(name="instructions")  private String instructions;//使用说明
	@Column(name="state") private String state;//状态 1在售 2下架

	private double total;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<String> getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(List<String> picUrl) {
		this.picUrl = picUrl;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public double getFreight() {
		return freight;
	}

	public void setFreight(double freight) {
		this.freight = freight;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public double getYhPrice() {
		return yhPrice;
	}

	public void setYhPrice(double yhPrice) {
		this.yhPrice = yhPrice;
	}
}
