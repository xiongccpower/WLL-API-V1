package com.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name="MjGoodsState")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class MjGoodsState {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id",unique=true)
    private Integer id;

    @Column(name="title",columnDefinition = "varchar(500)")
    private String title;//标题

    @Column(name="goodsMsg",columnDefinition = "varchar(500)")
    private String goodsMsg;//通知内容
    @Column(name="state")
    private String state;//是否显示

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGoodsMsg() {
        return goodsMsg;
    }

    public void setGoodsMsg(String goodsMsg) {
        this.goodsMsg = goodsMsg;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
