package com.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author czr
 * @since 2021-07-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WitBookingDiscount extends Model<WitBookingDiscount> {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 预约商品名称
     */
    private String productName;

    /**
     * 智能管家设备id
     */
    private String butlerId;

    /**
     * 用户手机号
     */
    private String phone;

    /**
     * 预约开始时间
     */
    private String startTime;

    /**
     * 预约结束时间
     */
    private String endTime;

    /**
     * 创建时间
     */
    private String createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
