package com.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="HealthContent")
public class HealthContent {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id",unique=true)
    private Integer id;

    @Column(name="tel")  private String tel;  //账号
    @Column(name="healthCode")  private String healthCode;  //小病的代码
    @Column(name="healthContent")  private String healthContent; //小病的病症

    @Column(name="healthDay")  private String healthDay; //诊断时间
    @Column(name="healthDc")  private String healthDc; //诊断医生
    @Column(name="healthHos")  private String healthHos; //诊断医院

    @Column(name="createTime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;//创建时间

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getHealthCode() {
        return healthCode;
    }

    public void setHealthCode(String healthCode) {
        this.healthCode = healthCode;
    }

    public String getHealthContent() {
        return healthContent;
    }

    public void setHealthContent(String healthContent) {
        this.healthContent = healthContent;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getHealthDay() {
        return healthDay;
    }

    public void setHealthDay(String healthDay) {
        this.healthDay = healthDay;
    }

    public String getHealthDc() {
        return healthDc;
    }

    public void setHealthDc(String healthDc) {
        this.healthDc = healthDc;
    }

    public String getHealthHos() {
        return healthHos;
    }

    public void setHealthHos(String healthHos) {
        this.healthHos = healthHos;
    }
}
