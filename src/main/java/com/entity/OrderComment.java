package com.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName mj_order_comment
 */
@TableName(value ="mj_order_comment")
@Data
public class OrderComment implements Serializable {
    /**
     * 
     */
    @TableId
    private Integer mocId;

    /**
     * 评论详情
     */
    private String content;

    /**
     * 商品评论
     */
    private Byte goods;

    /**
     * 物流评论
     */
    private Byte speed;

    /**
     * 商家服务评论
     */
    private Byte service;

    /**
     * 订单ID
     */
    private Integer orderId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 评论图片
     */
    private String images;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}