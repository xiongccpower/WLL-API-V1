package com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author czr
 * @since 2021-08-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ButlerDataTransfer extends Model<ButlerDataTransfer> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 用户id
     */
    private String uid;

    /**
     * 0：备份 1：恢复
     */
    private Integer states;

    /**
     * 备份时间
     */
    private String backupsTime;

    /**
     * 恢复时间
     */
    private String recoveryTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
