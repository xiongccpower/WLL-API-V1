package com.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="SaleWordsInfo")
public class SaleWordsInfo {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id",unique=true)
    private Integer id;

    @Column(name="goodsKinds")  private String wordsKinds; //商品的品类 例如 ：手机
    @Column(name="goodsWords")  private String goodsWords; //商品的关键字 例如：苹果

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getWordsKinds() {
        return wordsKinds;
    }

    public void setWordsKinds(String wordsKinds) {
        this.wordsKinds = wordsKinds;
    }

    public String getGoodsWords() {
        return goodsWords;
    }

    public void setGoodsWords(String goodsWords) {
        this.goodsWords = goodsWords;
    }
}
