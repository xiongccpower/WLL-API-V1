package com.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author czr
 * @since 2021-08-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WitGoodsBrandLexicon extends Model<WitGoodsBrandLexicon> {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 商品词库id
     */
    private Integer goodsId;

    /**
     * 品牌名称
     */
    private String brandName;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
