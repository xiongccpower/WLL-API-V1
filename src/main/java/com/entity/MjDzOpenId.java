package com.entity;

import javax.persistence.*;

@Entity
@Table(name="MjDzOpenId")
public class MjDzOpenId {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id",unique=true)
    private Integer id;

    @Column(name="openId")  private String openId;//
    @Column(name="dzOpenId") private String dzOpenId; //点赞人openId
    @Column(name="uid") private String uid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getDzOpenId() {
        return dzOpenId;
    }

    public void setDzOpenId(String dzOpenId) {
        this.dzOpenId = dzOpenId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}