package com.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *  达人分享规则表
 * </p>
 *
 * @author czr
 * @since 2021-09-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TalentShareRule extends Model<TalentShareRule> {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 达人分享规则
     */
    private String shareRule;

    /**
     * 奖品
     */
    private String prize;

    /**
     * 状态0：禁止 1：启动
     */
    private Integer state;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
