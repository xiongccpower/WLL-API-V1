package com.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity  
@Table(name="MyLikeData")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class MyLikeData {

    @Id  
    @GeneratedValue(strategy=GenerationType.AUTO)  
    @Column(name="id",unique=true)    
	private Integer id;
    
	@Column(name="tel")  private Integer tel;
	@Column(name="likeBig")  private String likeBig;//喜好大类
	@Column(name="likeSmall")  private String likeSmall;//喜好小类

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTel() {
		return tel;
	}

	public void setTel(Integer tel) {
		this.tel = tel;
	}

	public String getLikeBig() {
		return likeBig;
	}

	public void setLikeBig(String likeBig) {
		this.likeBig = likeBig;
	}

	public String getLikeSmall() {
		return likeSmall;
	}

	public void setLikeSmall(String likeSmall) {
		this.likeSmall = likeSmall;
	}
}
