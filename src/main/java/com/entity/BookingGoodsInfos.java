package com.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 预约优惠表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("booking_goods_info")
public class BookingGoodsInfos  extends Model<BookingGoodsInfos> {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 截止时间
     */
    private LocalDateTime endTime;

    /**
     * 意向品牌
     */
    private String goodsBand;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 为那个家庭成员购买
     */
    private String goodsRealtion;

    /**
     * 是否需要管家通知
     */
    private String isCheck;
    /**
     * 来源渠道：applets:小程序 APP：app web:网页
     */
    private String channel;
    /**
     * 用户id
     */
    private String openid;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}