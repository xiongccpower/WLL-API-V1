package com.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "mjOrder")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true)
    private Integer id;

    @Column(name = "userId")
    private Integer userId;
    @Column(name = "total")
    private double total;//计算总价
    @Column(name = "payTotal")
    private double payTotal;//折扣总价 、 最终支付价格
    @Column(name = "coupon")
    private Integer coupon;//优惠券后的价格价格

    @Column(name = "address")
    private String address;//收货地址
    @Column(name = "phone")
    private String phone;//收货人手机号
    @Column(name = "addressee")
    private String addressee;//收货人姓名

    @Column(name = "invoice")
    private String invoice;//是否需要开票

    @Column(name = "evaluate")
    private String evaluate;//评价
    @Column(name = "freight", columnDefinition = "INT default 0")
    private Integer freight = 0;//运费

    @Column(name = "expressNo")
    private String expressNo;//快递单号
    @Column(name = "orderNo")
    private String orderNo;//订单号
    @Column(name = "returnExchange")
    private String returnExchange;//退换货内容
    @Column(name = "scIds")
    private String scIds;//购物车ids
    @Column(name = "expressNum")
    private String expressNum;
    @Column(name = "uid")
    private String uid;
    @Column(name = "openId")
    private String openId;
    @Column(name = "goodsImg")
    private String goodsImg;//商品图片
    @Column(name = "transactionId")
    private String transactionId;//微信订单号
    @Column(name = "storeName")
    private String storeName;//店名称
    @Column(name = "unitPrice")
    private String unitPrice;//单价

    @Column(name = "code")
    private String code;

    @Column(name = "goodsIds", columnDefinition = "varchar(500)")
    private String goodsIds;//商品ids //2020.11.11
    @Column(name = "goodsName", columnDefinition = "varchar(500)")
    private String goodsName;//商品名称 //2020.11.11

    @ApiModelProperty(value = "订单状态 1已支付/待发货 2待收货  3待评价  4完成 5.退换货 6申请退款 7 拒绝退款 8退款成功 9待支付 10已取消")
    @Column(name = "state")
    private Integer state;

    /**
     * 如果state 为 1/2时
     * 该值为空或者-1时 为正常状态
     * 该值为0是表示 退款中
     * 该值为1是表示 拒绝退款
     * 该值为2是表示 已退款
     * 该值为11是表示 退换中
     * 该值为12是表示 拒绝退换
     * 该值为13是表示 已退换
     * <p>
     * 附加状态
     */
    @Column(name = "addState")
    @Getter
    @Setter
    private Integer addState;

    /**
     * 对应日志ID
     */
    @Column(name = "suId")
    @Getter
    @Setter
    private Long suId;

    @Column(name = "createTime")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createTime;//下单时间


    // delivery time
    @Column(name = "deliveryTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deliveryTime;//发货时间

    @Column(name = "extra")
    private String extra;

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "inventory")
    private List<Inventory> inventory;//商品详情

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getPayTotal() {
        return payTotal;
    }

    public void setPayTotal(double payTotal) {
        this.payTotal = payTotal;
    }

    public Integer getCoupon() {
        return coupon;
    }

    public void setCoupon(Integer coupon) {
        this.coupon = coupon;
    }

    public List<Inventory> getInventory() {
        return inventory;
    }

    public void setInventory(List<Inventory> inventory) {
        this.inventory = inventory;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddressee() {
        return addressee;
    }

    public void setAddressee(String addressee) {
        this.addressee = addressee;
    }

    public String getExpressNo() {
        return expressNo;
    }

    public void setExpressNo(String expressNo) {
        this.expressNo = expressNo;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(String evaluate) {
        this.evaluate = evaluate;
    }

    public Integer getFreight() {
        return freight;
    }

    public void setFreight(Integer freight) {
        this.freight = freight;
    }

    public String getReturnExchange() {
        return returnExchange;
    }

    public void setReturnExchange(String returnExchange) {
        this.returnExchange = returnExchange;
    }

    public String getScIds() {
        return scIds;
    }

    public void setScIds(String scIds) {
        this.scIds = scIds;
    }

    public String getGoodsIds() {
        return goodsIds;
    }

    public void setGoodsIds(String goodsIds) {
        this.goodsIds = goodsIds;
    }

    public String getExpressNum() {
        return expressNum;
    }

    public void setExpressNum(String expressNum) {
        this.expressNum = expressNum;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }
}
