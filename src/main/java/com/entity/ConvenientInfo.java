package com.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="ConvenientInfo")
public class ConvenientInfo {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id",unique=true)
    private Integer id;

    @Column(name="serviceType")  private String serviceType; //维修类型
    @Column(name="serviceArea")  private String serviceArea; //维修区域
    @Column(name="serviceWidth")  private String serviceWidth; //维修范围
    @Column(name="serviceLong")  private String serviceLong;//维修时长
    @Column(name="state")  private String state; //是否过期
    @Column(name="startTime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date startTime;//开始时间


    @Column(name="endTime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;//结束时间


    @Column(name="createTime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;//结束时间


    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getServiceArea() {
        return serviceArea;
    }

    public void setServiceArea(String serviceArea) {
        this.serviceArea = serviceArea;
    }

    public String getServiceWidth() {
        return serviceWidth;
    }

    public void setServiceWidth(String serviceWidth) {
        this.serviceWidth = serviceWidth;
    }

    public String getServiceLong() {
        return serviceLong;
    }

    public void setServiceLong(String serviceLong) {
        this.serviceLong = serviceLong;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
