package com.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;

/**
 * <p>
 * 达人分享用户预览记录表
 * </p>
 *
 * @author czr
 * @since 2021-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TalentShareRecord extends Model<TalentShareRecord> {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 达人openid
     */
    private String talentOpenid;

    private String launch;

    private String receive;

    /**
     * 预览者openid
     */
    private String previewOpenid;
    /**
     * 助力好友openid
     */
    private String helpOpenid;

    /**
     * 是否是助力 0：达人分享 1：亲友助力
     */
    private Integer type;

    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 预览时间
     */
    private String previewTime;
    /**
     * 达人门店名称
     */
    private String nickname;
    /**
     * 预览月份
     */
    private String previewMonth;
    /**
     * 头像
     */
    private String head;
    /**
     * 达人uid
     */
    private Integer talentUid;
    /**
     * 所属运营名称
     */
    private String operate;
    /**
     * 月统计数据
     */
    @Transient
    private String monthNum;
    /**
     * 天统计数据
     */
    @Transient
    private String datyNum;
    /**
     * pk状态
     */
    @Transient
    private String pkState;
    /**
     * 助力状态
     */
    @Transient
    private String zlState;
    /**
     * pk方甲
     */
    @Transient
    private String pkSquareA;
    /**
     * pk方乙
     */
    @Transient
    private String pkSquareB;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
