package com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author czr
 * @since 2021-08-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MjUser extends Model<MjUser> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 姓名
     */
    private String username;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 头像
     */
    private String head;

    /**
     * 行业
     */
    private String trade;

    /**
     * 电话
     */
    private String tel;

    /**
     * 判断是否为运营人员(1:是；0：否)
     */
    private Integer iskeeper;

    /**
     * 密码
     */
    private String pwd;

    /**
     * 手势密码
     */
    private String signpwd;

    /**
     * 设备号
     */
    private String equipmentid;

    private Integer shopid;

    /**
     * 是否为商户（0：业主；1商户；2绿植鲜花）
     */
    private Integer state;

    /**
     * 岗位
     */
    private String quarters;

    /**
     * 推送设备号
     */
    private String cid;

    /**
     * 修改时间
     */
    private Integer updatetime;

    /**
     * 创建时间
     */
    private Integer createtime;

    /**
     * 最后使用时间
     */
    private Integer lasttime;

    /**
     * 归属运营id
     */
    private Long sysUserId;
    /**
     * 主账号id
     */
    private Integer mainNum;
    /**
     * 运营名称
     */
    @TableField(exist = false)
    private String operateName;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
