package com.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author czr
 * @since 2021-08-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SuggestionInfo extends Model<SuggestionInfo> {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品id
     */
    private Integer productId;

    /**
     * 指标类型
     */
    private String indexType;

    /**
     * 指标
     */
    private String index;

    /**
     * 建议
     */
    private String proposal;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
