package com.entity;


import javax.persistence.*;

@Entity
@Table(name="MjSystemInfo")
public class MjSystemInfo {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id",unique=true)
    private Integer id;

    @Column(name="content")  private String content;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
