package com.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name="MjDictEntry")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class MjDictEntry {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id",unique=true)
    private Integer id;   //字典主键

    @Column(name="dictName")  private String dictName;  //字典名称
    @Column(name="dictValue")  private String dictValue;//字典值
    @Column(name="deOrder")  private String deOrder;//字典顺序

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

    public String getDeOrder() {
        return deOrder;
    }

    public void setDeOrder(String deOrder) {
        this.deOrder = deOrder;
    }
}
