package com.entity.union_entity;

import lombok.Data;

import java.util.List;

/**
 * 商品详情类
 */
@Data
public class GoodsDetails {
    //商品id
    private String goodsId;
    //商品名称
    private String goodsName;
    //商品描述
    private String goodsDesc;
    //商品落地页
    private String destUrl;
    //商品缩略图
    private String goodsThumbUrl;
    //商品轮播图：根据商品id查询时返回，商品列表不返回
    private List<String> goodsCarouselPictures;
    //商品详情图片：根据商品id查询商品信息时返回，商品列表不返回
    private List<String> goodsDetailPictures;
    //商品主图
    private String goodsMainPicture;
    //市场价（元）
    private String marketPrice;
    //唯品价（元）
    private String vipPrice;
    //佣金比例（%）
    private String commissionRate;
    //佣金金额（元）
    private String commission;
    //折扣:唯品价/市场价 保留两位小数字符串
    private String discount;
    //商品品牌sn
    private String brandStoreSn;
    //商品品牌名称
    private String brandName;
    //商品品牌logo全路径地址
    private String brandLogoFull;
    //店铺id
    private String storeId;
    //店铺名称
    private String storeName;
    //商品评论数
    private String comments;
    //商品好评率:百分比，不带百分号
    private String goodCommentsShare;
    //店铺评分：保留两位小数
    private String storeScore;
    //店铺同品类排名比例：例如10表示前10%
    private String storeRankRate;
    //红包信息
    private PMSCouponInfo couponInfo;
    //是否海淘商品标识：1是 0不是
    private String haiTao;
    //小程序id
    private String appid;
    //小程序地址
    private String appletUrl;
    //是否包邮0 否 1 是
    private String noPostage;

}
