package com.entity.union_entity;

import lombok.Data;

/**
 * 商品红包券类
 */
@Data
public class PMSCouponInfo {
    //优惠券批次号
    private String couponNo;
    //优惠劵名称
    private String couponName;
    //使用门槛
    private String buy;
    //优惠金额
    private String fav;
    //券激活开始时间,毫秒级时间戳
    private String activateBeginTime;
    //券激活结束时间,毫秒级时间戳
    private String activateEndTime;
    //使用开始时间，毫秒级时间戳
    private String useBeginTime;
    //使用结束时间，毫秒级时间戳
    private String useEndTime;
    //生成劵的总量
    private String totalAmount;
    //劵已激活的数量
    private String activedAmount;
    //券类型,(1:买赠 2:满减 3:折扣 4:免邮 5:多减多减)
    private String couponType;


}
