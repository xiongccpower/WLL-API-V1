package com.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author czr
 * @since 2021-07-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("my_home")
public class MyHomes extends Model<MyHomes> {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String birth;

    private String height;

    private String mainTel;

    private String nickname;

    private String relation;

    private String sex;

    private String tel;

    private String weight;

    private String checkCode;

    private String love;

    private Integer userId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
