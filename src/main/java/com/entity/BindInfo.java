package com.entity;

import javax.persistence.*;

@Entity
@Table(name="bindInfo")
public class BindInfo {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id",unique=true)
    private Integer id;

    @Column(name="content")  private String content;
    @Column(name="nationOrder") private String nationOrder;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNationOrder() {
        return nationOrder;
    }

    public void setNationOrder(String nationOrder) {
        this.nationOrder = nationOrder;
    }
}
