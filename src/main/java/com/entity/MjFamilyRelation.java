package com.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author czr
 * @since 2021-07-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MjFamilyRelation extends Model<MjFamilyRelation> {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 关系
     */
    private String relation;

    /**
     * 性别
     */
    private String  sex;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 状态
     */
    private Integer states;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
