package com.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 家庭清单表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("my_home_sale_data")
public class MyHomeSaleDatas extends Model<MyHomeSaleDatas> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
     * 预约优惠表id
     */
    private Integer subscribeId;

    /**
     * 用户微信id
     */
    private String openId;

    /**
     * 图片url
     */
    private String picUrl;

    /**
     * 价格
     */
    private String price;

    /**
     * 产品发布日期
     */
    private LocalDateTime productDate;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 电话
     */
    private String tel;

    /**
     * 小程序地址
     */
    private String wxProductUrl;

    /**
     * 小程序appid
     */
    private String appId;

    /**
     * 商品编码
     */
    private String productCode;

    /**
     * 是否预约特惠
     */
    private String isBooking;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 商品渠道 sn:苏宁 wph:唯品会 klzk:考拉赚客 yqf:亿起发
     */
    private String productChannel;
    /**
     * 移动端商品链接
     */
    private String mobileProductUrl;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
