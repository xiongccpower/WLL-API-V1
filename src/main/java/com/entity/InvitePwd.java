package com.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "InvitePwd")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class InvitePwd {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true)
    private Integer id;

    @Column(name = "openId")
    private String openId;

    @Column(name = "phone")
    private String phone;

    @Column(name = "uid")
    private String uid;

    @Column(name = "invitePwd")
    private String invitePwd;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInvitePwd() {
        return invitePwd;
    }

    public void setInvitePwd(String invitePwd) {
        this.invitePwd = invitePwd;
    }
}
