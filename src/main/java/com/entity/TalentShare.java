package com.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *达人分享表
 * </p>
 *
 * @author czr
 * @since 2021-09-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TalentShare extends Model<TalentShare> {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 达人openid
     */
    private String talentOpenid;

    /**
     * 助力亲友openid
     */
    private String helpOpenid;

    /**
     * 分享时间
     */
    private String createTime;

    /**
     * 总票数
     */
    private Integer totalNum;
    /**
     * 分享月份
     */
    private String shareMonth;
    /**
     * 排名
     */
    private String rowno;
    /**
     *达人门店名称
     */
    private String nickname;
    /**
     *头像
     */
    private String head;
    /**
     *所属运营名称
     */
    private String operate;
    /**平台Id
     */
    private int uid;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
