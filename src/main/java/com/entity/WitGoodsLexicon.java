package com.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author czr
 * @since 2021-08-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WitGoodsLexicon extends Model<WitGoodsLexicon> {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 商品类别
     */
    private String goodsType;

    /**
     * 商品名称
     */
    private String goodsName;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
