package com.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity  
@Table(name="MyHomeSaleData")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class MyHomeSaleData { //家庭清单

    @Id  
    @GeneratedValue(strategy=GenerationType.AUTO)  
    @Column(name="id",unique=true)    
	private Integer id;

	@Column(name="openId")  private String openId;
	@Column(name="tel")  private String tel;
	@Column(name="productName")  private String productName;
	@Column(name="picUrl")  private String picUrl;
	@Column(name="wxProductUrl")  private String wxProductUrl;
	@Column(name="appId")  private String appId;
	@Column(name="price") private String price;
	@Column(name="productCode") private String productCode;
	@Column(name="isBooking") private String isBooking;  //是否预约特惠
	@Column(name="productChannel") private String productChannel;  //商品来源渠道

	@Column(name="productDate")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date productDate;//添加日期

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getWxProductUrl() {
		return wxProductUrl;
	}

	public void setWxProductUrl(String wxProductUrl) {
		this.wxProductUrl = wxProductUrl;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Date getProductDate() {
		return productDate;
	}

	public void setProductDate(Date productDate) {
		this.productDate = productDate;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getIsBooking() {
		return isBooking;
	}

	public void setIsBooking(String isBooking) {
		this.isBooking = isBooking;
	}

	public String getProductChannel() {
		return productChannel;
	}

	public void setProductChannel(String productChannel) {
		this.productChannel = productChannel;
	}
}
