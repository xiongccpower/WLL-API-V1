package com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 管家生活服务指令记录表
 * </p>
 *
 * @author czr
 * @since 2021-07-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MyServiceInstructRecord extends Model<MyServiceInstructRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 服务类型 1:家政服务 2:家用维修 3:房屋维修 4:环保回收 5:居家美饰 6:家庭休闲 7:家庭教育 8:商品搜索
     */
    private Integer serviceType;

    /**
     * 服务编号
     */
    private Integer mtype;

    /**
     * 用户手机号
     */
    private String mjPhone;

    /**
     * 设备号
     */
    private String deviceCode;

    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 搜索的商品名称
     */
    private String goodsName;
    /**
     * 状态 0：未读 1：已读
     */
    private Integer starts;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
