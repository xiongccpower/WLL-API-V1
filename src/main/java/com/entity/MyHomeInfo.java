package com.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="MyHomeInfo")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class MyHomeInfo { //便民资讯

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id",unique=true)
	private Integer id;

	@Column(name="openId")  private String openId;
	@Column(name="tel")  private String tel;
	@Column(name="contentId")  private String contentId; //UUid
	@Column(name="content")  private String content;
	@Column(name="type")  private String type; //咨询类型 0 停水 1.停电 2 其他
	@Column(name="timeLong")  private String timeLong; //时长
	@Column(name="contentType")  private String contentType; //
	@Column(name="contentArea")  private String contentArea; //维修区域
	@Column(name="uid")  private String uid; //维修区域

	@Column(name="contentDate")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm",timezone = "GMT+8")
	private Date contentDate;//开始日期

	@Column(name="state") private String state; //状态 ：0 未查看 1,已查看

	@Column(name="createTime")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")

	private Date createTime;//创建时间

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getContentDate() {
		return contentDate;
	}

	public void setContentDate(Date contentDate) {
		this.contentDate = contentDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTimeLong() {
		return timeLong;
	}

	public void setTimeLong(String timeLong) {
		this.timeLong = timeLong;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentArea() {
		return contentArea;
	}

	public void setContentArea(String contentArea) {
		this.contentArea = contentArea;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
}

