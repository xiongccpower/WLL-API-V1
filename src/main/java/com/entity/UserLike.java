package com.entity;

import javax.persistence.*;

@Entity
@Table(name="UserLike")
public class UserLike {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id",unique=true)
    private Integer id;

    @Column(name="openId")  private String openId;//
    @Column(name="likeCount") private Integer likeCount;
    @Column(name="isBuy") private Integer isBuy; //是否购买
    @Column(name="isFull") private Integer isFull; //是否点赞已满
    @Column(name="uid") private String uid; //用户统一唯一id

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getIsBuy() {
        return isBuy;
    }

    public void setIsBuy(Integer isBuy) {
        this.isBuy = isBuy;
    }

    public Integer getIsFull() {
        return isFull;
    }

    public void setIsFull(Integer isFull) {
        this.isFull = isFull;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
