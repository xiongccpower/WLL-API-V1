package com.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity  
@Table(name="shareInfo")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class ShareInfo {

    @Id  
    @GeneratedValue(strategy=GenerationType.AUTO)  
    @Column(name="id",unique=true)    
	private Integer id;
    
	@Column(name="picture")  private String picture;
	@Column(name="content")  private String content;
	@Column(name="nationOrder") private String nationOrder;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getNationOrder() {
		return nationOrder;
	}

	public void setNationOrder(String nationOrder) {
		this.nationOrder = nationOrder;
	}
}
