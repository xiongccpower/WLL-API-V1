package com.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName mj_order
 */
@TableName(value ="mj_order")
@Data
public class Order implements Serializable {
    /**
     * 
     */
    @TableId
    private Integer id;

    /** if (state === 1 || state === 2) {
             switch (addState) {
               case -1:
                 return OrderStatus[state];
               case 0:
                 return "申请退款";
               case 1:
                 return "拒绝退款";
               case 2:
                 return "已退款";
               case 11:
                 return "申请退换";
               case 12:
                 return "拒绝退换";
               case 13:
                 return "已退换";
             }
           }
     *
     */
    private Byte addState;

    /**
     * 
     */
    private String address;

    /**
     * 
     */
    private String addressee;

    /**
     * 
     */
    private Integer coupon;

    /**
     * 
     */
    private Date createTime;

    /**
     * 发货时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deliveryTime;

    /**
     * 评价
     */
    private String evaluate;

    /**
     * 快递单号
     */
    private String expressNo;

    /**
     * 
     */
    private Integer freight;

    /**
     * 
     */
    private String invoice;

    /**
     * 
     */
    private String orderNo;

    /**
     * 
     */
    private Double payTotal;

    /**
     * 
     */
    private String phone;

    /**
     * 
     */
    private String returnExchange;

    /**
     * 
     */
    private String scIds;

    /**
     *  '1': '已支付/待发货',
     *   '2': '待收货',
     *   '3': '待评价',
     *   '4': '完成',
     *   '9': '待支付',
     *   '10': '已取消'
     */
    private Integer state;

    /**
     * 
     */
    private Double total;

    /**
     * 
     */
    private Integer userId;

    /**
     * 
     */
    private String expressNum;

    /**
     * 
     */
    private String goodsIds;

    /**
     * 
     */
    private String goodsName;

    /**
     * 
     */
    private String code;

    /**
     * 
     */
    private String uid;

    /**
     * 
     */
    private String openId;

    /**
     * 微信订单号
     */
    private String transactionId;

    /**
     * 商品图片地址
     */
    private String goodsImg;

    /**
     * 店名称
     */
    private String storeName;

    /**
     * 单价
     */
    private String unitPrice;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}