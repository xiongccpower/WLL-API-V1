package com.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 运营活动表
 *
 * @TableName mj_business_event
 */
@TableName(value = "mj_business_event")
@Data
public class BusinessEvent implements Serializable {
    /**
     *
     */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long mbeId;

    /**
     * 归属运营id
     */
    private Integer uid;

    /**
     * 序号
     */
    @TableField("`order`")
    private Integer order;

    /**
     * 批次号（自动生成）
     */
    private String code;

    /**
     * 活动运营者/辅助运营人员
     */
    private String name;

    /**
     * 运营区域
     */
    private String area;

    /**
     * 小程序二维码url
     */
    private String url;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 结束时间
     */
    private LocalDateTime endTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}