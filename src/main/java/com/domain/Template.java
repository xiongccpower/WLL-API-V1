package com.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * 分享模板表
 * @TableName mj_template
 */
@TableName(value ="mj_template")
@Data
public class Template implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer mtId;

    /**
     * 模板名
     */
    private String name;

    /**
     * 模板图片路径
     */
    private String url;

    /**
     * 模板类型
     */
    private Integer type;

    /**
     * 
     */
    private LocalDateTime createTime;

    /**
     * 
     */
    private LocalDateTime updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}