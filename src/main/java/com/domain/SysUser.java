package com.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 系统用户
 * @TableName sys_user
 */
@TableName(value ="sys_user")
@Data
public class SysUser implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long userId;

    /**
     * 
     */
    private String wxImgUrl;

    /**
     * 用户名
     */
    private String name;

    /**
     * 登录账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 状态  0：禁用   1：正常
     */
    private Byte status;

    /**
     * 
     */
    private Long createUserId;

    /**
     * 
     */
    private Integer createtime;

    /**
     * 
     */
    private String email;

    /**
     * 
     */
    private String tel;

    /**
     * 运营人员的id
     */
    private Integer uid;

    /**
     * 
     */
    private Integer shopnum;

    /**
     * 
     */
    private String nameyy;

    /**
     * 
     */
    private Integer yycreatetime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}