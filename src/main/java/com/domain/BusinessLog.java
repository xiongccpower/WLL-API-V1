package com.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @TableName mj_business_log
 */
@TableName(value = "mj_business_log")
@Data
public class BusinessLog implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long mblId;

    /**
     *
     */
    private Long uid;

    /**
     *
     */
    private Long mbeId;

    /**
     *
     */
    @TableField("`order`")
    private Integer order;

    /**
     * 1:分享
     * 2:注册
     * 3:购买
     */
    private Byte type;


    private Integer muId;

    private String openId;

    /**
     * 状态：
     * 1：已录入统计
     * 0：未录入统计
     */
    private Integer status;

    /**
     *
     */
    private Integer num;

    /**
     *
     */
    private Date createTime;

    /**
     *
     */
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}