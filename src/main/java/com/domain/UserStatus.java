package com.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户状态
 * todo 后期状态可考虑用二进制方式
 *
 * @TableName mj_user_status
 */
@TableName(value = "mj_user_status")
@Data
public class UserStatus implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long musId;

    /**
     *
     */
    private String openId;

    /**
     *
     */
    private Integer uid;

    /**
     *
     */
    private Byte share;

    /**
     *
     */
    private Byte register;

    /**
     *
     */
    private Byte buy;

    /**
     * 首次购买返现flag
     */
    private Byte first;

    /**
     *
     */
    private LocalDateTime createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}