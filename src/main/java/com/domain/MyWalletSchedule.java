package com.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @TableName my_wallet_schedule
 */
@TableName(value = "my_wallet_schedule")
@Data
public class MyWalletSchedule implements Serializable {
    /**
     *
     */
    @TableId
    private Long mwsId;

    /**
     *
     */
    private String orderCode;

    /**
     *
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 1 待处理，2 已处理，3 已废弃
     */
    private Byte status;

    private String code;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}