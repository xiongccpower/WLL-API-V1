package com.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 配置信息
 *
 * @TableName mj_sys_properties
 */
@TableName(value = "mj_sys_properties")
@Data
public class SysProperties implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long mspId;

    /**
     *
     */
    private String code;

    /**
     *
     */
    private String value;

    /**
     *
     */
    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 1：启用
     * 0：不启用
     */
    private Byte status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}