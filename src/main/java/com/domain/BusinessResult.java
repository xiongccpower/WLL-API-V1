package com.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 运营效果产出表
 *
 * @TableName mj_business_result
 */
@TableName(value = "mj_business_result")
@Data
public class BusinessResult implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long mbrId;

    /**
     * 归属运营ID
     */
    private Integer uid;

    /**
     *
     */
    @TableField("`order`")
    private Integer order;

    /**
     * 事件ID
     */
    private Long mbeId;

    /**
     *
     */
    private String area;

    /**
     * 分享次数
     */
    private Integer share;

    /**
     *
     */
    private String url;

    /**
     *
     */
    private String name;

    /**
     * 注册数量
     */
    private Integer register;

    /**
     * 购买数量
     */
    private Integer buy;

    /**
     *
     */
    private LocalDateTime createTime;

    /**
     *
     */
    private LocalDateTime endTime;

    /**
     * 1:生效
     * 0:已失效
     */
    private Byte status;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 设备ID
     */
    private String deviceId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}