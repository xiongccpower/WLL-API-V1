package com.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @TableName mj_share_content
 */
@TableName(value = "mj_share_content")
@Data
public class ShareContent implements Serializable {
    /**
     *
     */
    @TableId
    private Long mscId;

    /**
     * 图片路径
     */
    private String url;

    /**
     * 标题
     */
    private String title;

    /**
     * 场景值
     */
    private Integer code;

    /**
     *
     */
    private Date createTime;

    /**
     *
     */
    private Date updateTime;

    /**
     * 平台
     */
    private Integer platform;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}