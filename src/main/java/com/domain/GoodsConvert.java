package com.domain;

import lombok.Data;

/**
 * created in 2022/1/17 17:14
 *
 * @author zhuxuelei
 */
@Data
public class GoodsConvert {

    private String url;

    private String siteId;

    private String source;

    private String channel;
//    /**
//     * 计划id，和url、product_id必传一个
//     */
//    private String adsId;
}
