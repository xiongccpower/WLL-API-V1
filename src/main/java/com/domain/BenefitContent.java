package com.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @TableName mj_benefit_content
 */
@TableName(value = "mj_benefit_content")
@Data
public class BenefitContent implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer mbcId;

    /**
     *
     */
    private String form;

    /**
     *
     */
    private String content;

    private String goods;

    /**
     *
     */
    private LocalDateTime createTime;

    /**
     *
     */
    private LocalDateTime updateTime;

    /**
     *
     */
    private Integer uid;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}