package com.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 收藏商品表
 *
 * @TableName mj_collection_goods
 */
@Data
@TableName("mj_collection_goods")
public class CollectionGoods implements Serializable {
    /**
     *
     */
    private Integer id;

    /**
     * 工作人员id
     */
    private Long sysUserId;

    /**
     * 商品标题
     */
    private String title;

    /**
     * 商品图片路径
     */
    private String imgUrl;

    /**
     * 商品其他信息
     */
    private String rests;


    private String isCollection;



    /**
     *
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    private Date createDate;

    /**
     * 显示名称
     */
    private String name;

    /**
     * 是否过期
     */
    @TableField(value = "`out`")
    private Integer out;

    private static final long serialVersionUID = 1L;

}