package com.domain.dto;

import lombok.Data;

/**
 * created in 2022/3/8 17:27
 *
 * @author zhuxuelei
 */
@Data
public class SysUserDto {
    private Integer userId;

    private String wxImgUrl;
}
