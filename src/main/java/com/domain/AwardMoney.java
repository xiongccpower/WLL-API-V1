package com.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName mj_award_money
 */
@TableName(value ="mj_award_money")
@Data
public class AwardMoney implements Serializable {
    /**
     * 
     */
    @TableId
    private Integer id;

    /**
     * 奖励类型：1 购买智能系统 2消费奖励
     */
    private String awardType;

    /**
     * 奖励金
     */
    private String awardMoney;

    /**
     * 奖励规则
     */
    private Integer awardRule;

    /**
     * 奖励范围 1.合作点 2商户 3所有业主
     */
    private String awardWidth;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 修改时间
     */
    private String updateTime;

    /**
     * 修改人
     */
    private String updateUser;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}