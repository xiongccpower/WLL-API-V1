package com.domain;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @TableName mj_third_order
 */
@TableName(value = "mj_third_order")
@Data
public class ThirdOrder implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.INPUT)
    @JSONField(name = "id")
    @TableField("mto_id")
    private Long mtoId;

    /**
     *
     */
    private Integer adsId;

    /**
     *
     */
    private Integer siteId;

    /**
     *
     */
    private Integer linkId;

    /**
     *
     */
    private String euid;

    /**
     *
     */
    private Long orderSn;

    /**
     *
     */
    private String confirmPrice;

    /**
     *
     */
    private String confirmSiterCommission;

    /**
     *
     */
    private Date orderTime;

    /**
     *
     */
    private String ordersPrice;

    /**
     *
     */
    private String siterCommission;

    /**
     *
     */
    private Integer status;

    /**
     *
     */
    private Date chargeTime;

    /**
     *
     */
    private String parentOrderSn;

    /**
     *
     */
    private Date updateTime;

    /**
     *
     */
    private String adsName;

    @TableField(exist = false)
    private List<ThirdOrderDetail> details;

    private Integer uid;

    private String source;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}