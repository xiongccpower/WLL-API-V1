package com.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @TableName mj_third_order_detail
 */
@TableName(value = "mj_third_order_detail")
@Data
public class ThirdOrderDetail implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer mtoDid;

    /**
     *
     */
    private Long mtoId;

    /**
     *
     */
    private Integer goodsCate;

    /**
     *
     */
    private Integer adsId;

    /**
     *
     */
    private String goodsCateName;

    /**
     *
     */
    private String goodsId;

    /**
     *
     */
    private String goodsName;

    /**
     *
     */
    private String goodsPrice;

    /**
     *
     */
    private String goodsTa;

    /**
     *
     */
    private String ordersPrice;

    /**
     *
     */
    private String goodsImg;

    /**
     *
     */
    private String orderStatus;

    /**
     *
     */
    private String orderCommission;

    /**
     *
     */
    private String orderSn;

    /**
     *
     */
    private String dmOrderStatus;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}