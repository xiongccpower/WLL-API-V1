package com.configuration;

import com.netflix.client.config.DefaultClientConfigImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * created in 2021/12/17 9:02
 *
 * @author zhuxuelei
 */
@Configuration
public class ProjectConfiguration {
    @Bean
    public DefaultClientConfigImpl iClientConfig() {
        return new DefaultClientConfigImpl();
    }
}
