package com.butlerApi;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.entity.MyHome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 新增家庭成员API
 */
public class ButlerApiUtil {
    protected static Logger log = LoggerFactory.getLogger(ButlerApiUtil.class);

    /**
     * 新增家庭成员
     *
     * @param home
     * @param macIds
     * @return
     */
    public static Map<String, Object> addFamilyMemberApi(MyHome home, String macIds) {
        Map<String, Object> result = new HashMap<>();

        String macId[] = macIds.split(",");
        for (String id : macId) {

            JSONObject params = new JSONObject();
            params.put("birthday", home.getBirth());
            params.put("familyRoleName", home.getRelation());
            if (!StrUtil.hasEmpty(home.getLike())) {
                params.put("like", home.getLike());
            }
            params.put("mobile", home.getTel());
            params.put("memberId", home.getId());
            params.put("nickname", home.getNickname());
            params.put("sexName", home.getSex());
            if (home.getSex().equals("男")) {
                params.put("sex", 1);
            } else {
                params.put("sex", 0);
            }
            params.put("uid", home.getUid());
            params.put("macId", id);
            System.out.println(params);

            String post = HttpRequest.post("http://localhost:8888/intelligent-device/family/addFamilyMember")
                    .body(params.toString())
                    .execute().body();

            if (!StrUtil.hasEmpty(post)) {
                JSONObject json = JSONUtil.parseObj(post);
                if (json.getInt("code") != 0) {
                    result.put("code", "500");
                    result.put("msg", json.get("msg"));
                    break;
                } else {
                    result.put("code", "200");
                    result.put("msg", "添加成功");
                }

            } else {
                result.put("code", "500");
                result.put("msg", "设备无响应");
                break;
            }

        }

        return result;
    }

    /**
     * 查询家庭成员列表
     *
     * @param uid
     * @param macId
     * @return
     */
    public static Map<String, Object> queryUserFamilyDetail(String uid, String macId) {
        Map<String, Object> result = new HashMap<>();

        JSONObject param = new JSONObject();
        param.put("uid", uid);
        param.put("macId", macId);
        String post = HttpRequest.post("http://localhost:8888/intelligent-device/family/queryUserFamilyDetail")
                .body(param.toString())
                .execute().body();

        if (!StrUtil.hasEmpty(post)) {
            JSONObject json = JSONUtil.parseObj(post);
            if (json.getInt("code") != 200) {
                result.put("code", "500");
                result.put("msg", json.get("msg"));
            } else {
                result.put("code", "200");
                result.put("data", json.get("data"));
            }

        } else {
            result.put("code", "500");
            result.put("msg", "设备无响应");
        }

        return result;

    }

    /**
     * 修改家庭成员接口
     *
     * @param home
     * @param macIds
     * @return
     */
    public static Map<String, Object> updateFamilyMember(MyHome home, String macIds) {
        Map<String, Object> result = new HashMap<>();

        String macId[] = macIds.split(",");

        for (String id : macId) {
            JSONObject param = new JSONObject();
            param.put("uid", home.getUid());
            param.put("macId", id);
            param.put("mobile", home.getTel());
            if (!StrUtil.hasEmpty(home.getLike())) {
                param.put("like", home.getLike());
            }
            param.put("memberId", home.getId());
            param.put("familyRoleName", home.getRelation());
            param.put("birthday", home.getBirth());
            param.put("nickname", home.getNickname());
            param.put("sexName", home.getSex());
            if (home.getSex().equals("男")) {
                param.put("sex", 1);
            } else {
                param.put("sex", 0);
            }

            String post = HttpRequest.post("http://localhost:8888/intelligent-device/family/updateFamilyMember")
                    .body(param.toString())
                    .execute().body();

            if (!StrUtil.hasEmpty(post)) {
                JSONObject json = JSONUtil.parseObj(post);
                if (json.getInt("code") != 0) {
                    result.put("code", "500");
                    result.put("msg", json.get("msg"));
                    break;
                } else {
                    result.put("code", "200");
                    result.put("msg", "修改成功");
                }
            } else {
                result.put("code", "500");
                result.put("msg", "设备无响应");
                break;
            }
        }
        return result;
    }

    /**
     * 查询家庭成员详情
     *
     * @return
     */
    public static Map<String, Object> queryFamilyMemberInfo(String macId, String memberId, String uid) {

        Map<String, Object> result = new HashMap<>();
        JSONObject param = new JSONObject();
        param.put("uid", uid);
        param.put("macId", macId);
        param.put("memberId", memberId);
        String post = HttpRequest.post("http://localhost:8888/intelligent-device/family/queryFamilyMemberInfo")
                .body(param.toString())
                .execute().body();
        if (!StrUtil.hasEmpty(post)) {
            JSONObject json = JSONUtil.parseObj(post);
            if (json.getInt("code") != 200) {
                result.put("code", "500");
                result.put("msg", json.get("msg"));
            } else {
                System.out.println("data参数=" + json.get("data").toString());
                if (!StrUtil.hasEmpty(json.get("data").toString()) && json.get("data").toString() != null && !json.get("data").toString().equals("null")) {
                    result.put("code", "200");
                    result.put("data", json.get("data"));
                } else {
                    result.put("code", "500");
                    result.put("msg", "暂无家庭成员数据");
                }

            }
        } else {
            result.put("code", "500");
            result.put("msg", "设备无响应");
        }


        return result;
    }


    /**
     * 查询家庭成员单个健康数据列表
     *
     * @return
     */
    public static Map<String, Object> queryMemberDetectionDataList(String macId, String uid, String memberId, String type, String startTime, String endTime) {
        Map<String, Object> result = new HashMap<>();
        JSONObject params = new JSONObject();
        params.put("uid", uid);
        params.put("macId", macId);
        params.put("memberId", memberId);
        params.put("type", Integer.parseInt(type));
        params.put("startTime", startTime);
        params.put("endTime", endTime);

        String post = HttpRequest.post("http://localhost:8888/intelligent-device/detectionData/queryMemberDetectionDataList")
                .body(params.toString())
                .execute().body();

        if (!StrUtil.hasEmpty(post)) {
            JSONObject json = JSONUtil.parseObj(post);
            if (json.getInt("code") != 200) {
                result.put("code", "500");
                result.put("msg", json.get("msg"));
            } else {
                result.put("code", "200");
                result.put("data", json.get("data"));
            }
        } else {
            result.put("code", "500");
            result.put("msg", "设备无响应");
        }
        return result;
    }

    /**
     * 添加健康数据接口
     */
    public static Map<String, Object> addDetectionData(Map<String, String> parm) {
        Map<String, Object> result = new HashMap<>();

        JSONObject param = new JSONObject();
        param.put("macId", parm.get("macId"));
        param.put("uid", parm.get("uid"));
        param.put("memberId", parm.get("memberId"));
        param.put("created_time", parm.get("time"));
        param.put("dataCome", parm.get("dataCome"));
        if (parm.get("type").equals("身高")) {
            param.put("height", parm.get("height"));
            param.put("type", 2);
        } else if (parm.get("type").equals("体重")) {
            param.put("weight", parm.get("weight"));
            param.put("type", 1);
        } else if (parm.get("type").equals("心率")) {
            param.put("heartbeat", parm.get("heartbeat"));
            param.put("type", 0);
        } else if (parm.get("type").equals("血脂")) {
            param.put("otherValue", parm.get("bloodFat"));
            param.put("type", 3);
        } else if (parm.get("type").equals("血糖")) {
            param.put("otherValue", parm.get("bloodSugar"));
            param.put("type", 4);
        } else if (parm.get("type").equals("血氧")) {
            param.put("otherValue", parm.get("bloodOxygen"));
            param.put("type", 5);
        } else if (parm.get("type").equals("血压")) {
            param.put("otherValue", parm.get("bloodPressure"));
            param.put("type", 6);
        }

        String post = HttpRequest.post("http://localhost:8888/intelligent-device/detectionData/addDetectionData")
                .body(param.toString())
                .execute().body();

        if (!StrUtil.hasEmpty(post)) {
            JSONObject json = JSONUtil.parseObj(post);
            if (json.getInt("code") != 200) {
                result.put("code", "500");
                result.put("msg", json.get("msg"));
            } else {
                result.put("code", "200");
                result.put("msg", "成功");
            }
        } else {
            result.put("code", "500");
            result.put("msg", "设备无响应");
        }

        return result;
    }

    /**
     * 查询绑定设备
     *
     * @return
     */
    public static Map<String, Object> queryDeviceBinding(String uid) {
        Map<String, Object> result = new HashMap<>();
        JSONObject params = new JSONObject();
        params.put("uid", uid);

        String post = HttpRequest.post("http://localhost:8888/intelligent-device/deviceUserBind/query")
                .body(params.toString())
                .execute().body();

        if (!StrUtil.hasEmpty(post)) {
            JSONObject json = JSONUtil.parseObj(post);
            if (json.getInt("code") != 200) {
                result.put("code", "500");
                result.put("msg", json.get("msg"));
            } else {
                String macIds = "";
                JSONArray datas = JSONUtil.parseArray(json.get("data"));
                if (datas.size() > 0 && datas != null) {
                    for (int i = 0; i < datas.size(); i++) {
                        JSONObject dataJson = datas.getJSONObject(i);
                        macIds += dataJson.get("macId") + ",";
                    }
                    result.put("code", "200");
                    result.put("macIds", macIds.substring(0, macIds.length() - 1));
                } else {
                    result.put("code", "500");
                    result.put("msg", "请先绑定设备");
                }

            }

        } else {
            result.put("code", "500");
            result.put("msg", "设备无响应");
        }

        return result;
    }

    /**
     * 添加绑定设备
     *
     * @return
     */
    public static Map<String, Object> addDeviceBinding(String macId, String uid) {
        Map<String, Object> result = new HashMap<>();

        JSONObject param = new JSONObject();
        param.put("macId", macId);
        param.put("uid", uid);

        String post = HttpRequest.post("http://localhost:8888/intelligent-device/deviceUserBind/add")
                .body(param.toString())
                .execute().body();

        if (!StrUtil.hasEmpty(post)) {
            JSONObject json = JSONUtil.parseObj(post);
            if (json.getInt("code") != 0) {
                result.put("code", "500");
                result.put("msg", json.get("msg"));
            } else {
                result.put("code", "200");
                result.put("msg", "成功");
            }

        } else {
            result.put("code", "500");
            result.put("msg", "设备无响应");
        }

        return result;
    }

    /**
     * 管家数据备份
     *
     * @return
     */
    public static Map<String, Object> triggerDataBack(String macId) {
        Map<String, Object> result = new HashMap<>();

        JSONObject param = new JSONObject();
        param.put("macId", macId);

        String post = HttpRequest.put("http://localhost:8888/intelligent-device/deviceDataBack/triggerDataBack/" + macId)
//                .body(param.toString())
                .execute().body();
        if (!StrUtil.hasEmpty(post)) {
            JSONObject json = JSONUtil.parseObj(post);
            if (json.getInt("code") != 200) {
                result.put("code", "500");
                result.put("msg", json.get("msg"));
            } else {
                result.put("code", "200");
                result.put("msg", "成功");
            }

        } else {
            result.put("code", "500");
            result.put("msg", "设备无响应");
        }
        return result;
    }

    /**
     * 管家数据恢复
     *
     * @return
     */
    public static Map<String, Object> triggerDataRecover(String macId) {
        Map<String, Object> result = new HashMap<>();

        JSONObject param = new JSONObject();
        param.put("macId", macId);

        String post = HttpRequest.put("http://localhost:8888/intelligent-device/deviceDataBack/triggerDataRecover/" + macId)
//                .body(param.toString())
                .execute().body();

        if (!StrUtil.hasEmpty(post)) {
            JSONObject json = JSONUtil.parseObj(post);
            if (json.getInt("code") != 200) {
                result.put("code", "500");
                result.put("msg", json.get("msg"));
            } else {
                result.put("code", "200");
                result.put("msg", "成功");
            }

        } else {
            result.put("code", "500");
            result.put("msg", "设备无响应");
        }

        return result;
    }

    /**
     * 修改wifi密码
     *
     * @param macId
     * @param password
     * @return
     */
    public static Map<String, String> upWifiPassword(String macId, String password) {
        Map<String, String> result = new HashMap<>();
        JSONObject param = new JSONObject();
        param.put("macId", macId);
        param.put("password", password);

        String post = HttpRequest.post("http://localhost:8888/intelligent-device/deviceOpenAPI/upWifiPassword")
//                .body(param.toString())
                .header("macId", macId)
                .header("password", password)
                .execute().body();
        System.out.println("修改返回:" + post);

        if (!StrUtil.hasEmpty(post)) {
            JSONObject json = JSONUtil.parseObj(post);
            if (json.getInt("code") != 200) {
                result.put("code", "500");
                result.put("msg", json.get("msg").toString());
            } else {
                result.put("code", "200");
                result.put("msg", "成功");
            }

        } else {
            result.put("code", "500");
            result.put("msg", "设备无响应");
        }
        return result;
    }

    public static void main(String[] args) {

        upWifiPassword("42:ab:d0:03:ec:96", "123456789");
    }

}
