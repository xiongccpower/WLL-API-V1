package com.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
/**
 * 读取配置文件的工具类
 * jk
 */
public class ReadUtil extends Properties{
	
	private static String properiesName = "application.properties";

	public ReadUtil(String fileName) {
		this.properiesName = fileName;
	}
	public ReadUtil() {
	}
	//获取对应的Property的值
	/*
	 * key  		: 健
	 * return ： 值
	 */
	public static  String read(String key) {
		String value = "";
		InputStream is = null;
		try {
			is = ReadUtil.class.getClassLoader().getResourceAsStream(properiesName);
			Properties p = new Properties();
			p.load(is);
			value = p.getProperty(key);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return value;
	}


	public static void main(String[] args) {
		//测试  jdbc.properties读取的文件名字
		System.out.println(ReadUtil.read("spring.resources.static-locations").split("file:")[1]);
	}
}
