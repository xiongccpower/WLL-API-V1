package com.util;

import cn.hutool.core.util.StrUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ToolUtil {
	private static final Logger logger = LoggerFactory.getLogger(ToolUtil.class);
	private static final String Output = "%04d";

	// 日期格式
	private static final SimpleDateFormat yyMMddHHmmss = new SimpleDateFormat("yyMMddHHmmssSSS");
	private static final SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss");


	/**
	 * 获取订单号
	 *
	 * @return
	 */
	public static String getOrderNo() {
		// 获取6位随机数
		int sn = (int) (Math.random() * 10000);
		Date date = new Date();
		return yyMMddHHmmss.format(date) + String.format(Output, sn);
	}

	/**
	 * 获取订单号
	 *
	 * @return
	 */
	public static Integer getRandom(int num) {
		// 获取num位随机数
		int sn = (int) (Math.random() * Math.pow(10, num));
		return sn;
	}

	/**
	 * 获取随机数
	 *
	 * @return
	 */
	public static String getOrderNo(int num) {
		// 获取num位随机数
		int sn = (int) (Math.random() * Math.pow(10, num));
		return yyyyMMddHHmmss.format(new Date()) + String.format(Output, sn);
	}

	/**
	 * 计算折扣
	 *
	 * @param price
	 * @param discount
	 * @return
	 */
	public static BigDecimal countDiscount(Integer price, Double discount) {
		double tmp = price * discount;
		BigDecimal r = BigDecimal.valueOf(tmp);
		r = r.setScale(2, BigDecimal.ROUND_DOWN);
		return r;

	}




	/**
	 * 增加几天时间，并返回格式化的时间数据
	 * @param day
	 * @param format
	 * @return
	 */
	public static String addDateOfDay(int day, String format){
		Date mDate = addDataOfDay(new Date(), day);
		return dateToStr(mDate, format);
	}

	/**
	 * 比较折扣后的价格
	 *
	 * @param price
	 * @param discount
	 * @param pay
	 * @return
	 */
	public static Boolean isDiscount(Integer price, Double discount,
									 BigDecimal pay) {
		BigDecimal money = countDiscount(price, discount);
		if (money.intValue() == pay.intValue()) {
			return true;
		}
		return false;
	}

	/**
	 * 获取一天的开始时间，00:00:00
	 *
	 * @param date
	 * @return
	 */
	public static String DayMinTime(Date date) {

		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		date = calendar.getTime();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return formatter.format(date);

	}

	/**
	 * 获取一天结束时间
	 *
	 * @param date
	 * @return
	 */
	public static String DayMaxTime(Date date) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		date = calendar.getTime();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return formatter.format(date);
	}

	public static String toFormatDate(String time) throws ParseException {
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = sdf1.parse(time);

		return sdf.format(date);
	}

	/**
	 * 获取当月第一天
	 *
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static Date getCurrentFirst(Date date) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.parse(formatter.format(date));
	}

	/**
	 * 获取当月最后一天
	 *
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static Date getCurrentEnd(Date date) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		Calendar ca = Calendar.getInstance();
		ca.setTime(date);
		ca.set(Calendar.DAY_OF_MONTH,
				ca.getActualMaximum(Calendar.DAY_OF_MONTH));

		return format.parse(format.format(ca.getTime()));
	}

	/**
	 * 按照ascall码从小到大排序
	 *
	 * @return
	 */
	public static Map<String, String> sort(Map<String, String> map) {

		map = new TreeMap<String, String>(new Comparator<String>() {

			public int compare(String o1, String o2) {
				// TODO Auto-generated method stub
				return (o1.compareTo(o2));
			}
		});
		return map;
	}

	/**
	 * 获取分
	 *
	 * @param money
	 * @return
	 */
	public static int getFen(BigDecimal money) {
		int newMoney = money.multiply(new BigDecimal(100)).intValue();
		return newMoney;
	}

	/**
	 * map to str
	 */
	public static String map2Str(Map<String, Object> map) {
		StringBuffer buffer = new StringBuffer();
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			buffer.append(entry.getKey()).append("->").append(entry.getValue());
		}
		return buffer.toString();
	}

	/**
	 * map to str
	 *
	 * @param map
	 * @return
	 */
	public static String mapstr2Str(Map<String, String> map) {
		StringBuffer buffer = new StringBuffer();
		for (Map.Entry<String, String> entry : map.entrySet()) {
			buffer.append(entry.getKey()).append("->").append(entry.getValue());
		}
		return buffer.toString();
	}

	// 日期加天数
	public static Date getAddDayDate(Date date, int day) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_WEEK, day);
		return calendar.getTime();
	}

	// 日期加月份
	public static Date getAddMonthDate(Date date, int month) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, month);
		return calendar.getTime();
	}

	// list转string，逗号隔开
	public static String listToString(List<?> list) {
		if (list == null) {
			return null;
		}
		StringBuilder result = new StringBuilder();
		boolean flag = false;
		for (Object string : list) {
			if (flag) {
				result.append(",");
			} else {
				flag = true;
			}
			result.append(string.toString());
		}
		return result.toString();
	}

	/**
	 * String转list
	 *
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public static List<String> strToList(String value) throws Exception {
		String[] values = value.split(",");
		List<String> list = new ArrayList<String>();
		for (String item : values) {
			list.add(item);
		}
		return list;
	}

	/**
	 * 将驼峰式命名的字符串转换为下划线大写方式。如果转换前的驼峰式命名的字符串为空，则返回空字符串。</br>
	 * 例如：helloWorld->hello_world</br>
	 *
	 * @param name
	 *            转换前的驼峰式命名的字符串</br>
	 * @return 转换后下划线大写方式命名的字符串</br>
	 */
	public static String underscoreName(String name) {
		StringBuilder result = new StringBuilder();
		if (name != null && name.length() > 0) {
			// 将第一个字符处理成小写
			result.append(name.substring(0, 1).toLowerCase());
			// 循环处理其余字符
			for (int i = 1; i < name.length(); i++) {
				String s = name.substring(i, i + 1);
				// 在大写字母前添加下划线
				if (s.equals(s.toUpperCase())
						&& !Character.isDigit(s.charAt(0))) {
					result.append("_");
				}
				// 其他字符直接转成小写
				result.append(s.toLowerCase());
			}
		}
		return result.toString();
	}

	/**
	 * 将下划线大写方式命名的字符串转换为驼峰式。如果转换前的下划线大写方式命名的字符串为空，则返回空字符串。</br>
	 * 例如：hello_world->helloWorld</br>
	 *
	 * @param name
	 *            转换前的下划线大写方式命名的字符串</br>
	 * @return 转换后的驼峰式命名的字符串</br>
	 */
	public static String camelName(String name) {
		StringBuilder result = new StringBuilder();
		// 快速检查
		if (name == null || name.isEmpty()) {
			// 没必要转换
			return "";
		} else if (!name.contains("_")) {
			// 不含下划线，仅将首字母小写
			return name.substring(0, 1).toLowerCase() + name.substring(1);
		}
		// 用下划线将原始字符串分割
		String camels[] = name.split("_");
		for (String camel : camels) {
			// 跳过原始字符串中开头、结尾的下换线或双重下划线
			if (camel.isEmpty()) {
				continue;
			}
			// 处理真正的驼峰片段
			if (result.length() == 0) {
				// 第一个驼峰片段，全部字母都小写
				result.append(camel.toLowerCase());
			} else {
				// 其他的驼峰片段，首字母大写
				result.append(camel.substring(0, 1).toLowerCase());
				result.append(camel.substring(1).toLowerCase());
			}
		}
		return result.toString();
	}

	/**
	 * map拼装成xml<br>
	 * <Request><CardNo></CardNo><ChannelCode></ChannelCode></Request><br>
	 * xml格式
	 *
	 * @param map
	 * @return
	 */
	public static String Map2Xml(Map<String, Object> map) {

		StringBuffer sb = new StringBuffer(
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>");

		sb.append("<Request>");
		for (Map.Entry<String, Object> entry : map.entrySet()) {

			String key = entry.getKey();
			Object value = entry.getValue();
			if (null != value && !"".equals(value)) {
				key = key.substring(0, 1).toUpperCase() + key.substring(1);
				sb.append("<" + key + ">" + value.toString() + "</" + key + ">");
			}
		}
		sb.append("</Request>");

		return sb.toString();
	}


	/**
	 * 获取随机字符组合(大小写字母或数字)
	 *
	 * @param length 字符串长度
	 */
	public static String getRandomChar(int length) {
		String str = "";
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			if (random.nextBoolean()) { // 字符串
				int choice = random.nextBoolean() ? 65 : 97; //取得65大写字母还是97小写字母
				str += (char) (choice + random.nextInt(26));
			} else { // 数字
				str += String.valueOf(random.nextInt(10));
			}
		}
		return str;
	}

	/**
	 * 取出浮点数中的整数部分
	 *
	 * @param s 浮点数
	 * @return 整数部分
	 */
	public static String getInteger(Object s) {
		return getInteger(s.toString());
	}

	/**
	 * 取出浮点数中的整数部分
	 *
	 * @param s 浮点数
	 * @return 整数部分
	 */
	public static String getInteger(String s) {
		return s.substring(0, s.indexOf("."));
	}


	/**
	 * 时间加减天数
	 * @param date
	 * @param day
	 * @return
	 */
	public static Date addDataOfDay(Date date, int day){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, day);
		return cal.getTime();
	}

	/**
	 * 时间转字符串
	 * 例如：date, "yyyy-MM-dd HH:mm:ss"
	 * @param date
	 * @param format
	 * @return
	 */
	public static String dateToStr(Date date, String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	/**
	 * 处理时间格式
	 * @param dValue
	 * @return
	 */
	public static String formatDouble(String dValue){
		String strRet = dValue;
		DecimalFormat df = new DecimalFormat("############0.##");
		try{
			strRet = df.format(Double.parseDouble(dValue));
		}catch (Exception e){
			logger.error(e.getMessage(), e);
		}
		return  strRet;
	}

	/**
	 * 取当前时间
	 * "yyyy-MM-dd HH:mm:ss"
	 * @param format
	 * @return
	 */
	public static String getSystemCurrentInfo(String format){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}


	/**
	 * 取当前时间/yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String getSystemCurrentInfo(){
		return getSystemCurrentInfo("yyyy-MM-dd HH:mm:ss");
	}


	/**
     * 返回的是秒
     * @param signTime
     * @return
     */
	public static long getSignTimeInfo(long signTime){
	    return (System.currentTimeMillis() - signTime)/1000;
    }

	/**
	 * 查询加密
	 * @param param
	 * @param key
	 * @param customer
	 * @return
	 */
	public static String querySign(String param,String key,String customer){
		return sign(param+key+customer);
	}
	/**
	 * 快递100加密方式统一为MD5后转大写
	 *
	 * @param msg
	 * @return
	 */
	public static String sign(String msg) {
		return DigestUtils.md5Hex(msg).toUpperCase();
	}

	/**
	 * 判断字符串不为空
	 * @param start
	 * @return
	 */
	public static boolean isNotEmpty(Object start){
		if(start!=null || !start.toString().equals("") || !start.toString().equals("null") || start.toString().length()>0){
			return true;
		}else{
			return false;
		}

	}
	/**
	 * 判断字符串为空
	 * @param start
	 * @return
	 */
	public static boolean isEmpty(Object start){
		if(start==null || start.toString().equals("") || start.toString().equals("null") || start.toString().length()==0){
			return true;
		}else{
			return false;
		}

	}

	/**
	 * 判断对象为空字符串或者为null，如果满足其中一个条件，则返回true
	 *
	 * @param
	 * @return
	 */
	public static boolean isNullOrEmpty(Object obj) {
		boolean isEmpty = false;
		if (obj == null) {
			isEmpty = true;
		} else if (obj instanceof String) {
			isEmpty = ((String) obj).trim().isEmpty();
		} else if (obj instanceof Collection) {
			isEmpty = (((Collection) obj).size() == 0);
		} else if (obj instanceof Map) {
			isEmpty = ((Map) obj).size() == 0;
		} else if (obj.getClass().isArray()) {
			isEmpty = Array.getLength(obj) == 0;
		}
		return isEmpty;
	}

	public static boolean isEmpty(String str){
		boolean result = false;
		if(str==null){
			result = true;
		}else if(str.equals("null")){
			result = true;
		}else if(str.length() == 0){
			result = true;
		}
		return result;
	}

	public static void main(String[] args) {
		String test = null;

				System.out.println(isEmpty(test));
	}

}
