package com.util;

import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

public class MyClientHandler implements IoHandler {

    @Override
    public void exceptionCaught(IoSession arg0, Throwable arg1) throws Exception {
        // TODO Auto-generated method stub
        System.out.println("exceptionCaught");
    }

    @Override
    public void inputClosed(IoSession arg0) throws Exception {
        // TODO Auto-generated method stub
        //System.out.println("111");
        System.out.println("inputClosed");
    }

    @Override
    public void messageReceived(IoSession arg0, Object message) throws Exception {
        // TODO Auto-generated method stub
        System.out.println("接收信息");
        System.out.println("message:"+message.toString());
    }

    @Override
    public void messageSent(IoSession arg0, Object arg1) throws Exception {
        // TODO Auto-generated method stub
        System.out.println("发送信息");
    }

    @Override
    public void sessionClosed(IoSession arg0) throws Exception {
        // TODO Auto-generated method stub
        System.out.println( "server端IP：" + arg0.getRemoteAddress().toString() + " 关闭连接" );
//		System.out.println("关闭session");
    }

    @Override
    public void sessionCreated(IoSession arg0) throws Exception {
        // TODO Auto-generated method stub
        System.out.println("创建session");
    }

    @Override
    public void sessionIdle(IoSession arg0, IdleStatus arg1) throws Exception {
        // TODO Auto-generated method stub
        System.out.println( "server端闲置连接：会话 " + arg0.getId() + " 被触发 " + arg0.getIdleCount(arg1) + " 次" );
    }

    @Override
    public void sessionOpened(IoSession arg0) throws Exception {
        // TODO Auto-generated method stub
        System.out.println("打开session");
    }
}
