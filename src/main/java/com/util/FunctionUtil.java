
package com.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.entity.Order;
import com.google.gson.Gson;
import com.kuaidi100.sdk.CompanyConstant;
import com.kuaidi100.sdk.api.QueryTrack;
import com.kuaidi100.sdk.core.IBaseClient;
import com.kuaidi100.sdk.request.QueryTrackParam;
import com.kuaidi100.sdk.request.QueryTrackReq;
import com.kuaidi100.sdk.utils.SignUtils;
import com.sun.xml.bind.v2.util.ByteArrayOutputStreamEx;
import com.suning.api.DefaultSuningClient;
import com.suning.api.entity.netalliance.AppletextensionlinkGetRequest;
import com.suning.api.entity.netalliance.AppletextensionlinkGetResponse;
import com.suning.api.entity.netalliance.SearchcommodityQueryRequest;
import com.suning.api.entity.netalliance.SearchcommodityQueryResponse;
import com.suning.api.exception.SuningApiException;
import lombok.extern.log4j.Log4j2;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import java.io.*;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;


/***
 * 微信统一下单支付接口工具类v2
 */
@Log4j2
public class FunctionUtil {
    private static CloseableHttpClient httpClient;// HTTP请求器
    private static RequestConfig requestConfig;// 请求器的配置
    private static int socketTimeout = 10000;// 连接超时时间，默认10秒
    private static int connectTimeout = 30000;// 传输超时时间，默认30秒

    //支付
    public static JSONObject pay(Order order, String openId) {

        int pay = (int) (order.getPayTotal() * 100);
        System.out.println(pay);
        String amountFen = pay + "";
        String url = "https://api2.mch.weixin.qq.com/pay/unifiedorder";//统一下单接口
        String appId = ReadUtil.read("XCX_appid");
        String mchId = ReadUtil.read("XCX_mchId");
        String key = ReadUtil.read("XCX_key");
        String tradeType = "JSAPI";
        String orderNo = order.getOrderNo();
        SortedMap<String, String> paraMap = new TreeMap<String, String>();
        String body = "";
        if (order.getInventory() == null) {
            body = order.getGoodsName();
        } else {
            body = order.getInventory().get(0).getTitle();
        }
        String nonceStr = orderNo;
        String outTradeNo = orderNo;
        paraMap.put("appid", appId);
        paraMap.put("mch_id", mchId);
        paraMap.put("nonce_str", "1add1a30ac87aa2db72f57a2375d8fec");
        paraMap.put("body", body);
        paraMap.put("out_trade_no", outTradeNo);
        paraMap.put("total_fee", amountFen);
        paraMap.put("spbill_create_ip", ReadUtil.read("ip"));
        paraMap.put("notify_url", ReadUtil.read("domain") + "/order/payCallback");
        paraMap.put("trade_type", tradeType);
        paraMap.put("openid", openId);
        String prestr = PayUtil.createLinkString(paraMap);
        String sign = PayUtil.sign(prestr, key, "utf-8").toUpperCase();

        StringBuffer paramBuffer = new StringBuffer();
        paramBuffer.append("<xml>");
        paramBuffer.append("<appid>" + appId + "</appid>");
        paramBuffer.append("<mch_id>" + mchId + "</mch_id>");
        paramBuffer.append("<nonce_str>" + paraMap.get("nonce_str") + "</nonce_str>");
        paramBuffer.append("<sign>" + sign + "</sign>");
        paramBuffer.append("<body>" + body + "</body>");
        paramBuffer.append("<out_trade_no>" + paraMap.get("out_trade_no") + "</out_trade_no>");
        paramBuffer.append("<total_fee>" + paraMap.get("total_fee") + "</total_fee>");
        paramBuffer.append("<spbill_create_ip>" + paraMap.get("spbill_create_ip") + "</spbill_create_ip>");
        paramBuffer.append("<notify_url>" + paraMap.get("notify_url") + "</notify_url>");
        paramBuffer.append("<trade_type>" + paraMap.get("trade_type") + "</trade_type>");
        paramBuffer.append("<openid>" + paraMap.get("openid") + "</openid>");
        paramBuffer.append("</xml>");
        log.info("微信支付参数：" + paramBuffer.toString());
        String wxResult = HttpClientUtil.doPostJson(url, paramBuffer.toString());
        Map map = new HashMap();
        try {
            log.info("微信支付返回信息为：" + wxResult);
            map = PayUtil.doXMLParse(wxResult);
            String msg = (String) map.get("return_msg");
            System.out.println("支付订单返回数据=" + map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject object = new JSONObject();
        object.put("prepayId", map.get("prepay_id"));
        object.put("outTradeNo", paraMap.get("out_trade_no"));
        return object;
    }


    public static JSONObject pay(Order order) {

        int pay = (int) (order.getPayTotal() * 100);
        System.out.println(pay);
        String amountFen = pay + "";
        String url = "https://api2.mch.weixin.qq.com/pay/unifiedorder";//统一下单接口
        String appId = ReadUtil.read("XCX_appid");
        String mchId = ReadUtil.read("XCX_mchId");
        String key = ReadUtil.read("XCX_key");
        String tradeType = "MWEB";
        String orderNo = order.getOrderNo();
        SortedMap<String, String> paraMap = new TreeMap<String, String>();
        String body = "";
        if (order.getInventory() == null) {
            body = order.getGoodsName();
        } else {
            body = order.getInventory().get(0).getTitle();
        }
        String nonceStr = orderNo;
        String outTradeNo = orderNo;
        paraMap.put("appid", appId);
        paraMap.put("mch_id", mchId);
        paraMap.put("nonce_str", "1add1a30ac87aa2db72f57a2375d8fec");
        paraMap.put("body", body);
        paraMap.put("out_trade_no", outTradeNo);
        paraMap.put("total_fee", amountFen);
        paraMap.put("spbill_create_ip", ReadUtil.read("ip"));
        paraMap.put("notify_url", ReadUtil.read("domain") + "/order/payCallback");
        paraMap.put("trade_type", tradeType);
        String prestr = PayUtil.createLinkString(paraMap);
        String sign = PayUtil.sign(prestr, key, "utf-8").toUpperCase();

        StringBuffer paramBuffer = new StringBuffer();
        paramBuffer.append("<xml>");
        paramBuffer.append("<appid>" + appId + "</appid>");
        paramBuffer.append("<mch_id>" + mchId + "</mch_id>");
        paramBuffer.append("<nonce_str>" + paraMap.get("nonce_str") + "</nonce_str>");
        paramBuffer.append("<sign>" + sign + "</sign>");
        paramBuffer.append("<body>" + body + "</body>");
        paramBuffer.append("<out_trade_no>" + paraMap.get("out_trade_no") + "</out_trade_no>");
        paramBuffer.append("<total_fee>" + paraMap.get("total_fee") + "</total_fee>");
        paramBuffer.append("<spbill_create_ip>" + paraMap.get("spbill_create_ip") + "</spbill_create_ip>");
        paramBuffer.append("<notify_url>" + paraMap.get("notify_url") + "</notify_url>");
        paramBuffer.append("<trade_type>" + paraMap.get("trade_type") + "</trade_type>");
        paramBuffer.append("</xml>");
        String wxResult = HttpClientUtil.doPostJson(url, paramBuffer.toString());
        Map map = new HashMap();
        try {
            log.info("微信支付返回信息为：" + wxResult);
            map = PayUtil.doXMLParse(wxResult);
            String msg = (String) map.get("return_msg");
            System.out.println("支付订单返回数据=" + map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject object = new JSONObject();
        object.put("prepayId", map.get("prepay_id"));
        object.put("outTradeNo", paraMap.get("out_trade_no"));
        return object;
    }

    //退款
    public static Map newrefund(Map parmsMap) throws Exception {
        String code = "000";
        String msg = "退款成功!";
        Map<String, String> data = new HashMap<String, String>();
        //退款到用户微信
        String nonce_str = "1add1a30ac87aa2db72f57a2375d8fec";
//         data.put("userId", String.valueOf(userecord.getUserId()));
        data.put("appid", ReadUtil.read("XCX_appid"));
        data.put("mch_id", ReadUtil.read("XCX_mchId"));
        data.put("nonce_str", nonce_str);
        data.put("sign_type", "MD5");
        data.put("out_trade_no", parmsMap.get("order_code").toString());//商户订单号
        data.put("out_refund_no", parmsMap.get("y_order_code").toString());//商户退款单号
        // System.out.println((int) (Double.parseDouble(parmsMap.get("total_price").toString()) * 100)+"");
        data.put("total_fee", (int) (Double.parseDouble(parmsMap.get("total_price").toString()) * 100) + "");//支付金额，微信支付提交的金额是不能带小数点的，且是以分为单位,这边需要转成字符串类型，否则后面的签名会失败
        data.put("refund_fee", (int) (Double.parseDouble(parmsMap.get("total_price").toString()) * 100) + "");//退款总金额,订单总金额,单位为分,只能为整数
//         data.put("notify_url", Constants.NOTIFY_URL_REFUND);//退款成功后的回调地址
        String preStr = PayUtil.createLinkString(data); // 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
        //MD5运算生成签名，这里是第一次签名，用于调用统一下单接口
        String mySign = PayUtil.sign(preStr, ReadUtil.read("XCX_key"), "utf-8").toUpperCase();
        data.put("sign", mySign);

        String xmlStr = postData("https://api2.mch.weixin.qq.com/secapi/pay/refund", PayUtil.GetMapToXML(data)); //支付结果通知的xml格式数据
        // System.out.println(xmlStr);
        Map notifyMap = PayUtil.doXMLParse(xmlStr);
        if ("SUCCESS".equals(notifyMap.get("return_code"))) {
            if ("SUCCESS".equals(notifyMap.get("return_code"))) {
                //退款成功的操作
                String prepay_id = (String) notifyMap.get("prepay_id");//返回的预付单信息
                Long timeStamp = System.currentTimeMillis() / 1000;
                //拼接签名需要的参数
                String stringSignTemp = "appId=" + ReadUtil.read("XCX_appid") + "&nonceStr=" + nonce_str + "&package=prepay_id=" + prepay_id + "&signType=MD5&timeStamp=" + timeStamp;
                //签名算法生成签名
                String paySign = PayUtil.sign(stringSignTemp, ReadUtil.read("XCX_key"), "utf-8").toUpperCase();
                data.put("package", "prepay_id=" + prepay_id);
                data.put("timeStamp", String.valueOf(timeStamp));
                data.put("paySign", paySign);
            } else {
                System.out.println("退款失败:原因" + notifyMap.get("return_msg"));
                code = "999";
                msg = (String) notifyMap.get("return_msg");
            }
        } else {
            System.out.println("退款失败:原因" + notifyMap.get("return_msg"));
            code = "999";
            msg = (String) notifyMap.get("return_msg");
        }
        Map reMap = new HashMap<String, String>();
        reMap.put("code", code);
        reMap.put("msg", msg);
        reMap.put("wxmsg", xmlStr);
        return reMap;


    }

    public static Map ToUserMoney(Map parmsMap) throws Exception {
        String code = "000";
        String msg = "提现成功";
        Map<String, String> dataMap = new HashMap<String, String>();
        //退款到用户微信零钱
//        double abc = (double) parmsMap.get("money");
//        int abcd = Double.valueOf(abc).intValue();//转换为Int类型
        double abc = (double) parmsMap.get("money");
        int money = (int) (abc * 100);
        String nonce_str = "1add1a30ac87aa2db72f57a2375d8fec";
        String orderNo = "MJTX" + System.currentTimeMillis();//订单号
        dataMap.put("mch_appid", ReadUtil.read("XCX_appid"));
        dataMap.put("mchid", ReadUtil.read("XCX_mchId"));
        dataMap.put("nonce_str", nonce_str);
        dataMap.put("partner_trade_no", orderNo);//商户订单号
        dataMap.put("amount", money + "");//支付金额，微信支付提交的金额是不能带小数点的，且是以分为单位,这边需要转成字符串类型，否则后面的签名会失败
        dataMap.put("openid", (String) parmsMap.get("openId"));//退款总金额,订单总金额,单位为分,只能为整数
        dataMap.put("desc", "奖励金提现");
        dataMap.put("check_name", "NO_CHECK");
        String preStr = PayUtil.createLinkString(dataMap); // 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
        System.out.println(ReadUtil.read("XCX_key"));
        String mySign = PayUtil.sign(preStr, ReadUtil.read("XCX_key"), "utf-8").toUpperCase();
        dataMap.put("sign", mySign);

        String xmlStr = postData("https://api.mch2.weixin.qq.com/mmpaymkttransfers/promotion/transfers", PayUtil.GetMapToXML(dataMap)); //支付结果通知的xml格式数据
        // System.out.println(xmlStr);
        Map notifyMap = PayUtil.doXMLParse(xmlStr);
        if ("SUCCESS".equals(notifyMap.get("return_code"))) {
            if ("SUCCESS".equals(notifyMap.get("result_code"))) {
                //退款成功的操作
                System.out.println("提现成功:原因" + notifyMap.get("return_msg"));
                code = "000";
                msg = (String) notifyMap.get("return_msg");
            } else {
                System.out.println("提现失败:原因" + notifyMap.get("return_msg"));
                code = "999";
                msg = (String) notifyMap.get("return_msg");
            }
        } else {
            System.out.println("提现失败:原因" + notifyMap.get("return_msg"));
            code = "999";
            msg = (String) notifyMap.get("return_msg");
        }
        Map reMap = new HashMap<String, String>();
        reMap.put("code", code);
        reMap.put("msg", msg);
        reMap.put("wxmsg", xmlStr);
        return reMap;
    }

    //提现到用户微信零钱

    /**
     * 加载证书
     */
    private static void initCert() throws Exception {
        // 证书密码，默认为商户ID
        String key = ReadUtil.read("XCX_mchId");
        // 商户证书的路径
        String path = ReadUtil.read("XCX_CERT_PATH");

        // 指定读取证书格式为PKCS12
        KeyStore keyStore = KeyStore.getInstance("PKCS12");

        // 读取本机存放的PKCS12证书文件
        FileInputStream instream = new FileInputStream(new File(path));
        try {
            // 指定PKCS12的密码(商户ID)
            keyStore.load(instream, key.toCharArray());
        } finally {
            instream.close();
        }

        SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(keyStore, key.toCharArray()).build();

        // 指定TLS版本
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[]{"TLSv1"}, null, SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
        // 设置httpclient的SSLSocketFactory
        httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
    }


    /**
     * 通过Https往API post xml数据
     *
     * @param url    API地址
     * @param xmlObj 要提交的XML数据对象
     * @return
     */
    public static String postData(String url, String xmlObj) {
        // 加载证书
        try {
            initCert();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String result = null;
        HttpPost httpPost = new HttpPost(url);
        // 得指明使用UTF-8编码，否则到API服务器XML的中文不能被成功识别
        StringEntity postEntity = new StringEntity(xmlObj, "UTF-8");
        httpPost.addHeader("Content-Type", "text/xml");
//        httpPost.
        httpPost.setEntity(postEntity);
        // 根据默认超时限制初始化requestConfig
        requestConfig = RequestConfig.custom()
                .setSocketTimeout(socketTimeout)
                .setConnectTimeout(connectTimeout)
                .build();
        // 设置请求器的配置
        httpPost.setConfig(requestConfig);
        try {
            HttpResponse response = null;
            try {
                response = httpClient.execute(httpPost);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity entity = response.getEntity();
            try {
                result = EntityUtils.toString(entity, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } finally {
            httpPost.abort();
        }
        return result;
    }

    //发送验证码
    public static String getMessagePwd(String tel) {
//	    String url = "http://localhost:9000/registerMessage/senderRegisterMessage.htm";
        String url = "https://www.mjshenghuo.com/registerMessage/senderRegisterMessage.htm";
        Map<String, String> parmMap = getParamMap(tel);
        JSONObject jo = JSON.parseObject(HttpClientUtil.doPost(url, parmMap));
        String num = jo.getString("register");
        return num;
    }

    ;

    //发送补货通知
    public static String sendMsg(String tel) {
        String url = "https://www.mjshenghuo.com/registerMessage/sendMsg.htm";
//        String url = "http://localhost:9000/registerMessage/sendMsg.htm";
        Map<String, String> parmMap = getParamMap(tel);
        JSONObject jo = JSON.parseObject(HttpClientUtil.doPost(url, parmMap));
        String num = jo.getString("register");
        return num;
    }

    ;

    //发送密码给用户
    public static JSONObject senderUserPwd(String tel, String pwd, String mainNum) {
        String url = "https://www.mjshenghuo.com/registerMessage/senderUserPwd.htm";
//        String url = "http://localhost:9000/registerMessage/senderUserPwd.htm";
        Map<String, String> parmMap = getParamMap(tel);
        parmMap.put("pwd", pwd);
        // TODO 此处的名称 "main_tel"编写不规范
        parmMap.put("main_tel", mainNum);
        System.out.println("请求数据=" + parmMap);
        JSONObject jo = JSON.parseObject(HttpClientUtil.doPost(url, parmMap));
        System.out.println("添加家庭成员：" + jo);
        return jo;
    }


    //获取奖励金金额和奖励规则
    public static Map<String, Object> getLikeCountAndMoney(String awardType) {
        Map<String, Object> result = new HashMap<String, Object>();

        String url = "http://localhost:8081/merchant/getAwardMoney.htm";

        Map<String, String> parmMap = new HashMap<String, String>();
        Map<String, Object> resultMap = new HashMap<String, Object>();
        parmMap.put("awardType", awardType);
        JSONObject jo = JSON.parseObject(HttpClientUtil.doPost(url, parmMap));
        //
        JSONObject dataObject = jo.getJSONObject("data");
        Double awardMoney = Double.parseDouble(dataObject.getString("awardMoney"));
        Integer awardRule = dataObject.getInteger("awardRule");
        resultMap.put("awardMoney", awardMoney);
        resultMap.put("awardRule", awardRule);

        return resultMap;
    }

    ;

//    public static  boolean addMoney(String tel,String money,String remark,String orderCode){
//        Map<String,Object> result = new HashMap<String,Object>();
//
//        String url = "https://www.mjshenghuo.com/userAction/addMoney.htm";
//
//        Map<String,String> parmMap = new HashMap<String,String>();
//        parmMap.put("tel",tel);
//        parmMap.put("money",money);
//        parmMap.put("remark",remark);
//        parmMap.put("orderCode",orderCode);
//        JSONObject jo = JSON.parseObject(HttpClientUtil.doPost(url,parmMap));
//        boolean success = jo.getBoolean("success");
//        return success;
//    };


    //注册
    public static JSONObject userRegister(String address,
                                          String longitude, String latitude, String city, String add, String tel, String pwd,
                                          String cid, String nation, String province, String district, String uid) {
        String url = "http://www.mjshenghuo.com/userAction/newIsRegister.htm";
//        String url = "http://localhost:9000/userAction/newIsRegister.htm";

        Map<String, String> parmMap = new HashMap<String, String>();
        parmMap.put("address", address);
        parmMap.put("longitude", longitude);
        parmMap.put("latitude", latitude);
        parmMap.put("city", city);
        parmMap.put("add", add);
        parmMap.put("tel", tel);
        parmMap.put("pwd", pwd);
        parmMap.put("cid", cid);
        parmMap.put("nation", nation);
        parmMap.put("province", province);
        parmMap.put("district", district);
        parmMap.put("mainNum", uid);
        System.out.println("传入数据：" + parmMap);
        JSONObject jo = JSON.parseObject(HttpClientUtil.doPost(url, parmMap));
        System.out.println("保存家庭成员信息返回：" + jo);
        return jo;
    }

    ;

    public static String getSHA256StrJava(String str) {
        MessageDigest messageDigest;
        String encodeStr = "";
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(str.getBytes("UTF-8"));
            encodeStr = byte2Hex(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encodeStr;
    }

    private static String byte2Hex(byte[] bytes) {
        StringBuffer stringBuffer = new StringBuffer();
        String temp = null;
        for (int i = 0; i < bytes.length; i++) {
            temp = Integer.toHexString(bytes[i] & 0xFF);
            if (temp.length() == 1) {
                //1得到一位的进行补0操作
                stringBuffer.append("0");
            }
            stringBuffer.append(temp);
        }
        return stringBuffer.toString();
    }


    //快递信息
    public static JSONObject expressInfo(String com, String expressId, String phone) throws Exception {
        QueryTrackReq queryTrackReq = new QueryTrackReq();
        QueryTrackParam queryTrackParam = new QueryTrackParam();
        queryTrackParam.setCom(com);
        queryTrackParam.setNum(expressId);
        queryTrackParam.setPhone(phone);
//        queryTrackParam.setCom(CompanyConstant.YD);
//        queryTrackParam.setNum("4609871994214");
//        queryTrackParam.setPhone("18915964342");
        String param = new Gson().toJson(queryTrackParam);

        queryTrackReq.setParam(param);
        queryTrackReq.setCustomer("44E3241B26F61633A0331C6FA64F8398");
        queryTrackReq.setSign(SignUtils.querySign(param, "CosvxLVf3798", "44E3241B26F61633A0331C6FA64F8398"));

        IBaseClient baseClient = new QueryTrack();
        System.out.println(baseClient.execute(queryTrackReq));
        JSONObject jsonObject = JSONObject.parseObject(baseClient.execute(queryTrackReq).getBody());

        return jsonObject;
    }

    //获取邀请码
    public static String invitePwd(String userBack) {
        int i = (int) (Math.random() * 900) + 100;
        String back = String.valueOf(i);
        return userBack + back;
    }

    public static String randomPwd() {
        String code = "";
        Random random = new Random();
        for (int i = 0; i < 6; i++) {

            int r = random.nextInt(10); //每次随机出一个数字（0-9）
            if (r != 4) {
                code = code + r;  //把每次随机出的数字拼在一起
            } else {
                int c = r + 1; //随机到4就加1
                code = code + c;
            }
        }
        return code;

    }

    public static Map<String, String> getParamMap(String tel) {
        String code = getSHA256StrJava(tel + ReadUtil.read("Phone_code"));
        Map<String, String> parmMap = new HashMap<String, String>();
        parmMap.put("tel", tel);
        parmMap.put("code", code);
        return parmMap;
    }

    //声明字节输出输出流
    public static byte[] toByteArray(InputStream stream) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStreamEx();
        byte[] buffer = new byte[1024 * 4];
        int n = 0;
        while (-1 != (n = stream.read(buffer))) {
            outputStream.write(buffer, 0, n);
        }
        return outputStream.toByteArray();
    }

    public static JSONObject yiqifaGoddsInfo(String productName, String pageIndex) {
        String url = "http://o.yiqifa.com/servlet/interface?method=yiqifa.high.commission.product.list.get";
        String key = ReadUtil.read("YIQIFA_KEY");
        String secret = ReadUtil.read("YIQIFA_SECRET");
        Map<String, String> object = new HashMap<String, String>();
        object.put("app_key", key);
        object.put("app_secret", secret);
        object.put("encoding", "UTF-8");
        object.put("pageIndex", pageIndex);
        object.put("pageSize", "6");
        object.put("productName", productName);
        String obj = HttpClientUtil.doGet(url, object);
        JSONObject jsonObject = JSONObject.parseObject(obj);

        return jsonObject;

    }

    public static JSONObject suningWx(String htmlUrl) {
        AppletextensionlinkGetRequest request = new AppletextensionlinkGetRequest();
        request.setProductUrl(htmlUrl);
        String serverUrl = "https://open.suning.com/api/http/sopRequest";
        String appKey = "6b5d578238682105e392759403da6e1f";
        String appSecret = "5d3409890a77e270249709fe3fc806f5";
        JSONObject jo = null;
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey, appSecret, "json");
        try {
            AppletextensionlinkGetResponse response = client.excute(request);
//            System.out.println("返回json/xml格式数据 :" + response.getBody());
            jo = JSONObject.parseObject(response.getBody());
            jo.getString("sn_responseContent");
            System.out.println(jo.getString("sn_responseContent"));
        } catch (SuningApiException e) {
            e.printStackTrace();
        }
        return jo.getJSONObject("sn_responseContent");
    }

    public static JSONObject suning(String productName, String pageIndex) {
        SearchcommodityQueryRequest request = new SearchcommodityQueryRequest();
        JSONObject jo = null;
//        request.setBranch("1");
//        request.setCityCode("025");
//        request.setCoupon("1");
//        request.setCouponMark("1");
//        request.setEndPrice("20.00");
        request.setKeyword(productName);
        request.setPageIndex(pageIndex);
//        request.setPgSearch("1");
        request.setPicHeight("1000");
        request.setPicWidth("1000");
//        request.setSaleCategoryCode("50000");
        request.setSize("6");
//        request.setSnfwservice("1");
//        request.setSnhwg("1");
//        request.setSortType("1");
//        request.setStartPrice("10.00");
//        request.setSuningService("1");
//        request.setCheckParam(true);
        String serverUrl = "https://open.suning.com/api/http/sopRequest";
        String appKey = "6b5d578238682105e392759403da6e1f";
        String appSecret = "5d3409890a77e270249709fe3fc806f5";
        DefaultSuningClient client = new DefaultSuningClient(serverUrl, appKey, appSecret, "json");
        try {
            SearchcommodityQueryResponse response = client.excute(request);
            jo = JSONObject.parseObject(response.getBody());
            JSONObject object = jo.getJSONObject("sn_responseContent").getJSONObject("sn_body");
            JSONArray list = object.getJSONArray("querySearchcommodity");
            for (int i = 0; i < list.size(); i++) {
                JSONObject obj = new JSONObject();
                if (!"".equals(list.getJSONObject(i).getJSONObject("couponInfo").get("couponUrl"))) {
                    //需领劵
                    obj.put("juan ", "1");
                }
                if ("1".equals(list.getJSONObject(i).getJSONObject("commodityInfo").get("baoyou"))) {
                    //包邮
                    // JSONObject obj = new JSONObject();
                    obj.put("you ", "1");
                }
                if ("99%".equals(list.getJSONObject(i).getJSONObject("cmmdtyReviewInfo").get("goodRate"))) {
                    //好评率高
                    //JSONObject obj = new JSONObject();
                    obj.put("good ", "1");
                }
                if (list.getJSONObject(i).getJSONObject("commodityInfo").getInteger("monthSales") > 1000) {
                    // JSONObject obj = new JSONObject();
                    obj.put("sale ", "1");
                }
                if ("".equals(list.getJSONObject(i).getJSONObject("cmmdtyReviewInfo").get("productUrl")) && "".equals(list.getJSONObject(i).getJSONObject("pgInfo").get("pgUrl"))) {
                    obj.put("app ", "1");
                }
                list.getJSONObject(i).put("result", obj);
            }

        } catch (SuningApiException e) {
            e.printStackTrace();
        }
        return jo;
    }

    public static void main(String[] args) throws Exception {
//            Random random = new Random();
//
//            int num = -1 ;
//
//            while(true) {
//
//                num = (int)(random.nextDouble()*(1000000 - 100000) + 100000);
//
//                if(!(num+"").contains("4"))
//                    break ;
//
//            }
//        JSONObject s= yiqifaGoddsInfo("裤子","1");

//                System.out.println(expressInfo("韵达快递","4609871994214","18915964342"));

        QueryTrackReq queryTrackReq = new QueryTrackReq();
        QueryTrackParam queryTrackParam = new QueryTrackParam();
        queryTrackParam.setCom(CompanyConstant.ZT);
        queryTrackParam.setNum("75887649414768");
        queryTrackParam.setPhone("15861825851");
        String param = new Gson().toJson(queryTrackParam);

        queryTrackReq.setParam(param);
        queryTrackReq.setCustomer("44E3241B26F61633A0331C6FA64F8398");
        queryTrackReq.setSign(SignUtils.querySign(param, "CosvxLVf3798", "44E3241B26F61633A0331C6FA64F8398"));

        IBaseClient baseClient = new QueryTrack();
        System.out.println(baseClient.execute(queryTrackReq).toString());

    }


    public static JSONObject pay1(Order order, String openId) {
        int pay = (int) (order.getPayTotal() * 100);
        System.out.println(pay);
        String amountFen = pay + "";
        String url = "https://api2.mch.weixin.qq.com/pay/unifiedorder";//统一下单接口
        String appId = ReadUtil.read("GZH_appid");
        String mchId = ReadUtil.read("GZH_mchId");
        String key = ReadUtil.read("GZH_key");
        String tradeType = "JSAPI";
        String orderNo = order.getOrderNo();
        SortedMap<String, String> paraMap = new TreeMap<String, String>();
        String body = "";
        if (order.getInventory() == null) {
            body = order.getGoodsName();
        } else {
            body = order.getInventory().get(0).getTitle();
        }
        String nonceStr = orderNo;
        String outTradeNo = orderNo;
        paraMap.put("appid", appId);
        paraMap.put("mch_id", mchId);
        paraMap.put("nonce_str", "1add1a30ac87aa2db72f57a2375d8fec");
        paraMap.put("body", body);
        paraMap.put("out_trade_no", outTradeNo);
        paraMap.put("total_fee", amountFen);
        paraMap.put("spbill_create_ip", ReadUtil.read("ip"));
        paraMap.put("notify_url", ReadUtil.read("domain") + "/order/payCallback");
        paraMap.put("trade_type", tradeType);
        paraMap.put("openid", openId);
        String prestr = PayUtil.createLinkString(paraMap);
        String sign = PayUtil.sign(prestr, key, "utf-8").toUpperCase();

        StringBuffer paramBuffer = new StringBuffer();
        paramBuffer.append("<xml>");
        paramBuffer.append("<appid>" + appId + "</appid>");
        paramBuffer.append("<mch_id>" + mchId + "</mch_id>");
        paramBuffer.append("<nonce_str>" + paraMap.get("nonce_str") + "</nonce_str>");
        paramBuffer.append("<sign>" + sign + "</sign>");
        paramBuffer.append("<body>" + body + "</body>");
        paramBuffer.append("<out_trade_no>" + paraMap.get("out_trade_no") + "</out_trade_no>");
        paramBuffer.append("<total_fee>" + paraMap.get("total_fee") + "</total_fee>");
        paramBuffer.append("<spbill_create_ip>" + paraMap.get("spbill_create_ip") + "</spbill_create_ip>");
        paramBuffer.append("<notify_url>" + paraMap.get("notify_url") + "</notify_url>");
        paramBuffer.append("<trade_type>" + paraMap.get("trade_type") + "</trade_type>");
        paramBuffer.append("<openid>" + paraMap.get("openid") + "</openid>");
        paramBuffer.append("</xml>");
        log.info("微信支付参数：" + paramBuffer.toString());
        String wxResult = HttpClientUtil.doPostJson(url, paramBuffer.toString());
        Map map = new HashMap();
        try {
            log.info("微信支付返回信息为：" + wxResult);
            map = PayUtil.doXMLParse(wxResult);
            String msg = (String) map.get("return_msg");
            System.out.println("支付订单返回数据=" + map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject object = new JSONObject();
        object.put("prepayId", map.get("prepay_id"));
        object.put("outTradeNo", paraMap.get("out_trade_no"));
        return object;
    }
}
