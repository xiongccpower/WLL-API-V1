package com.util;

import java.io.*;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;


public class PayUtils {
	
    public static String getIpAddress(HttpServletRequest request) {
        // 避免反向代理不能获取真实地址, 取X-Forwarded-For中第一个非unknown的有效IP字符串
        String ip = request.getHeader("x-forwarded-for");
       if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
           ip = request.getHeader("Proxy-Client-IP");
       }
       if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
           ip = request.getHeader("WL-Proxy-Client-IP");
       }
       if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
           ip = request.getRemoteAddr();
       }
        return ip;
    }
    
     public static String formatUrlMap(Map<String, String> paraMap, boolean urlEncode, boolean keyToLower){  
         String buff = "";  
         Map<String, String> tmpMap = paraMap;  
         try  
         {  
             List<Map.Entry<String, String>> infoIds = new ArrayList<Map.Entry<String, String>>(tmpMap.entrySet());  
             // 对所有传入参数按照字段名的 ASCII 码从小到大排序（字典序）  
             Collections.sort(infoIds, new Comparator<Map.Entry<String, String>>()  
             {  
                 @Override  
                 public int compare(Map.Entry<String, String> o1, Map.Entry<String, String> o2)  
                 {  
                     return (o1.getKey()).toString().compareTo(o2.getKey());  
                 }  
             });  
             // 构造URL 键值对的格式  
             StringBuilder buf = new StringBuilder();  
             for (Map.Entry<String, String> item : infoIds)  
             {  
                 if (StringUtils.isNotBlank(item.getKey()))  
                 {  
                     String key = item.getKey();  
                     String val = item.getValue();  
                     if (urlEncode)  
                     {  
                         val = URLEncoder.encode(val, "utf-8");  
                     }  
                     if (keyToLower)  
                     {  
                         buf.append(key.toLowerCase() + "=" + val);  
                     } else  
                     {  
                         buf.append(key + "=" + val);  
                     }  
                     buf.append("&");  
                 }  
    
             }  
             buff = buf.toString();  
             if (buff.isEmpty() == false)  
             {  
                 buff = buff.substring(0, buff.length() - 1);  
             }  
         } catch (Exception e)  
         {  
            return null;  
         }  
         return buff;  
     }  
     

         @SuppressWarnings("rawtypes")
		public static Map<String,String> doXMLParse(String strxml) throws Exception {
             if(null == strxml || "".equals(strxml)) {
                 return null;
             }
             
             Map<String,String> m = new HashMap<String,String>();
             InputStream in = String2Inputstream(strxml);
             SAXBuilder builder = new SAXBuilder();
             Document doc = builder.build(in);
             Element root = doc.getRootElement();
             List list = root.getChildren();
             Iterator it = list.iterator();
             while(it.hasNext()) {
                 Element e = (Element) it.next();
                 String k = e.getName();
                 String v = "";
                 List children = e.getChildren();
                 if(children.isEmpty()) {
                     v = e.getTextNormalize();
                 } else {
                     v = getChildrenText(children);
                 }
                 
                 m.put(k, v);
             }
             
             //关闭流
             in.close();
             
             return m;
         }
         
         private static  InputStream String2Inputstream(String str) {
             return new ByteArrayInputStream(str.getBytes());
         }
         
         @SuppressWarnings("rawtypes")
         private static String getChildrenText(List children) {
             StringBuffer sb = new StringBuffer();
             if(!children.isEmpty()) {
                 Iterator it = children.iterator();
                 while(it.hasNext()) {
                     Element e = (Element) it.next();
                     String name = e.getName();
                     String value = e.getTextNormalize();
                     List list = e.getChildren();
                     sb.append("<" + name + ">");
                     if(!list.isEmpty()) {
                         sb.append(getChildrenText(list));
                     }
                     sb.append(value);
                     sb.append("</" + name + ">");
                 }
             }
             
             return sb.toString();
         }

    /**

     * 从流中读取微信返回的xml数据

     *

     * @param httpServletRequest

     * @return

     * @throws IOException

     */

    public static String readXmlFromStream(HttpServletRequest httpServletRequest) throws IOException {
        InputStream inputStream = httpServletRequest.getInputStream();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        final StringBuffer sb = new StringBuffer();

        String line = null;

        try {
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);

            }

        } finally {
            bufferedReader.close();

            inputStream.close();

        }

        return sb.toString();

    }

}
