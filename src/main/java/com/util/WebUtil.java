package com.util;

import java.util.HashMap;
import java.util.Map;

public class WebUtil {
    
    public static Object ok(Object data,Integer total) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("rows", data);
        obj.put("total", total);
        obj.put("code", "success");
        return obj;
    }
    
    public static Object ok(Object data) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("rows", data);
        obj.put("code", "success");
        return obj;
    }
    
    public static Object ok() {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("code", "success");
        return obj;
    }
    
    public static Object error(String info) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("code", "error");
        obj.put("info", info);
        return obj;
    }
    
    public static Object error() {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("code", "error");
        return obj;
    }
    
}

