package com.util;

import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.LineDelimiter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.Properties;
import java.util.Scanner;

public class MinaClient {
    //private static String host  ="127.0.0.1";
    private static String host  ="121.43.165.214";
    private static int port =8888;
    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            //properties.load(MinaClient.class.getClassLoader().getResourceAsStream("gameLog4j.properties"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //PropertyConfigurator.configure(properties);

        IoConnector conn= new NioSocketConnector();
        conn.setConnectTimeout(3000);
//	   	conn.getFilterChain().addLast("codec", new ProtocolCodecFilter(
//	   			new TextLineCodecFactory(Charset.forName("utf-8"), LineDelimiter.WINDOWS.getValue(), LineDelimiter.WINDOWS.getValue())));
        conn.getFilterChain().addLast("codec", new ProtocolCodecFilter(new TextLineCodecFactory(Charset.forName("utf-8"), LineDelimiter.WINDOWS.getValue(), LineDelimiter.WINDOWS.getValue())));
        conn.setHandler(new MyClientHandler());
        ConnectFuture connectFuture = conn.connect(new InetSocketAddress(host, port));
        connectFuture.awaitUninterruptibly();
        IoSession session = connectFuture.getSession();
        Scanner sc = new Scanner(System.in);
        System.out.println("客户端内容输入：");
        boolean exit = false;
        // 输入exit，退出系统
        while(!exit)
        {
            String str = sc.next();
            session.write(str);
            if(str.equalsIgnoreCase("exit"))
            {
                exit = true;
                session.close(true);
            }
        }

    }
}
