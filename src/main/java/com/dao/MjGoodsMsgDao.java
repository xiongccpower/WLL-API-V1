package com.dao;

import com.entity.MjGoodsMsg;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MjGoodsMsgDao extends JpaRepository<MjGoodsMsg,Integer> {

}
