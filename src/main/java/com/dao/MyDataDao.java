package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.entity.MyData;

@Repository
@Service
public interface MyDataDao extends JpaRepository<MyData, Integer>{

	@Query("from MyData d where d.userId=:userId")
	public List<MyData> getByUserId(@Param("userId") Integer userId);
	
	
}
