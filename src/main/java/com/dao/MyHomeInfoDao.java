package com.dao;

import com.entity.MyHomeInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service
public interface MyHomeInfoDao extends JpaRepository<MyHomeInfo,Integer> {
    @Query("from MyHomeInfo d where d.uid like :uid order by d.createTime desc")
    public List<MyHomeInfo> getByTel(@Param("uid")String uid);

    @Query("from MyHomeInfo d where  d.uid like :uid and d.state=:state order by d.createTime desc")
    public List<MyHomeInfo> getisNew(@Param("uid")String uid,@Param("state")String state);

    @Query("from MyHomeInfo d where d.contentId =:contentId ")
    public MyHomeInfo getByContentId(@Param("contentId")String contentId);

}
