package com.dao;

import com.entity.ShareInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface ShareInfoDao extends JpaRepository<ShareInfo, Integer>{

	
}
