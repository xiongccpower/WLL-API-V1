package com.dao;

import com.entity.QuestionInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service
public interface QuestionInfoDao extends JpaRepository<QuestionInfo,Integer> {
    @Query("from QuestionInfo d where d.tel =:tel  order by d.createTime desc ")
    public List<QuestionInfo> getByTel(@Param("tel")String tel);

    @Query("from QuestionInfo d where d.questionId =:questionId  and d.tel =:tel")
    public List<QuestionInfo> getByQuestionId(@Param("questionId")String questionId,@Param("tel")String tel);

    @Query("from QuestionInfo d  order by d.createTime desc ")
    public List<QuestionInfo> getAll();
}
