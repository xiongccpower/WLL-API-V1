package com.dao;

import com.entity.UseInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface UseInfoDao extends JpaRepository<UseInfo, Integer>{

	
}
