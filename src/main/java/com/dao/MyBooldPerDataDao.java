package com.dao;

import com.entity.MyBooldPerData;
import com.entity.MyData;
import com.entity.MyWeightData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Repository
@Service
public interface MyBooldPerDataDao extends JpaRepository<MyBooldPerData, Integer>{

	@Query("from MyBooldPerData d where d.userId=:userId order by d.createTime desc ")
	public List<MyBooldPerData> getByUserId(@Param("userId") Integer userId);

	@Query("from MyBooldPerData d where d.userId=:userId and d.createTime between :startTime and :endTime order by d.createTime desc")
	public List<MyBooldPerData> getByUserIdandAndCreateTime(@Param("userId") Integer userId, @Param("startTime") Date startTime, @Param("endTime")Date endTime);
}
