package com.dao;

import com.entity.AwardRule;
import com.entity.BindInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface AwardRuleDao extends JpaRepository<AwardRule,Integer> {
}
