package com.dao;

import com.entity.MjSystemInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface MjSystemInfoDao extends JpaRepository<MjSystemInfo,Integer> {
}
