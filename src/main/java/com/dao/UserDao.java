package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.entity.Demo;
import com.entity.Users;

@Repository
@Service
public interface UserDao extends JpaRepository<Users, Integer>{

	//查询全部
	@Query("from Users d where d.openId=:openId ")
	public Users getByOpenId(@Param("openId") String openId);

	@Query("from Users d where d.phone=:tel ")
	public Users getByTel(@Param("tel") String tel);


}
