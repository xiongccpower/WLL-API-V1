package com.dao;

import com.entity.MyHeartTimesData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Repository
@Service
public interface MyHeartTimesDataDao extends JpaRepository<MyHeartTimesData, Integer>{

	@Query("from MyHeartTimesData d where d.userId=:userId order by d.createTime desc")
	public List<MyHeartTimesData> getByUserId(@Param("userId") Integer userId);

	@Query("from MyHeartTimesData d where d.userId=:userId and d.createTime between :startTime and :endTime order by d.createTime desc")
	public List<MyHeartTimesData> getByUserIdandAndCreateTime(@Param("userId") Integer userId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);
	
}
