package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.entity.Problem;

@Repository
@Service
public interface ProblemDao extends JpaRepository<Problem, Integer>{
	@Query("from Problem d  order by d.id asc ")
	List<Problem> findAllByOrdeByid();


}
