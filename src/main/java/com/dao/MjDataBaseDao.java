package com.dao;

import com.entity.MjDataBase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface MjDataBaseDao extends JpaRepository<MjDataBase,Integer> {

    @Query("from MjDataBase d where d.tel=:tel")
    public MjDataBase getByTel(@Param("tel") String tel);
}
