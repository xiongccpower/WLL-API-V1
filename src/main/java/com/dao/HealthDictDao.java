package com.dao;

import com.entity.HealthDict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service
public interface HealthDictDao extends JpaRepository<HealthDict,Integer> {

    @Query("from HealthDict  d  where d.tel =:tel order by d.createTime desc ")
    public List<HealthDict> getByTel(@Param("tel") String tel);
}
