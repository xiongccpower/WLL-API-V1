package com.dao;

import com.entity.AnswerInfo;
import com.entity.SaleWordsInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service
public interface SaleWordsInfoDao extends JpaRepository<SaleWordsInfo,Integer> {

    @Query("from SaleWordsInfo  d  where d.goodsWords  like CONCAT('%',:goodsWords,'%') ")
    public List<SaleWordsInfo> getByKeyWords(@Param("goodsWords") String goodsWords);
}
