package com.dao;

import com.entity.BindInfo;
import com.entity.CheckInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface CheckInfoDao extends JpaRepository<CheckInfo,Integer> {
}
