package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.entity.Dictionaries;

@Repository
@Service
public interface DictionariesDao extends JpaRepository<Dictionaries, Integer>{

	//查询全部
	@Query("from Dictionaries d where d.dicKey=:dicKey ")
	public Dictionaries getByKey(@Param("dicKey") String dicKey);
	
}
