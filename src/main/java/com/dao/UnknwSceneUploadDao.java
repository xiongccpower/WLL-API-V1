package com.dao;

import com.entity.UnknowSceneUpload;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;


@Repository
@Service
public interface UnknwSceneUploadDao extends JpaRepository<UnknowSceneUpload,Integer> {

}
