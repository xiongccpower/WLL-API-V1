package com.dao;

import com.entity.MyWallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface MyWalletDao extends JpaRepository<MyWallet, Integer> {
    @Query("from MyWallet d where d.tel=:tel ")
    public MyWallet getByTel(@Param("tel") String tel);

    @Query("from MyWallet d where d.code=:code ")
    public MyWallet getByCode(@Param("code") String code);
}
