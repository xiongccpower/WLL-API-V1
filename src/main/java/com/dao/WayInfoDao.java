package com.dao;

import com.entity.WayInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface WayInfoDao extends JpaRepository<WayInfo,Integer> {
}
