package com.dao;

import com.entity.MyHomeZj;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Repository
@Service
public interface MyHomeZjDao extends JpaRepository<MyHomeZj, Integer> {
    @Query("from MyHomeZj d where d.openId=:openId and d.tel =:tel order by d.productDate desc")
    public List<MyHomeZj> getByTel(@Param("tel") String tel, @Param("openId") String openId);

    @Query("from MyHomeZj d where d.openId=:openId and d.tel =:tel and d.productDate =:productDate order by d.productDate desc")
    public List<MyHomeZj> getByOpenId(@Param("tel") String tel, @Param("openId") String openId, @Param("productDate") Date productDate);
}
