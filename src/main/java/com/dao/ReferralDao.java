package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.entity.Referral;

@Repository
@Service
public interface ReferralDao extends JpaRepository<Referral, Integer>{

	
}
