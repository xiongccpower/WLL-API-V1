package com.dao;

import com.entity.MyHomeSaleData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service
public interface MyHomeSaleDataDao extends JpaRepository<MyHomeSaleData,Integer> {

    @Query("from MyHomeSaleData d where d.tel=:tel and d.openId=:openId order by d.productDate desc")
    public List<MyHomeSaleData> getByTel(@Param("tel") String tel, @Param("openId") String openId);

    @Query("from MyHomeSaleData d where d.productCode =:productCode ")
    public MyHomeSaleData getByProductCode(@Param("productCode") String productCode);

    @Query("from MyHomeSaleData d where d.productCode =:productCode and d.openId=:openId ")
    public MyHomeSaleData getByProductCodeOPenid(@Param("productCode") String productCode,@Param("openId") String openId);


}
