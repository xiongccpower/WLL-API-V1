package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.entity.Coupon;

@Repository
@Service
public interface CouponDao extends JpaRepository<Coupon, Integer>{

	//查询全部
	@Query("from Coupon d where d.userId=:userId ")
	public List<Coupon> getByUserId(@Param("userId") Integer userId);
	
	
}
