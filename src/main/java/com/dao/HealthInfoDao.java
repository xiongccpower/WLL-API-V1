package com.dao;

import com.entity.HealthInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface HealthInfoDao extends JpaRepository<HealthInfo,Integer> {

}
