package com.dao;

import com.entity.MyData;
import com.entity.MyHeightData;
import com.entity.MyWeightData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Repository
@Service
public interface MyHeightDataDao extends JpaRepository<MyHeightData, Integer>{

	@Query("from MyHeightData d where d.userId=:userId order by d.createTime desc")
	public List<MyHeightData> getByUserId(@Param("userId") Integer userId);

	@Query("from MyHeightData d where d.userId=:userId and d.createTime between :startTime and :endTime order by d.createTime desc")
	public List<MyHeightData> getByUserIdandAndCreateTime(@Param("userId") Integer userId, @Param("startTime") Date startTime, @Param("endTime")Date endTime);
	
}
