package com.dao;

import com.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Repository
@Service
public interface OrderDao extends JpaRepository<Order, Integer> {

    @Query("from Order d where d.userId=:userId and d.state>0 order by d.createTime desc")
    public List<Order> getByUserId(@Param("userId") Integer userId);

    @Query("from Order d where d.userId=:userId and d.state=:state order by d.createTime desc")
    public List<Order> getByUserId(@Param("userId") Integer user, @Param("state") Integer state);

    @Query("from Order d where d.orderNo=:orderNo order by d.createTime desc")
    public Order getByOrderNo(@Param("orderNo") String orderNo);

    @Query("from Order d where d.goodsIds like %?1% order by d.createTime desc")
    public List<Order> getByGoodsIds(@Param("goodsIds") String goodsIds);

    @Query("from Order d where d.scIds =:scIds order by d.createTime desc")
    public List<Order> getByScIds(@Param("scIds") String scIds);

    @Query("from Order d where d.scIds <>:scIds order by d.createTime desc")
    public List<Order> getNotSystem(@Param("scIds") String scIds);

    @Query("from Order d where d.scIds =:scIds and d.createTime between :startDate and :endDate order by d.createTime desc")
    public List<Order> getOrderByTime(@Param("scIds") String scIds, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    @Query("from Order d where d.scIds =:scIds and d.code =:code  order by d.createTime desc")
    public List<Order> getOrderByCode(@Param("scIds") String scIds, @Param("code") String code);

    @Query("from Order d where d.uid=:uid and d.state>=1")
    public List<Order> findBuyRecord(@Param("uid") String uid);

    @Query("from Order d where d.openId=:openid and d.state>=1")
    public List<Order> findBuyRecordOpenid(@Param("openid") String openid);

    @Query("from Order d where d.openId=:openId and d.state>0 order by d.createTime desc")
    public List<Order> getByOrder(@Param("openId") String openId);

    @Query("from Order d where d.uid=:uid and d.state>0 order by d.createTime desc")
    public List<Order> getByUid(@Param("uid") String uid);

    @Query("from Order d where d.openId=:openId and d.state=:state order by d.createTime desc")
    public List<Order> getByState(@Param("openId") String openId, @Param("state") Integer state);

    @Query("from Order d where d.uid=:uid and d.state=:state order by d.createTime desc")
    public List<Order> getByStateUid(@Param("uid") String uid, @Param("state") Integer state);

    @Query(" from Order d where d.id=:id ")
    public Order getById(@Param("id") Integer id);

    @Query(value = "select count(c.state) as unm from  Order c where c.uid=:uid and c.state=:state ")
    public int statistics(@Param("uid") String uid, @Param("state") Integer state);

//    @Query("from Order d where d.scIds <>:scIds order by d.createTime desc")
//    Page<Order> getNotSystem(PageRequest page, @Param("scIds") String scIds);
}
