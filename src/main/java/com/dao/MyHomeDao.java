package com.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.entity.MyData;
import com.entity.MyHome;

@Repository
@Service
public interface MyHomeDao extends JpaRepository<MyHome, Integer>{

	@Query("from MyHome d where d.mainTel=:mainTel ")
	public List<MyHome> getByMainTel(@Param("mainTel") String mainTel);

	@Query("from MyHome d where d.tel=:tel ")
	public MyHome getByTel(@Param("tel") String tel);

	@Query("from MyHome d where d.mainTel=:mainTel and d.relation =:relation")
	public MyHome getByMainTelandAndRelation(@Param("mainTel") String mainTel,@Param("relation") String relation);

	@Query("from MyHome d where d.id=:id ")
	public MyHome getById(@Param("id") Integer id);



}
