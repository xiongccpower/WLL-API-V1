package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.entity.Facility;

@Repository
@Service
public interface FacilityDao extends JpaRepository<Facility, Integer>{

	@Query("from Facility d where d.userId=:userId and d.isBind='Y' ")
	public List<Facility> getByUserId(@Param("userId") Integer userId);

	@Query("from Facility d where d.id=:id  ")
	public Facility getById(@Param("id") Integer id);
}
