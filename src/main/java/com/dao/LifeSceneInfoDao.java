package com.dao;

import com.entity.LifeSceneInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service
public interface LifeSceneInfoDao extends JpaRepository<LifeSceneInfo,Integer> {

    @Query("from LifeSceneInfo  d  where d.sceneCode =:sceneCode order by d.createTime asc")
    public List<LifeSceneInfo> getBysceneCode(@Param("sceneCode") String sceneCode);
}
