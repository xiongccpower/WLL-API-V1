package com.dao;

import com.entity.BookingGoodsInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service
public interface BookingGoodsInfoDao extends JpaRepository<BookingGoodsInfo,Integer> {

//    @Query("from BookingGoodsInfo  ")
//    public List<BookingGoodsInfo> getByQuestionId(@Param("questionId") String questionId);
}
