package com.dao;

import com.entity.MyLikeData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface MyLikeDataDao extends JpaRepository<MyLikeData,Integer>{

}
