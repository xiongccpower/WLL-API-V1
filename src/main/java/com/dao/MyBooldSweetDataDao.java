package com.dao;

import com.entity.MyBooldSweetData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Repository
@Service
public interface MyBooldSweetDataDao extends JpaRepository<MyBooldSweetData, Integer>{

	@Query("from MyBooldSweetData d where d.userId=:userId order by d.createTime desc")
	public List<MyBooldSweetData> getByUserId(@Param("userId") Integer userId);

	@Query("from MyBooldSweetData d where d.userId=:userId and d.createTime between :startTime and :endTime order by d.createTime desc")
	public List<MyBooldSweetData> getByUserIdandAndCreateTime(@Param("userId") Integer userId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);
	
}
