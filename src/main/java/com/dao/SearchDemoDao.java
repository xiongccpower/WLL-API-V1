package com.dao;

import com.entity.SearchDemo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface SearchDemoDao extends JpaRepository<SearchDemo, Integer>{

}
