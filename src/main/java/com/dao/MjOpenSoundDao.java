package com.dao;

import com.entity.MjOpenSound;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface MjOpenSoundDao extends JpaRepository<MjOpenSound,Integer> {

}
