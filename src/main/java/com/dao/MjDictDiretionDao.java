package com.dao;

import com.entity.MjDictDiretion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MjDictDiretionDao extends JpaRepository<MjDictDiretion,Integer> {
    @Query("from MjDictDiretion d where d.dictEntry=:dictEntry ")
    public List<MjDictDiretion> getByDictName(@Param("dictEntry") String dictEntry);

    @Query("from MjDictDiretion d where d.dictEntry=:dictEntry and d.dictValue=:dictValue")
    public MjDictDiretion getMjDictDiretion(@Param("dictEntry") String dictEntry,@Param("dictValue") String dictValue);

    @Query("from MjDictDiretion d where d.dictName=:dictName ")
    public MjDictDiretion getByDictNames(@Param("dictName") String dictName);

}
