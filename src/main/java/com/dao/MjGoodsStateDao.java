package com.dao;

import com.entity.MjGoodsState;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MjGoodsStateDao extends JpaRepository<MjGoodsState,Integer> {

}
