package com.dao;

import com.entity.AnswerInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service
public interface AnswerInfoDao extends JpaRepository<AnswerInfo,Integer> {

    @Query("from AnswerInfo  d  where d.questionId =:questionId order by d.createTime asc")
    public List<AnswerInfo> getByQuestionId(@Param("questionId")String questionId);
}
