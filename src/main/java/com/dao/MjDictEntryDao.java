package com.dao;

import com.entity.InvitePwd;
import com.entity.MjDictEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MjDictEntryDao extends JpaRepository<MjDictEntry,Integer> {
    @Query("from MjDictEntry d where d.dictName=:dictName ")
    public List<MjDictEntry> getByDictName(@Param("dictName") String dictName);
}
