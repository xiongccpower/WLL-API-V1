package com.dao;

import com.entity.MyWalletLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service
public interface MyWalletLogDao extends JpaRepository<MyWalletLog, Integer> {
    @Query("from MyWalletLog d where d.tel=:tel and d.type =:type")
    public List<MyWalletLog> getByTel(@Param("tel") String tel, @Param("type") int type);

    @Query("from MyWalletLog d where d.code=:code and d.type =:type order by d.updatetime desc")
    public List<MyWalletLog> getByCode(@Param("code") String code, @Param("type") int type);

    @Query("from MyWalletLog d where d.order_code=:orderNo ")
    public MyWalletLog getByOrderNo(@Param("orderNo") String orderNo);

    @Query("from MyWalletLog d where d.order_code=:orderNo and d.status = :status")
    public MyWalletLog getByOrderNo(@Param("orderNo") String orderNo, @Param("status") Integer status);
}
