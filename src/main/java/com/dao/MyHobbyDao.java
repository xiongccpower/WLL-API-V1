package com.dao;

import com.entity.MyHobby;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service
public interface MyHobbyDao extends JpaRepository<MyHobby,Integer> {

    @Query("from MyHobby d where d.tel=:tel ")
    public List<MyHobby> getByTel(@Param("tel") String tel);

    @Query("from MyHobby d where d.tel=:tel and d.dictValue =:dictValue ")
    public MyHobby getByTelAndDictValue(@Param("tel")String tel,@Param("dictValue")String dictValue);
}
