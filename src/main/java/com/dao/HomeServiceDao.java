package com.dao;

import com.entity.HomeService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;


@Repository
@Service
public interface HomeServiceDao extends JpaRepository<HomeService,Integer> {

    @Query("from HomeService  d  where d.tel =:tel order by d.createTime desc ")
    public  List<HomeService> getByTel(@Param("tel")String tel);


    @Query("from HomeService  d  where d.serviceFwTel =:tel order by d.createTime desc ")
    public  List<HomeService> getListByServiceFwTel(@Param("tel")String tel);

}
