package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.entity.Goods;

@Repository
@Service
public interface GoodsDao extends JpaRepository<Goods, Integer>{

	//查询全部
	@Query("from Goods d where d.id=:id ")
	public Goods getById(@Param("id") Integer id);
	
}
