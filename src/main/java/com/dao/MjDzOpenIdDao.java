package com.dao;

import com.entity.MjDzOpenId;
import com.entity.UserLike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MjDzOpenIdDao extends JpaRepository<MjDzOpenId, Integer> {
    //查询全部
    @Query("from MjDzOpenId d where d.openId=:openId ")
    public List<MjDzOpenId> getByOpenId(@Param("openId") String openId);

    @Query("from MjDzOpenId d where d.dzOpenId=:dzOpenId and d.uid=:uid ")
    public MjDzOpenId getByDzOpenId (@Param("dzOpenId") String dzOpenId,@Param("uid") String uid);

    @Query("from MjDzOpenId d where d.dzOpenId=:dzOpenId and d.openId=:openId")
    public MjDzOpenId getByDzOpenIdAndOpenId (@Param("dzOpenId") String dzOpenId,@Param("openId") String openId);


}