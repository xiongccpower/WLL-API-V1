package com.dao;

import com.entity.UserLike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;

public interface UserLikeDao extends JpaRepository<UserLike, Integer> {
    //查询全部
    @Query("from UserLike d where d.uid=:uid ")
    public UserLike getByUid(@Param("uid") String uid);

    @Query("from UserLike d where d.openId=:openId ")
    public UserLike getByOpenId(@Param("openId") String openId);

    @Query("from UserLike d where d.openId=:openId or d.uid=:uid ")
    public UserLike getByUidOrOpenid(@Param("openId") String openId,@Param("uid") String uid);

}
