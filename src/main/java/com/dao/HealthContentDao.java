package com.dao;

import com.entity.HealthContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service
public interface HealthContentDao extends JpaRepository<HealthContent,Integer> {

    @Query("from HealthContent  d  where d.healthCode =:healthCode order by d.createTime desc ")
    public List<HealthContent> getByHealthCode(@Param("healthCode") String healthCode);
}
