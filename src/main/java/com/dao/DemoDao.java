package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.entity.Demo;

@Repository
@Service
public interface DemoDao extends JpaRepository<Demo, Integer>{

	//查询全部
	@Query("from Demo d where d.id=:id ")
	public Demo getById(@Param("id") Integer id);
	
//	//查询所有已经注册的下级
//	@Query("from Users u where u.isRegister='Y' and u.parentId=:parentId  ")
//	public List<Users> findByParentId(@Param("parentId") Integer parentId);
//	
//	//查询所有已经注册的子孙个数
//	@Query("select count(u) from Users u where u.isRegister='Y' and u.parentList like :parentList")
//	public Integer teamTotal(@Param("parentList") String parentId);
//	
//	//获取全部手机
//	@Query("select phone from Users u where phone is not null")
//	public List<String> getPhone(Pageable pageable);
//	
//	//查询所有已经注册的用户
//	@Query("from Users u where u.isRegister='Y'")
//	public List<Users>  findByRegister();
//	
//	//查询所有已经注册的用户
//	@Query("from Users u where u.phone=:phone")
//	public List<Users>  getByPhone(@Param("phone") String phone);
//
//	//查询需要分红的用户  必须是合伙人 或者联合创始人 必须要有销售额
//	@Query("from Users u where u.saleroom>0 and grade>0")
//	public List<Users> getRewardUser();
//	
//	//查询所有已经注册的下级
//	@Query("from Users u where u.isRegister='Y' and u.parentId=:parentId and u.saleroom>0 and grade>0 ")
//	public List<Users> findByParentIdAndGrade(@Param("parentId") Integer parentId);
	
	
}
