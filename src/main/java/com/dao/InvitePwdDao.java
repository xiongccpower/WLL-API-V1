package com.dao;

import com.entity.InvitePwd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface InvitePwdDao extends JpaRepository<InvitePwd, Integer> {

    @Query("from InvitePwd d where d.phone=:phone ")
    public InvitePwd getByTel(@Param("phone") String phone);


    @Query("from InvitePwd d where d.openId=:openId ")
    public InvitePwd getByOpenId(@Param("openId") String openId);

    @Query("from InvitePwd d where d.uid=:openId ")
    public InvitePwd getByUid(@Param("openId") String openId);

    @Query("from InvitePwd d where d.invitePwd=:invitePwd ")
    public InvitePwd getByInvitePwd(@Param("invitePwd") String invitePwd);

}
