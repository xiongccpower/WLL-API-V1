package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.entity.ShopCart;

@Repository
@Service
public interface ShopCartDao extends JpaRepository<ShopCart, Integer>{

	@Query("from ShopCart s where s.userId=:userId ")
	public List<ShopCart> getByUserId(@Param("userId") Integer userId);
	
	@Query("from ShopCart s where s.userId=:userId and s.goodsId=:goodsId and s.type=:type")
	public ShopCart get(@Param("userId") Integer userId,@Param("goodsId") Integer goodsId,@Param("type") String type);
}
