package com.dao;

import com.entity.ConvenientInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service
public interface ConvenientInfoDao extends JpaRepository<ConvenientInfo,Integer> {

    @Query("from ConvenientInfo  d  where d.serviceArea =:area order by d.createTime desc")
    public List<ConvenientInfo> getByArea(@Param("area") String area);
}
